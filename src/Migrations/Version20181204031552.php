<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181204031552 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Администратор' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Создавать' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_CREATE'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Удалять' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_DELETE'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Редактировать' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDIT'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Редактор' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Экспорт' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EXPORT'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Список' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_LIST'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Читатель' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_READER'");
        $this->addSql("UPDATE roles SET description = '[ADMIN] Уведомления - Идентификаторы PUSH уведомлений - Просмотр' WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_VIEW'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
