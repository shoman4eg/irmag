<?php

namespace Irmag\SiteBundle\Repository;

class SecretPromocodePageRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function getOneBySlug(string $slug)
    {
        return $this->createQueryBuilder('scp')
            ->where('scp.slug = :slug')
            ->andWhere('scp.isActive = true')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
