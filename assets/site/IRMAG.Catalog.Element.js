/**
 * Карточка товара.
 *
 * @namespace IRMAG.Catalog.Element
 *
 * @requires jquery
 * @requires fancybox
 */

import { bind } from '../core/modules/jumper';

IRMAG.Catalog.Element = {
    init: function () {
        this.bindShowMore();
        this.bindChildrensImageZoom();
        this.bindKits();
        IRMAG.Site.bindBootstrapPopover();
        this.bindActionsCountdownPopover();
        this.bindWatchElement();
        new ClipboardJS('.clipboard');

        bind('buy-me'); // from barcode search
    },

    /**
     * Разворачивает описание
     */
    bindShowMore: function () {
        $('p.truncate > a.show-more').on('click', function () {
            $(this).parents('p').removeClass('truncate');
        });

        $('p.truncate').on('click', function () {
            $(this).removeClass('truncate');
        });
    },

    /**
     * Увеличение картинки тона
     */
    bindChildrensImageZoom: function () {
        $('#childrens .image .hover-area').on('mouseenter', function () {
            $(this).parent().addClass('hovered');
        }).on('mouseleave', function () {
            $(this).parent().removeClass('hovered');
        });
    },

    /**
     * Комлекты/образ
     */
    bindKits: function () {
        const $block = $('.block-kit');

        if (0 === $block.length) {
            return;
        }

        // childrens toggler
        $('.block-childrens .kit-toggler').on('click', function () {
            const $toggler = $(this);
            const $divKit = $(this).parents('div.element-row').next('section.kit-row');

            if ($toggler.hasClass('expanded')) {
                $toggler.removeClass('expanded');
                $divKit.slideUp(function () {
                    $divKit.hide();
                });
            } else {
                $toggler.addClass('expanded');
                $divKit.slideDown();
            }
        }).filter(':first').trigger('click');

        // checkbox
        $block.on('click', '.kit-checkbox', function (e) {
            e.stopPropagation();
            $(this).parents('.catalog-element').toggleClass('disabled');

            const $catalog = $(this).parents('.catalog');
            let totalPrice = 0.00;

            // пересчитываем сумму комплекта
            $catalog.find('.catalog-element[data-price]:not(.disabled)').each(function () {
                totalPrice += parseFloat($(this).data('price'));
            });

            totalPrice = totalPrice.toFixed(2);
            const $kitTotalPrice = $catalog.find('.kit-total-price');

            // counter shuffle animation
            $({ counter: 0 }).animate({ counter: totalPrice }, {
                duration: 300,
                easing: 'swing',
                step: function () {
                    $kitTotalPrice.text(this.counter.toFixed(2));
                },
                complete: function () {
                    $kitTotalPrice.text(totalPrice);
                },
            });
        });

        // добавление в корзину
        $block.on('click', '.add-elements-to-basket', function () {
            const $addBtn = $(this);
            const ids = [];

            $(this).parents('.catalog').find('.catalog-element[data-id]:not(.disabled)').each(function () {
                ids.push($(this).data('id'));
            });

            IRMAG.API.Basket.addElement(ids, 1, function (data) {
                if (data.success) {
                    $addBtn.removeClass('btn-primary').addClass('btn-success');
                    IRMAG.API.Basket.notifyAdd();
                    IRMAG.Basket.updateData();
                }
            });
        });
    },

    bindActionsCountdownPopover: function () {
        $('.element-actions a')
            .popover({ trigger: 'hover' })
            .on('shown.bs.popover', function () {
                IRMAG.Action.initCountdown();
            })
        ;
    },

    bindWatchElement: function () {
        const $watchElement = $('#watch-element');

        if (0 === $watchElement.length) {
            return;
        }

        const $watchElementBtn = $('#watch-element-btn');

        const BTN_HTML_FALSE = '<i class="fa fa-eye fa-fw"></i> уведомить при приходе';
        const BTN_TITLE_FALSE = 'Нажмите, чтобы начать отслеживать товар. Вам придёт уведомление, когда товар появится на остатках.';

        const BTN_HTML_TRUE = '<i class="fa fa-eye-slash fa-fw"></i> отменить';
        const BTN_TITLE_TRUE = 'Нажмите, чтобы отменить отслеживание товара.';

        function setState($btn, fixTitle = false) {
            if ($btn.data('is-watching')) {
                $btn
                    .html(BTN_HTML_TRUE)
                    .attr('title', BTN_TITLE_TRUE);
            } else {
                $btn
                    .html(BTN_HTML_FALSE)
                    .attr('title', BTN_TITLE_FALSE);
            }

            if (true === fixTitle) {
                $btn.tooltip('fixTitle').tooltip('show');
            }
        }

        setState($watchElementBtn);

        $watchElementBtn.on('click', function () {
            const $btn = $(this);
            const elementId = $btn.data('id');
            const favoriteName = $btn.data('favorite-name');

            if ($(this).data('is-watching')) {
                IRMAG.API.Favorite.removeElement(elementId, favoriteName, function (data) {
                    if (data.success) {
                        $btn.data('is-watching', false);
                        setState($btn, true);
                    }
                });
            } else {
                IRMAG.API.Favorite.addElement(elementId, favoriteName, function (data) {
                    if (data.success) {
                        $btn.data('is-watching', true);
                        setState($btn, true);
                    }
                });
            }
        });

        $watchElement.hide().removeClass('hidden').slideDown();
    },
};
