<?php

namespace Irmag\DriveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="orders_changes_mixed_payments")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderChangesMixedPayment
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var OrderChanges
     *
     * @ORM\OneToOne(targetEntity="OrderChanges", inversedBy="mixedPayment", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $order;

    /**
     * Сумма наличными.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0})
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_mixed_payment"})
     */
    private $sumCash;

    /**
     * Сумма картой.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0})
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_mixed_payment"})
     */
    private $sumCard;

    /**
     * Сумма сертификатом (кратно 500).
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0})
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_mixed_payment"})
     */
    private $sumCertificate;

    public function __construct()
    {
        $this->sumCard = 0.00;
        $this->sumCash = 0.00;
        $this->sumCertificate = 0.00;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSumCash()
    {
        return $this->sumCash;
    }

    public function setSumCash($sumCash): self
    {
        $this->sumCash = $sumCash;

        return $this;
    }

    public function getSumCard()
    {
        return $this->sumCard;
    }

    public function setSumCard($sumCard): self
    {
        $this->sumCard = $sumCard;

        return $this;
    }

    public function getSumCertificate()
    {
        return $this->sumCertificate;
    }

    public function setSumCertificate($sumCertificate): self
    {
        $this->sumCertificate = $sumCertificate;

        return $this;
    }

    public function getOrder(): ?OrderChanges
    {
        return $this->order;
    }

    public function setOrder(OrderChanges $order): self
    {
        $this->order = $order;

        $order->setMixedPayment($this);

        return $this;
    }
}
