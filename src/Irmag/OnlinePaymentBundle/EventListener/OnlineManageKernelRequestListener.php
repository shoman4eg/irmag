<?php

namespace Irmag\OnlinePaymentBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class OnlineManageKernelRequestListener
{
    /**
     * Роуты, которые исключены от редиректа, т.к. обслуживают саму онлайн-оплату.
     */
    const ONLINE_PAYMENT_MANAGING_ROUTES = [
        'irmag_order_payment',
        'irmag_order_cancel',
        'irmag_order_new_payment',
        'irmag_order_cancel_online_payment',
        'irmag_order_force_cancel_payment',
        'irmag_order_finish_online_payment',
        'irmag_order_validate_apple_pay_merchant',
        'irmag_order_process_apple_pay',
        'irmag_order_payment_form',
    ];

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserStateManager
     */
    private $userState;

    /**
     * @var string
     */
    private $siteDomain;

    /**
     * @param TokenStorageInterface         $tokenStorage
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param UrlGeneratorInterface         $router
     * @param UserStateManager              $userState
     * @param string                        $siteDomain
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        UrlGeneratorInterface $router,
        UserStateManager $userState,
        string $siteDomain
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
        $this->userState = $userState;
        $this->siteDomain = $siteDomain;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        // если заказ не оплачен - редирект на страницу оплаты
        if (
            false === $request->isXmlHttpRequest()
            && false === $this->isFragmentRequest($request)
            && $request->getHttpHost() === $this->siteDomain
            && !\in_array($request->get('_route'), self::ONLINE_PAYMENT_MANAGING_ROUTES, true)
            && $this->tokenStorage->getToken()
            && $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')
            && (
                $this->userState->get(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY)
                ||
                $this->userState->get(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH)
            )
        ) {
            $event->setResponse(new RedirectResponse($this->router->generate('irmag_order_payment')));
        }
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isFragmentRequest(Request $request): bool
    {
        return '/_fragment' === $request->getPathInfo();
    }
}
