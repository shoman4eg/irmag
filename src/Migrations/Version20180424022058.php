<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180424022058 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE coupon_elements_lists ADD action_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE coupon_elements_lists ADD CONSTRAINT FK_219E7F639D32F035 FOREIGN KEY (action_id) REFERENCES actions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_219E7F639D32F035 ON coupon_elements_lists (action_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE coupon_elements_lists DROP CONSTRAINT FK_219E7F639D32F035');
        $this->addSql('DROP INDEX UNIQ_219E7F639D32F035');
        $this->addSql('ALTER TABLE coupon_elements_lists DROP action_id');
    }
}
