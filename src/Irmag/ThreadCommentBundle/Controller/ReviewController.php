<?php

namespace Irmag\ThreadCommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Entity\Action;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\News;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;

/**
 * @Route("/reviews")
 */
class ReviewController extends Controller
{
    /**
     * @Route("/", name="irmag_reviews", host="%irmag_site_domain%", options={"sitemap": true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $query = $this->get('doctrine.orm.default_entity_manager')->createQueryBuilder()
            ->select('tc, t, e.name as elementName, e.toneName as elementToneName')
            ->addSelect('n.title as newsTitle, a.title as actionTitle, b.title as blogPostTitle')
            ->from(ThreadComment::class, 'tc')
            ->leftJoin('tc.thread', 't')
            ->leftJoin(Element::class, 'e', \Doctrine\ORM\Query\Expr\Join::WITH, 't.ownerId = e.id')
            ->leftJoin(News::class, 'n', \Doctrine\ORM\Query\Expr\Join::WITH, 't.ownerId = n.id')
            ->leftJoin(Action::class, 'a', \Doctrine\ORM\Query\Expr\Join::WITH, 't.ownerId = a.id')
            ->leftJoin(BlogPost::class, 'b', \Doctrine\ORM\Query\Expr\Join::WITH, 't.ownerId = b.id')
            ->where('tc.isActive = true')
            ->orderBy('tc.createdAt', 'DESC');

        if ($request->query->has('user')) {
            $query
                ->andWhere('tc.user = :user')
                ->setParameter('user', $request->query->getInt('user'));
        }

        /** @var \Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            100
        );

        foreach ($pagination as $item) {
            /** @var ThreadComment $threadComment */
            $threadComment = $item[0];
            $thread = $threadComment->getThread();
            $thread
                ->setIsScorable(false)
                ->setIsCommentable(false)
                ->setIsRatable(false);
        }

        return $this->render('@IrmagThreadComment/Review/index.html.twig', [
            'pagination' => $pagination,
            'comment_configs' => $this->getParameter('irmag_thread_comment.configs'),
        ]);
    }
}
