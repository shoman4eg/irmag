<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\ProfileBundle\Entity\Role;

class LoadRolesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        // common
        $this->addRole('ROLE_USER', 'Пользователь');
        $this->addRole('ROLE_MODERATOR', 'Модератор');
        $this->addRole('ROLE_ADMIN', 'Администратор');
        $this->addRole('ROLE_SUPER_ADMIN', 'Суперадминистратор');
        $this->addRole('ROLE_ALLOW_ADVANCED_COMMENTS', 'Расширенные возможности комментирования');

        // disallow roles
        $this->addRole('ROLE_IRMAG_DISALLOW_FORUM_POST_CREATE', 'Запрет на комментирование на форуме');
        $this->addRole('ROLE_IRMAG_DISALLOW_FORUM_TOPIC_CREATE', 'Запрет на создание тем на форуме');
        $this->addRole('ROLE_IRMAG_DISALLOW_THREAD_COMMENT_BLOG_CREATE', 'Запрет на комментирование в блоге');
        $this->addRole('ROLE_IRMAG_DISALLOW_THREAD_COMMENT_ELEMENT_CREATE', 'Запрет на комментирование в товарах');
        $this->addRole('ROLE_IRMAG_DISALLOW_THREAD_COMMENT_ACTION_CREATE', 'Запрет на комментирование в акциях');
        $this->addRole('ROLE_IRMAG_DISALLOW_THREAD_COMMENT_NEWS_CREATE', 'Запрет на комментирование в новостях');

        // sonata admin bundle
        $this->addRole('ROLE_ALLOWED_TO_SWITCH', '[Admin] Может войти под другим пользователем');
        $this->addRole('ROLE_SONATA_ADMIN', '[Admin] Может войти в админку');

        /* Администрирование */
        $this->buildAdminRolesTree('ADMINISTRATION_USER', 'Администрирование - Пользователи');
        $this->buildAdminRolesTree('ADMINISTRATION_GROUP', 'Администрирование - Группы');
        $this->buildAdminRolesTree('ADMINISTRATION_ROLE', 'Администрирование - Роли');
        $this->buildAdminRolesTree('ADMINISTRATION_USER_BLOCK_HISTORY', 'Администрирование - История банов');
        $this->buildAdminRolesTree('ADMINISTRATION_USER_CITY', 'Администрирование - Города');
        $this->buildAdminRolesTree('ADMINISTRATION_USER_BONUS_HISTORY', 'Администрирование - История операций по бонусному счёту');
        $this->buildAdminRolesTree('ADMINISTRATION_EXCHANGE_1_C_QUEUE', 'Администрирование - Очередь обмена с 1С');

        /* Блог */
        $this->buildAdminRolesTree('BLOG_POST_SECTION', 'Блог - Разделы');
        $this->buildAdminRolesTree('BLOG_POST', 'Блог - Посты');
        $this->buildAdminRolesTree('BLOG_POST_ELEMENT', 'Блог - Товары в обзоре');
        $this->buildAdminRolesTree('BLOG_POST_ATTACHMENT', 'Блог - Изображения в обзоре');
        $this->buildAdminRolesTree('BLOG_POST_TAG', 'Блог - Теги');

        /* Форум */
        $this->buildAdminRolesTree('FORUM', 'Форум - Форумы');
        $this->buildAdminRolesTree('FORUM_TOPIC', 'Форум - Темы');
        $this->buildAdminRolesTree('FORUM_POST', 'Форум - Посты');
        $this->buildAdminRolesTree('FORUM_POST_ATTACHMENT', 'Форум - Изображения в посте');

        /* Контент */
        $this->buildAdminRolesTree('CONTENT_FILE', 'Контент - Файлы');
        $this->buildAdminRolesTree('CONTENT_ACTION', 'Контент - Акции');
        $this->buildAdminRolesTree('CONTENT_NEWS', 'Контент - Новости');
        $this->buildAdminRolesTree('CONTENT_PAGE', 'Контент - Страницы');
        $this->buildAdminRolesTree('CONTENT_INDEX_ELEMENTS_CAROUSEL', 'Контент - Карусель товаров на главной');
        $this->buildAdminRolesTree('CONTENT_SECRET_PROMOCODE_PAGE', 'Контент - Секретная страница промокода');
        $this->buildAdminRolesTree('CONTENT_POLL', 'Контент - Опросы');
        $this->buildAdminRolesTree('CONTENT_POLL_VARIANT', 'Контент - Опросы (варианты ответов)');
        $this->buildAdminRolesTree('CONTENT_THREAD_COMMENT', 'Контент - Комментарии');

        /* Интернет-магазин */
        $this->buildAdminRolesTree('ECOMMERCE_BASKET', 'Интернет-магазин - Корзины');
        $this->buildAdminRolesTree('ECOMMERCE_BASKET_ELEMENT', 'Интернет-магазин - Товар в корзине');
        $this->buildAdminRolesTree('ECOMMERCE_FAVORITE', 'Интернет-магазин - Избранные');
        $this->buildAdminRolesTree('ECOMMERCE_PROMOCODE', 'Интернет-магазин - Промокоды');
        $this->buildAdminRolesTree('ECOMMERCE_PROMOCODE_ELEMENTS_LIST', 'Интернет-магазин - Товары по промокодам');
        $this->buildAdminRolesTree('ECOMMERCE_SECTION', 'Интернет-магазин - Разделы');
        $this->buildAdminRolesTree('ECOMMERCE_ELEMENT_COUNTRY', 'Интернет-магазин - Страны');
        $this->buildAdminRolesTree('ECOMMERCE_ELEMENT_BRAND', 'Интернет-магазин - Бренды');
        $this->buildAdminRolesTree('ECOMMERCE_ELEMENT_MANUFACTURER', 'Интернет-магазин - Производители');
        $this->buildAdminRolesTree('ECOMMERCE_ELEMENT', 'Интернет-магазин - Товары');
        $this->buildAdminRolesTree('ECOMMERCE_ELEMENT_PICTURE', 'Интернет-магазин - Изображения товаров');

        /* Оформление заказа */
        $this->buildAdminRolesTree('ORDER', 'Оформление заказа - Заказы');
        $this->buildAdminRolesTree('ORDER_ELEMENT', 'Оформление заказа - Товар заказа');
        $this->buildAdminRolesTree('ORDER_STATUS', 'Оформление заказа - Статусы заказа');
        $this->buildAdminRolesTree('ORDER_DELIVERY_CITY', 'Оформление заказа - Города доставки');
        $this->buildAdminRolesTree('ORDER_DELIVERY_METHOD', 'Оформление заказа - Способы доставки');
        $this->buildAdminRolesTree('ORDER_DELIVERY_TIME', 'Оформление заказа - Временные интервалы');
        $this->buildAdminRolesTree('ORDER_DELIVERY_CITY_DAY_OF_WEEK', 'Оформление заказа - Города доставки (дни недели)');
        $this->buildAdminRolesTree('ORDER_DELIVERY_CITY_BAN_DAY', 'Оформление заказа - Города доставки (бан дат)');
        $this->buildAdminRolesTree('ORDER_ONLINE_PAYMENT', 'Оформление заказа - Онлайн платежи');
        $this->buildAdminRolesTree('ORDER_ONLINE_PAYMENT_PROVIDER', 'Оформление заказа - Платёжные шлюзы');
        $this->buildAdminRolesTree('ORDER_PAYMENT_METHOD', 'Оформление заказа - Способы оплаты');
    }

    /**
     * Построение иерархии ролей для админки.
     *
     * @param string $roleNameChunk
     * @param string $roleDescriptionChunk
     */
    private function buildAdminRolesTree(string $roleNameChunk, string $roleDescriptionChunk): void
    {
        $subRole = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_READER', $roleNameChunk), sprintf('[ADMIN] %s - Читатель', $roleDescriptionChunk));
        $firstChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_VIEW', $roleNameChunk), sprintf('[ADMIN] %s - Просмотр', $roleDescriptionChunk));
        $secondChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_LIST', $roleNameChunk), sprintf('[ADMIN] %s - Список', $roleDescriptionChunk));

        $this->addChild($subRole, $firstChild);
        $this->addChild($subRole, $secondChild);

        $role = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_EDITOR', $roleNameChunk), sprintf('[ADMIN] %s - Редактор', $roleDescriptionChunk));
        $firstChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_EDIT', $roleNameChunk), sprintf('[ADMIN] %s - Редактировать', $roleDescriptionChunk));
        $secondChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_CREATE', $roleNameChunk), sprintf('[ADMIN] %s - Создавать', $roleDescriptionChunk));

        $this->addChild($role, $firstChild);
        $this->addChild($role, $secondChild);
        $this->addChild($role, $subRole);
        $subRole = $role;

        $role = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_ADMIN', $roleNameChunk), sprintf('[ADMIN] %s - Администратор', $roleDescriptionChunk));
        $firstChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_DELETE', $roleNameChunk), sprintf('[ADMIN] %s - Удалять', $roleDescriptionChunk));
        $secondChild = $this->addRole(sprintf('ROLE_IRMAG_ADMIN_%s_EXPORT', $roleNameChunk), sprintf('[ADMIN] %s - Экспорт', $roleDescriptionChunk));

        $this->addChild($role, $firstChild);
        $this->addChild($role, $secondChild);
        $this->addChild($role, $subRole);

        $this->manager->flush();
    }

    /**
     * Добавить роль, если она ещё не создана, или вернуть имеющуюся.
     *
     * @param string      $name
     * @param string|null $description
     *
     * @return Role
     */
    protected function addRole(string $name, string $description = null): Role
    {
        $existingRole = $this->manager->getRepository(Role::class)->findOneBy(['name' => $name]);

        if (!$existingRole) {
            $role = new Role($name);

            if (!empty($description)) {
                $role->setDescription($description);
            }

            $this->manager->persist($role);
        }

        return $existingRole ?: $role;
    }

    /**
     * Добавить роль в качестве потомка.
     *
     * @param Role $parentRole
     * @param Role $childRole
     */
    private function addChild(Role $parentRole, Role $childRole): void
    {
        if (false === $parentRole->getChildrens()->contains($childRole)) {
            $parentRole->addChildren($childRole);
        }

        $this->manager->persist($parentRole);
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 2;
    }
}
