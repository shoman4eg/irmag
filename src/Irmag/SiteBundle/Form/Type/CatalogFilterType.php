<?php

namespace Irmag\SiteBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Irmag\SiteBundle\Form\Helper\CatalogFilterHelper;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementCountry;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\CoreBundle\Utils\Formatter;

class CatalogFilterType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CatalogFilterHelper
     */
    private $catalogFilterHelper;

    /**
     * @param EntityManagerInterface $em
     * @param CatalogFilterHelper    $catalogFilterHelper
     */
    public function __construct(
        EntityManagerInterface $em,
        CatalogFilterHelper $catalogFilterHelper
    ) {
        $this->em = $em;
        $this->catalogFilterHelper = $catalogFilterHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $filterData = $options['filter_data'];

        $manufacturer = !empty($filterData['manufacturer'][0]) ? $this->em->getRepository(ElementManufacturer::class)->find((int) $filterData['manufacturer'][0]) : null;
        $brand = !empty($filterData['brand'][0]) ? $this->em->getRepository(ElementBrand::class)->find((int) $filterData['brand'][0]) : null;
        $country = !empty($filterData['country'][0]) ? $this->em->getRepository(ElementCountry::class)->find((int) $filterData['country'][0]) : null;

        $helper = $this->catalogFilterHelper
            ->setSection($options['section'])
            ->setCountry($country)
            ->setManufacturer($manufacturer)
            ->setBrand($brand);

        if (!empty($filterData['price_start']) && is_numeric($filterData['price_start'])) {
            $helper->setMinPrice($filterData['price_start']);
        }

        if (!empty($filterData['price_end']) && is_numeric($filterData['price_end'])) {
            $helper->setMaxPrice($filterData['price_end']);
        }

        if ($options['is_filter_used']) {
            $helper->setFilterData($options['filter_data']);
        }

        if ($options['is_search_used']) {
            $helper->setSearchQuery($options['search_query']);
        }

        // мин. и макс. возможная цена для слайдера
        $minPrice = $helper->getMinPrice();
        $maxPrice = $helper->getMaxPrice();

        $defaultData = [
            'min_price' => $minPrice,
            'max_price' => $maxPrice,
        ];

        $builder
            ->add('manufacturer', CollectionType::class, [
                'entry_type' => HiddenType::class,
            ])
            ->add('brand', CollectionType::class, [
                'entry_type' => HiddenType::class,
                'mapped' => false,
            ])
            ->add('country', CollectionType::class, [
                'entry_type' => HiddenType::class,
            ])
            ->add('manufacturer_and_brands_select', CatalogFilterSelectType::class, [
                'label' => 'element.brand',
                'mapped_to' => 'manufacturer',
                'mapped_to_lvl_2' => 'brand',
                'data' => $helper->getManufacturerAndBrandsList(),
                'selected' => $manufacturer,
                'selected_lvl_2' => $brand,
            ])
            ->add('country_select', CatalogFilterSelectType::class, [
                'label' => 'element.country',
                'mapped_to' => 'country',
                'data' => $helper->getCountryList(),
                'selected' => $country,
            ])
            ->add('price_start', IntegerType::class, [
                'label' => false,
                'data' => $minPrice,
                'empty_data' => (string) $minPrice,
            ])
            ->add('price_end', IntegerType::class, [
                'label' => false,
                'data' => $maxPrice,
                'empty_data' => (string) $maxPrice,
            ])
            ->add('prop', CatalogFilterCheckboxType::class, [
                'label' => 'element.props',
                'multiple' => true,
                'expanded' => true,
                'attr' => [
                    'class' => 'props',
                ],
                'choices' => [
                    'element.special_price' => 'SpecialPrice',
                    'element.eco' => 'Eco',
                    'element.safe_animal' => 'SafeAnimal',
                    'element.action' => 'Action',
                    'element.transfer' => 'Transfer',
                ],
                'choice_attr' => function ($val/*, $key, $index*/) use ($helper) {
                    $attrs = [
                        'icon_css_name' => Formatter::camelCaseToDashLowerCase($val),
                    ];

                    if (true === $helper->getPropIsDisabled($val)) {
                        $attrs['disabled'] = 'disabled';
                    }

                    return $attrs;
                },
            ])
            ->setMethod('GET')
            ->setData(array_merge($defaultData, $builder->getData() ?? []))
        ;

        // не даём указать цену меньше минимально и больше максимально возможной
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($minPrice, $maxPrice) {
            $data = $event->getData();

            if (isset($data['price_start']) && $data['price_start'] < $minPrice) {
                $data['price_start'] = $minPrice;
            }

            if (isset($data['price_end']) && $data['price_end'] > $maxPrice) {
                $data['price_end'] = $maxPrice;
            }

            $event->setData($data);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'filter';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'csrf_protection' => false,
            'translation_domain' => 'irmag_site',
            'is_filter_used' => false,
            'is_search_used' => false,
            'filter_data' => [],
            'search_query' => '',
        ]);

        $resolver->setRequired('section');
        $resolver->setAllowedTypes('section', [Section::class, 'null']);
        $resolver->setAllowedTypes('is_filter_used', ['bool']);
        $resolver->setAllowedTypes('is_search_used', ['bool']);
        $resolver->setAllowedTypes('filter_data', ['array']);
        $resolver->setAllowedTypes('search_query', ['string']);
    }
}
