<?php

namespace Irmag\ThreadCommentBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class ThreadCommentMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    public function sendNewCommentToModeratorsMessage($to, $context): int
    {
        return $this->mailer->sendMessage($to, '@IrmagThreadComment/Emails/new-comment-message.html.twig', $context);
    }

    public function sendNewCommentToAuthorMessage($to, $context): int
    {
        return $this->mailer->sendMessage($to, '@IrmagThreadComment/Emails/new-comment-to-author-message.html.twig', $context);
    }
}
