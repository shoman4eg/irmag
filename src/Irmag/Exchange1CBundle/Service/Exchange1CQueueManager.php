<?php

namespace Irmag\Exchange1CBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;

class Exchange1CQueueManager
{
    /**
     * Через какое время считать процесс зависшим.
     */
    const CUTOFF_TIME = '5 hours';

    /**
     * Максимальное число попыток перезапуска.
     */
    const MAX_ATTEMPTS = 10;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * Пометить элемент, как запущенный в обработку.
     *
     * @param Exchange1CQueue $item
     * @param int             $pid
     */
    public function markRunning(Exchange1CQueue $item, int $pid): void
    {
        $runAt = new \DateTime();

        $item
            ->setStatus(Exchange1CQueue::STATUS_RUNNING)
            ->setRunAt($runAt)
            ->setPid($pid);

        $this->em->persist($item);
        $this->em->flush();
    }

    /**
     * Отложить элемент очереди на перезапуск.
     *
     * @param Exchange1CQueue $item
     * @param bool            $forceKill Убить процесс насильно
     */
    public function postpone(Exchange1CQueue $item, bool $forceKill = false): void
    {
        $pid = $item->getPid();
        $currentAttempts = $item->getAttempts();

        $item
            ->setAttempts(++$currentAttempts)
            ->setPid(null);

        if ($currentAttempts < self::MAX_ATTEMPTS) {
            $item->setStatus(Exchange1CQueue::STATUS_WAITING);
        } else {
            $item->setStatus(Exchange1CQueue::STATUS_FAILED);
        }

        $this->em->persist($item);
        $this->em->flush();

        if (\is_int($pid)
            &&
            ($forceKill || $this->isItemTimedOut($item))
        ) {
            $this->killProcess($pid);
        }
    }

    /**
     * Зарегистрировать фейл процессинга.
     *
     * @param Exchange1CQueue $item
     * @param string|null     $message
     */
    public function fail(Exchange1CQueue $item, string $message = null): void
    {
        $attempts = $item->getAttempts();
        $item
            ->setStatus(Exchange1CQueue::STATUS_FAILED)
            ->setAttempts(++$attempts)
            ->setLastOutputMessage($message);

        $this->em->persist($item);
        $this->em->flush();
    }

    /**
     * Удаляет элемент очереди.
     *
     * @param Exchange1CQueue $item
     */
    public function release(Exchange1CQueue $item): void
    {
        $queueItem = $this->em->merge($item);
        $this->em->remove($queueItem);
        $this->em->flush();
    }

    /**
     * Действительно ли процесс существует.
     *
     * @param Exchange1CQueue $item
     *
     * @return bool
     */
    public function isItemReallyRunning(Exchange1CQueue $item): bool
    {
        return !empty(exec(sprintf('ps -p %d -o pid=', $item->getPid())));
    }

    /**
     * Не завис ли процесс.
     *
     * @param Exchange1CQueue $item
     *
     * @return bool
     */
    public function isItemTimedOut(Exchange1CQueue $item): bool
    {
        $runAt = $item->getRunAt();
        $now = new \DateTime();

        return $runAt->modify(sprintf('+%s', self::CUTOFF_TIME)) < $now;
    }

    /**
     * @param Exchange1CQueue $item
     */
    public function priorityUp(Exchange1CQueue $item): void
    {
        if (Exchange1CQueue::PRIORITY_HIGH !== $item->getPriority()) {
            if (Exchange1CQueue::PRIORITY_LOW === $item->getPriority()) {
                $item->setPriority(Exchange1CQueue::PRIORITY_NORMAL);
            } elseif (Exchange1CQueue::PRIORITY_NORMAL === $item->getPriority()) {
                $item->setPriority(Exchange1CQueue::PRIORITY_HIGH);
            }

            $this->em->persist($item);
            $this->em->flush();
        }
    }

    /**
     * @param Exchange1CQueue $item
     */
    public function priorityDown(Exchange1CQueue $item): void
    {
        if (Exchange1CQueue::PRIORITY_LOW !== $item->getPriority()) {
            if (Exchange1CQueue::PRIORITY_HIGH === $item->getPriority()) {
                $item->setPriority(Exchange1CQueue::PRIORITY_NORMAL);
            } elseif (Exchange1CQueue::PRIORITY_NORMAL === $item->getPriority()) {
                $item->setPriority(Exchange1CQueue::PRIORITY_LOW);
            }

            $this->em->persist($item);
            $this->em->flush();
        }
    }

    /**
     * Убить процесс.
     *
     * @param int $pid
     */
    private function killProcess(int $pid): void
    {
        exec(sprintf('kill -SIGTERM %d', $pid));
    }
}
