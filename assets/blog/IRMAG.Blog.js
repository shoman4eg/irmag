/**
 * Блог Ирмага.
 *
 * @namespace IRMAG.Blog
 */
IRMAG.Blog = {
    init: function () {
        IRMAG.Blog.Sidebar.init();
    },
};
