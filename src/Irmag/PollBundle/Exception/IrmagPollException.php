<?php

namespace Irmag\PollBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagPollException extends IrmagException
{
}
