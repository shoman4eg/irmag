<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ThreadCommentBundle\Entity\Traits\ThreadTrait;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;
use Irmag\PollBundle\Entity\Traits\PollTrait;
use Irmag\PollBundle\Entity\PollOwnerInterface;

/**
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\NewsRepository")
 */
class News implements ThreadOwnerInterface, PollOwnerInterface
{
    use TimestampableEntity;
    use ThreadTrait;
    use PollTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $extraContentOnBanner;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max=128)
     *
     * @Gedmo\Slug(fields={"title"})
     *
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $slug;

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $banner;

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $bannerBg;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $position;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isImportant;

    /**
     * Показывать баннеры на главной страницы.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isShowInIndexBanners;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->position = 0;
        $this->isImportant = false;
        $this->isShowInIndexBanners = false;
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->title;
    }

    /**
     * XXX: Custom for ThreadCommentBundle.
     */
    public function setThreadOwnerRoute(): void
    {
        $this->getThread()->setOwnerRoute([
            'name' => 'irmag_news_detail',
            'parameters' => [
                'slug' => $this->getSlug(),
            ],
        ]);
    }

    /**
     * XXX: Custom for PollBundle.
     */
    public function setPollOwnerRoute(): void
    {
        $this->getPoll()->setOwnerRoute([
            'name' => 'irmag_news_detail',
            'parameters' => [
                'slug' => $this->getSlug(),
            ],
        ]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getExtraContentOnBanner(): ?string
    {
        return $this->extraContentOnBanner;
    }

    public function setExtraContentOnBanner(?string $extraContentOnBanner): self
    {
        $this->extraContentOnBanner = $extraContentOnBanner;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getIsImportant(): ?bool
    {
        return $this->isImportant;
    }

    public function setIsImportant(bool $isImportant): self
    {
        $this->isImportant = $isImportant;

        return $this;
    }

    public function getIsShowInIndexBanners(): ?bool
    {
        return $this->isShowInIndexBanners;
    }

    public function setIsShowInIndexBanners(bool $isShowInIndexBanners): self
    {
        $this->isShowInIndexBanners = $isShowInIndexBanners;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getBanner(): ?File
    {
        return $this->banner;
    }

    public function setBanner(?File $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getBannerBg(): ?File
    {
        return $this->bannerBg;
    }

    public function setBannerBg(?File $bannerBg): self
    {
        $this->bannerBg = $bannerBg;

        return $this;
    }
}
