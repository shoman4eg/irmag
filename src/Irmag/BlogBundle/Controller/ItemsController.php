<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Entity\Element;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostElement;
use Irmag\BlogBundle\Entity\BlogPostAttachment;

class ItemsController extends Controller
{
    /**
     * @Route("/items/", name="irmag_blog_items")
     *
     * @return Response
     */
    public function itemsAction(): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $blogElementsRepo = $em->getRepository(BlogPostElement::class);
        $elements = $blogElementsRepo->getAllItemsWithRates();

        return $this->render('@IrmagBlog/Lists/items.html.twig', [
            'elements' => $elements,
            'slug' => 'items',
        ]);
    }

    /**
     * @Route("/items/{id}/", name="irmag_blog_item_id", requirements={"id": "\d+"})
     *
     * @param int $id ID товара
     *
     * @return Response
     */
    public function itemAction(int $id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $postAttachmentsRepo = $em->getRepository(BlogPostAttachment::class);
        $element = $em->getRepository(Element::class)->find($id);

        $posts = [];
        $pics = [];

        if (!$element) {
            throw $this->createNotFoundException('Element was not found!');
        }

        $list = $em->getRepository(BlogPost::class)->getAllPostsByElement($element);

        foreach ($list as $key => $post) {
            $posts[$key]['id'] = $post->getId();
            $posts[$key]['shortlink'] = $post->getBlogSection()->getShortlink();
            $posts[$key]['title'] = $post->getTitle();
            $posts[$key]['subtitle'] = $post->getSubtitle();
            $posts[$key]['user'] = $post->getUser();

            $attachment = $postAttachmentsRepo->findOneBy([
                'object' => $post,
                'isPrimary' => true,
            ]);

            if ($attachment) {
                $file = $attachment->getFile();
                if (!$file) {
                    throw $this->createNotFoundException('File attachment was not found!');
                }

                $pics[$key] = $file->getWebPath();
            }
        }

        return $this->render('@IrmagBlog/Item/index.html.twig', [
            'posts' => $posts,
            'slug' => 'items',
            'pics' => $pics,
            'element' => $element,
        ]);
    }
}
