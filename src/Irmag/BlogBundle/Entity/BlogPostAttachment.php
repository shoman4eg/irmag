<?php

namespace Irmag\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\AttachmentBundle\Traits\AttachableTrait;

/**
 * @ORM\Table(name="blog_posts_attachments")
 * @ORM\Entity(repositoryClass="Irmag\BlogBundle\Repository\BlogPostAttachmentRepository")
 */
class BlogPostAttachment implements IrmagAttachable
{
    use TimestampableEntity;
    use AttachableTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var BlogPost
     *
     * @ORM\ManyToOne(targetEntity="BlogPost", inversedBy="attachments")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $object;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPrimary;

    public function __construct()
    {
        $this->isActive = true;
        $this->isPrimary = false;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsPrimary(): ?bool
    {
        return $this->isPrimary;
    }

    public function setIsPrimary(bool $isPrimary): self
    {
        $this->isPrimary = $isPrimary;

        return $this;
    }

    public function getObject(): ?IrmagAttachmentPoint
    {
        return $this->object;
    }

    public function setObject(?IrmagAttachmentPoint $object): self
    {
        $this->object = $object;

        return $this;
    }
}
