<?php

namespace Irmag\SberbankAcquiringBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class SberbankAcquiringException extends IrmagException
{
}
