<?php

namespace Irmag\SiteBundle\Variable;

use Irmag\BasketBundle\Service\DataCollector;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;

class BasketVariable
{
    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var DataCollector
     */
    private $dataCollector;

    /**
     * @param BasketManager $basketManager
     * @param DataCollector $dataCollector
     */
    public function __construct(
        BasketManager $basketManager,
        DataCollector $dataCollector
    ) {
        $this->basketManager = $basketManager;
        $this->dataCollector = $dataCollector;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->basketManager->isEmpty(true);
    }

    /**
     * @return \Irmag\BasketBundle\Entity\BasketElement[]
     */
    public function getBasketElements(): array
    {
        return $this->basketManager->getBasketElements() ?? [];
    }

    /**
     * @return \Irmag\SiteBundle\Entity\Element[]
     */
    public function getElements(): array
    {
        return $this->basketManager->getElements();
    }

    /**
     * @return DataCollector
     */
    public function getData(): DataCollector
    {
        return $this->dataCollector->getData();
    }
}
