<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Irmag\ProfileBundle\Entity\UserOrderProfile;

class UserOrderProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $em = $options['em'];

        $primary = $em->createQueryBuilder()
            ->select('op.id')
            ->from(UserOrderProfile::class, 'op')
            ->where('op.user = :user')
            ->andWhere('op.isPrimary = true')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        $builder
            ->add('order_profile', EntityType::class, [
                'class' => UserOrderProfile::class,
                'query_builder' => function (EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('op')
                        ->where('op.user = :user')
                        ->setParameter('user', $user);
                },
                'data' => $em->getReference(UserOrderProfile::class, $primary),
                'label' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Применить',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'translation_domain' => false,
        ]);

        $resolver->setRequired('user');
        $resolver->setRequired('em');
        $resolver->setAllowedTypes('user', UserInterface::class);
        $resolver->setAllowedTypes('em', EntityManager::class);
    }
}
