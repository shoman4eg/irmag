<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180802053108 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_READER', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_VIEW', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_LIST', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDITOR', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDIT', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_CREATE', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_ADMIN', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_DELETE', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EXPORT', '[ADMIN] Оформление заказа - Города доставки <=> Способы самовывоза - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM roles_childrens WHERE children_id IN (SELECT id FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_%')");
        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DELIVRY_SELFSERVICE_METHOD_%'");
    }
}
