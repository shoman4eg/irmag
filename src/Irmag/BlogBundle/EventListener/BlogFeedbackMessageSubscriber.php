<?php

namespace Irmag\BlogBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\BlogBundle\Mailer\BlogMailer;
use Irmag\BlogBundle\Event\BlogFeedbackMessageEvent;
use Irmag\BlogBundle\IrmagBlogEvents;

class BlogFeedbackMessageSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BlogMailer
     */
    private $mailer;

    /**
     * @param EntityManagerInterface $em
     * @param BlogMailer             $mailer
     */
    public function __construct(
        EntityManagerInterface $em,
        BlogMailer $mailer
    ) {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagBlogEvents::BLOG_NEW_FEEDBACK_MESSAGE_CREATED => 'sendEmailOnBlogFeedbackMessageCreate',
        ];
    }

    /**
     * @param BlogFeedbackMessageEvent $event
     */
    public function sendEmailOnBlogFeedbackMessageCreate(BlogFeedbackMessageEvent $event): void
    {
        $message = $event->getBlogFeedbackMessage();

        $moderatorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => 'blog_moderators']);

        if ($moderatorGroup) {
            $moderators = $moderatorGroup->getUsers();

            foreach ($moderators as $moderator) {
                if (!empty($moderator->getEmail()) && $message->getUser() !== $moderator) {
                    $this->mailer->sendNewFeedbackMessage($moderator->getEmail(), ['message' => $message]);
                }
            }
        }
    }
}
