<?php

namespace Irmag\ProfileBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\ProfileBundle\Events;
use Irmag\ProfileBundle\Event\UserEvent;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Mailer\SecurityMailer;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;

class UserRegistrationEmailConfirmedSubscriber implements EventSubscriberInterface
{
    /**
     * Сообщение, которое будет фигурировать в истории начисления бонусов.
     */
    const BONUS_MESSAGE = 'Бонус при регистрации на сайте';

    /**
     * @var SecurityMailer
     */
    private $mailer;

    /**
     * @var BonusManager
     */
    private $bonusManager;

    /**
     * @param SecurityMailer $mailer
     * @param BonusManager   $bonusManager
     */
    public function __construct(
        SecurityMailer $mailer,
        BonusManager $bonusManager
    ) {
        $this->mailer = $mailer;
        $this->bonusManager = $bonusManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::USER_REGISTRATION_EMAIL_CONFIRMED => 'onRegistrationEmailConfirmed',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onRegistrationEmailConfirmed(UserEvent $event): void
    {
        $user = $event->getUser();

        $this->sendRegistrationEmail($user);
        $this->payBonusesForEmailConfirmation($user);
    }

    /**
     * @param User $user
     */
    private function sendRegistrationEmail(User $user): void
    {
        if (!empty($user->getEmail())) {
            $this->mailer->sendRegistrationMessage($user->getEmail(), [
                'user' => $user,
            ]);
        }
    }

    /**
     * @param User $user
     */
    private function payBonusesForEmailConfirmation(User $user): void
    {
        $this->bonusManager->setUser($user);
        $this->bonusManager->update(Config::BONUS_FOR_REGISTRATION, self::BONUS_MESSAGE);
    }
}
