<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="orders_delivery_cities_ban_days")
 * @ORM\Entity
 */
class OrderDeliveryCityBanDay
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Город доставки.
     *
     * @var OrderDeliveryCity
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryCity", inversedBy="banDates")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $deliveryCity;

    /**
     * Дата бана.
     *
     * @var \DateTimeInterface
     *
     * @Assert\Date
     *
     * @ORM\Column(type="date")
     */
    private $banDate;

    public function __construct()
    {
        $this->banDate = new \DateTime();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBanDate(): ?\DateTimeInterface
    {
        return $this->banDate;
    }

    public function setBanDate(\DateTimeInterface $banDate): self
    {
        $this->banDate = $banDate;

        return $this;
    }

    public function getDeliveryCity(): ?OrderDeliveryCity
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?OrderDeliveryCity $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }
}
