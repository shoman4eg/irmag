<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserNotificationEventAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('isPushDisabled')
            ->add('isEmailDisabled')
            ->add('isSiteDisabled')
            ->add('isSmsDisabled')
            ->add('isActive')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('description')
            ->add('isPushDisabled')
            ->add('isEmailDisabled')
            ->add('isSiteDisabled')
            ->add('isSmsDisabled')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('description')
            ->add('isPushDisabled', null, ['editable' => true])
            ->add('isEmailDisabled', null, ['editable' => true])
            ->add('isSiteDisabled', null, ['editable' => true])
            ->add('isSmsDisabled', null, ['editable' => true])
            ->add('isActive', null, ['editable' => true])
        ;
    }
}
