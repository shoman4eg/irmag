<?php

namespace Irmag\OnlinePaymentBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagOnlinePaymentException extends IrmagException
{
}
