<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022045054 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_push_queue_user_push_id DROP CONSTRAINT fk_a7eea862cda51ba4');
        $this->addSql('DROP SEQUENCE users_push_queue_id_seq CASCADE');
        $this->addSql('DROP TABLE users_push_queue');
        $this->addSql('DROP TABLE user_push_queue_user_push_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_push_queue_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_push_queue (id INT NOT NULL, title VARCHAR(50) NOT NULL, body VARCHAR(150) NOT NULL, url VARCHAR(255) DEFAULT NULL, is_done BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ttl TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_push_queue_user_push_id (user_push_queue_id INT NOT NULL, user_push_id_id INT NOT NULL, PRIMARY KEY(user_push_queue_id, user_push_id_id))');
        $this->addSql('CREATE INDEX idx_a7eea862d6a8ea01 ON user_push_queue_user_push_id (user_push_id_id)');
        $this->addSql('CREATE INDEX idx_a7eea862cda51ba4 ON user_push_queue_user_push_id (user_push_queue_id)');
        $this->addSql('ALTER TABLE user_push_queue_user_push_id ADD CONSTRAINT fk_a7eea862cda51ba4 FOREIGN KEY (user_push_queue_id) REFERENCES users_push_queue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_push_queue_user_push_id ADD CONSTRAINT fk_a7eea862d6a8ea01 FOREIGN KEY (user_push_id_id) REFERENCES users_push_ids (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
