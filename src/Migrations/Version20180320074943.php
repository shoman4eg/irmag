<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180320074943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE users SET discount = 0 WHERE discount IS NULL');
        $this->addSql('ALTER TABLE users ALTER discount SET DEFAULT 0');
        $this->addSql('ALTER TABLE users ALTER discount SET NOT NULL');
        $this->addSql('UPDATE users SET spend_money = 0 WHERE spend_money IS NULL');
        $this->addSql('ALTER TABLE users ALTER spend_money TYPE INT');
        $this->addSql('ALTER TABLE users ALTER spend_money SET DEFAULT 0');
        $this->addSql('ALTER TABLE users ALTER spend_money SET NOT NULL');
        $this->addSql('UPDATE users SET bonus = 0 WHERE bonus IS NULL');
        $this->addSql('ALTER TABLE users ALTER bonus TYPE INT');
        $this->addSql('ALTER TABLE users ALTER bonus SET DEFAULT 0');
        $this->addSql('ALTER TABLE users ALTER bonus SET NOT NULL');
        $this->addSql('ALTER TABLE users_audit ALTER discount SET DEFAULT 0');
        $this->addSql('ALTER TABLE users_audit ALTER spend_money TYPE INT');
        $this->addSql('ALTER TABLE users_audit ALTER spend_money SET DEFAULT 0');
        $this->addSql('ALTER TABLE users_audit ALTER bonus TYPE INT');
        $this->addSql('ALTER TABLE users_audit ALTER bonus SET DEFAULT 0');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_audit ALTER discount DROP DEFAULT');
        $this->addSql('ALTER TABLE users_audit ALTER spend_money TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE users_audit ALTER spend_money DROP DEFAULT');
        $this->addSql('ALTER TABLE users_audit ALTER bonus TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE users_audit ALTER bonus DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER discount DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER discount DROP NOT NULL');
        $this->addSql('ALTER TABLE users ALTER spend_money TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE users ALTER spend_money DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER spend_money DROP NOT NULL');
        $this->addSql('ALTER TABLE users ALTER bonus TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE users ALTER bonus DROP DEFAULT');
        $this->addSql('ALTER TABLE users ALTER bonus DROP NOT NULL');
    }
}
