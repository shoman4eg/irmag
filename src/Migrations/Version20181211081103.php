<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181211081103 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE roles SET name = replace(name, 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION', 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_SUBSCRIPTION') WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_%'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
