##
# IRMAG Makefile
##

.DEFAULT_GOAL    := black@hole
CONSOLE          := bin/console
YARN             := yarn
COMPOSER         := composer
MAINTENANCE_FILE := /var/lock/irmag.maintenance.lock
PHP_VERSION      := 7.2

# deploy servers
DEPLOY_SSH_BETA     := user@deploy.irmag.beta
DEPLOY_SSH_PROD     := user@deploy.irmag.ru
# deploy paths
DEPLOY_RELEASES_DIR := /var/www/irmag/releases
DEPLOY_CURRENT_DIR  := /var/www/irmag/current
DEPLOY_SHARED_DIR   := /var/www/irmag/shared

# for sendxmpp
DEV_OPS_JIDS := 154@openfire 338@openfire

# inject env file
include .env
export $(shell sed 's/=.*//' .env)

# cache clear
cc:
	@test -f $(CONSOLE) && $(CONSOLE) cache:clear --no-warmup --no-debug || rm -rf var/cache/*
.PHONY: cc

# cache warmup
cw: cc
cw:
	@test -f $(CONSOLE) && $(CONSOLE) cache:warmup || echo "cannot warmup the cache (needs $(CONSOLE))"
.PHONY: cw

# lint: yaml/twig and cs fix
lint:
	$(CONSOLE) lint:yaml config/
	$(CONSOLE) lint:yaml src/
	$(CONSOLE) lint:twig src/
	bin/php-cs-fixer fix --dry-run --diff
.PHONY: lint

# php mess detector
phpmd:
	phpmd src/ text phpmd.xml
.PHONY: phpmd

# php static analysis tool
phpstan:
	bin/phpstan analyse -l 4 -c phpstan.neon src/Irmag/
.PHONY: phpstan

# test
test:
	# PHPUnit
	#bin/simple-phpunit
	# Behat
	bin/behat
.PHONY: test

# build-frontend [@dev] {@prod}
build-frontend:
	make dump-routes
	$(YARN) run encore dev
	make rm-routes
.PHONY: build-frontend

build-frontend@prod:
	make dump-routes
	$(YARN) run encore production
	make rm-routes

# watch-frontend [@dev]
watch-frontend:
	make dump-routes
	$(YARN) run encore dev --watch
	make rm-routes
.PHONY: watch-frontend

# routes.json
dump-routes:
	$(CONSOLE) fos:js-routing:dump --target=var/routes.json --format=json
.PHONY: dump-routes

rm-routes:
	rm -f var/routes.json
.PHONY: rm-routes

# dump sitemaps
dump-sitemaps:
	$(CONSOLE) presta:sitemaps:dump
	$(CONSOLE) presta:sitemaps:dump --section=blog
.PHONY: dump-sitemaps

# composer-install
composer-install:
	$(COMPOSER) install --no-interaction -v
.PHONY: composer-install

# yarn-install
yarn-install:
	$(YARN) install --prefer-offline
.PHONY: yarn-install

# build [@dev] {@prod}
build:
	make composer-install
	make yarn-install
	make build-frontend

build@prod:
	make composer-install
	make yarn-install
	make build-frontend@prod
.PHONY: build

# notify DevOps
#notify-devops: export TOKEN=682274640:AAHMOev4l0m46lTziRjqvfUuH-3VkhtMBZI
#notify-devops: export CHAT_ID=-1001187380815
notify-devops:
	echo "$(MSG)" | sendxmpp -s "GitLab" $(DEV_OPS_JIDS)
	#curl -s -XPOST "https://api.telegram.org/bot$(TOKEN)/sendMessage" -d chat_id=$(CHAT_ID) -d text="$(MSG)" --socks5-hostname 114.33.10.163:30003 &> /dev/null
.PHONY: notify-devops

# maintenance mode (@on, @off)
maintenance@on:
	touch $(MAINTENANCE_FILE)

maintenance@off:
	rm -f $(MAINTENANCE_FILE)
.PHONY: maintenance

pgsql-recreate-elements-grouped: export FILE_SQL=src/Irmag/SiteBundle/Entity/SQL/elements_grouped.sql
pgsql-recreate-elements-grouped:
	$(CONSOLE) doctrine:query:sql "`cat $(FILE_SQL)`"
.PHONY: pgsql-recreate-elements-grouped

pgsql-recreate-ts-search: export FILE_SQL=src/Irmag/SiteBundle/Entity/SQL/postgresql_ts_configuration.sql
pgsql-recreate-ts-search:
	$(CONSOLE) doctrine:query:sql "`sed '/^--/ d' $(FILE_SQL)`"
.PHONY: pgsql-recreate-ts-search

pgsql-drop-tables:
	$(CONSOLE) doctrine:schema:drop --force
.PHONY: pgsql-drop-tables

# pgsql-prepare
pgsql-prepare:
	# !!! CAUTION !!!
	# do not use `--force` on prod, its drop tsv indexes.
	$(CONSOLE) doctrine:schema:update --force
	make pgsql-recreate-elements-grouped
.PHONY: pgsql-prepare

# deploy
deploy: export RELEASE_NAME=$(shell date +'%Y%m%d%H%M%S')
deploy: export RELEASE_PATH=$(DEPLOY_RELEASES_DIR)/$(RELEASE_NAME)
deploy:
	@echo "Deploy irmag.ru to $(SSH_CMD)$(RELEASE_PATH)"

	# keep last 5 releases
	@ssh $(SSH_CMD) 'rm -rf `ls -dt1 $(DEPLOY_RELEASES_DIR)/* | tail -n +6`'

	@rsync \
		-az \
		--force \
		--delete \
		--exclude-from=rsync_exclude.txt \
		./ \
		$(SSH_CMD):$(RELEASE_PATH)

	ssh $(SSH_CMD) "ln -s $(DEPLOY_SHARED_DIR)/.env $(RELEASE_PATH)/"

	# on maintenance mode. max 2 min, see crontab job.
	ssh $(SSH_CMD) "make -C $(RELEASE_PATH) maintenance@on"
	ssh $(SSH_CMD) "sudo systemctl reload nginx.service"

	ssh $(SSH_CMD) "rm -f $(RELEASE_PATH)/config/parameters.yml"
	ssh $(SSH_CMD) "ln -s $(DEPLOY_SHARED_DIR)/config/parameters.yml $(RELEASE_PATH)/config/parameters.yml"

	ssh $(SSH_CMD) "mkdir $(RELEASE_PATH)/var"
	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/var/cache/ $(RELEASE_PATH)/var"
	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/var/exchange_1c/ $(RELEASE_PATH)/var"
	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/var/log/ $(RELEASE_PATH)/var"
	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/var/sessions/ $(RELEASE_PATH)/var"

	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/web/uploads $(RELEASE_PATH)/web"
	ssh $(SSH_CMD) "ln -sf $(DEPLOY_SHARED_DIR)/web/media $(RELEASE_PATH)/web"

	ssh $(SSH_CMD) "make -C $(RELEASE_PATH) migrate"
	ssh $(SSH_CMD) "make -C $(RELEASE_PATH) dump-sitemaps"

	# off maintenance mode.
	ssh $(SSH_CMD) "make -C $(RELEASE_PATH) maintenance@off"
	ssh $(SSH_CMD) "sudo systemctl reload nginx.service"

	ssh $(SSH_CMD) "rm -f $(DEPLOY_CURRENT_DIR) && ln -s $(RELEASE_PATH) $(DEPLOY_CURRENT_DIR) && make -C $(RELEASE_PATH) cw"
	ssh $(SSH_CMD) "sudo systemctl reload php$(PHP_VERSION)-fpm.service"

# deploy prod
deploy@prod: export SSH_CMD=$(DEPLOY_SSH_PROD)
deploy@prod: deploy
# deploy beta
deploy@beta: export SSH_CMD=$(DEPLOY_SSH_BETA)
deploy@beta: deploy

# deploy prod
deploy@prod: export SSH_CMD=$(DEPLOY_SSH_PROD)
deploy@prod: deploy
# deploy beta
deploy@beta: export SSH_CMD=$(DEPLOY_SSH_BETA)
deploy@beta: deploy
.PHONY: deploy

# load data
migrate:
	$(CONSOLE) doctrine:migrations:migrate --allow-no-migration --no-interaction
.PHONY: migrate

load-default-data:
	$(CONSOLE) doctrine:schema:create --no-interaction
	make migrate
	$(CONSOLE) doctrine:fixtures:load --fixtures=src/Irmag/DefaultDataBundle/DataFixtures/ORM --no-interaction
.PHONY: load-default-data

load-generator-data:
	$(COMPOSER) req irmag-extra/generator-bundle ^2.0
	$(CONSOLE) doctrine:fixtures:load --fixtures=vendor/irmag-extra/generator-bundle/DataFixtures/ORM --append --no-interaction
.PHONY: load-generator-data

# configure php env
dev-configure-php-env: export FILE_WWWCONF=/etc/php/$(PHP_VERSION)/fpm/pool.d/www.conf
dev-configure-php-env: export FILE_PHPINI_FPM=/etc/php/$(PHP_VERSION)/fpm/php.ini
dev-configure-php-env: export FILE_PHPFPMINI_CONF=/etc/php/$(PHP_VERSION)/fpm/php-fpm.conf
dev-configure-php-env: export FILE_PHPINI_CLI=/etc/php/$(PHP_VERSION)/cli/php.ini
dev-configure-php-env: export FILE_PHPINI_APC=/etc/php/$(PHP_VERSION)/mods-available/apcu.ini
dev-configure-php-env: export FILE_PHPINI_OPCACHE=/etc/php/$(PHP_VERSION)/mods-available/opcache.ini
dev-configure-php-env:
	sed -e 's/;clear_env = no/clear_env = no/' -i $(FILE_WWWCONF)
	sed -e 's/^;date.timezone.*/date.timezone = Asia\/Irkutsk/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^;date.timezone.*/date.timezone = Asia\/Irkutsk/' -i $(FILE_PHPINI_CLI)
	sed -e 's/^variables_order.*/variables_order = "EGPCS"/' -i $(FILE_PHPINI_FPM) $(FILE_PHPINI_CLI)
	sed -e 's/^post_max_size.*/post_max_size = 24M/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^post_max_size.*/post_max_size = 24M/' -i $(FILE_PHPINI_CLI)
	sed -e 's/^upload_max_filesize.*/upload_max_filesize = 24M/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^upload_max_filesize.*/upload_max_filesize = 24M/' -i $(FILE_PHPINI_CLI)
	sed -e 's/^max_execution_time.*/max_execution_time = 120/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^max_input_time.*/max_input_time = 120/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^memory_limit.*/memory_limit = 256M/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^;realpath_cache_ttl.*/realpath_cache_ttl = 600/' -i $(FILE_PHPINI_FPM)

	sed -e 's/^session.save_handler = .*/session.save_handler = redis/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^;session.save_path = .*/session.save_path = "tcp:\/\/localhost:6379"/' -i $(FILE_PHPINI_FPM)
	sed -e 's/^session.gc_maxlifetime = .*/session.gc_maxlifetime = 7200/' -i $(FILE_PHPINI_FPM)

	sed -e 's/^;emergency_restart_threshold.*/emergency_restart_threshold = 20/' -i $(FILE_PHPFPMINI_CONF)
	sed -e 's/^;emergency_restart_interval.*/emergency_restart_interval = 1m/' -i $(FILE_PHPFPMINI_CONF)
	sed -e 's/^;process_control_timeout.*/process_control_timeout = 30s/' -i $(FILE_PHPFPMINI_CONF)
	sed -e 's/^;events.mechanism.*/events.mechanism = epoll/' -i $(FILE_PHPFPMINI_CONF)
	sed -e 's/^;.*process.priority = .*/process.priority = -10/' -i $(FILE_PHPFPMINI_CONF)
	sed -e 's/^;events.mechanism.*/events.mechanism = epoll/' -i $(FILE_PHPFPMINI_CONF)

	for cfg in \
		"apc.enable_cli=1" \
		"apc.shm_size=256M"; \
	do \
		grep -q -F "$$cfg" $(FILE_PHPINI_APC) || echo "$$cfg" >> $(FILE_PHPINI_APC); \
	done

	for cfg in \
		"opcache.memory_consumption=256" \
		"opcache.max_accelerated_files=20000" \
		"#opcache.validate_timestamps=0"; \
	do \
		grep -q -F "$$cfg" $(FILE_PHPINI_OPCACHE) || echo "$$cfg" >> $(FILE_PHPINI_OPCACHE); \
	done
.PHONY: dev-configure-php-env

dev-configure-sudoers: export IRMAG_USER=user
dev-configure-sudoers: export FILE_SUDOERS=/etc/sudoers.d/90-irmag
dev-configure-sudoers:
	echo "$(IRMAG_USER) ALL=(ALL) NOPASSWD: /bin/systemctl reload php$(PHP_VERSION)-fpm.service" > $(FILE_SUDOERS)
	echo "$(IRMAG_USER) ALL=(ALL) NOPASSWD: /bin/systemctl reload nginx.service" >> $(FILE_SUDOERS)
.PHONY: .dev-configure-sudoers

# depends for debian only
dev-install-depends-apt:
	apt install \
		php$(PHP_VERSION)-common \
		php$(PHP_VERSION)-cli \
		php$(PHP_VERSION)-fpm \
		php$(PHP_VERSION)-pgsql \
		php$(PHP_VERSION)-intl \
		php$(PHP_VERSION)-gd \
		php$(PHP_VERSION)-curl \
		php$(PHP_VERSION)-readline \
		php$(PHP_VERSION)-xml \
		php$(PHP_VERSION)-json \
		php$(PHP_VERSION)-mbstring \
		php$(PHP_VERSION)-soap \
		php$(PHP_VERSION)-zip \
		php$(PHP_VERSION)-opcache \
		php-redis \
		php-apcu \
		php-apcu-bc \
		postgresql-9.6 \
		postgresql-client-9.6 \
		nodejs \
		yarn \
		nginx \
		jpegoptim \
		acl
.PHONY: dev-install-depends-apt
