<?php

namespace Irmag\Exchange1CBundle\EventDispatcher;

use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;
use Irmag\Exchange1CBundle\Event\Exchange1CQueueEvent;
use Irmag\Exchange1CBundle\IrmagExchange1CEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Exchange1CQueueEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Exchange1CQueue $queue
     * @param int             $pid
     */
    public function dispatchQueueItemProcessingStarted(Exchange1CQueue $queue, int $pid): void
    {
        $this->eventDispatcher->dispatch(
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_STARTED,
            $this->getExchange1CQueueEvent($queue, ['pid' => $pid])
        );
    }

    /**
     * @param Exchange1CQueue $queue
     * @param string          $message
     */
    public function dispatchQueueItemFailed(Exchange1CQueue $queue, string $message): void
    {
        $this->eventDispatcher->dispatch(
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_FAILED,
            $this->getExchange1CQueueEvent($queue, ['message' => $message])
        );
    }

    /**
     * @param Exchange1CQueue $queue
     */
    public function dispatchQueueItemProcessed(Exchange1CQueue $queue): void
    {
        $this->eventDispatcher->dispatch(
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_COMPLETED,
            $this->getExchange1CQueueEvent($queue)
        );
    }

    /**
     * @param Exchange1CQueue $queue
     * @param array           $data
     *
     * @return Exchange1CQueueEvent
     */
    private function getExchange1CQueueEvent(Exchange1CQueue $queue, array $data = []): Exchange1CQueueEvent
    {
        return new Exchange1CQueueEvent($queue, $data);
    }
}
