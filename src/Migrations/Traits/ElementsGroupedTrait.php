<?php

namespace Migrations\Traits;

use Irmag\CoreBundle\Exception\IrmagException;

trait ElementsGroupedTrait
{
    /**
     * @var string
     */
    private $ddlPath = __DIR__.'/../../../src/Irmag/SiteBundle/Entity/SQL/elements_grouped.sql';

    /**
     * Пересоздаёт elements_grouped view.
     *
     * @throws IrmagException If SQL file not found
     */
    private function createOrReplaceElementView()
    {
        if (is_readable($this->ddlPath)) {
            foreach (explode(';', file_get_contents($this->ddlPath)) as $sql) {
                $sql = trim($sql);

                if (empty($sql)) {
                    continue;
                }

                $this->addSql($sql);
            }
        } else {
            throw new IrmagException(sprintf('File "%s" not found.', $this->ddlPath));
        }
    }

    /**
     * Удаляет elements_grouped view.
     */
    private function dropElementView()
    {
        $this->addSql('DROP VIEW IF EXISTS elements_grouped');
    }
}
