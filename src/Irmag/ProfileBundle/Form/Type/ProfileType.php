<?php

namespace Irmag\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\City;

class ProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Личные данные
            ->add('username', null, [
                'label' => 'label.login',
                'disabled' => true,
            ])
            ->add('fullname', null, [
                'label' => 'label.fullname',
            ])
            ->add('birthday', BirthdayType::class, [
                'label' => 'profile.label.birthday',
                'placeholder' => 'placeholder.select',
            ])
            ->add('gender', GenderType::class)
            ->add('profession', null, [
                'label' => 'profile.label.profession',
            ])
            ->add('about', null, [
                'label' => 'profile.label.about',
            ])
            ->add('use_fullname', CheckboxType::class, [
                'label' => 'profile.label.use_fillname',
            ])
            // Контактные данные
            ->add('email', null, [
                'label' => 'profile.label.email',
                'disabled' => true,
            ])
            ->add('isEmailSubscribed', null, [
                'label' => 'profile.label.is_email_subscribed',
            ])
            ->add('telephone', null, [
                'label' => 'profile.label.telephone',
            ])
            ->add('isMobileTelephone', null, [
                'label' => false,
            ])
            ->add('isSmsSubscribed', null, [
                'label' => 'profile.label.is_sms_subscribed',
            ])
            ->add('isSiteSubscribed', null, [
                'label' => 'profile.label.is_site_subscribed',
            ])
            ->add('addressCity', EntityType::class, [
                'label' => 'profile.label.city',
                'class' => City::class,
                'choice_label' => 'name',
                'empty_data' => null,
                'placeholder' => 'placeholder.select',
                'attr' => ['class' => 'select2'],
            ])
            // Уведомления
            ->add('notificationEvents', ProfileNotificationEventType::class)
            // save
            ->add('save', SubmitType::class, [
                'label' => 'button.save',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['Default', 'change_profile'],
            'required' => false,
            'translation_domain' => 'irmag_profile',
        ]);
    }
}
