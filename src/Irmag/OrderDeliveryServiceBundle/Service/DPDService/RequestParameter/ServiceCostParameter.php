<?php

namespace Irmag\OrderDeliveryServiceBundle\Service\DPDService\RequestParameter;

use Irmag\OrderDeliveryServiceBundle\Model\OrderDeliveryServiceAddress;
use Symfony\Component\Validator\Constraints as Assert;
use Irmag\OrderDeliveryServiceBundle\RequestContract\OrderDeliveryServiceRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\ServiceCostRequestParameterInterface;

/**
 * Класс для параметров запроса к сервису DPD getServiceCost.
 */
class ServiceCostParameter implements OrderDeliveryServiceRequestParameterInterface, ServiceCostRequestParameterInterface
{
    /**
     * @var OrderDeliveryServiceAddress
     *
     * @Assert\NotBlank
     */
    private $pickupAddress;

    /**
     * @var OrderDeliveryServiceAddress
     *
     * @Assert\NotBlank
     */
    private $deliveryAddress;

    /**
     * @var bool Самостоятельно привезти на терминал?
     *
     * @Assert\NotNull
     */
    private $isSelfPickup = true;

    /**
     * @var bool Самостоятельно забрать?
     *
     * @Assert\NotNull
     */
    private $isSelfDelivery = false;

    /**
     * @var float Вес отправления
     *
     * @Assert\NotBlank
     */
    private $weight;

    /**
     * @var float Объём отправления (кубометры)
     */
    private $volume;

    /**
     * @var string Сервис доставки
     */
    private $serviceCode;

    /**
     * @var \DateTimeInterface Дата забора груза
     *
     * @Assert\Date
     */
    private $pickupDate;

    /**
     * @var int Максимально допустимое кол-во дней ожидания
     */
    private $maxDays;

    /**
     * @var float Максимально допустимая стоимость доставки
     */
    private $maxCost;

    /**
     * @var float Объявленная ценность груза
     */
    private $declaredValue;

    public function __construct()
    {
        $this->isSelfPickup = true;
        $this->isSelfDelivery = false;
    }

    /**
     * Get pickup address.
     *
     * @return OrderDeliveryServiceAddress
     */
    public function getPickupAddress(): OrderDeliveryServiceAddress
    {
        return $this->pickupAddress;
    }

    /**
     * Set pickup address.
     *
     * @param OrderDeliveryServiceAddress $pickupAddress
     *
     * @return $this
     */
    public function setPickupAddress(OrderDeliveryServiceAddress $pickupAddress): self
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * Get delivery address.
     *
     * @return OrderDeliveryServiceAddress
     */
    public function getDeliveryAddress(): OrderDeliveryServiceAddress
    {
        return $this->deliveryAddress;
    }

    /**
     * Set delivery address.
     *
     * @param OrderDeliveryServiceAddress $deliveryAddress
     *
     * @return $this
     */
    public function setDeliveryAddress(OrderDeliveryServiceAddress $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get is self pickup.
     *
     * @return bool
     */
    public function getIsSelfPickup(): bool
    {
        return $this->isSelfPickup;
    }

    /**
     * Set is self pickup.
     *
     * @param bool $isSelfPickup Is self pickup
     *
     * @return ServiceCostParameter
     */
    public function setIsSelfPickup(bool $isSelfPickup): self
    {
        $this->isSelfPickup = $isSelfPickup;

        return $this;
    }

    /**
     * Get is self delivery.
     *
     * @return bool
     */
    public function getIsSelfDelivery(): bool
    {
        return $this->isSelfDelivery;
    }

    /**
     * Set is self delivery.
     *
     * @param bool $isSelfDelivery Is self delivery
     *
     * @return ServiceCostParameter
     */
    public function setIsSelfDelivery(bool $isSelfDelivery): self
    {
        $this->isSelfDelivery = $isSelfDelivery;

        return $this;
    }

    /**
     * Get weight.
     *
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * Set weight.
     *
     * @param float $weight Weight
     *
     * @return ServiceCostParameter
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get volume.
     *
     * @return float|null
     */
    public function getVolume(): ?float
    {
        return $this->volume;
    }

    /**
     * Set volume.
     *
     * @param float $volume Volume
     *
     * @return ServiceCostParameter
     */
    public function setVolume(float $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get service code.
     *
     * @return string|null
     */
    public function getServiceCode(): ?string
    {
        return $this->serviceCode;
    }

    /**
     * Set service code.
     *
     * @param string $serviceCode Service code
     *
     * @return ServiceCostParameter
     */
    public function setServiceCode(string $serviceCode): self
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * Get pickup date.
     *
     * @return \DateTimeInterface
     */
    public function getPickupDate(): ?\DateTimeInterface
    {
        return $this->pickupDate;
    }

    /**
     * Set pickup date.
     *
     * @param \DateTimeInterface $pickupDate Pickup date
     *
     * @return ServiceCostParameter
     */
    public function setPickupDate(\DateTimeInterface $pickupDate): self
    {
        $this->pickupDate = $pickupDate;

        return $this;
    }

    /**
     * Get max days.
     *
     * @return int|null
     */
    public function getMaxDays(): ?int
    {
        return $this->maxDays;
    }

    /**
     * Set max days.
     *
     * @param int $maxDays Max days
     *
     * @return ServiceCostParameter
     */
    public function setMaxDays(int $maxDays): self
    {
        $this->maxDays = $maxDays;

        return $this;
    }

    /**
     * Get max cost.
     *
     * @return float|null
     */
    public function getMaxCost(): ?float
    {
        return $this->maxCost;
    }

    /**
     * Set max cost.
     *
     * @param float $maxCost Max cost
     *
     * @return ServiceCostParameter
     */
    public function setMaxCost(float $maxCost): self
    {
        $this->maxCost = $maxCost;

        return $this;
    }

    /**
     * Get declared value.
     *
     * @return float|null
     */
    public function getDeclaredValue(): ?float
    {
        return $this->declaredValue;
    }

    /**
     * Set declared value.
     *
     * @param float $declaredValue Declared value
     *
     * @return ServiceCostParameter
     */
    public function setDeclaredValue(float $declaredValue): self
    {
        $this->declaredValue = $declaredValue;

        return $this;
    }

    /**
     * Преобразовать объект в массив для отправления по SOAP.
     *
     * @return array
     */
    public function toArray(): array
    {
        $required = [
            'pickup' => [
                'cityId' => $this->getPickupAddress()->getCity()->getServiceCityCode(),
                'cityName' => $this->getPickupAddress()->getCity()->getName(),
                'regionCode' => $this->getPickupAddress()->getRegion()->getId(),
                'countryCode' => $this->getPickupAddress()->getCountry()->getCountryCode(),
            ],
            'delivery' => [
                'cityId' => $this->getDeliveryAddress()->getCity()->getServiceCityCode(),
                'cityName' => $this->getDeliveryAddress()->getCity()->getName(),
                'regionCode' => $this->getDeliveryAddress()->getRegion()->getId(),
                'countryCode' => $this->getDeliveryAddress()->getCountry()->getCountryCode(),
            ],
            'selfPickup' => $this->getIsSelfPickup(),
            'selfDelivery' => $this->getIsSelfDelivery(),
            'weight' => $this->getWeight(),
        ];

        $extra = [];

        if (!empty($this->getVolume())) {
            $extra['volume'] = $this->getVolume();
        }

        if (!empty($this->getServiceCode())) {
            $extra['serviceCode'] = $this->getServiceCode();
        }

        if (!empty($this->getPickupDate())) {
            $extra['pickupDate'] = $this->getPickupDate()->format('Y-m-d');
        }

        if (!empty($this->getMaxDays())) {
            $extra['maxDays'] = $this->getMaxDays();
        }

        if (!empty($this->getMaxCost())) {
            $extra['maxCost'] = $this->getMaxCost();
        }

        if (!empty($this->getDeclaredValue())) {
            $extra['declaredValue'] = $this->getDeclaredValue();
        }

        return array_merge($required, $extra);
    }
}
