<?php

namespace Irmag\OnlinePaymentBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class IrmagSyncOnlinePaymentCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'irmag:online_payments:sync';

    /**
     * @var OnlinePaymentManager
     */
    private $onlinePaymentManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $providerConfigs;

    /**
     * @param OnlinePaymentManager $onlinePaymentManager
     * @param ContainerInterface   $container
     * @param array                $providerConfigs
     */
    public function __construct(
        OnlinePaymentManager $onlinePaymentManager,
        ContainerInterface $container,
        array $providerConfigs
    ) {
        $this->onlinePaymentManager = $onlinePaymentManager;
        $this->container = $container;
        $this->providerConfigs = $providerConfigs;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Synchronize online payment states via Sberbank API.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        if ($this->lock()) {
            $providerServices = [];

            foreach ($this->providerConfigs as $method => $config) {
                $providerServices[$method] = $this->container->get($config['service']);
            }

            $payments = $this->onlinePaymentManager->getAllPaymentsToSync();
            $output->writeln('Start syncing...');

            if ($output->isVerbose()) {
                $output->writeln(sprintf('Total payments to sync: <info>%s</info>', \count($payments)));

                $progress = new ProgressBar($output, \count($payments));
                $progress->start();
            }

            foreach ($payments as $payment) {
                try {
                    $paymentMethodShortname = $payment->getOrder()->getPaymentMethod()->getShortname();

                    if (true === isset($providerServices[$paymentMethodShortname])) {
                        $currentService = $providerServices[$paymentMethodShortname];
                        $paymentData = $currentService->getState($payment);
                    } else {
                        $output->writeln(sprintf('<comment>Undefined payment service for payment method "%s", skipping ...</comment>', $paymentMethodShortname));
                        $output->writeln('');
                    }
                } catch (\Exception $ex) {
                    if ($output->isVerbose()) {
                        $output->writeln('');
                        $output->writeln(sprintf('<error>%s</error>', $ex->getMessage()));
                    }

                    continue;
                }

                if ($output->isVeryVerbose()) {
                    $output->writeln('');
                    $output->writeln(sprintf(' <comment>Syncing order №%d</comment>', $payment->getOrder()->getId()));
                    $progress->advance();
                }

                try {
                    $this->onlinePaymentManager->updateOrderOnlinePayment($payment, $paymentData);
                } catch (\Exception $ex) {
                    if ($output->isVerbose()) {
                        $output->writeln('');
                        $output->writeln(sprintf('<error>%s</error>', $ex->getMessage()));
                    }

                    continue;
                }
            }

            if ($output->isVerbose()) {
                $progress->finish();
            }

            $output->writeln('');
            $output->writeln(sprintf('<info>Finished.</info>'));

            $this->release();
        }
    }
}
