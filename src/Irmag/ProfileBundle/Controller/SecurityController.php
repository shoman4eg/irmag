<?php

namespace Irmag\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\CoreBundle\Utils\TokenGenerator;
use Irmag\ProfileBundle\EventDispatcher\UserEventDispatcher;
use Irmag\ProfileBundle\Form\Type\RegistrationType;
use Irmag\ProfileBundle\Form\Type\RestorePasswordType;
use Irmag\ProfileBundle\Form\Type\RestorePasswordByConfirmationTokenType;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserConfirmation;
use Irmag\ProfileBundle\Mailer\SecurityMailer;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager\UserNotificationSubscriptionManager;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\BasketFavoriteManager\FavoriteManager;
use Irmag\SiteBundle\Service\ReferralManager\ReferralManager;

/**
 * @Route("/")
 * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
 */
class SecurityController extends Controller
{
    /**
     * Страница аутентификации.
     *
     * @Route("/login/", name="security_login")
     */
    public function loginAction()
    {
        // если аутентифицирован, то редирект на главную
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername(); // username or email

        // поясняем, что нужно потвердить email для активации, если ошибка из-за этого
        if (!empty($error) && !empty($lastUsername)) {
            /** @var User $user */
            $user = $this->get('doctrine.orm.default_entity_manager')->getRepository(User::class)
                ->findByUsernameOrEmail($lastUsername);

            /** @var UserConfirmation $userConfirmation */
            if ($user && $userConfirmation = $user->getConfirmations()->first()) {
                $confirmationEmail = $userConfirmation->getEmail();
            }
        }

        return $this->render('@IrmagProfile/Security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'confirmation_email' => $confirmationEmail ?? null,
        ]);
    }

    /**
     * Регистрация пользователя.
     *
     * @Route("/registration/", name="security_registration")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registrationAction(Request $request)
    {
        // если аутентифицирован, то редирект на главную
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $form->getData();
            $em = $this->get('doctrine.orm.default_entity_manager');
            $session = $request->getSession();

            // назначаем пароль
            $pwdPlain = $user->getPassword();
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwdPlain);
            $user->setPassword($pwd);

            // если пользователь указал email, то требуем подтверждение.
            if ($email = $user->getEmail()) {
                // удаляем email, т.к он в подтверждение (userConfirmation)
                $user->setEmail(null);
                // деактивируем пользователя
                $user->setIsActive(false);
                $em->persist($user);

                // создаём токен
                $userConfirmation = new UserConfirmation();
                $userConfirmation->setToken(TokenGenerator::generateToken());
                $userConfirmation->setUser($user);
                $userConfirmation->setEmail($email);
                $em->persist($userConfirmation);

                // отсылаем письмо с подтверждением email
                $this->get(SecurityMailer::class)->sendConfirmationEmailMessage($email, [
                    'userConfirmation' => $userConfirmation,
                    'user' => $user,
                ]);

                $em->flush();
                $session->set('user_confirmation_user_id', $user->getId());
                $redirectRoute = 'security_confirmation_email_sended';
            } else {
                $this->authenticate($user);
                $em->persist($user);
                $em->flush();

                $redirectRoute = 'security_registration_success';
            }

            // запоминаем реферала
            $this->get(ReferralManager::class)->rememberFromLink($user);

            // подписки по-умолчанию
            $this->doDefaultNewUserSubscriptions($user);

            return $this->redirectToRoute($redirectRoute);
        }

        return $this->render('@IrmagProfile/Security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function doDefaultNewUserSubscriptions(User $user)
    {
        $userNotificationSubscriptionManager = $this->get(UserNotificationSubscriptionManager::class);
        $userNotificationEventManager = $this->get(UserNotificationEventManager::class);

        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::FAVORITE_ELEMENT_IS_ACTIVE), $user, false, false, true, false);
        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::ORDER_STATUS), $user, false, false, true, false);
        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::BONUS_SEND), $user, false, false, true, false);
        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::FORUM_POST_REPLY), $user, false, false, true, false);
        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::BLOG_COMMENT_REPLY), $user, false, false, true, false);
        $userNotificationSubscriptionManager->subscribe($userNotificationEventManager->getEventByName(NotificationEvents::SITE_COMMENT_REPLY), $user, false, false, true, false);
    }

    /**
     * Проверка логина на занятость.
     *
     * @Route("/logincheck/", name="security_check_login", options={"expose": true}, condition="request.isXmlHttpRequest()")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function checkLoginAction(Request $request)
    {
        $login = trim(mb_strtolower($request->get('login')));

        if (!empty($login)) {
            $user = $this
                ->get('doctrine.orm.default_entity_manager')
                ->createQueryBuilder()
                ->select('u')
                ->from(User::class, 'u')
                ->where('LOWER(u.username) = :login')
                ->orWhere('LOWER(u.email) = :login')
                ->setParameter('login', $login)
                ->getQuery()
                ->getOneOrNullResult();
        }

        return new JsonResponse([
            'isFree' => empty($user),
        ]);
    }

    /**
     * Аутентификация пользователя.
     *
     * @param User $user
     */
    protected function authenticate(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);

        // записываем время аутентификации
        $user->setLastLoginAt(new \DateTime());
    }

    /**
     * Сообщает об успешной регистрации.
     *
     * @Route("/registration/success/", name="security_registration_success")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function registrationSuccessAction()
    {
        // merge basket
        $this->container->get(BasketManager::class)->merge();
        // merge favorite
        $this->container->get(FavoriteManager::class)->merge();

        return $this->render('@IrmagProfile/Security/registration-success.html.twig');
    }

    /**
     * Сообщает об отправленном письме с подтверждением.
     *
     * @Route("/confirmation/email/sended/", name="security_confirmation_email_sended")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function confirmationEmailSendedAction(Request $request)
    {
        $session = $request->getSession();

        if (false === $session->has('user_confirmation_user_id')) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $userConfirmation = $em->getRepository(UserConfirmation::class)
            ->findOneBy(['user' => $session->get('user_confirmation_user_id')]);

        if (!$userConfirmation) {
            throw $this->createNotFoundException('UserConfirmation not found');
        }

        $session->remove('user_confirmation_user_id');

        return $this->render('@IrmagProfile/Security/confirmation-email-sended.html.twig', [
            'userConfirmation' => $userConfirmation,
            'user' => $userConfirmation->getUser(),
        ]);
    }

    /**
     * Подтверждает email (по ссылке из письма).
     *
     * Два развития событий:
     *
     * 1. Подтверждение при регистрация.
     *    Нужно аутентифицироваться и сообщить об успешной регистрации.
     * 2. Подтверждение при изменение email.
     *    Мы уже аутентифицированные, нужно сообщить об успешном изменение email.
     *
     * @Route("/confirmation/email/{token}/", name="security_confirmation_email_confirm")
     *
     * @param string $token
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function confirmationEmailSuccessAction(string $token)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        /** @var UserConfirmation $userConfirmation */
        if (!$userConfirmation = $em->getRepository(UserConfirmation::class)->findOneBy(['token' => $token])) {
            return $this->redirectToRoute('irmag_homepage');
        }

        if (!$user = $userConfirmation->getUser()) {
            throw $this->createNotFoundException('User not found!');
        }

        $email = $userConfirmation->getEmail();

        // уже аутентифицированные, значит смена email
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // если токен не принадлежит текущему пользователю, то кидаем на главную
            if ($user !== $this->getUser()) {
                return $this->redirectToRoute('irmag_homepage');
            }

            // устанавливаем email
            $user->setEmail($email);

            $redirectToRoute = 'irmag_profile_change_email_success';
        } else {
            // подтверждение email при регистрации
            // аутентифицируемся
            $this->authenticate($user);
            // активируем пользователя
            $user->setIsActive(true);
            // устанавливаем email
            $user->setEmail($email);

            $this->get(UserEventDispatcher::class)->dispatchUserRegistrationEmailConfirmed($user);
            $this->addFlash(UserConfirmation::FLASH_REGISTRATION_EMAIL_CONFIRMED, true);

            $redirectToRoute = 'security_registration_success';
        }

        $em->persist($user);
        $em->flush();
        // удаляем все токены пользователя
        $em->getRepository(UserConfirmation::class)->removeAllByUser($user);

        return $this->redirectToRoute($redirectToRoute);
    }

    /**
     * Форма запроса на восстановление пароля по токену,
     * который отправляеться на email пользователя.
     *
     * @Route("/restore-password/", name="security_restore_password")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function restorePasswordAction(Request $request)
    {
        // если аутентифицирован, то редирект на главную
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $form = $this->createForm(RestorePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userNameOrEmail = $form->getData()['username_or_email'];
            $em = $this->get('doctrine.orm.default_entity_manager');

            // XXX: n timeout ban
            // manual delay
            sleep(1);

            $user = $em->getRepository(User::class)->findByUsernameOrEmail($userNameOrEmail);

            // только когда у пользователя есть email
            if ($user && $user->getEmail()) {
                // записываем токен
                $userConfirmation = new UserConfirmation();
                $userConfirmation->setUser($user);
                $userConfirmation->setToken(TokenGenerator::generateToken());
                $em->persist($userConfirmation);
                $em->flush();

                // отправляем письмо на email
                $this->get(SecurityMailer::class)->sendConfirmationRestorePasswordMessage($user->getEmail(), [
                    'userConfirmation' => $userConfirmation,
                    'user' => $user,
                ]);

                $request->getSession()->set('security_restore_password_email', $user->getEmail());

                return $this->redirectToRoute('security_restore_password_email_sended');
            }
        }

        if ($form->isSubmitted() && !isset($user)) {
            $form->addError(new FormError('Пользователь не найден'));
        }

        return $this->render('@IrmagProfile/Security/restore-password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Сообщение о том, что письмо с токеном
     * отправлено на email пользователя.
     *
     * @Route("/restore-password/email-sended/", name="security_restore_password_email_sended")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function restorePasswordEmailSendedAction(Request $request)
    {
        $session = $request->getSession();

        // если аутентифицирован или session сообщения не существует,
        // то редирект на главную
        if (false === $session->has('security_restore_password_email')
            || $this->isGranted('IS_AUTHENTICATED_REMEMBERED')
        ) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $email = $session->get('security_restore_password_email');

        $session->remove('security_restore_password_email');

        return $this->render('@IrmagProfile/Security/restore-password-email-sended.html.twig', [
            'email' => $email,
        ]);
    }

    /**
     * Восстановление\назначение нового пароля по токену с последующей аутентификацией.
     *
     * @Route("/restore-password/{token}/", name="security_restore_password_by_token")
     *
     * @param Request $request
     * @param string  $token
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function restorePasswordByTokenAction(Request $request, string $token)
    {
        // если аутентифицирован или token пустой, то редирект на главную
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED') || empty($token)) {
            return $this->redirectToRoute('irmag_homepage');
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $userConfirmation = $em->getRepository(UserConfirmation::class)
            ->findOneBy(['token' => $token]);

        if (!$userConfirmation) {
            return $this->redirectToRoute('irmag_homepage');
        }

        if (!$user = $userConfirmation->getUser()) {
            throw $this->createNotFoundException('User not found');
        }

        $form = $this->createForm(RestorePasswordByConfirmationTokenType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            // назначаем новый пароль
            $pwdPlain = $user->getPassword();
            $encoder = $this->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwdPlain);
            $user->setPassword($pwd);
            // удаляем токен
            $em->remove($userConfirmation);
            // аутентифицируемся
            $this->authenticate($user);

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('security_restore_password_success');
        }

        return $this->render('@IrmagProfile/Security/restore-password-by-confirmation-token.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * Сообщение о успешном восстановление\назначение пароля.
     *
     * @Route("/restore-password/success/", name="security_restore_password_success")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function restorePasswordSuccessAction()
    {
        return $this->render('@IrmagProfile/Security/restore-password-success.html.twig');
    }

    /**
     * Удаление токена из бд.
     * Ссылка в письме у пользователя.
     *
     * @Route("/confirmation/remove/{token}/", name="security_confirmation_remove_token")
     *
     * @param string $token
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function confirmationRemoveTokenAction(string $token)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $userConfirmation = $em->getRepository(UserConfirmation::class)
            ->findOneBy(['token' => $token]);

        if ($userConfirmation) {
            // удаляем токен
            $em->remove($userConfirmation);
            $em->flush();
        }

        return $this->redirectToRoute('irmag_homepage');
    }
}
