/**
 * @namespace IRMAG.Profile
 *
 * @requires jquery
 * @requires jquery.multiTelephoneField
 * @requires select2
 */

import log from '../core/modules/logger';
import { ajax } from '../core/modules/irmag-utils';

IRMAG.Profile = {
    PUSH_COL: 2,
    EMAIL_COL: 3,
    SITE_COL: 4,
    SMS_COL: 5,
    $notificationTable: null,
    isPushSubscribed: false,
    $isEmailSubscribed: null,
    $isSiteSubscribed: null,
    $isSmsSubscribed: null,
    currentPushId: '',

    init: function () {
        log('[Profile]', 'init');

        this.$notificationTable = $('#tab-notifications .notification-events');
        this.$isEmailSubscribed = $('#profile_isEmailSubscribed');
        this.$isSiteSubscribed = $('#profile_isSiteSubscribed');
        this.$isSmsSubscribed = $('#profile_isSmsSubscribed');

        this.bindBootstrapTabs();
        this.initMultiTelephoneField();
        this.initSelect2(); // city list
        this.bindNotificationCheckboxes();
        this.bindDeletePushId();
        this.bindShowPushIdsListModal();
    },

    bindShowPushIdsListModal: function () {
        const self = this;

        $(document).on('click', '.show-modal-push-ids', function () {
            ajax('irmag_profile_push_ids_list', { currentPushId: self.currentPushId }, function (data) {
                bootbox.alert({
                    size: 'large',
                    message: data,
                    title: 'Мои PUSH устройства',
                });
            });
        });
    },

    bindDeletePushId: function () {
        const self = this;

        $(document).on('click', '.delete-push-id', function () {
            const $btn = $(this);
            const uuid = $btn.data('uuid');

            self.pushUnregister(uuid, function () {
                $('.tooltip').remove();
                $btn.parents('tr').remove();

                if (0 === $('#table-push-ids tbody tr').length) {
                    window.location.reload(true);
                }
            });
        });
    },

    pushRegister: function (uuid, device, ip) {
        ajax('irmag_profile_push_register_unregister', {
            is_subscribed: true,
            uuid: uuid,
            device: device,
            ip: ip,
            ua: navigator.userAgent,
        }, function () {
            IRMAG.Profile.activateTab('#notifications');
        });
    },

    pushUnregister: function (uuid, callback) {
        ajax('irmag_profile_push_register_unregister', {
            is_subscribed: false,
            uuid: uuid,
        }, function () {
            if ($.isFunction(callback)) {
                callback();
            }
            IRMAG.Profile.activateTab('#notifications');
        });
    },

    bindBootstrapTabs: function () {
        $('a[data-toggle="tab"]').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            window.location.hash = $(e.target).attr('href').substr(1);
        });

        const hash = window.location.hash || '#personal';
        this.activateTab(hash);
    },

    activateTab: function (hash) {
        $('#profile-tabs a[href="' + hash + '"]').tab('show');
        window.location.hash = hash;
    },

    bindNotificationCheckboxes: function () {
        const self = this;

        if (self.isPushSubscribed) {
            self.enableNotificationCol(self.PUSH_COL);
        } else {
            self.disableNotificationCol(self.PUSH_COL);
        }

        this.$isEmailSubscribed.on('change', function () {
            if ($(this).is(':checked')) {
                self.enableNotificationCol(self.EMAIL_COL);
            } else {
                self.disableNotificationCol(self.EMAIL_COL);
            }
        }).trigger('change');

        this.$isSiteSubscribed.on('change', function () {
            if ($(this).is(':checked')) {
                self.enableNotificationCol(self.SITE_COL);
            } else {
                self.disableNotificationCol(self.SITE_COL);
            }
        }).trigger('change');

        this.$isSmsSubscribed.on('change', function () {
            if ($(this).is(':checked')) {
                self.enableNotificationCol(self.SMS_COL);
            } else {
                self.disableNotificationCol(self.SMS_COL);
            }
        }).trigger('change');
    },

    enableNotificationCol: function (col) {
        this.$notificationTable.find('tbody td:nth-child(' + col + ') input:not(.is-disabled)').attr('disabled', false);
        this.$notificationTable.find('thead th:nth-child(' + col + ')').removeClass('text-muted');
    },

    disableNotificationCol: function (col) {
        this.$notificationTable.find('tbody td:nth-child(' + col + ') input:not(.is-disabled)').attr('disabled', true);
        this.$notificationTable.find('thead th:nth-child(' + col + ')').addClass('text-muted');
    },

    initMultiTelephoneField: function () {
        const self = this;

        $('.multi-telephone-field').multiTelephoneField({
            onCheckboxChanged: function (checkbox, value) {
                if ($(checkbox).is(':checked') && 18 === value.length) {
                    self.enableSmsSubscribeRow();
                } else {
                    self.disableSmsSubscribeRow();
                }
            },
            onValueChanged: function (value) {
                if (18 === value.length) {
                    self.enableSmsSubscribeRow();
                } else {
                    self.disableSmsSubscribeRow();
                }
            },
        });
    },

    initSelect2: function () {
        $('.select2').select2({ width: '100%' });
    },

    disableSmsSubscribeRow: function () {
        this._getSmsSubscribeRow()
            .find('label').addClass('disabled')
            .find('input').prop('disabled', true).prop('checked', false).trigger('change'); // disable and uncheck
    },

    enableSmsSubscribeRow: function () {
        this._getSmsSubscribeRow()
            .find('label').removeClass('disabled')
            .find('input').prop('disabled', false).trigger('change');
    },

    /**
     * @private
     */
    _getSmsSubscribeRow: function () {
        return $('#sms-subscribe-row');
    },
};
