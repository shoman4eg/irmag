<?php

namespace Irmag\Exchange1CBundle\Parser;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\Exchange1CBundle\Event\ParserEvent;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Irmag\Exchange1CBundle\IrmagExchange1CEvents;

abstract class AbstractBaseParser implements ParserInterface
{
    /**
     * @see http://api.symfony.com/3.1/Symfony/Component/Console/Output/OutputInterface.html
     */
    const VERBOSITY_QUIET = 'quiet';
    const VERBOSITY_NORMAL = 'normal';
    const VERBOSITY_VERBOSE = 'verbose';
    const VERBOSITY_VERY_VERBOSE = 'very_verbose';

    const TMP_FILE_PREFIX = 'irmag_file_';

    /**
     * @var string
     */
    protected $xmlFile;

    /**
     * @var Crawler
     */
    protected $crawler;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var bool
     */
    protected $isDryRun = false;

    /**
     * Пропускать не найденные сущности.
     *
     * @var bool
     */
    protected $skipNotFoundEntities = false;

    /**
     * @var ProgressBar
     */
    protected $progressBar;

    /**
     * Список полей для заполнения.
     *
     * @var array
     */
    protected $fieldsToFill = [];

    /**
     * Список полей со связями (relationship) для заполнения.
     *
     * @var array
     */
    protected $fieldsRelationshipToFill = [];

    /**
     * Через сколько операций вызывать flush().
     *
     * @var int
     */
    protected $howManyOperationsToFlush = 20;

    /**
     * Счётчик операций.
     *
     * @var int
     */
    protected $operations = 0;

    /**
     * @var array
     */
    protected $tmpFilesToRemove = [];

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * @param EntityManagerInterface   $em
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $dispatcher
    ) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    public function __destruct()
    {
        $this->removeTmpFiles();
    }

    /**
     * Назначить XML файл для парсера.
     * XML файл может быть сжатый gzip'ом.
     *
     * @param string $xmlFile Пусть до XML файла
     */
    public function setXmlFile($xmlFile)
    {
        if (!is_file($xmlFile)) {
            throw new NotFoundHttpException(sprintf('XML file "%s" not found.', $xmlFile));
        }

        if ('gz' === pathinfo($xmlFile)['extension']) {
            $xmlFile = 'compress.zlib://'.$xmlFile;
        }

        $this->xmlFile = $xmlFile;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param bool $isDryRun
     */
    public function setIsDryRun($isDryRun)
    {
        $this->isDryRun = $isDryRun;
    }

    /**
     * @param bool $skipNotFoundEntities
     */
    public function setSkipNotFoundEntities($skipNotFoundEntities)
    {
        $this->skipNotFoundEntities = $skipNotFoundEntities;
    }

    /**
     * @param ProgressBar $progressBar
     */
    public function setProgressBar(ProgressBar $progressBar)
    {
        $this->progressBar = $progressBar;
    }

    /**
     * Возвращает кол-во шагов для ProgressBar.
     *
     * @return int
     */
    public function getUnits()
    {
        return 0;
    }

    /**
     * Обёртка для $this->parseXml().
     *
     * @param string $parserService Название сервиса
     */
    public function parse($parserService)
    {
        $this->output->writeln(sprintf('<comment>[%s] Start parsing...</comment>', (new \DateTime())->format('d.m.Y H:i:s')));

        if ($this->output->isVerbose()) {
            $this->output->writeln(sprintf('Verbosity level: <info>%s</info>', $this->output->getVerbosity()));
            $this->output->writeln(sprintf('Operations: <info>%d</info>', $this->getUnits()));
            $this->output->writeln(sprintf('Memory usage before: <info>%f Kb.</info>', memory_get_usage() / 1024));
        }

        // DBAL logger
        if ($this->output->isVeryVerbose()) {
            $logger = $this->getLogger();
        }

        // start progress bar
        if ($this->output->isVerbose()) {
            $this->output->writeln('');
            $this->progressBar->start();
        }

        // начинаем парсить
        $this->parseXml();

        if (false === $this->isDryRun) {
            $this->em->flush();
        }

        // stop progress bar
        if ($this->output->isVerbose()) {
            $this->progressBar->finish();
            $this->output->writeln('');
            $this->output->writeln('');
        }

        // выводим все запросы в OutputInterface
        if (isset($logger)) {
            $totalDbExecutionTime = 0.00;

            foreach ($logger->queries as $query) {
                $this->output->writeln(sprintf(
                    '<comment>%s</comment> [%s] { %s } <info>%f secs.</info>',
                    $query['sql'],
                    implode(', ', $this->getFormattedQueryParams($query['params'])),
                    implode(', ', $query['types'] ?? []),
                    $query['executionMS']
                ));

                $totalDbExecutionTime += $query['executionMS'];
            }

            $this->output->writeln('');
            $this->output->writeln(sprintf('DB total queries: <info>%d</info>', $logger->currentQuery));
            $this->output->writeln(sprintf('DB execution time: <info>%f secs.</info>', $totalDbExecutionTime));
        }

        if ($this->output->isVerbose()) {
            $this->output->writeln(sprintf('Memory usage after: <info>%f Kb.</info>', memory_get_usage() / 1024));
            $this->output->writeln(sprintf('Total execution time: <info>%f secs.</info>', microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']));
        }

        //$event = new ParserEvent($this, $parserService);
        //$this->dispatcher->dispatch(IrmagExchange1CEvents::PARSE_COMPLETE, $event); // not used

        $this->output->writeln(sprintf('<comment>[%s] Complete</comment>', (new \DateTime())->format('d.m.Y H:i:s')));
    }

    /**
     * @return Crawler
     */
    protected function getCrawler()
    {
        if (null === $this->crawler) {
            $this->crawler = new Crawler();
            $this->crawler->addXmlContent(file_get_contents($this->xmlFile));
        }

        return $this->crawler;
    }

    /**
     * Заполняет поля из $this->fieldsToFill.
     *
     * @param object  $entity Entity для заполнения
     * @param Crawler $node
     */
    protected function fillFields($entity, Crawler $node)
    {
        foreach ($this->fieldsToFill as $field) {
            if (0 !== $node->filter($field)->count()) {
                $value = $node->filter($node->nodeName().' > '.$field)->text();
                $value = empty($value) && '0' !== $value ? null : $value;

                $this->accessor->setValue($entity, $field, $value);
            }
        }
    }

    /**
     * Заполняет поля со связями из $this->fieldsRelationshipToFill.
     *
     * @param object  $entity Entity для заполнения
     * @param Crawler $node
     */
    protected function fillFieldsRelationship($entity, Crawler $node)
    {
        foreach ($this->fieldsRelationshipToFill as $field => $entityFQCN) {
            if (0 !== $node->filter($field)->count()) {
                $id = (int) $node->filter($field)->attr('id');
                $value = empty($id) ? null : $this->getEntityById($entityFQCN, $id);
                $this->accessor->setValue($entity, $field, $value);
            }
        }
    }

    /**
     * Возвращает сущность по $id.
     *
     * @param string $entityFQCN
     * @param int    $id
     *
     * @throws NotFoundHttpException Если сущьность на найдена
     *
     * @return object|null
     */
    protected function getEntityById($entityFQCN, $id)
    {
        $entity = $this->em->getRepository($entityFQCN)->find($id);

        if (!$entity && !$this->skipNotFoundEntities) {
            throw new NotFoundHttpException(sprintf('Entity "%s" with id "%d" not found.', $entityFQCN, $id));
        }

        return $entity;
    }

    /**
     * Очищает таблицу.
     *
     * @param string $entity  FQCN (Entity::class)
     * @param bool   $cascade
     */
    protected function truncate($entity, $cascade = true)
    {
        $connection = $this->em->getConnection();
        $platform = $connection->getDatabasePlatform();
        $tableName = $this->em->getClassMetadata($entity)->getTableName();
        $connection->executeUpdate($platform->getTruncateTableSQL($tableName, $cascade));

        if ($this->output->isVerbose()) {
            $this->output->writeln('');
            $this->output->writeln(sprintf('Truncate table: <info>%s</info>', $entity));
        }
    }

    /**
     * @param string $base64 Картинка в base64
     *
     * @return UploadedFile|null
     */
    protected function getUploadedFileFromBase64($base64)
    {
        $binaryData = base64_decode(trim($base64), true);

        if (false === $binaryData) {
            return null;
        }

        $tmpfname = tempnam(sys_get_temp_dir(), self::TMP_FILE_PREFIX);
        file_put_contents($tmpfname, $binaryData);

        $file = new File($tmpfname);
        $uploadedFile = new UploadedFile($file->getRealPath(), $file->getFilename(), $file->getMimeType(), $file->getSize(), 0, true);

        $this->tmpFilesToRemove[] = $tmpfname;

        return $uploadedFile;
    }

    /**
     * Удаляет временные файлы.
     */
    protected function removeTmpFiles()
    {
        if (null === $this->output) {
            return;
        }

        if ($this->output->isVerbose()) {
            $this->output->writeln(sprintf('Remove tmp files: <info>%d files</info>', \count($this->tmpFilesToRemove)));
        }

        foreach ($this->tmpFilesToRemove as $tmpFile) {
            @unlink($tmpFile);
        }

        if ($this->output->isVerbose()) {
            $this->output->writeln('Remove tmp files: <info>Complete</info>');
        }
    }

    /**
     * @param bool $useClear Вызвать $em->clear() после $em->flush()
     */
    protected function flushIfNeed($useClear = false)
    {
        if (true === $this->isDryRun) {
            return;
        }

        if ($this->output->isVeryVerbose()) {
            $this->em->flush();
        } elseif (0 === $this->operations % $this->howManyOperationsToFlush) {
            $this->em->flush();

            if (true === $useClear) {
                $this->em->clear();
            }

            $this->operations = 0;
        }
    }

    /**
     * Возвращает отформатированные параметры запроса.
     *
     * @param array|null $params DBAL Logger query params
     *
     * @return array
     */
    protected function getFormattedQueryParams($params)
    {
        $formattedParams = [];

        if (\is_array($params)) {
            foreach ($params as $param) {
                if ($param instanceof \DateTime) {
                    $param = $param->format('d.m.Y H:i:s');
                }

                $formattedParams[] = $param;
            }
        }

        return $formattedParams;
    }

    /**
     * @return \Doctrine\DBAL\Logging\DebugStack
     */
    protected function getLogger()
    {
        $logger = new \Doctrine\DBAL\Logging\DebugStack();

        $this->em->getConnection()
            ->getConfiguration()
            ->setSQLLogger($logger);

        return $logger;
    }

    abstract protected function parseXml();
}
