<?php

namespace Irmag\SiteBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Section;

class ElementRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param int $id
     *
     * @return Element|null
     */
    public function getOneById(int $id): ?Element
    {
        return $this->createQueryBuilder('e')
            ->addSelect('s, se, b, m, c, ep, epf, etp, t')
            ->leftJoin('e.sectionElements', 'se')
            ->leftJoin('se.section', 's')
            ->leftJoin('e.brand', 'b')
            ->leftJoin('e.manufacturer', 'm')
            ->leftJoin('e.country', 'c')
            ->leftJoin('e.pictures', 'ep')
            ->leftJoin('ep.file', 'epf')
            ->leftJoin('e.tonePicture', 'etp')
            ->leftJoin('e.thread', 't')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $searchQuery
     *
     * @return QueryBuilder
     */
    public function getSearchQb(string $searchQuery): QueryBuilder
    {
        return $this->createQueryBuilder('e')
            ->select('partial e.{id,name,toneName,price,propSpecialPrice}')
            ->addSelect('PLAINTSRANK(e.tsv, :searchQuery, \'irmag\') as HIDDEN rank')
            ->addSelect('p, f, tp')
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'f')
            ->leftJoin('e.tonePicture', 'tp')
            ->andWhere('p.position = 1 OR p.position IS NULL')
            ->andWhere('PLAINTSQUERY(e.tsv, :searchQuery, \'irmag\') = true')
            ->setParameter('searchQuery', $searchQuery)
            ->orderBy('rank', 'desc');
    }

    /**
     * @param string $searchQuery
     * @param int    $limit
     *
     * @return array
     */
    public function search(string $searchQuery, int $limit): array
    {
        return $this->getSearchQb($searchQuery)
            ->andWhere('e.isActive = true')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }

    /**
     * Запрос для получения списка эл-ов в текущем разделе,
     * если раздел не задан, то все эл-ты.
     *
     * XXX: join section
     * Товар может находиться сразу в нескольких section.
     * HYDRATION может быть избыточным
     *
     * @param Section $section
     *
     * @return QueryBuilder
     */
    public function getElementsQueryBuilderBySection(Section $section = null): QueryBuilder
    {
        $elementsQb = $this->createQueryBuilder('e')
            ->select('partial e.{id, name, toneName, price, propEco, propSpecialPrice, propSpecialPrice, propSafeAnimal, propTransfer, groupBy, createdAt, isActive}')
            ->addSelect('p, f')
            ->addSelect('partial t.{id, avgRating, commentsTotalCount}')
            // for knp paginator sorting
            ->addSelect('e.createdAt AS HIDDEN createdAt')
            ->addSelect('COALESCE(t.avgRating, 0) AS HIDDEN avgRating')
            ->addSelect('e.name AS HIDDEN name')
            ->addSelect('e.price AS HIDDEN price')
            ->addSelect('e.weight AS HIDDEN weight')
            ->addSelect('e.volume AS HIDDEN volume')
            // end sorting
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'f')
            ->leftJoin('e.thread', 't')
            ->where('p.position = 1 OR p.position IS NULL')
            ->andWhere('e.isActive = true')
        ;

        if (null !== $section) {
            $elementsQb
                ->addSelect('se', 's')
                ->leftJoin('e.sectionElements', 'se')
                ->leftJoin('se.section', 's')
                ->andWhere('s.isActive = true')
                ->andWhere('s.lft >= :lft')
                ->andWhere('s.rgt <= :rgt')
                ->andWhere('s.root = :root')
                ->setParameters([
                    'lft' => $section->getLft(),
                    'rgt' => $section->getRgt(),
                    'root' => $section->getRoot(),
                ]);
        }

        return $elementsQb;
    }

    /**
     * Возвращает сгруппированные товары (groupBy) в карточке товара.
     *
     * @param Element $element
     *
     * @return array
     */
    public function getGroupByElements(Element $element)
    {
        $elements = [];

        if ($element->getGroupBy()) {
            $elements = $this->createQueryBuilder('e')
                ->addSelect('tp, t')
                ->leftJoin('e.tonePicture', 'tp')
                ->leftJoin('e.thread', 't')
                ->where('e.groupBy = :groupBy')
                ->andWhere('e != :element')
                ->setParameter('groupBy', $element->getGroupBy())
                ->setParameter('element', $element)
                ->addOrderBy('e.abcGroup', 'DESC')
                ->addOrderBy('e.toneName', 'ASC')
                ->getQuery()
                ->getResult();

            array_unshift($elements, $element);
        }

        return $elements;
    }

    /**
     * Возвращает рандомные товары для показа в предкассовой зоне ("Часто добавляют к заказу").
     * Если таких товаров меньше минимально настроенного количества - возвращает пустой массив.
     *
     * @return array
     */
    public function getPreOrderZoneCarousels(): array
    {
        $carousels = [];

        $result = $this->createQueryBuilder('e')
            ->addSelect('se', 's', 'ep', 'epf', 'etp')
            ->join('e.sectionElements', 'se')
            ->join('se.section', 's')
            ->join('e.pictures', 'ep')
            ->leftJoin('ep.file', 'epf')
            ->leftJoin('e.tonePicture', 'etp')
            ->where('e.isPreOrderZone = true')
            ->andWhere('e.isActive = true')
            ->andWhere('ep.position = 1')
            ->orderBy('RANDOM()')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult();

        if (\count($result) < Element::PRE_ORDER_ZONE_COLS_AMOUNT) {
            return $carousels;
        }

        $elementSectionIds = [];
        $elementIds = [];
        $i = 0;

        /** @var Element $element */
        foreach ($result as $key => $element) {
            $elementIds[] = $element->getId();

            if (0 === $key % Element::PRE_ORDER_ZONE_COLS_AMOUNT) {
                ++$i;
            }

            $carousels[$i][] = $element;

            /** @var \irmag\SiteBundle\Entity\SectionElement $sectionElement */
            foreach ($element->getSectionElements() as $sectionElement) {
                $elementSectionIds[$i][$element->getId()][] = $sectionElement->getSection()->getId();
            }
        }

        $usedSections = [];
        $elementsToReplace = [];

        foreach ($elementSectionIds as $carouselId => $sectionArray) {
            $usedSections[$carouselId] = [];

            foreach ($sectionArray as $elmId => $sections) {
                foreach ($sections as $sectionId) {
                    if (\in_array($sectionId, $usedSections[$carouselId], true)) {
                        $elementsToReplace[$carouselId] = $elmId;
                    }

                    $usedSections[$carouselId][] = $sectionId;
                }
            }
        }

        // если есть товары из одного раздела в пределах слайда карусели, то пытаемся получить другой
        if (!empty($elementsToReplace)) {
            foreach ($elementsToReplace as $key => $elm) {
                $newElement = $this->createQueryBuilder('e')
                    ->addSelect('se', 's', 'ep', 'epf', 'etp')
                    ->join('e.sectionElements', 'se')
                    ->join('se.section', 's')
                    ->join('e.pictures', 'ep')
                    ->join('ep.file', 'epf')
                    ->leftJoin('e.tonePicture', 'etp')
                    ->where('e.isPreOrderZone = true')
                    ->andWhere('e.isActive = true')
                    ->andWhere('ep.position = 1')
                    ->andWhere('e.id NOT IN (:currentElements)')
                    ->andWhere('s.id NOT IN (:currentSections)')
                    ->setParameter('currentElements', $elementIds)
                    ->setParameter('currentSections', $usedSections[$key])
                    ->orderBy('RANDOM()')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();

                if (!empty($newElement)) {
                    $elementIndexAtCarouselSlide = array_search($elm, array_keys($elementSectionIds[$key]), true);

                    if (!empty($elementIndexAtCarouselSlide)) {
                        $carousels[$key][$elementIndexAtCarouselSlide] = $newElement;
                    }
                }
            }
        }

        return $carousels;
    }

    /**
     * Возвращает рекомендованные товары ("С этим товаром смотрят") в карточке товара.
     *
     * @param Element $element
     * @param Section $section
     * @param int     $totalResult
     *
     * @return array
     */
    public function getRecommendElements(Element $element, Section $section, int $totalResult = 6)
    {
        // выбираем товары из токого же раздела с таким же брендом
        // и исключаем товары с таким же groupBy
        $qb = $this->getQbForRecommendElementsOrSeries($section)
            ->andWhere('e.id != :currentId')
            ->setParameter('currentId', $element->getId());

        if ($element->getBrand()) {
            $qb
                ->andWhere('b.id = :brandId')
                ->setParameter('brandId', $element->getBrand()->getId());
        }

        if ($element->getGroupBy()) {
            $qb
                ->andWhere('e.groupBy != :groupBy OR e.groupBy IS NULL')
                ->setParameter('groupBy', $element->getGroupBy());
        }

        $recommendElements = $qb
            ->setMaxResults($totalResult)
            ->getQuery()
            ->getResult();

        // если товаров по бренду мало, дополняем любыми другими из такого же раздела
        if (\count($recommendElements) < $totalResult) {
            $excludeElementIds = [];

            if ($recommendElements) {
                $excludeElementIds = array_map(function ($element) {
                    /* @var Element $element */
                    return $element->getId();
                }, $recommendElements);
            }

            $excludeElementIds[] = $element->getId();

            $recommendElementsAny = $this->getQbForRecommendElementsOrSeries($section)
                ->andWhere('e.id NOT IN (:excludeElementIds)')
                ->setParameter('excludeElementIds', $excludeElementIds)
                ->setMaxResults($totalResult - \count($recommendElements))
                ->getQuery()
                ->getResult();

            if ($recommendElementsAny) {
                $recommendElements = array_merge($recommendElements, $recommendElementsAny);
            }
        }

        return $recommendElements;
    }

    public function getSeriesElements(Element $element, int $totalResult = 6)
    {
        $series = $element->getSeries();

        if (null === $series) {
            return [];
        }

        return $this->getQbForRecommendElementsOrSeries()
            ->andWhere('e.series = :series')
            ->andWhere('e != :e')
            ->setParameter('series', $series)
            ->setParameter('e', $element)
            ->setMaxResults($totalResult)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Section $section
     *
     * @return QueryBuilder
     */
    private function getQbForRecommendElementsOrSeries(Section $section = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('e')
            ->addSelect('ep, epf, etp, b')
            ->leftJoin('e.brand', 'b')
            ->leftJoin('e.sectionElements', 'se')
            ->leftJoin('se.section', 's')
            ->leftJoin('e.pictures', 'ep')
            ->leftJoin('ep.file', 'epf')
            ->leftJoin('e.tonePicture', 'etp')
            ->where('e.isActive = true')
            ->andWhere('ep.position = 1 OR ep.position IS NULL')
            ->orderBy('RANDOM()');

        if (null !== $section) {
            $qb
                 ->andWhere('s.id = :sectionId')
                 ->setParameter('sectionId', $section->getId());
        }

        return $qb;
    }
}
