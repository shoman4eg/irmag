<?php

namespace Irmag\OrderDeliveryServiceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Irmag\OrderDeliveryServiceBundle\Form\Helper\DeliveryCalculatorHelper;

class DeliveryCalculatorType extends AbstractType
{
    /**
     * @var DeliveryCalculatorHelper
     */
    private $deliveryCalculatorHelper;

    /**
     * @param DeliveryCalculatorHelper $deliveryCalculatorHelper
     */
    public function __construct(
        DeliveryCalculatorHelper $deliveryCalculatorHelper
    ) {
        $this->deliveryCalculatorHelper = $deliveryCalculatorHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $selectedCity = $this->deliveryCalculatorHelper->getSelectedCity();
        $builder
            ->add('deliveryService', ChoiceType::class, [
                'label' => 'Транспортная компания:',
                'choices' => $this->deliveryCalculatorHelper->getAvailableDeliveryServices(),
                'choice_value' => function ($item) {
                    return empty($item) ? null : $item->getId();
                },
                'attr' => [
                    'class' => 'delivery-service-service',
                ],
                'choice_label' => function ($item) {
                    return $item->getName();
                },
            ])
            ->add('deliveryPoint', TextType::class, [
                'label' => 'Куда доставить?: ',
                'attr' => [
                    'class' => 'delivery-service-point',
                ],
            ])
            ->add('calculate', ButtonType::class, [
                'attr' => [
                    'class' => 'btn btn-warning btn-delivery-calculate',
                ],
                'label' => 'Рассчитать',
            ])
            ->add('selectedCity', HiddenType::class, [
                'required' => false,
                'data' => !empty($selectedCity) ? json_encode($selectedCity) : '',
            ])
            ->add('variantsToDoor', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'placeholder' => false,
                'multiple' => false,
                'choices' => $this->deliveryCalculatorHelper->getResultsToDoor(),
                'choice_label' => function ($item) {
                    return $item->getName();
                },
            ])
            ->add('variantsToTerminal', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'placeholder' => false,
                'multiple' => false,
                'choices' => $this->deliveryCalculatorHelper->getResultsToTerminal(),
                'choice_label' => function ($item) {
                    return $item->getName();
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'calculator';
    }
}
