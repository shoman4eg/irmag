<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\SiteBundle\Entity\Section;

class SectionParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = ['name'];

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root sections')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->parseSection($this->getCrawler()->filter('root > sections'));
    }

    /**
     * @param Crawler $crawler
     * @param Section $parent
     */
    protected function parseSection(Crawler $crawler, Section $parent = null)
    {
        $crawler->filterXPath('sections/section')->each(function (Crawler $node) use ($parent) {
            $id = $node->filter('id')->text();

            $section = $this->em->getRepository(Section::class)->find($id);

            if (!$section) {
                $section = new Section();
                $section->setId($id);
            }

            if (null !== $parent) {
                $section->setParent($parent);
            }

            $this->fillFields($section, $node);
            $this->em->persist($section);
            ++$this->operations;
            $this->flushIfNeed();

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }

            if (0 !== $node->filterXPath('section/sections')->count()) {
                $this->parseSection($node->filterXPath('section/sections'), $section);
            }
        });
    }
}
