<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="promocodes")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\PromocodeRepository")
 *
 * @UniqueEntity(fields="code", message="Промокод с таким кодом уже существует")
 *
 * @Serializer\ExclusionPolicy("ALL")
 */
class Promocode
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $id;

    /**
     * Код.
     *
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=32, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $code;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $description;

    /**
     * Скидка в процентах.
     *
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $discount;

    /**
     * Дата активации.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $activatedAt;

    /**
     * Персональный промокод (воспользоваться может только один человек).
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $isPersonal;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $isActive;

    /**
     * @var PromocodeElementsList
     *
     * @ORM\ManyToOne(targetEntity="PromocodeElementsList", inversedBy="promocodes")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $elementList;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode"})
     */
    private $bonus;

    public function __construct()
    {
        $this->isPersonal = false;
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->code;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getActivatedAt(): ?\DateTimeInterface
    {
        return $this->activatedAt;
    }

    public function setActivatedAt(?\DateTimeInterface $activatedAt): self
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    public function getIsPersonal(): ?bool
    {
        return $this->isPersonal;
    }

    public function setIsPersonal(bool $isPersonal): self
    {
        $this->isPersonal = $isPersonal;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getElementList(): ?PromocodeElementsList
    {
        return $this->elementList;
    }

    public function setElementList(?PromocodeElementsList $elementList): self
    {
        $this->elementList = $elementList;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(?int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }
}
