<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181013091458 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_READER', '[ADMIN] Drive - Токены аутентификации - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_VIEW', '[ADMIN] Drive - Токены аутентификации - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_LIST', '[ADMIN] Drive - Токены аутентификации - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDITOR', '[ADMIN] Drive - Токены аутентификации - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDIT', '[ADMIN] Drive - Токены аутентификации - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_CREATE', '[ADMIN] Drive - Токены аутентификации - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_ADMIN', '[ADMIN] Drive - Токены аутентификации - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_DELETE', '[ADMIN] Drive - Токены аутентификации - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EXPORT', '[ADMIN] Drive - Токены аутентификации - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_DRIVE_USER_DRIVE_TOKEN_ADMIN')
            );
        ");

        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
