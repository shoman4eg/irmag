<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\ProfileBundle\Entity\User;

/**
 * @Rest\RouteResource("Users")
 */
class UserController extends AbstractRestController
{
    protected $entityClassName = User::class;

    /**
     * Список пользователей.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     * @Model(type=User::class, groups={"api_user"}))
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_user"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return User[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy([], null, $limit, $offset);
    }

    /**
     * Получить пользователя по id или username.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=User::class, groups={"api_user"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_user"})
     *
     * @param $idOrUsername
     *
     * @return User
     */
    public function getAction($idOrUsername)
    {
        if (is_numeric($idOrUsername)) {
            return $this->getEntityByIdOrThrowNotFoundHttpException($idOrUsername);
        }

        $entity = $this->getEntityManager()->getRepository($this->entityClassName)->findOneBy(['username' => $idOrUsername]);

        if (!$entity) {
            throw new NotFoundHttpException(
                sprintf('Entity "%s" with username "%s" not found!', $this->entityClassName, $idOrUsername)
            );
        }

        return $entity;
    }
}
