<?php

namespace Irmag\ThreadCommentBundle\Repository;

use Irmag\ProfileBundle\Entity\User;
use Irmag\ThreadCommentBundle\Entity\Thread;

class ThreadCommentVoteRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Возвращает массив с очками Like/Dislike пользователя у комментариев.
     *
     * @param Thread $thread
     * @param User   $user
     *
     * @return array
     */
    public function getVotesByThreadAndUser(Thread $thread, User $user): array
    {
        $userVotes = $this->createQueryBuilder('tcv')
            ->select('tcv.score, tc.id as comment_id')
            ->where('tcv.user = :user')
            ->leftJoin('tcv.threadComment', 'tc')
            ->andWhere('tc.thread = :thread')
            ->setParameters([
                'thread' => $thread,
                'user' => $user,
            ])
            ->getQuery()
            ->getResult();

        $votes = [];

        foreach ($userVotes as $vote) {
            $votes[$vote['comment_id']] = $vote['score'];
        }

        return $votes;
    }
}
