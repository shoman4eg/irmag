<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181221093514 extends AbstractMigration
{
    private const MESSAGE_NEW = 'message_new';

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("
            INSERT INTO users_notification_events (id, name, description, is_push_disabled, is_email_disabled, is_site_disabled, is_sms_disabled, is_active)
            VALUES (nextval('users_notification_events_id_seq'), '%s', 'Личное сообщение', FALSE, FALSE, FALSE, TRUE, TRUE);
        ", self::MESSAGE_NEW));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("
            DELETE FROM users_notification_events WHERE name = '%s'
        ", self::MESSAGE_NEW));
    }
}
