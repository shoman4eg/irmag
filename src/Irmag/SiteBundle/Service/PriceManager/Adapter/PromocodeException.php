<?php

namespace Irmag\SiteBundle\Service\PriceManager\Adapter;

use Irmag\CoreBundle\Exception\IrmagException;

class PromocodeException extends IrmagException
{
}
