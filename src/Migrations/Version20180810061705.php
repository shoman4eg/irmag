<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180810061705 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_delivery_cities_districts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_delivery_cities_districts (id INT NOT NULL, delivery_city_id INT NOT NULL, name VARCHAR(255) NOT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_365E8E2E81B2DD71 ON orders_delivery_cities_districts (delivery_city_id)');
        $this->addSql('ALTER TABLE orders_delivery_cities_districts ADD CONSTRAINT FK_365E8E2E81B2DD71 FOREIGN KEY (delivery_city_id) REFERENCES orders_delivery_cities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_order_profiles ADD delivery_district_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users_order_profiles DROP delivery_district');
        $this->addSql('ALTER TABLE users_order_profiles ADD CONSTRAINT FK_1470FE348C920334 FOREIGN KEY (delivery_district_id) REFERENCES orders_delivery_cities_districts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1470FE348C920334 ON users_order_profiles (delivery_district_id)');
        $this->addSql('ALTER TABLE orders ADD delivery_district_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders DROP delivery_district');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE8C920334 FOREIGN KEY (delivery_district_id) REFERENCES orders_delivery_cities_districts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E52FFDEE8C920334 ON orders (delivery_district_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles DROP CONSTRAINT FK_1470FE348C920334');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEE8C920334');
        $this->addSql('DROP SEQUENCE orders_delivery_cities_districts_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_delivery_cities_districts');
        $this->addSql('DROP INDEX IDX_1470FE348C920334');
        $this->addSql('ALTER TABLE users_order_profiles ADD delivery_district VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE users_order_profiles DROP delivery_district_id');
        $this->addSql('DROP INDEX IDX_E52FFDEE8C920334');
        $this->addSql('ALTER TABLE orders ADD delivery_district VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE orders DROP delivery_district_id');
    }
}
