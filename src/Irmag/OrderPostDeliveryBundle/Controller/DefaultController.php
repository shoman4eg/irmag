<?php

namespace Irmag\OrderPostDeliveryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;
use Irmag\OrderPostDeliveryBundle\Form\Type\PostDeliveryCalculatorType;
use Irmag\OrderPostDeliveryBundle\Exception\IrmagOrderPostDeliveryBundleException;

/**
 * @Route("/post_delivery", host="%irmag_site_domain%", options={"expose": true})
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="irmag_get_post_delivery")
     *
     * @param int $weight Общий вес товаров в корзине (в граммах)
     * @param int $volume Объём
     *
     * @return Response
     */
    public function indexAction(int $weight, int $volume): Response
    {
        $dm = $this->get(OrderPostDeliveryManager::class);
        $data = $dm->getResult();

        if (true === $dm->isBasketContentChanged()) {
            $dm->addFlashMessage();
            $dm->resetResult();
        }

        return $this->render('@IrmagOrderPostDelivery/status.html.twig', [
            'data' => $data,
            'weight' => $weight,
            'volume' => $volume,
        ]);
    }

    /**
     * @Route("/calculator", name="irmag_get_post_delivery_form")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormAction(Request $request): Response
    {
        $weight = (float) $request->request->get('weight');
        $deliveryForm = $this->createForm(PostDeliveryCalculatorType::class, null, ['weight' => $weight]);

        $result = $this->get(OrderPostDeliveryManager::class)->getResult();

        return $this->render('@IrmagOrderPostDelivery/calculator.html.twig', [
            'form' => $deliveryForm->createView(),
            'result' => $result,
        ]);
    }

    /**
     * @Route("/calculate", name="irmag_get_post_delivery_cost")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function deliveryCostAction(Request $request): Response
    {
        $options = [
            'weight' => (float) $request->request->get('weight'),
            'indexTo' => $request->request->getInt('indexTo'),
        ];

        $dm = $this->get(OrderPostDeliveryManager::class);

        try {
            $dm->saveCalculatedDeliveryCost($options);
            $dm->saveBasketContent();
            $success = true;
        } catch (IrmagOrderPostDeliveryBundleException $ex) {
            $message = $ex->getMessage();
            $success = false;
        }

        return new JsonResponse([
            'success' => $success,
            'message' => $message ?? '',
        ]);
    }

    /**
     * @Route("/reset", name="irmag_reset_post_delivery_data")
     *
     * @return JsonResponse
     */
    public function resetAction(): JsonResponse
    {
        $this->get(OrderPostDeliveryManager::class)->resetResult();

        return new JsonResponse([
            'success' => true,
        ]);
    }

    /**
     * @Route("/select", name="irmag_select_post_delivery")
     *
     * @return JsonResponse
     */
    public function selectAction(): JsonResponse
    {
        $this->get(OrderPostDeliveryManager::class)->setIsSelected();

        return new JsonResponse([
            'success' => true,
        ]);
    }
}
