<?php

namespace Irmag\AdminBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Irmag\AdminBundle\Form\Type\DayOfWeekType;
use Irmag\SiteBundle\Entity\OrderDeliveryTime;

class OrderDeliveryCityDayOfWeekAdmin extends AbstractBaseAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('dayOfWeek', DayOfWeekType::class)
            ->add('minDaysBeforeDelivery')
            ->add('deliveryCity')
            ->add('deliveryTime', EntityType::class, [
                'class' => OrderDeliveryTime::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.isActive = true')
                        ->orderBy('o.name', 'ASC');
                },
                'expanded' => true,
                'multiple' => true,
                'required' => true,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('dayOfWeek')
            ->add('minDaysBeforeDelivery')
            ->add('deliveryCity')
            ->add('deliveryTime')
        ;
    }
}
