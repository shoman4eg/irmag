<?php

namespace Irmag\ProfileBundle\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationQueue;

trait NotificationDataTrait
{
    /**
     * @var string
     *
     * @Assert\Length(max=50)
     *
     * @ORM\Column(type="string", length=50)
     */
    private $title;

    /**
     * Len 150 for Push.
     *
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @var string
     *
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private $expiredAt;

    /**
     * @var UserNotificationEvent
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\UserNotificationEvent")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $event;

    /**
     * XXX: Custom.
     */
    public function calculateExpiredAt()
    {
        $this->expiredAt = (new \DateTime())->modify(UserNotificationQueue::DEFAULT_TTL);
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = mb_substr($title, 0, 50);

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getExpiredAt(): ?\DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(\DateTimeInterface $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getEvent(): ?UserNotificationEvent
    {
        return $this->event;
    }

    public function setEvent(?UserNotificationEvent $event): self
    {
        $this->event = $event;

        return $this;
    }
}
