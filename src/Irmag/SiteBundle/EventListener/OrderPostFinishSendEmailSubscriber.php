<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\SiteBundle\Mailer\SiteMailer;

class OrderPostFinishSendEmailSubscriber implements EventSubscriberInterface
{
    /**
     * @var SiteMailer
     */
    private $mailer;

    /**
     * @param SiteMailer $mailer
     */
    public function __construct(
        SiteMailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_POST_FINISH => 'onOrderPostFinish',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderPostFinish(OrderEvent $event): void
    {
        $order = $event->getOrder();

        if (!empty($email = $order->getPayerEmail())) {
            $this->mailer->sendOrderFinishMessage($email, [
                'order' => $order,
            ]);
        }
    }
}
