<?php

namespace Irmag\SiteBundle\Utils;

class BonusCalculator
{
    /**
     * Возвращает бонус в процентах от суммы заказа.
     *
     * @param float $price Цена
     *
     * @return int Бонус в %
     */
    public static function getBonusPercentByPrice(float $price): int
    {
        if ($price >= 10000) {
            return 10;
        } elseif ($price >= 9000) {
            return 9;
        } elseif ($price >= 8000) {
            return 8;
        } elseif ($price >= 7000) {
            return 7;
        } elseif ($price >= 6000) {
            return 6;
        } elseif ($price >= 5000) {
            return 5;
        } elseif ($price >= 4000) {
            return 4;
        } elseif ($price >= 3000) {
            return 3;
        } elseif ($price >= 2000) {
            return 2;
        }

        return 0;
    }

    /**
     * Возвращает бонус от суммы заказа.
     *
     * @param float $price Цена
     *
     * @return int Бонус
     */
    public static function getBonusByPrice(float $price): int
    {
        $bonusPercent = self::getBonusPercentByPrice($price);

        return (int) floor($price / 100 * $bonusPercent);
    }
}
