<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseElementsCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.element';
    const XML_NAME = 'elements';
}
