<?php

namespace Irmag\SiteBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\SiteBundle\Entity\Action;

class NotificationActionSubscriber implements EventSubscriber
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param EntityManagerInterface       $em
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->em = $em;
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postUpdate',
        ];
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Action) {
            $uow = $this->em->getUnitOfWork();
            $changes = $uow->getEntityChangeSet($entity);

            if (isset($changes['isActive'])) {
                $newIsActive = $changes['isActive'][1];

                if (true === $newIsActive) {
                    $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::ACTION_NEW);
                    $url = $this->router->generate('irmag_action_detail', ['slug' => $entity->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
                    $this->userNotificationQueueManager->create($event, null, $entity->getTitle(), $url, true, true);
                }
            }
        }
    }
}
