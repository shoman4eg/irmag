<?php

namespace Irmag\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="polls")
 * @ORM\Entity
 */
class Poll
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Создатель опроса.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     */
    private $user;

    /**
     * Название опроса.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * Дата завершения опроса.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $estimatedAt;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * Множественные варианты ответа.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isMultiple;

    /**
     * Анонимный опрос.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isAnonymous;

    /**
     * ID класса-собственника.
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $ownerId;

    /**
     * Класс-собственник опроса.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $ownerFQCN;

    /**
     * Массив с информацией для генерации url.
     *
     * [
     *     'name' => '...'
     *     'parameters' => [
     *         'id' => '...',
     *         'slug => '...,
     *     ]
     * ]
     *
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $ownerRoute;

    /**
     * Всего голосов.
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $totalVotes;

    /**
     * Всего проголосовавших.
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $totalVoters;

    /**
     * Варианты ответов.
     *
     * @var Collection|PollVariant[]
     *
     * @ORM\OneToMany(targetEntity="PollVariant", mappedBy="poll", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"id": "ASC"})
     */
    private $variants;

    /**
     * Массив с информацией о результатах голосования.
     *
     * [
     *     'variant_key_1' => 'total_votes',
     *     'variant_key_2' => 'total_votes',
     *     ...
     * ]
     *
     * @var array
     *
     * @ORM\Column(type="array", nullable=true)
     */
    private $stats;

    public function __construct()
    {
        $this->isActive = true;
        $this->isAnonymous = false;
        $this->isMultiple = false;
        $this->totalVotes = 0;
        $this->totalVoters = 0;
        $this->stats = [];
        $this->variants = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->title ?? $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setEstimatedAt(?\DateTimeInterface $estimatedAt): self
    {
        $this->estimatedAt = $estimatedAt;

        return $this;
    }

    public function getEstimatedAt(): ?\DateTimeInterface
    {
        return $this->estimatedAt;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsMultiple(bool $isMultiple): self
    {
        $this->isMultiple = $isMultiple;

        return $this;
    }

    public function getIsMultiple(): ?bool
    {
        return $this->isMultiple;
    }

    public function setIsAnonymous(bool $isAnonymous): self
    {
        $this->isAnonymous = $isAnonymous;

        return $this;
    }

    public function getIsAnonymous(): ?bool
    {
        return $this->isAnonymous;
    }

    public function setOwnerId(int $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function setOwnerFQCN(string $ownerFQCN): self
    {
        $this->ownerFQCN = $ownerFQCN;

        return $this;
    }

    public function getOwnerFQCN(): ?string
    {
        return $this->ownerFQCN;
    }

    public function setOwnerRoute(array $ownerRoute): self
    {
        $this->ownerRoute = $ownerRoute;

        return $this;
    }

    public function getOwnerRoute(): ?array
    {
        return $this->ownerRoute;
    }

    public function setTotalVoters(int $totalVoters): self
    {
        $this->totalVoters = $totalVoters;

        return $this;
    }

    public function getTotalVoters(): ?int
    {
        return $this->totalVoters;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function addVariant(PollVariant $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setPoll($this);
        }

        return $this;
    }

    public function removeVariant(PollVariant $variant): self
    {
        if ($this->variants->contains($variant)) {
            $this->variants->removeElement($variant);
            // set the owning side to null (unless already changed)
            if ($variant->getPoll() === $this) {
                $variant->setPoll(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PollVariant[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setStats(?array $stats): self
    {
        $this->stats = $stats;

        return $this;
    }

    public function getStats(): ?array
    {
        return $this->stats;
    }

    public function setTotalVotes(int $totalVotes): self
    {
        $this->totalVotes = $totalVotes;

        return $this;
    }

    public function getTotalVotes(): ?int
    {
        return $this->totalVotes;
    }
}
