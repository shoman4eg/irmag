<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181221084335 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT fk_d155ca4654da761');
        $this->addSql('DROP INDEX uniq_d155ca4654da761');
        $this->addSql('ALTER TABLE orders_changes DROP mixed_payment_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes ADD mixed_payment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT fk_d155ca4654da761 FOREIGN KEY (mixed_payment_id) REFERENCES orders_changes_mixed_payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_d155ca4654da761 ON orders_changes (mixed_payment_id)');
    }
}
