<?php

namespace Irmag\SiteBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Irmag\SiteBundle\Entity\OrderDeliveryCityDistrict;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;

class DeliveryAddressType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var OrderDeliveryCity $deliveryCity */
        $deliveryCity = $options['delivery_city'];

        $builder
            ->add('deliveryCity', EntityType::class, [
                'class' => OrderDeliveryCity::class,
                'query_builder' => function (EntityRepository $repository) use ($options) {
                    $qb = $repository->createQueryBuilder('odc')
                        ->where('odc.isActive = true')
                        ->orderBy('odc.position', 'ASC');

                    // disable other city
                    if (true === $options['has_only_for_home']) {
                        $qb
                            ->andWhere('odc.shortname != :other')
                            ->setParameter('other', 'other');
                    }

                    return $qb;
                },
                'choice_attr' => function ($choiceValue, $key, $value) {
                    /* @var OrderDeliveryCity $choiceValue */
                    return ['data-shortname' => $choiceValue->getShortname()];
                },
                'data' => $deliveryCity,
                'choice_label' => 'name',
                'label' => 'form.label.delivery_city',
                'required' => true,
            ])
        ;

        if (null !== $deliveryCity && !$deliveryCity->getDistricts()->isEmpty()) {
            $builder->add('deliveryDistrict', EntityType::class, [
                'class' => OrderDeliveryCityDistrict::class,
                'query_builder' => function (EntityRepository $repository) use ($deliveryCity) {
                    $qb = $repository->createQueryBuilder('cd')
                        ->where('cd.isActive = true')
                        ->andWhere('cd.deliveryCity = :deliveryCity')
                        ->orderBy('cd.name', 'ASC')
                        ->setParameter('deliveryCity', $deliveryCity);

                    return $qb;
                },
                'label' => 'form.label.delivery_district',
                'placeholder' => 'Выберите район',
                'required' => true,
            ]);
        }

        $builder
            ->add('deliveryStreet', null, [
                'label' => 'form.label.delivery_street',
            ])
            ->add('deliveryHouse', null, [
                'label' => 'form.label.delivery_house',
            ])
            ->add('deliveryApartment', null, [
                'label' => 'form.label.delivery_apartment',
                'required' => false,
            ])
            ->add('deliveryComment', TextareaType::class, [
                'attr' => ['placeholder' => 'form.placeholder.comment'],
                'label' => 'form.label.delivery_comment',
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'inherit_data' => true,
            'label' => false,
            'has_only_for_home' => false,
            'delivery_city' => null,
        ]);

        $resolver
            ->setAllowedTypes('has_only_for_home', 'bool')
            ->setAllowedTypes('delivery_city', ['null', '\Irmag\SiteBundle\Entity\OrderDeliveryCity']);
    }
}
