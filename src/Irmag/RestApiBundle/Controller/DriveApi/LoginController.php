<?php

namespace Irmag\RestApiBundle\Controller\DriveApi;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\CoreBundle\Utils\TokenGenerator;
use Irmag\ProfileBundle\Entity\UserDriveToken;
use Irmag\ProfileBundle\Entity\User;
use Irmag\RestApiBundle\EventListener\DriveApiRequestListener;
use Irmag\RestApiBundle\Controller\AbstractRestController;

class LoginController extends AbstractRestController
{
    /**
     * Аутентификация по JSON для водителей.
     *
     * @Rest\Route("/login")
     *
     * @SWG\Tag(name="Аутентификация")
     *
     * @SWG\Parameter(
     *     name="json_login_drivers",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\JsonLoginDriversType")
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=User::class, groups={"api_waybill"})
     * )
     *
     * @SWG\Response(
     *     response="403",
     *     description="Returned when invalid credential"
     * )
     *
     * @param Request $request
     *
     * @return array|View
     */
    public function postAction(Request $request)
    {
        $content = $request->getContent();

        if (!empty($content)) {
            $data = json_decode($content, true);

            if (!empty($data['username']) && !empty($data['password'])) {
                $em = $this->getEntityManager();
                $user = $em->getRepository(User::class)->findOneBy(['username' => $data['username']]);

                if (!$user instanceof User) {
                    return $this->getInvalidCredentialsView();
                }

                $isPasswordValid = $this->get('security.password_encoder')->isPasswordValid($user, $data['password']);

                if (false === $isPasswordValid) {
                    return $this->getInvalidCredentialsView();
                }

                if (!DriveApiRequestListener::isDriverUser($user)) {
                    return $this->view(['error' => sprintf('Forbidden. This user has no "%s" role.', User::ROLE_IRMAG_DRIVER)], 403);
                }

                // generate token and update user last login at.
                $driveToken = new UserDriveToken();
                $driveToken->setUser($user);
                $driveToken->setToken(mb_substr(TokenGenerator::generateToken(), 0, 32));
                $em->persist($driveToken);

                // delete old tokens
                $this->getEntityManager()->createQueryBuilder()
                    ->delete()
                    ->from(UserDriveToken::class, 'udt')
                    ->where('udt.user = :user')
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->execute();

                $user->setLastLoginAt(new \DateTime());
                $em->persist($user);

                $em->flush();

                $serializer = SerializerBuilder::create()->build();
                $userData = $serializer->toArray($user, SerializationContext::create()->setGroups(['api_waybill']));

                return ['user' => $userData, 'token' => $driveToken->getToken()];
            }
        }

        return $this->view(['error' => 'Forbidden.'], 403);
    }

    private function getInvalidCredentialsView(): View
    {
        return $this->view(['error' => 'Invalid credentials.'], 403);
    }
}
