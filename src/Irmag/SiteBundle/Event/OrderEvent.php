<?php

namespace Irmag\SiteBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\SiteBundle\Entity\Order;

class OrderEvent extends Event
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}
