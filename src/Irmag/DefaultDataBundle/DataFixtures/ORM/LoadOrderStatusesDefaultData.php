<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\OrderStatus;

class LoadOrderStatusesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $statuses = [
            ['A', 'Доставлен', 'Заказ привезен покупателю'],
            ['C', 'Отменен', 'Заказ был отменен по каким-либо причинам (справки по тел. 560-565)'],
            ['D', 'В доставке', 'Заказ передан в доставку'],
            ['F', 'Выполнен', 'Заказ доставлен и оплачен'],
            ['L', 'В доставке с изменениями', 'Заказ передан в доставку'],
            ['M', 'Объединён', 'Заказ был объединён'],
            ['N', 'Принят', 'Заказ принят, но пока не обрабатывается (например, заказ только что создан или ожидается оплата заказа)'],
            ['O', 'Ожидает', 'Принят магазином и ожидает обработки'],
            ['P', 'В обработке', 'Заказ передан на склад.'],
            ['W', 'Набран', 'Заказ укомплектован на складе'],
            ['Z', 'Набран с изменениями', 'При сборке есть изменения в заказе'],
            ['S', 'Ожидает онлайн оплату', 'Заказ принят, но не оплачен'],
        ];

        foreach ($statuses as $status) {
            $existingOrderStatus = $manager->getRepository(OrderStatus::class)->findOneBy(['shortname' => $status[0]]);

            if (!$existingOrderStatus) {
                $orderStatus = new OrderStatus();
                $orderStatus->setShortname($status[0]);
                $orderStatus->setName($status[1]);

                if (!empty($status[2])) {
                    $orderStatus->setDescription($status[2]);
                }

                $manager->persist($orderStatus);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 5;
    }
}
