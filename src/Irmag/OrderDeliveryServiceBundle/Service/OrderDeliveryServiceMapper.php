<?php

namespace Irmag\OrderDeliveryServiceBundle\Service;

use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\OrderDeliveryServiceBundle\Service\DPDService\DPDService;

class OrderDeliveryServiceMapper
{
    /**
     * @see \Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService --> shortname as key
     */
    const DELIVERY_SERVICES_MAP = [
        'dpd' => DPDService::class,
    ];

    /**
     * Возвращает название сервиса службы доставки по его имени.
     *
     * @param string $name Название службы доставки
     *
     * @throws IrmagException
     *
     * @return string
     */
    public static function getService(string $name): string
    {
        $map = self::DELIVERY_SERVICES_MAP;

        if (!isset($map[$name])) {
            throw new IrmagException(
                sprintf(
                    'Specified service "%s" have not been configured at "%s".',
                    $name,
                    self::class
                )
            );
        }

        return $map[$name];
    }
}
