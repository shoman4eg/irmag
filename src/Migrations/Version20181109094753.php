<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181109094753 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_queues RENAME COLUMN ttl TO expired_at');
        $this->addSql('ALTER TABLE users_notifications RENAME COLUMN ttl TO expired_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notifications RENAME COLUMN expired_at TO ttl');
        $this->addSql('ALTER TABLE users_notification_queues RENAME COLUMN expired_at TO ttl');
    }
}
