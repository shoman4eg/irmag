<?php

namespace Irmag\SiteBundle\Service\OrderManager;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\BasketBundle\Service\DataCollector;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderElement;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;

class BasketToOrderDataTransformer
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var DataCollector
     */
    private $dataCollector;

    /**
     * @var PriceManager
     */
    private $priceManager;

    /**
     * @param EntityManagerInterface $em
     * @param DataCollector          $dataCollector
     * @param PriceManager           $priceManager
     */
    public function __construct(
        EntityManagerInterface $em,
        DataCollector $dataCollector,
        PriceManager $priceManager
    ) {
        $this->em = $em;
        $this->dataCollector = $dataCollector;
        $this->priceManager = $priceManager;
    }

    /**
     * @param Order $order
     */
    public function transform(Order $order): void
    {
        $basketData = $this->dataCollector->getData();

        // товары
        foreach ($basketData->getBasketElements() as $basketElement) {
            $element = $basketElement->getElement();

            $orderElement = new OrderElement();
            $orderElement
                ->setElement($element)
                ->setName($element->getName())
                ->setPrice($this->priceManager->getPrice($element))
                ->setPriceWithoutDiscount($element->getPrice())
                ->setAmount($basketElement->getAmount())
                ->setOrder($order);

            $this->em->persist($orderElement);
        }

        // оплачено фантиками
        if ($spendBonus = $basketData->getBonusSpend()) {
            $order->setSpendBonus($spendBonus);
        }

        // промокод
        if ($promocode = $basketData->getPromocode()) {
            $order
                ->setPromocodeDiscount($promocode->getDiscount())
                ->setPromocode($promocode->getCode());
        }

        // цены
        $order
            ->setTotalPrice($basketData->getTotalPrice())
            ->setTotalPriceWithoutDiscount($basketData->getTotalPriceWithoutDiscount());
    }
}
