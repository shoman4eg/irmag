<?php

namespace Irmag\AdminBundle\Block;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;

class RecentCommentsBlockService extends AbstractBlockService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Pool
     */
    protected $adminPool;

    /**
     * @param string                 $name
     * @param EngineInterface        $templating
     * @param EntityManagerInterface $em
     * @param Pool                   $adminPool
     */
    public function __construct(
        $name,
        EngineInterface $templating,
        EntityManagerInterface $em,
        Pool $adminPool = null
    ) {
        $this->em = $em;
        $this->adminPool = $adminPool;

        parent::__construct($name, $templating);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $parameters = [
            'context' => $blockContext,
            'settings' => $blockContext->getSettings(),
            'block' => $blockContext->getBlock(),
            'comments' => $this->em->getRepository(ThreadComment::class)->findBy([], ['createdAt' => 'DESC'], $blockContext->getSetting('number')),
            'admin_pool' => $this->adminPool,
        ];

        return $this->renderPrivateResponse($blockContext->getTemplate(), $parameters, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'number' => 5,
            'title' => '',
            'template' => '@IrmagAdmin/Block/recent_comments.html.twig',
        ]);
    }
}
