<?php

namespace Irmag\PollBundle;

final class IrmagPollEvents
{
    /**
     * Это событие вызывается, когда пользователь голосует.
     */
    const POLL_USER_VOTED = 'irmag.poll.vote';
}
