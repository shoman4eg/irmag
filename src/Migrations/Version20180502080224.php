<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20180502080224 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE sections_elements_backup AS SELECT * FROM sections_elements');
        $this->addSql('DROP TABLE sections_elements');
        $this->addSql('CREATE TABLE sections_elements (element_id INT NOT NULL, section_id INT NOT NULL, PRIMARY KEY(element_id, section_id))');
        $this->addSql('CREATE INDEX IDX_5DA6D1E61F1F2A24 ON sections_elements (element_id)');
        $this->addSql('CREATE INDEX IDX_5DA6D1E6D823E37A ON sections_elements (section_id)');
        $this->addSql('ALTER TABLE sections_elements ADD CONSTRAINT FK_5DA6D1E61F1F2A24 FOREIGN KEY (element_id) REFERENCES elements (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sections_elements ADD CONSTRAINT FK_5DA6D1E6D823E37A FOREIGN KEY (section_id) REFERENCES sections (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('INSERT INTO sections_elements (section_id, element_id) (SELECT section_id, element_id FROM sections_elements_backup)');
        $this->addSql('DROP TABLE sections_elements_backup');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
