<?php

namespace Irmag\ThreadCommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="threads_comments_votes")
 * @ORM\Entity(repositoryClass="Irmag\ThreadCommentBundle\Repository\ThreadCommentVoteRepository")
 */
class ThreadCommentVote
{
    /**
     * @var User
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var ThreadComment
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="ThreadComment")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $threadComment;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $score;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getThreadComment(): ?ThreadComment
    {
        return $this->threadComment;
    }

    public function setThreadComment(?ThreadComment $threadComment): self
    {
        $this->threadComment = $threadComment;

        return $this;
    }
}
