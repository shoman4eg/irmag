<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\File as IrmagFile;

class ManufacturerAndBrandParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = ['name'];

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root manufacturer, root brand')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->getCrawler()->filter('root > manufacturers > manufacturer')->each(function (Crawler $manufacturer) {
            $id = (int) $manufacturer->filter('manufacturer > id')->text();
            $elementManufacturer = $this->em->getRepository(ElementManufacturer::class)->find($id);

            // картинка
            if (0 !== $manufacturer->filter('manufacturer > image')->count()) {
                $base64Picture = $manufacturer->filter('manufacturer > image')->text();
            }

            if (!$elementManufacturer) {
                // create
                $elementManufacturer = new ElementManufacturer();
                $elementManufacturer->setId($id);
            }

            $this->fillFields($elementManufacturer, $manufacturer);

            if (isset($base64Picture)) {
                // если в xml пустые данные, то удаляем картинку.
                if (empty($base64Picture)) {
                    $elementManufacturer->setPicture(null);
                } else {
                    // загружаем новую картинку
                    $uploadedFile = $this->getUploadedFileFromBase64($base64Picture);

                    if (null !== $uploadedFile) {
                        if ($elementManufacturer->getPicture()) {
                            $elementManufacturer->getPicture()->setFile($uploadedFile);
                        } else {
                            $picture = new IrmagFile();
                            $picture->setFile($uploadedFile);
                            $elementManufacturer->setPicture($picture);
                            $this->em->persist($picture);
                        }
                    }
                }
            }

            $this->em->persist($elementManufacturer);
            ++$this->operations;
            $this->flushIfNeed();

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }

            $manufacturer->filter('brands > brand')->each(function (Crawler $brand) use ($elementManufacturer) {
                $id = $brand->filter('id')->text();
                $elementBrand = $this->em->getRepository(ElementBrand::class)->find($id);

                // картинка
                if (0 !== $brand->filter('image')->count()) {
                    $base64Picture = $brand->filter('image')->text();
                }

                if (!$elementBrand) {
                    // create
                    $elementBrand = new ElementBrand();
                    $elementBrand->setId($id);
                }

                $this->fillFields($elementBrand, $brand);
                $elementBrand->setManufacturer($elementManufacturer);

                if (isset($base64Picture)) {
                    if (empty($base64Picture)) {
                        // если в xml пустые данные, то удаляем картинку.
                        $elementBrand->setPicture(null);
                    } else {
                        // загружаем новую картинку
                        $uploadedFile = $this->getUploadedFileFromBase64($base64Picture);

                        if (null !== $uploadedFile) {
                            if ($elementBrand->getPicture()) {
                                $elementBrand->getPicture()->setFile($uploadedFile);
                            } else {
                                $picture = new IrmagFile();
                                $picture->setFile($uploadedFile);
                                $elementBrand->setPicture($picture);
                                $this->em->persist($picture);
                            }
                        }
                    }
                }

                $this->em->persist($elementBrand);
                ++$this->operations;
                $this->flushIfNeed();

                if (null !== $this->progressBar) {
                    $this->progressBar->advance();
                }
            });
        });
    }
}
