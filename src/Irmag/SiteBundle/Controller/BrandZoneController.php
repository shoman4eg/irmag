<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Entity\ElementManufacturer;

class BrandZoneController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @Route("/brand-zones/", name="irmag_brand_zones")
     */
    public function indexAction()
    {
        return $this->render('@IrmagSite/BrandZone/index.html.twig', [
            'manufacturers_and_brands' => $this->em->getRepository(ElementManufacturer::class)->getRandomManufacturerAndBrandsWithPicture(),
        ]);
    }
}
