<?php

namespace Irmag\CoreBundle\Utils;

class MaskEmail
{
    /**
     * Маскирует email.
     *
     * @example vasiliy.utkin@gmail.com -> v************@gmail.com
     *
     * @param string $email Email
     * @param string $mask  Маска (по-умолчанию *)
     *
     * @return string
     */
    public static function mask(string $email, string $mask = '*'): string
    {
        return preg_replace('/(?<=.).(?=.*@)/u', $mask, $email);
    }
}
