<?php

namespace Irmag\RestApiBundle\Form\DriveApi\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\RestApiBundle\Form\Api\Type\OrderMixedPaymentType;
use Irmag\DriveBundle\Entity\OrderChangesMixedPayment;

class OrderChangesMixedPaymentType extends OrderMixedPaymentType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderChangesMixedPayment::class,
        ]);
    }
}
