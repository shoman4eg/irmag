<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Irmag\ProfileBundle\NotificationEvents;

final class Version20181211040127 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("
            UPDATE users_notification_events
            SET description = 'Рассылка с уникальными предложениями',
                is_push_disabled = true,
                is_email_disabled = false,
                is_site_disabled = true,
                is_sms_disabled = false,
                is_active = true
            WHERE name = '%s'
        ", NotificationEvents::NEWSLETTER));

        $this->addSql(sprintf("UPDATE users_notification_events SET is_sms_disabled = false WHERE name = '%s'", NotificationEvents::NEWS_NEW));
        $this->addSql(sprintf("UPDATE users_notification_events SET is_sms_disabled = false WHERE name = '%s'", NotificationEvents::FAVORITE_ELEMENT_IS_ACTIVE));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
