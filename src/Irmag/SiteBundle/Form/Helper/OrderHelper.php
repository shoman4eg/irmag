<?php

namespace Irmag\SiteBundle\Form\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Utils\DeviceDetector;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;
use Irmag\SiteBundle\Entity\OrderDeliveryMethod;
use Irmag\SiteBundle\Entity\OrderPaymentMethod;
use Irmag\SiteBundle\Entity\OrderDeliveryCityDayOfWeek;
use Irmag\SiteBundle\Entity\OrderDeliveryTime;
use Irmag\SiteBundle\Entity\OrderDeliveryCitySelfserviceMethod;
use Irmag\SiteBundle\Entity\OrderDeliverySelfserviceMethod;
use Irmag\SiteBundle\Repository\OrderDeliveryTimeRepository;
use Irmag\ProfileBundle\Service\UserTrait;

class OrderHelper
{
    use UserTrait;

    /**
     * @var int[]
     */
    private $availableDaysOfWeek;

    /**
     * @var OrderDeliveryTime[]
     */
    private $availableTimeIntervals;

    /**
     * @var string[]
     */
    private $disabledDates;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param EntityManagerInterface $em
     * @param RequestStack           $requestStack
     * @param SessionInterface       $session
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->availableDaysOfWeek = [];
        $this->availableTimeIntervals = [];
        $this->disabledDates = [];
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Назначает минимальное кол-во дней до доставки.
     * Не путать с `$deliveryCityDayOfWeek->getMinDaysBeforeDelivery`.
     *
     * @param int $days
     */
    public function setGlobalMinDaysBeforeDelivery(int $days)
    {
        ++$days; // баним текущую дату тоже
        $now = new \DateTime();

        for ($i = 0; $i < $days; ++$i) {
            $this->disabledDates[] = $now->format(Config::DATE_FORMAT);
            $now->modify('+1 day');
        }
    }

    /**
     * @param Order $order
     */
    public function process(Order $order)
    {
        $this->setOrder($order);
        $this->fillOrderFieldsFromData();
        $deliveryCity = $this->order->getDeliveryCity();
        $now = new \DateTime();

        // delivery service data
        if ($this->session->has(OrderDeliveryServiceManager::DELIVERY_DATA_SESSION_LITERAL)) {
            $deliveryServiceData = $this->session->get(OrderDeliveryServiceManager::DELIVERY_DATA_SESSION_LITERAL);

            if (isset($deliveryServiceData['days'])) {
                $this->setGlobalMinDaysBeforeDelivery((int) $deliveryServiceData['days']);
            }
        }

        /** @var \Irmag\SiteBundle\Entity\OrderDeliveryCityBanDay $banDate */
        foreach ($deliveryCity->getBanDates() as $banDate) {
            $this->disabledDates[] = $banDate->getBanDate()->format(Config::DATE_FORMAT);
        }

        if ($pickedDate = $this->order->getDeliveryDate()) {
            // set time
            $pickedDayOfWeek = (int) $pickedDate->format('w');
        }

        /** @var \Irmag\SiteBundle\Entity\OrderDeliveryCityDayOfWeek $deliveryCityDayOfWeek */
        foreach ($deliveryCity->getDeliveryCityDayOfWeek() as $deliveryCityDayOfWeek) {
            $dayOfWeek = (int) $deliveryCityDayOfWeek->getDayOfWeek();
            $this->availableDaysOfWeek[] = $dayOfWeek;
            $minDaysBeforeDelivery = (int) $deliveryCityDayOfWeek->getMinDaysBeforeDelivery();

            foreach ($this->speedRun($dayOfWeek, $deliveryCityDayOfWeek->getMinDaysBeforeDelivery()) as $needToCheckDateTime) {
                if ($needToCheckDateTime instanceof \DateTime) {
                    $this->checkTimeInterval($deliveryCityDayOfWeek, $needToCheckDateTime);
                }
            }

            // если выбран день недели
            if (isset($pickedDayOfWeek) && $pickedDayOfWeek === $dayOfWeek) {
                /** @var \Irmag\SiteBundle\Entity\OrderDeliveryTime $deliveryTime */
                foreach ($deliveryCityDayOfWeek->getDeliveryTime() as $deliveryTime) {
                    // проверяем активность временного интервала
                    if (false === $deliveryTime->getIsActive()) {
                        continue;
                    }

                    // если у временного интервала есть лимит
                    if ($timeLimit = $deliveryTime->getTimeLimit()) {
                        if ((int) $now->format('Hi') > (int) $timeLimit->format('Hi')) {
                            $tmpDate = $pickedDate->modify(sprintf('-%d day', $minDaysBeforeDelivery));

                            if ($tmpDate->format(Config::DATE_FORMAT) === $now->format(Config::DATE_FORMAT)) {
                                continue;
                            }
                        }
                    }

                    $this->availableTimeIntervals[] = $deliveryTime;
                }
            }
        }

        $this->anyTimeToggler();
    }

    /**
     * Выявляет заблокированные даты по дню недели, заглядывая в БУДУЩЕЕ из ПРОШЛОГО!
     *
     * Пробегает от вчерашнего дня с шагом в `+1 день` до `минимального количества дней до доставки`.
     * Если дни недели совпадают, тогда отфильтровываем даты.
     *
     * @param int $dayOfWeek             До какого дня недели бежать
     * @param int $minDaysBeforeDelivery Минимальное кол-во дней до доставки
     *
     * @return \Generator Если возвращает \DateTime, то эту дату нужно проверить
     */
    private function speedRun(int $dayOfWeek, int $minDaysBeforeDelivery)
    {
        $now = new \DateTime('-1 day');

        for ($i = 0; $i <= $minDaysBeforeDelivery; ++$i) {
            $now->modify('+1 day');

            if ((int) $now->format('w') === $dayOfWeek) {
                // отфильтровываем
                if ($i < $minDaysBeforeDelivery) {
                    // out of range, dd
                    $this->disabledDates[] = $now->format(Config::DATE_FORMAT);
                } elseif ($i === $minDaysBeforeDelivery) {
                    // eq, n to check time intervals
                    yield $now;
                }
            }
        }
    }

    /**
     * Проверяем временные интервалы у дня недели и блокируем по дате.
     *
     * @param OrderDeliveryCityDayOfWeek $deliveryCityDayOfWeek
     * @param \DateTime                  $dateTime
     */
    public function checkTimeInterval(OrderDeliveryCityDayOfWeek $deliveryCityDayOfWeek, \DateTime $dateTime)
    {
        $now = new \DateTime();
        $deliveryCityDayOfWeek = clone $deliveryCityDayOfWeek;
        $deliveryTimes = $deliveryCityDayOfWeek->getDeliveryTime();

        /** @var OrderDeliveryTime $deliveryTime */
        foreach ($deliveryTimes as $deliveryTime) {
            // если есть ограничение времени
            if ($timeLimit = $deliveryTime->getTimeLimit()) {
                // сравниваем текущее время c ограничением
                if ((int) $now->format('Hi') > (int) $timeLimit->format('Hi')) {
                    // исключаем временной интервал
                    //dump(sprintf('exclude "%s" interval from date "%s".', $deliveryTime->getName(), $dateTime->format(Config::DATE_FORMAT)));
                    $deliveryCityDayOfWeek->removeDeliveryTime($deliveryTime);
                }
            }
        }

        // если нет доступный временных интервалов, то блокируем дату
        if (0 === $deliveryTimes->count()) {
            $this->disabledDates[] = $dateTime->format(Config::DATE_FORMAT);
        }
    }

    /**
     * Добавляет/удаляет интервал "В любое время" в зависимости от общего кол-ва интервалов.
     */
    public function anyTimeToggler()
    {
        $anyTime = $this->em->getRepository(OrderDeliveryTime::class)->getAnyTime();
        $count = \count($this->availableTimeIntervals);

        // если присутствует интервал "В любое время"
        if (\in_array($anyTime, $this->availableTimeIntervals, true)) {
            if (1 === $count) {
                // если остался один, тогда ничего не трогаем (удалённый город)
                return;
            }
        } elseif ($count > 1) {
            // если осталось несколько интервалов и нет "В любое время", то добавляем его
            $this->availableTimeIntervals[] = $anyTime;
        }
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderDeliveryMethod[]
     */
    public function getAvailableDeliveryMethods()
    {
        return $this->order->getDeliveryCity()->getDeliveryMethod();
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderDeliverySelfserviceMethod[]
     */
    public function getAvailableSelfserviceMethods()
    {
        $selfservices = new ArrayCollection();

        foreach ($this->order->getDeliveryCity()->getSelfserviceMethod() as $selfservice) {
            $selfservices[] = $selfservice->getSelfservice();
        }

        return $selfservices;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|OrderPaymentMethod[]
     */
    public function getAvailablePaymentMethods()
    {
        $deliveryMethod = $this->order->getDeliveryMethod();

        if ($deliveryMethod && 'selfservice' === $deliveryMethod->getShortname()) {
            $selfserviceMethod = $this->order->getSelfserviceMethod();

            if ($selectedSelfserviceMethod = $this->em->getRepository(OrderDeliveryCitySelfserviceMethod::class)->findOneBy(['selfservice' => $selfserviceMethod, 'deliveryCity' => $this->order->getDeliveryCity()])) {
                $paymentMethods = clone $selectedSelfserviceMethod->getPaymentMethod();
            }
        }

        if (!isset($paymentMethods)) {
            $paymentMethods = clone $this->order->getDeliveryCity()->getPaymentMethod();
        }

        $isPortableDevice = DeviceDetector::DESKTOP_TYPE !== DeviceDetector::getDevice();

        /** @var \Irmag\SiteBundle\Entity\OrderPaymentMethod $paymentMethod */
        foreach ($paymentMethods as $i => $paymentMethod) {
            // скрываем неактивные
            if (false === $paymentMethod->getIsActive()) {
                unset($paymentMethods[$i]);
                continue;
            }

            // если не мобильники
            if (false === $isPortableDevice && true === \in_array($paymentMethod->getShortname(), ['apple_pay', 'android_pay'], true)) {
                unset($paymentMethods[$i]);
                continue;
            }

            // если "Расчётный счёт", то скрываем везде, кроме "Юр. лица"
            if (true !== $this->order->getIsPayerLegal()) {
                if ('legal_rs' === $paymentMethod->getShortname()) {
                    unset($paymentMethods[$i]);
                }
            }

            // по настройкам пользователя
            if (true === $this->getUser()->getIsPrePaymentOnly()
                && false === \in_array($paymentMethod->getShortname(), OrderPaymentMethod::PRE_PAYMENT_METHODS, true)
            ) {
                unset($paymentMethods[$i]);
            }
        }

        return $paymentMethods;
    }

    /**
     * @return int[]
     */
    public function getAvailableDaysOfWeek()
    {
        return $this->availableDaysOfWeek;
    }

    /**
     * @return \DateTime[]
     */
    public function getDisabledDates()
    {
        return $this->disabledDates;
    }

    /**
     * @return array
     */
    public function getAvailableTimeIntervals()
    {
        if ($deliveryMethod = $this->order->getDeliveryMethod()) {
            // XXX: hardcore
            // если самовывоз, то доступен только один интервал (id: 9, name: 09:00 - 16:30)
            if ('selfservice' === $deliveryMethod->getShortname()) {
                $selfserviceMethod = $this->order->getSelfserviceMethod();

                if ($selfserviceMethod && !$selfserviceMethod->getDeliveryTime()->isEmpty()) {
                    $this->availableTimeIntervals = $selfserviceMethod->getDeliveryTime();

                    return $this->availableTimeIntervals;
                }
                /** @var \Irmag\SiteBundle\Entity\OrderDeliveryTime $timeInterval */
                foreach ($this->availableTimeIntervals as $i => $timeInterval) {
                    if (OrderDeliveryTimeRepository::SELF_SERVICE_TIME_INTERVAL_ID !== $timeInterval->getId()) {
                        unset($this->availableTimeIntervals[$i]);
                    }
                }
            } else {
                foreach ($this->availableTimeIntervals as $i => $timeInterval) {
                    if (OrderDeliveryTimeRepository::SELF_SERVICE_TIME_INTERVAL_ID === $timeInterval->getId()) {
                        unset($this->availableTimeIntervals[$i]);
                    }
                }
            }
        }

        return $this->availableTimeIntervals;
    }

    public function getAvailableDeliveryServices()
    {
        return $this->em->getRepository(OrderDeliveryService::class)->findBy(['isActive' => true]) ?: [];
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->order->getPayerEmail());
    }

    /**
     * @return bool
     */
    public function hasTelephone(): bool
    {
        return !empty($this->order->getPayerTelephone());
    }

    private function fillOrderFieldsFromData()
    {
        if (empty($orderData = $this->requestStack->getMasterRequest()->request->get('order'))) {
            return;
        }

        if (!empty($orderData['isPayerLegal']) && '1' === $orderData['isPayerLegal']) {
            $this->order->setIsPayerLegal(true);
        } else {
            $this->order->setIsPayerLegal(false);

            if (!empty($orderData['payerIndividual']['payerTelephone'])) {
                $this->order->setPayerTelephone($orderData['payerIndividual']['payerTelephone']);
            }
        }

        if (!empty($orderData['deliveryMethod'])) {
            $this->order->setDeliveryMethod($this->getEntityByIdOrThrowNotFoundException(OrderDeliveryMethod::class, $orderData['deliveryMethod']));
        }

        if (!empty($orderData['selfserviceMethod'])) {
            $this->order->setSelfserviceMethod($this->getEntityByIdOrThrowNotFoundException(OrderDeliverySelfserviceMethod::class, $orderData['selfserviceMethod']));
        }

        if (!empty($orderData['paymentMethod'])) {
            $this->order->setPaymentMethod($this->getEntityByIdOrThrowNotFoundException(OrderPaymentMethod::class, $orderData['paymentMethod']));
        }

        if (!empty($orderData['deliveryAddress']['deliveryCity'])) {
            $this->order->setDeliveryCity($this->getEntityByIdOrThrowNotFoundException(OrderDeliveryCity::class, $orderData['deliveryAddress']['deliveryCity']));
        }

        if (!empty($orderData['deliveryDate'])) {
            $this->order->setDeliveryDate(new \DateTime($orderData['deliveryDate']));
        }
    }

    /**
     * @param string $entity
     * @param int    $id
     *
     * @return \object|null
     */
    private function getEntityByIdOrThrowNotFoundException(string $entity, int $id): ?object
    {
        if (!$object = $this->em->getRepository($entity)->find($id)) {
            throw new NotFoundHttpException(sprintf('Entity "%s" with id "%d" not found.', $entity, $id));
        }

        return $object;
    }
}
