<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCountry;

class LoadOrderDeliveryServiceCountriesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $deliveryCountries = [
            ['Россия', 'RU'],
            ['Казахстан', 'KZ'],
            ['Армения', 'AM'],
            ['Беларусь', 'BY'],
            ['Киргизия', 'KG'],
        ];

        foreach ($deliveryCountries as $countries) {
            $existingCountry = $manager->getRepository(OrderDeliveryServiceCountry::class)->findOneBy(['countryCode' => $countries[1]]);

            if (!$existingCountry) {
                $country = new OrderDeliveryServiceCountry();
                $country->setName($countries[0]);
                $country->setCountryCode($countries[1]);

                $manager->persist($country);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 12;
    }
}
