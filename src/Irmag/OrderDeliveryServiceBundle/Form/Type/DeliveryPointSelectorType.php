<?php

namespace Irmag\OrderDeliveryServiceBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeliveryPointSelectorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('points', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'placeholder' => false,
                'multiple' => false,
                'choices' => $options['choices'],
                'choice_label' => function ($choice) {
                    return $choice;
                },
                'data' => $options['checked'] ?? '',
            ])
            ->add('select', ButtonType::class, [
               'label' => 'Выбрать',
                'attr' => [
                    'class' => 'btn btn-success',
                    'type' => 'button',
                    'disabled' => true,
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => false,
        ]);

        $resolver->setRequired('choices');
        $resolver->setAllowedTypes('choices', 'array');

        $resolver->setRequired('checked');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'terminal';
    }
}
