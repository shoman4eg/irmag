<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181112073332 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles RENAME COLUMN payer_legal_short_name TO payer_legal_consignee_address');
        $this->addSql('UPDATE users_order_profiles SET payer_legal_consignee_address = NULL');
        $this->addSql('ALTER TABLE orders RENAME COLUMN payer_legal_short_name TO payer_legal_consignee_address');
        $this->addSql('UPDATE orders SET payer_legal_consignee_address = NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles RENAME COLUMN payer_legal_consignee_address TO payer_legal_short_name');
        $this->addSql('UPDATE users_order_profiles SET payer_legal_short_name = NULL');
        $this->addSql('ALTER TABLE orders RENAME COLUMN payer_legal_consignee_address TO payer_legal_short_name');
        $this->addSql('UPDATE orders SET payer_legal_short_name = NULL');
    }
}
