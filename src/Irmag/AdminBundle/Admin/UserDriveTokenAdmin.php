<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Irmag\SiteBundle\Config;

class UserDriveTokenAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('edit')
            ->remove('create')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('token')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('user')
            ->add('token')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
