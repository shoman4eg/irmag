<?php

namespace Irmag\ThreadCommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\AttachmentBundle\Traits\AttachableTrait;

/**
 * @ORM\Table(name="threads_comments_attachments")
 * @ORM\Entity(repositoryClass="Irmag\ThreadCommentBundle\Repository\ThreadCommentAttachmentRepository")
 */
class ThreadCommentAttachment implements IrmagAttachable
{
    use TimestampableEntity;
    use AttachableTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ThreadComment
     *
     * @ORM\ManyToOne(targetEntity="ThreadComment", inversedBy="attachments")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $object;

    public function __construct()
    {
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setObject(?IrmagAttachmentPoint $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getObject(): ?IrmagAttachmentPoint
    {
        return $this->object;
    }
}
