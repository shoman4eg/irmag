<?php

namespace Irmag\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\BlogBundle\Entity\BlogPostElement;

class BlogElementsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('element', ElementType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'element-name',
                ],
            ])
            ->add('rate', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => array_flip([
                    '1' => '1 — ужасно',
                    '2' => '2 — плохо',
                    '3' => '3 — удовлетворительно',
                    '4' => '4 — хорошо',
                    '5' => '5 — отлично',
                ]),
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPostElement::class,
        ]);
    }
}
