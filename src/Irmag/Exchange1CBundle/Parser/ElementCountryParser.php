<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\SiteBundle\Entity\ElementCountry;

class ElementCountryParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'name',
    ];

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root country')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->getCrawler()->filter('root country')->each(function (Crawler $node) {
            $id = (int) $node->filter('id')->text();
            /* @var ElementCountry $elementCountry */
            $elementCountry = $this->em->getRepository(ElementCountry::class)->find($id);

            if (!$elementCountry) {
                $elementCountry = new ElementCountry();
                $elementCountry->setId($id);
            }

            $this->fillFields($elementCountry, $node);
            $this->em->persist($elementCountry);

            ++$this->operations;
            $this->flushIfNeed();

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }
        });
    }
}
