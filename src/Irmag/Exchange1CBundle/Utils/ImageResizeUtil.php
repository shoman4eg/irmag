<?php

namespace Irmag\Exchange1CBundle\Utils;

use Irmag\CoreBundle\Exception\IrmagException;

class ImageResizeUtil
{
    /**
     * Ресайзит картинку в меньшую сторону.
     *
     * @param string $filePath  Путь до файла
     * @param string $mimeType  MIME-type изображения
     * @param int    $maxWidth  Требуемая ширина
     * @param int    $maxHeight Требуемая высота
     * @param bool   $keepScale Сохранять пропорции?
     *
     * @throws IrmagException
     *
     * @return string
     */
    public function resize(string $filePath, string $mimeType, int $maxWidth = 0, int $maxHeight = 0, bool $keepScale = true): string
    {
        $properties = getimagesize($filePath);
        $width = $properties[0];
        $height = $properties[1];

        if (!empty($maxWidth) && !empty($maxHeight) && ($width > $maxWidth || $height > $maxHeight)) {
            if (false === $keepScale) {
                $newWidth = $maxWidth;
                $newHeight = $maxHeight;
            } else {
                if (($width > $maxWidth && $width > $height && $maxWidth < $maxHeight) || ($width > $height)) {
                    $percent = ($maxWidth * 100) / 100;
                    $newWidth = $maxWidth;
                    $newHeight = ($height * $percent) / $height;
                } else {
                    $percent = ($maxHeight * 100) / $height;
                    $newHeight = $maxHeight;
                    $newWidth = ($width * $percent) / 100;
                }
            }

            $imageCopy = imagecreatetruecolor($newWidth, $newHeight);

            switch ($mimeType) {
                case 'image/jpeg':
                    $imageContent = imagecreatefromjpeg($filePath);
                    break;
                case 'image/png':
                    $imageContent = imagecreatefrompng($filePath);
                    break;
                case 'image/gif':
                    $imageContent = imagecreatefromgif($filePath);
                    break;
                default:
                    throw new IrmagException(sprintf('Unknown image mime type: %s', $mimeType));
            }

            imagecopyresampled($imageCopy, $imageContent, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

            ob_start();
            switch ($mimeType) {
                case 'image/jpeg':
                    imagejpeg($imageCopy);
                    break;
                case 'image/png':
                    imagepng($imageCopy);
                    break;
                case 'image/gif':
                    imagegif($imageCopy);
                    break;
            }

            $contents = ob_get_contents();
            imagedestroy($imageCopy);
        } else {
            $contents = file_get_contents($filePath);
        }

        return $contents;
    }
}
