<?php

namespace Irmag\OrderDeliveryServiceBundle\Service;

use Irmag\OrderDeliveryServiceBundle\RequestContract\OrderDeliveryServiceRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\SelfDeliveryTerminalRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\ServiceCostRequestParameterInterface;

interface DeliveryServiceInterface
{
    /**
     * @param string $parameterInterface
     *
     * @return OrderDeliveryServiceRequestParameterInterface
     */
    public function getServiceParameter(string $parameterInterface);

    /**
     * Получить список пунктов доставки для службы.
     *
     * @return array
     */
    public function getCityList();

    /**
     * Получить список пунктов самовывоза.
     *
     * @return array
     */
    public function getSelfDeliveryTerminalsList();

    /**
     * Получить список пунктов самовывоза для указанного населенного пункта.
     *
     * @param SelfDeliveryTerminalRequestParameterInterface $params
     */
    public function getSelfDeliveryTerminalsAtPoint(SelfDeliveryTerminalRequestParameterInterface $params);

    /**
     * Получить все варианты доставки в указанный населённый пункт.
     *
     * @param ServiceCostRequestParameterInterface $params           Параметры запроса
     * @param bool                                 $needSelfDelivery Показывать варианты с самовывозом?
     *
     * @return array
     */
    public function getDeliveryVariants(ServiceCostRequestParameterInterface $params, bool $needSelfDelivery);

    /**
     * Доставлена ли посылка получателю?
     *
     * @param string $trackCode Трек-код для отслеживания посылки
     * @param array  $data      Заранее полученные данные (если есть)
     *
     * @return bool
     */
    public function isParcelDelivered(string $trackCode, array $data = []);
}
