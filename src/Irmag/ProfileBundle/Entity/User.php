<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\ForumBundle\Entity\ForumTopicSubscription;
use Irmag\ForumBundle\Entity\ForumVisit;
use Irmag\SiteBundle\Entity\File;

/**
 * XXX: Manual setup.
 * "CREATE UNIQUE INDEX UNIQ_USER_USERNAME ON users (LOWER(username));"
 * "CREATE UNIQUE INDEX UNIQ_USER_EMAIL ON users (LOWER(email));".
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Irmag\ProfileBundle\Repository\UserRepository")
 *
 * @UniqueEntity(fields="username", message="exists.username")
 * @UniqueEntity(fields="email", message="exists.email")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class User implements AdvancedUserInterface, \Serializable
{
    public const GRAVATAR_URL = 'https://secure.gravatar.com/avatar/%s?d=identicon&r=pg';

    public const ROLE_IRMAG_DRIVER = 'ROLE_IRMAG_DRIVER';

    /**
     * По истечении какого времени (в минутах) считать пользователя на форуме в оффлайне.
     * XXX: вынести.
     */
    const FORUM_OFFLINE_CUTOFF = 20;

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user", "api_user_id", "api_waybill"})
     */
    private $id;

    /**
     * Логин.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"Default", "change_profile"})
     * @Assert\Length(
     *     min=3,
     *     max=50,
     *     minMessage="login.min_length",
     *     maxMessage="login.max_length",
     *     groups={"Default", "change_profile"},
     * )
     *
     * @ORM\Column(type="string", length=50)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user", "api_waybill"})
     */
    private $username;

    /**
     * @Assert\Callback(groups={"registration"})
     */
    public function usernameValidate(ExecutionContextInterface $context, $payload)
    {
        if (false !== mb_strpos($this->getUsername(), '@')) {
            $context->buildViolation('error.username_is_email')
                ->setTranslationDomain('irmag_profile')
                ->atPath('username')
                ->addViolation();
        }
    }

    /**
     * Пароль.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"registration"})
     * @Assert\Length(
     *     min=6,
     *     max=64,
     *     minMessage="password.min_length",
     *     maxMessage="password.max_length",
     *     groups={"registration"},
     * )
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $password;

    /**
     * Email.
     *
     * @var string
     *
     * @Assert\Length(max=60)
     * @Assert\Email(checkHost=true)
     *
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $email;

    /**
     * Этот email не валиден?
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isEmailInvalid;

    /**
     * ФИО.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user", "api_waybill"})
     */
    private $fullname;

    /**
     * Показывать ФИО вместо логина.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private $useFullname;

    /**
     * Телефон.
     *
     * @var string
     *
     * @Assert\Length(max=32)
     * @Assert\Regex(
     *     pattern="/^[0-9\+\-\(\)\s]+$/",
     *     htmlPattern="^[0-9\+\-\(\)\s]+$",
     *     groups={"change_profile"},
     * )
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $telephone;

    /**
     * Это мобильный телефон.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $isMobileTelephone;

    /**
     * Согласен на email рассылку.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $isEmailSubscribed;

    /**
     * Согласен на SMS уведомления.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $isSmsSubscribed;

    /**
     * Согласен на Site уведомления.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $isSiteSubscribed;

    /**
     * День рождения.
     *
     * @var \DateTimeInterface
     *
     * @Assert\DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * Профессия.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profession;

    /**
     * О себе.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     * Пол.
     *
     * @var string
     *
     * @Assert\Length(max=1)
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\City")
     */
    private $addressCity;

    /**
     * Скидка в процентах.
     *
     * @var int
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user"})
     */
    private $discount;

    /**
     * Потрачено денег.
     *
     * @var int
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $spendMoney;

    /**
     * Фантики.
     *
     * @var int
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $bonus;

    /**
     * Gravatar.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $gravatar;

    /**
     * Аватар.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\File", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $avatar;

    /**
     * Позитивные роли.
     *
     * @var Collection|Role[]
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_positive_roles")
     */
    private $positiveRoles;

    /**
     * Негативные роли.
     *
     * @var Collection|Role[]
     *
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_negative_roles")
     */
    private $negativeRoles;

    /**
     * Посты в блоге.
     *
     * @var Collection|BlogPost[]
     *
     * @ORM\OneToMany(targetEntity="Irmag\BlogBundle\Entity\BlogPost", mappedBy="user")
     */
    private $posts;

    /**
     * Группы.
     *
     * @var Collection|Group[]
     *
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users")
     * @ORM\JoinTable(name="users_groups")
     */
    private $groups;

    /**
     * @var Collection|UserPushId[]
     *
     * @ORM\OneToMany(targetEntity="Irmag\ProfileBundle\Entity\UserPushId", mappedBy="user")
     */
    private $pushIds;

    /**
     * История блокировок.
     *
     * @var UserBonusHistory
     *
     * @ORM\OneToMany(targetEntity="UserBlockHistory", mappedBy="user")
     */
    private $blockHistory;

    /**
     * @var UserConfirmation
     *
     * @ORM\OneToMany(targetEntity="UserConfirmation", mappedBy="user")
     */
    private $confirmations;

    /**
     * @var Collection|UserDriveToken[]
     *
     * @ORM\OneToMany(targetEntity="Irmag\ProfileBundle\Entity\UserDriveToken", mappedBy="user")
     */
    private $driveTokens;

    /**
     * Подписки на топики форума.
     *
     * @var ForumTopicSubscription
     *
     * @ORM\OneToMany(targetEntity="\Irmag\ForumBundle\Entity\ForumTopicSubscription", mappedBy="user")
     */
    private $topicSubscriptions;

    /**
     * Просмотры для форума.
     *
     * @var ForumVisit
     *
     * @ORM\OneToMany(targetEntity="\Irmag\ForumBundle\Entity\ForumVisit", mappedBy="user")
     */
    private $forumVisits;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $forumRating;

    /**
     * Пользователю доступны только предоплаты.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPrePaymentOnly;

    /**
     * Активен.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * Дата последнего входа.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * Для назначения нового пароль.
     * XXX: Custom for Sonata Admin.
     *
     * @var string
     */
    private $plainPassword;

    public function __construct()
    {
        $this->useFullname = false;
        $this->isMobileTelephone = false;
        $this->isEmailSubscribed = true;
        $this->isSmsSubscribed = true;
        $this->isSiteSubscribed = true;
        $this->forumRating = 0;
        $this->discount = 0;
        $this->spendMoney = 0;
        $this->bonus = 0;
        $this->isActive = true;
        $this->isEmailInvalid = false;
        $this->isPrePaymentOnly = false;
        $this->posts = new ArrayCollection();
        $this->positiveRoles = new ArrayCollection();
        $this->negativeRoles = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->confirmations = new ArrayCollection();
        $this->topicSubscriptions = new ArrayCollection();
        $this->forumVisits = new ArrayCollection();
        $this->blockHistory = new ArrayCollection();
        $this->driveTokens = new ArrayCollection();
        $this->pushIds = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        if (empty($this->fullname)) {
            return (string) $this->username;
        }

        return sprintf('%s (%s)', $this->username, $this->fullname);
    }

    /**
     * XXX: Custom for Sonata Admin.
     *
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * XXX: Custom for Sonata Admin.
     *
     * @param string $plainPassword
     *
     * @return User
     */
    public function setPlainPassword(string $plainPassword = null): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * XXX: custom
     * Последний визит на форум.
     *
     * @return \DateTimeInterface|null
     */
    public function getLatestForumVisit(): ?\DateTimeInterface
    {
        $times = [];
        foreach ($this->getForumVisits() as $visit) {
            $times[] = $visit->getVisitTime();
        }

        sort($times);

        return !empty($times) ? end($times) : null;
    }

    /**
     * XXX: custom
     * Проверить, находится ли пользователь на форуме (в течение какого-то огранниченного промежутка времени).
     *
     * @return bool
     */
    public function getIsOnlineOnForum(): bool
    {
        $lastVisitTime = $this->getLatestForumVisit();

        if (!empty($lastVisitTime)) {
            $diff = $lastVisitTime->diff(new \DateTime(), true);

            if (0 === $diff->days && 0 === $diff->h && $diff->i < self::FORUM_OFFLINE_CUTOFF) {
                $isOnline = true;
            }
        }

        return $isOnline ?? false;
    }

    /**
     * Возвращает логин или ФИО в зависимости от настройки useFullname.
     * XXX: Custom.
     *
     * @return string
     */
    public function getName(): string
    {
        if (true === $this->useFullname && !empty($this->fullname)) {
            return $this->fullname;
        }

        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $rolesArray = ['ROLE_USER'];

        // group roles
        foreach ($this->getGroups() as $group) {
            /* @var Role $role */
            foreach ($group->getRoles() as $role) {
                $rolesArray[] = $role->getName();
            }
        }

        // positive roles
        foreach ($this->positiveRoles as $role) {
            $rolesArray[] = $role->getName();
        }

        // negative roles
        $negativeRoles = [];

        foreach ($this->getNegativeRoles() as $negativeRole) {
            $negativeRoles[] = $negativeRole->getName();
        }

        // remove negativeRoles from unique rolesArray
        return array_diff(array_unique($rolesArray), $negativeRoles);
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->isActive;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive) = unserialize($serialized);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        // XXX: Custom
        $this->setGravatar(sprintf(self::GRAVATAR_URL, md5(mb_strtolower(trim($email)))));

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setIsEmailInvalid(bool $isEmailInvalid): self
    {
        $this->isEmailInvalid = $isEmailInvalid;

        return $this;
    }

    public function getIsEmailInvalid(): ?bool
    {
        return $this->isEmailInvalid;
    }

    public function setFullname(?string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setUseFullname(bool $useFullname): self
    {
        $this->useFullname = $useFullname;

        return $this;
    }

    public function getUseFullname(): ?bool
    {
        return $this->useFullname;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setIsMobileTelephone(bool $isMobileTelephone): self
    {
        $this->isMobileTelephone = $isMobileTelephone;

        return $this;
    }

    public function getIsMobileTelephone(): ?bool
    {
        return $this->isMobileTelephone;
    }

    public function setIsEmailSubscribed(bool $isEmailSubscribed): self
    {
        $this->isEmailSubscribed = $isEmailSubscribed;

        return $this;
    }

    public function getIsEmailSubscribed(): ?bool
    {
        return $this->isEmailSubscribed;
    }

    public function setIsSmsSubscribed(bool $isSmsSubscribed): self
    {
        $this->isSmsSubscribed = $isSmsSubscribed;

        return $this;
    }

    public function getIsSmsSubscribed(): ?bool
    {
        return $this->isSmsSubscribed;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setProfession(?string $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getProfession(): ?string
    {
        return $this->profession;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setSpendMoney(int $spendMoney): self
    {
        $this->spendMoney = $spendMoney;

        return $this;
    }

    public function getSpendMoney(): ?int
    {
        return $this->spendMoney;
    }

    public function setBonus(int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setGravatar(?string $gravatar): self
    {
        $this->gravatar = $gravatar;

        return $this;
    }

    public function getGravatar(): ?string
    {
        return $this->gravatar;
    }

    public function setForumRating(int $forumRating): self
    {
        $this->forumRating = $forumRating;

        return $this;
    }

    public function getForumRating(): ?int
    {
        return $this->forumRating;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setLastLoginAt(?\DateTimeInterface $lastLoginAt): self
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->lastLoginAt;
    }

    public function setAddressCity(?City $addressCity): self
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    public function getAddressCity(): ?City
    {
        return $this->addressCity;
    }

    public function setAvatar(?File $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getAvatar(): ?File
    {
        return $this->avatar;
    }

    public function addPositiveRole(Role $positiveRole): self
    {
        if (!$this->positiveRoles->contains($positiveRole)) {
            $this->positiveRoles[] = $positiveRole;
        }

        return $this;
    }

    public function removePositiveRole(Role $positiveRole): self
    {
        if ($this->positiveRoles->contains($positiveRole)) {
            $this->positiveRoles->removeElement($positiveRole);
        }

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getPositiveRoles(): Collection
    {
        return $this->positiveRoles;
    }

    public function addNegativeRole(Role $negativeRole): self
    {
        if (!$this->negativeRoles->contains($negativeRole)) {
            $this->negativeRoles[] = $negativeRole;
        }

        return $this;
    }

    public function removeNegativeRole(Role $negativeRole): self
    {
        if ($this->negativeRoles->contains($negativeRole)) {
            $this->negativeRoles->removeElement($negativeRole);
        }

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getNegativeRoles(): Collection
    {
        return $this->negativeRoles;
    }

    public function addPost(BlogPost $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUser($this);
        }

        return $this;
    }

    public function removePost(BlogPost $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUser() === $this) {
                $post->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogPost[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addConfirmation(UserConfirmation $confirmation): self
    {
        if (!$this->confirmations->contains($confirmation)) {
            $this->confirmations[] = $confirmation;
            $confirmation->setUser($this);
        }

        return $this;
    }

    public function removeConfirmation(UserConfirmation $confirmation): self
    {
        if ($this->confirmations->contains($confirmation)) {
            $this->confirmations->removeElement($confirmation);
            // set the owning side to null (unless already changed)
            if ($confirmation->getUser() === $this) {
                $confirmation->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserConfirmation[]
     */
    public function getConfirmations(): Collection
    {
        return $this->confirmations;
    }

    public function addTopicSubscription(ForumTopicSubscription $topicSubscription): self
    {
        if (!$this->topicSubscriptions->contains($topicSubscription)) {
            $this->topicSubscriptions[] = $topicSubscription;
            $topicSubscription->setUser($this);
        }

        return $this;
    }

    public function removeTopicSubscription(ForumTopicSubscription $topicSubscription): self
    {
        if ($this->topicSubscriptions->contains($topicSubscription)) {
            $this->topicSubscriptions->removeElement($topicSubscription);
            // set the owning side to null (unless already changed)
            if ($topicSubscription->getUser() === $this) {
                $topicSubscription->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumTopicSubscription[]
     */
    public function getTopicSubscriptions(): Collection
    {
        return $this->topicSubscriptions;
    }

    public function addForumVisit(ForumVisit $forumVisit): self
    {
        if (!$this->forumVisits->contains($forumVisit)) {
            $this->forumVisits[] = $forumVisit;
            $forumVisit->setUser($this);
        }

        return $this;
    }

    public function removeForumVisit(ForumVisit $forumVisit): self
    {
        if ($this->forumVisits->contains($forumVisit)) {
            $this->forumVisits->removeElement($forumVisit);
            // set the owning side to null (unless already changed)
            if ($forumVisit->getUser() === $this) {
                $forumVisit->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumVisit[]
     */
    public function getForumVisits(): Collection
    {
        return $this->forumVisits;
    }

    public function addBlockHistory(UserBlockHistory $blockHistory): self
    {
        if (!$this->blockHistory->contains($blockHistory)) {
            $this->blockHistory[] = $blockHistory;
            $blockHistory->setUser($this);
        }

        return $this;
    }

    public function removeBlockHistory(UserBlockHistory $blockHistory): self
    {
        if ($this->blockHistory->contains($blockHistory)) {
            $this->blockHistory->removeElement($blockHistory);
            // set the owning side to null (unless already changed)
            if ($blockHistory->getUser() === $this) {
                $blockHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserBlockHistory[]
     */
    public function getBlockHistory(): Collection
    {
        return $this->blockHistory;
    }

    public function setIsPrePaymentOnly(bool $isPrePaymentOnly): self
    {
        $this->isPrePaymentOnly = $isPrePaymentOnly;

        return $this;
    }

    public function getIsPrePaymentOnly(): ?bool
    {
        return $this->isPrePaymentOnly;
    }

    /**
     * @return Collection|UserDriveToken[]
     */
    public function getDriveTokens(): Collection
    {
        return $this->driveTokens;
    }

    public function addDriveToken(UserDriveToken $driveToken): self
    {
        if (!$this->driveTokens->contains($driveToken)) {
            $this->driveTokens[] = $driveToken;
            $driveToken->setUser($this);
        }

        return $this;
    }

    public function removeDriveToken(UserDriveToken $driveToken): self
    {
        if ($this->driveTokens->contains($driveToken)) {
            $this->driveTokens->removeElement($driveToken);
            // set the owning side to null (unless already changed)
            if ($driveToken->getUser() === $this) {
                $driveToken->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPushId[]
     */
    public function getPushIds(): Collection
    {
        return $this->pushIds;
    }

    public function addPushId(UserPushId $pushId): self
    {
        if (!$this->pushIds->contains($pushId)) {
            $this->pushIds[] = $pushId;
            $pushId->setUser($this);
        }

        return $this;
    }

    public function removePushId(UserPushId $pushId): self
    {
        if ($this->pushIds->contains($pushId)) {
            $this->pushIds->removeElement($pushId);
            // set the owning side to null (unless already changed)
            if ($pushId->getUser() === $this) {
                $pushId->setUser(null);
            }
        }

        return $this;
    }

    public function getIsSiteSubscribed(): ?bool
    {
        return $this->isSiteSubscribed;
    }

    public function setIsSiteSubscribed(bool $isSiteSubscribed): self
    {
        $this->isSiteSubscribed = $isSiteSubscribed;

        return $this;
    }
}
