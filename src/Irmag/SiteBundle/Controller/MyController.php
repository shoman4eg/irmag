<?php

namespace Irmag\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserBonusHistory;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\ReferralManager\ReferralManager;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeAdapter;
use Irmag\SiteBundle\Form\Type\BonusDonateType;
use Irmag\SiteBundle\Form\Type\BonusPromocodeType;
use Irmag\SiteBundle\Repository\UserReferralOrderPayoutRepository;
use Irmag\SiteBundle\Entity\File;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\UserReferralOrderPayout;
use Irmag\SiteBundle\Entity\UserReferralLink;
use Irmag\SiteBundle\Entity\UserReferralTree;
use Irmag\SiteBundle\Entity\PromocodeHistory;

/**
 * @Route("/my")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class MyController extends Controller
{
    const SOCIAL_MONEY_DONATE_MESSAGE = 'Пожертвование на социальные проекты Ирмага';
    const SOCIAL_MONEY_RECEIVE_DONATE_MESSAGE = 'Получено пожертвование от пользователя "%d"';
    const PROMOCODE_BONUS_DONATE_MESSAGE = 'Фантики на счёт по промокоду.';

    /**
     * @Route("/discount/", name="irmag_profile_discount")
     */
    public function discountAction()
    {
        return $this->render('@IrmagSite/My/discount.html.twig');
    }

    /**
     * @Route("/bonus/", name="irmag_profile_bonus")
     *
     * @param Request $request
     *
     * @throws IrmagException when user not found
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response when user not found
     */
    public function bonusAction(Request $request)
    {
        $user = $this->getUser();
        $userBonus = $user->getBonus();

        $bonusDonateForm = $this->createForm(BonusDonateType::class, null, [
            'data' => ['value' => $userBonus ?: 0],
        ]);
        $bonusDonateForm->handleRequest($request);

        $em = $this->get('doctrine.orm.default_entity_manager');
        $socialMoneyBoxUserId = $this->getParameter('irmag_social_moneybox_user_id');
        $socialMoneyBoxUser = $em->getRepository(User::class)
            ->findOneBy(['id' => $socialMoneyBoxUserId]);

        if (!$socialMoneyBoxUser) {
            throw new IrmagException(sprintf('User with id "%d" not found.', $socialMoneyBoxUserId));
        }

        if ($bonusDonateForm->isSubmitted() && $bonusDonateForm->isValid()) {
            $value = $bonusDonateForm->getData()['value'];
            $bonusManager = $this->get(BonusManager::class);

            if ($value > 0 && $value <= $userBonus) {
                $em->beginTransaction();

                try {
                    // списываем бонусы у пользователя
                    $bonusManager->update(-$value, self::SOCIAL_MONEY_DONATE_MESSAGE);
                    // начисляем в социальную копилку
                    $bonusManager
                        ->setUser($socialMoneyBoxUser)
                        ->update($value, sprintf(self::SOCIAL_MONEY_RECEIVE_DONATE_MESSAGE, $user->getId()));

                    $em->commit();
                } catch (\Exception $exception) {
                    $em->rollBack();
                    throw $exception;
                }

                $this->addFlash('success-donate', $value);

                return $this->redirectToRoute('irmag_profile_bonus');
            }

            $bonusDonateForm->get('value')->addError(
                new FormError('Выбранное Вами значение недопустимо.')
            );
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $this->get('doctrine.orm.default_entity_manager')->getRepository(UserBonusHistory::class)->getAllByUser($user),
            $request->query->getInt('page', 1),
            20
        );

        /** @var UserReferralLink $referralLink */
        $referralLink = $em->getRepository(UserReferralLink::class)->findOneBy(['userReferral' => $user]);

        $bonusPromocodeForm = $this->createForm(BonusPromocodeType::class);
        $bonusPromocodeForm->handleRequest($request);

        if ($bonusPromocodeForm->isSubmitted() && $bonusPromocodeForm->isValid()) {
            $value = $bonusPromocodeForm->getData()['value'];

            try {
                $promocode = $this->get(PromocodeAdapter::class)->activate($value, true);

                $this->get(BonusManager::class)->update($promocode->getBonus(), self::PROMOCODE_BONUS_DONATE_MESSAGE);

                // записываем в историю использования промокода
                $promocodeHistory = new PromocodeHistory();
                $promocodeHistory->setCode($promocode->getCode());
                $promocodeHistory->setUser($this->getUser());
                $promocodeHistory->setBonus($promocode->getBonus());

                $promocode->setIsActive(false);

                $em->persist($promocodeHistory);
                $em->persist($promocode);
                $em->flush();

                $this->addFlash('success-promocode-bonus', $promocode->getBonus());

                return $this->redirectToRoute('irmag_profile_bonus');
            } catch (\Exception $e) {
                $bonusPromocodeForm->get('value')->addError(
                    new FormError($e->getMessage())
                );
            }
        }

        return $this->render('@IrmagSite/My/bonus.html.twig', [
            'bonus_donate_form' => $bonusDonateForm->createView(),
            'bonus_promocode_form' => $bonusPromocodeForm->createView(),
            'pagination' => $pagination,
            'social_moneybox_user' => $socialMoneyBoxUser,
            'user_owner' => $referralLink ? $referralLink->getUserOwner() : null,
        ]);
    }

    /**
     * @Route("/referral/",
     *     name="irmag_profile_referral",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()"
     * )
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function referralAction(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $treeRepo = $em->getRepository(UserReferralTree::class);
        /** @var User $userOwner */
        $userOwner = $this->getUser();
        $showAll = false;

        if ($request->query->has('all') && $this->isGranted('ROLE_SONATA_ADMIN')) {
            $rootNode = null;
            $showAll = true;
        } else {
            $rootNode = $treeRepo->findOneBy(['user' => $userOwner]);
        }

        if (
            (null !== $rootNode && !empty($treeRepo->getChildren($rootNode)))
            ||
            true === $showAll
        ) {
//            $em->getConfiguration()->addCustomHydrationMode('tree', \Gedmo\Tree\Hydrator\ORM\TreeObjectHydrator::class);
            $refTree = $treeRepo->getTree($rootNode);

            $router = $this->get('router');
            $imagineCacheManager = $this->get('liip_imagine.cache.manager');
            $startLvl = 0;

            $htmlTree = $treeRepo->buildTree($refTree, [
                'decorate' => true,
                'representationField' => 'user_id',
                'html' => true,
                'nodeDecorator' => function ($node) use ($em, $router, $imagineCacheManager, $userOwner, $showAll, &$startLvl) {
                    /** @var User $userReferral */
                    $userReferral = $em->getRepository(User::class)->find($node['user']['id']);

                    $userName = $node['user']['fullname'] ?? $node['user']['username'];

                    if (isset($node['user']['avatar']['path'])) {
                        $userAvatar = $imagineCacheManager->getBrowserPath(File::UPLOAD_DIR.'/'.$node['user']['avatar']['path'], 'avatar_thumb');
                    } else {
                        $userAvatar = $node['user']['gravatar'] ?? sprintf(User::GRAVATAR_URL, md5($node['user']['id']));
                    }

                    if ($userReferral->getId() === $userOwner->getId() || true === $showAll) {
                        $startLvl = $node['lvl'];

                        return sprintf(
                            '<div class="node"><small class="username">%s</small><div class="avatar"><img src="%s" alt="%1$s"></div></div>',
                            $userName,
                            $userAvatar
                        );
                    }

                    $lvl = $node['lvl'] - $startLvl;

                    /** @var UserReferralOrderPayoutRepository $userReferralOrderPayoutRepo */
                    $userReferralOrderPayoutRepo = $em->getRepository(UserReferralOrderPayout::class);
                    $ordersCount = $userReferralOrderPayoutRepo->getOrderCount($userReferral, $userOwner);
                    $allBonuses = $userReferralOrderPayoutRepo->getAllBonusProfits($userReferral, $userOwner);

                    return sprintf(
                        '<a href="%s" class="node" data-username="%s" data-lvl="%d" data-bonus-by-order="%d" data-orders-count="%d" data-toggle="popover.referral"><small class="username">%2$s</small><div class="avatar"><img src="%s" alt="%2$s"></div><span class="label total">%d ф.</span></a>',
                        $router->generate('irmag_profile_user', ['username' => $node['user']['username']], UrlGeneratorInterface::ABSOLUTE_URL),
                        $userName,
                        $lvl,
                        ReferralManager::getBonusPaymentValueByLvl($lvl),
                        $ordersCount,
                        $userAvatar,
                        $allBonuses
                    );
                },
            ]);
        }

        return $this->render('@IrmagSite/My/referral.html.twig', [
            'ref_tree' => $htmlTree ?? '',
            'show_all' => $showAll,
        ]);
    }

    /**
     * @Route("/orders/", name="irmag_profile_orders")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ordersAction(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        $query = $em->createQueryBuilder()
            ->select('o, oe, e, opm, odc, odm, odt, odsd, opdd, os, om')
            ->from(Order::class, 'o')
            ->leftJoin('o.orderElement', 'oe')
            ->leftJoin('oe.element', 'e')
            ->leftJoin('o.paymentMethod', 'opm')
            ->leftJoin('o.deliveryCity', 'odc')
            ->leftJoin('o.deliveryMethod', 'odm')
            ->leftJoin('o.deliveryTime', 'odt')
            ->leftJoin('o.deliveryServiceData', 'odsd')
            ->leftJoin('o.postDeliveryData', 'opdd')
            ->leftJoin('o.status', 'os')
            ->leftJoin('o.mixedPayment', 'om')
            ->where('o.user = :user')
            ->orderBy('o.id', 'DESC')
            ->setParameter('user', $this->getUser());

        switch ($tab = $request->query->get('tab')) {
            case 'history':
                $query->andWhere('os.shortname = :status')
                    ->andWhere('o.isRemoteDeliveryCompleted IS NULL OR o.isRemoteDeliveryCompleted = true')
                    ->setParameter('status', 'F'); // "Выполнен"
                break;

            case 'merged':
                $query->andWhere('os.shortname = :status')
                    ->setParameter('status', 'M'); // "Объединён"
                break;

            case 'canceled':
                $query->andWhere('os.shortname = :status')
                    ->setParameter('status', 'C'); // "Отменен"
                break;

            default:
                $tab = null;
                $query->andWhere('os.shortname NOT IN (:statuses) OR (os.shortname = :exceptingStatus AND o.isRemoteDeliveryCompleted = false)')
                    ->setParameter('exceptingStatus', 'F')
                    ->setParameter('statuses', ['F', 'M', 'C']); // Текущие заказы
                break;
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@IrmagSite/My/orders.html.twig', [
            'tab' => $tab,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/orders/{id}/", name="irmag_profile_order", requirements={"id": "\d+"})
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderAction(int $id)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $order = $em->getRepository(Order::class)->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if (!$order) {
            throw $this->createNotFoundException('Order not found!');
        }

        return $this->render('@IrmagSite/My/detail.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/orders/clone/{id}/", name="irmag_profile_order_clone", requirements={"id": "\d+"})
     *
     * @param int $id Ид заказа
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cloneAction($id)
    {
        /** @var Order $order */
        $order = $this->get('doctrine.orm.default_entity_manager')->getRepository(Order::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        $basketManager = $this->get(BasketManager::class);
        $counterAll = 0;
        $counterAdded = 0;

        foreach ($order->getOrderElement() as $orderElement) {
            $element = $orderElement->getElement();

            if (null !== $element && true === $element->getIsActive()) {
                $amount = $orderElement->getAmount() + $orderElement->getAmountDiff();
                $basketManager->addElement($element, $amount > 0 ? $amount : 1); // amount zero check
                ++$counterAdded;
            }

            ++$counterAll;
        }

        $this->addFlash(
            'success',
            sprintf('Заказ № %d добавлен в корзину. Добавлено %d из %d шт.', $id, $counterAdded, $counterAll)
        );

        return $this->redirectToRoute('irmag_profile_orders');
    }
}
