<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181009063616 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE waybill ALTER total_summ SET NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_cash DROP NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_card DROP NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_certificate DROP NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_rs DROP NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_online DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE waybill ALTER total_summ DROP NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_cash SET NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_card SET NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_certificate SET NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_rs SET NOT NULL');
        $this->addSql('ALTER TABLE waybill ALTER sum_online SET NOT NULL');
    }
}
