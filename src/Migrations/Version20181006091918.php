<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181006091918 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('
            UPDATE threads_comments tc
            SET is_purchased_from_us = true
            FROM threads_comments tc2
            LEFT JOIN threads t on tc2.thread_id = t.id
            WHERE t.owner_id IN (
                SELECT oe.element_id
                FROM orders_elements oe
                LEFT JOIN orders o ON oe.order_id = o.id
                WHERE o.user_id = tc2.user_id
            )
            AND tc.id = tc2.id
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
