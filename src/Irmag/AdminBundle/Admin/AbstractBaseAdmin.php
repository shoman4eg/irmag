<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Irmag\SiteBundle\Entity\File;

abstract class AbstractBaseAdmin extends AbstractAdmin
{
    /**
     * {@inheritdoc}
     */
    protected $translationDomain = 'irmag_admin';

    /**
     * {@inheritdoc}
     */
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    ];

    /**
     * @var PropertyAccessorInterface
     */
    protected $accessor;

    /**
     * @return PropertyAccessorInterface
     */
    protected function getAccessor(): PropertyAccessorInterface
    {
        if (null === $this->accessor) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        return $this->accessor;
    }

    /**
     * @param \object $object
     * @param array   $fields
     */
    protected function fixUploadFiles(object $object, array $fields = []): void
    {
        $accessor = $this->getAccessor();

        foreach ($fields as $field) {
            /* @var File $file */
            $file = $accessor->getValue($object, $field);

            if (!$file) {
                return;
            }

            if (!$file instanceof File) {
                throw new \InvalidArgumentException(sprintf('Field "%s" is not instance of "%s"', $field, File::class));
            }

            if (null !== $file && null === $file->getFile() && null === $file->getPath()) {
                $accessor->setValue($object, $field, null);
            }
        }
    }

    /**
     * @param \object $object
     * @param array   $fields
     */
    protected function fixDeleteFiles(object $object, array $fields = []): void
    {
        $accessor = $this->getAccessor();
        $params = $this->getRequest()->request->get($this->getUniqid());

        foreach ($fields as $field) {
            if (!empty($params[$field]['_delete'])) {
                $accessor->setValue($object, $field, null);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDataSourceIterator()
    {
        // disable translation for export
        $this->setTranslationDomain(null);

        return parent::getDataSourceIterator();
    }
}
