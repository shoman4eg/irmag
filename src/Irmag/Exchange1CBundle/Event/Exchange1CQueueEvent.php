<?php

namespace Irmag\Exchange1CBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;

class Exchange1CQueueEvent extends Event
{
    /**
     * @var Exchange1CQueue
     */
    private $queue;

    /**
     * @var array
     */
    private $data;

    /**
     * @param Exchange1CQueue $queue
     * @param array           $data
     */
    public function __construct(
        Exchange1CQueue $queue,
        array $data = []
    ) {
        $this->queue = $queue;
        $this->data = $data;
    }

    /**
     * @return Exchange1CQueue
     */
    public function getExchange1CQueue(): Exchange1CQueue
    {
        return $this->queue;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
