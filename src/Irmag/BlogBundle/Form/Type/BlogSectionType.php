<?php

namespace Irmag\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\BlogBundle\Entity\BlogSection;

class BlogSectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();
                $form->add('name', EntityType::class, [
                    'label' => false,
                    'class' => BlogSection::class,
                    'choice_label' => 'name',
                    'empty_data' => null,
                    'attr' => [
                        'data-id' => (null !== $data) ? $data->getId() : '',
                    ],
                ]);
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogSection::class,
        ]);
    }
}
