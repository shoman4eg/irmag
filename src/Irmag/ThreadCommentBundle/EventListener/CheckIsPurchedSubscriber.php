<?php

namespace Irmag\ThreadCommentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Order;
use Irmag\ThreadCommentBundle\Event\ThreadCommentEvent;
use Irmag\ThreadCommentBundle\IrmagThreadCommentEvents;

class CheckIsPurchedSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagThreadCommentEvents::THREAD_COMMENT_NEW_COMMENT_CREATED => 'check',
        ];
    }

    /**
     * @param ThreadCommentEvent $event
     */
    public function check(ThreadCommentEvent $event): void
    {
        $comment = $event->getThreadComment();
        $thread = $comment->getThread();

        // only for elements
        if (Element::class !== $thread->getOwnerFQCN()) {
            return;
        }

        $user = $comment->getUser();
        $elementId = $thread->getOwnerId();

        $result = $this->em->createQueryBuilder()
            ->select('COUNT(oe)')
            ->from(Order::class, 'o')
            ->leftJoin('o.orderElement', 'oe')
            ->where('oe.element = :element')
            ->andWhere('o.user = :user')
            ->setParameter('element', $elementId)
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        if ($result > 0) {
            $comment->setIsPurchasedFromUs(true);
            $this->em->persist($comment);
            $this->em->flush();
        }
    }
}
