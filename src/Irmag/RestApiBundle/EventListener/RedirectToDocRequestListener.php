<?php

namespace Irmag\RestApiBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class RedirectToDocRequestListener
{
    /**
     * @var string
     */
    private $apiDomain;

    /**
     * @var string
     */
    private $selfserviceApiDomain;

    /**
     * @var string
     */
    private $driveApiDomain;

    /**
     * @param string $apiDomain
     * @param string $selfserviceApiDomain
     * @param string $driveApiDomain
     */
    public function __construct(
        string $apiDomain,
        string $selfserviceApiDomain,
        string $driveApiDomain
    ) {
        $this->apiDomain = $apiDomain;
        $this->selfserviceApiDomain = $selfserviceApiDomain;
        $this->driveApiDomain = $driveApiDomain;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $host = $request->getHost();

        // only for API domains
        if (!\in_array($host, [$this->apiDomain, $this->selfserviceApiDomain, $this->driveApiDomain], true)) {
            return;
        }

        if ('/' === $request->getPathInfo()) {
            $event->setResponse(new RedirectResponse('/doc'));
        }
    }
}
