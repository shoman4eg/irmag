<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users_notification_events")
 * @ORM\Entity
 */
class UserNotificationEvent
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max="32")
     *
     * @ORM\Column(type="string", length=32)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     *
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPushDisabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isEmailDisabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSiteDisabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSmsDisabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->isPushDisabled = false;
        $this->isEmailDisabled = false;
        $this->isSiteDisabled = false;
        $this->isSmsDisabled = false;
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) sprintf('%s (%s)', $this->name, $this->description);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsPushDisabled(): ?bool
    {
        return $this->isPushDisabled;
    }

    public function setIsPushDisabled(bool $isPushDisabled): self
    {
        $this->isPushDisabled = $isPushDisabled;

        return $this;
    }

    public function getIsEmailDisabled(): ?bool
    {
        return $this->isEmailDisabled;
    }

    public function setIsEmailDisabled(bool $isEmailDisabled): self
    {
        $this->isEmailDisabled = $isEmailDisabled;

        return $this;
    }

    public function getIsSiteDisabled(): ?bool
    {
        return $this->isSiteDisabled;
    }

    public function setIsSiteDisabled(bool $isSiteDisabled): self
    {
        $this->isSiteDisabled = $isSiteDisabled;

        return $this;
    }

    public function getIsSmsDisabled(): ?bool
    {
        return $this->isSmsDisabled;
    }

    public function setIsSmsDisabled(bool $isSmsDisabled): self
    {
        $this->isSmsDisabled = $isSmsDisabled;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
