<?php

namespace Irmag\PollBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\PollBundle\Entity\PollVariant;

class PollType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('variant', PollVariantType::class, [
                'data' => null,
                'label' => false,
                'expanded' => true,
                'required' => true,
                'multiple' => $options['isMultiple'],
                'choices' => $options['choices'],
                'placeholder' => false,
                'choice_label' => function ($choice) {
                    /* @var PollVariant $choice */
                    return $choice->getTextHtml();
                },
                'choice_value' => function ($choice) {
                    /* @var PollVariant $choice */
                    return empty($choice) ? null : $choice->getId();
                },
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Голосовать',
                'attr' => [
                    'disabled' => true,
                    'class' => 'btn btn-success mt-15',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // XXX: wtf? where is data class?
            'translation_domain' => false,
        ]);

        $resolver->setRequired('choices');
        $resolver->setRequired('isMultiple');
        $resolver->setAllowedTypes('choices', 'array');
        $resolver->setAllowedTypes('isMultiple', 'bool');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'poll';
    }
}
