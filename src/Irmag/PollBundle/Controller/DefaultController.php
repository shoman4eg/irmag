<?php

namespace Irmag\PollBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Irmag\PollBundle\EventDispatcher\PollEventDispatcher;
use Irmag\PollBundle\Entity\PollOwnerInterface;
use Irmag\PollBundle\Form\Type\PollType;
use Irmag\PollBundle\Service\PollService;

class DefaultController extends Controller
{
    /**
     * @param PollOwnerInterface $entity
     *
     * @return Response
     */
    public function indexAction(PollOwnerInterface $entity): Response
    {
        $poll = $entity->getPoll();
        $pollService = $this->get(PollService::class);
        $pollService->setPoll($poll);

        $masterRequest = $this->get('request_stack')->getMasterRequest();

        $form = $this->createForm(PollType::class, null, [
            'choices' => $poll->getVariants()->toArray(),
            'isMultiple' => $poll->getIsMultiple(),
        ]);

        $form->handleRequest($masterRequest);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if (!empty($data['variant'])) {
                $variants = \is_array($data['variant']) ? $data['variant'] : [$data['variant']];
                $pollService->saveOpinions($variants);

                if ($this->getUser()) {
                    $this->get(PollEventDispatcher::class)->dispatchPollVoted($poll);
                }
            }
        }

        return $this->render('@IrmagPoll/poll.html.twig', [
            'poll' => $poll,
            'lead_result' => $pollService->getLeadResult(),
            'show_poll' => $pollService->isShowForm(),
            'form' => $form->createView(),
        ]);
    }
}
