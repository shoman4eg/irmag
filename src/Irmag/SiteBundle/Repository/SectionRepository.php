<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\Traits\TSearchTrait;

class SectionRepository extends \Gedmo\Tree\Entity\Repository\NestedTreeRepository
{
    use TSearchTrait;
}
