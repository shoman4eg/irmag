<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\Group;

class IrmagGroupListCommand extends Command
{
    protected static $defaultName = 'irmag:group:list';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('List of Groups')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limit', 50)
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, 'Offset', 0)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = $input->getOption('limit');
        $offset = $input->getOption('offset');
        $groups = $this->em->getRepository(Group::class)->findBy([], ['id' => 'DESC'], $limit, $offset);
        $io = new SymfonyStyle($input, $output);

        $io->section('List of Groups');

        $groupsAsPlainArrays = array_map(function (Group $group) {
            return [
                $group->getId(),
                $group->getName(),
                $group->getUsers()->count(),
                implode(', ', $group->getRoles()->toArray()),
            ];
        }, $groups);

        $io->table(
            ['Id', 'Name', 'Count of Users', 'Roles'],
            $groupsAsPlainArrays
        );
    }
}
