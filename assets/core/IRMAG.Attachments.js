/**
 * Прикрепления.
 *
 * @namespace IRMAG.Attachments
 *
 * @requires jquery
 * @requires Routing
 */

import Routing from './modules/routing';
import error from './modules/errorbox';

IRMAG.Attachments = {
    mode: '',

    init: function (mode) {
        this.mode = mode;

        this.showGhostAttaches();
        this.initFileUploader();
        this.bindRemoveAttachment();
        this.bindLinkAttachToPost();
    },

    /**
     * Показывает загруженные картинки, которые не были прикреплены ни к одному объекту, если такие есть
     */
    showGhostAttaches: function () {
        const $countGhostAttaches = $('div.uploaded-pic-container').length;

        if ($countGhostAttaches > 0) {
            $('.post-uploaded-pics').css('visibility', 'visible');
        }
    },

    /**
     * Загрузить фотографии
     */
    initFileUploader: function () {
        $(document).on('click', '.attach-to', function (e) {
            e.preventDefault();
            $('input.file-hidden').trigger('click');
        });

        $('input.file-hidden').fileupload({
            url: Routing.generate('irmag_' + IRMAG.Attachments.mode + '_upload_pics'),
            dataType: 'json',
            add: function (e, data) {
                data.context = $([
                    '<div class="uploaded-pic-container">',
                        '<i class="fa fa-times-circle-o fa-lg"></i>',
                        '<p class="attachments-note">Загрузка ... 0%</p>',
                    '</div>',
                ].join('\n')).appendTo($('div.post-uploaded-pics'));
                data.submit();
            },
            done: function (e, data) {
                if (data.result.success) {
                    data.context.css('visibility', 'visible');
                    data.context.attr('data-id', data.result.response.attach_id);

                    // сокращаем название файла, если слишком длинное
                    const attachName = (data.files[0].name.length > 13)
                        ? data.files[0].name.substring(0, 6) + '... ' + data.files[0].name.substring(data.files[0].name.length - 4)
                        : data.files[0].name;
                    let countAttaches = $('.attachment-items input').length;
                    let attachInput = $('.attachment-items').attr('data-prototype');

                    data.context.children('p').text(attachName);
                    data.context.append(data.result.response.image_code);

                    countAttaches++;
                    attachInput = attachInput.replace(/__name__/g, countAttaches);
                    $('.attachment-items').append(attachInput);
                    $('.attachment-items input:last').val(data.result.response.attach_id);
                } else {
                    error('Что-то пошло не так. Пожалуйста, повторите загрузку файла.');
                }
            },
            error: function (req) {
                let msg = '';
                $('div.uploaded-pic-container:last').remove();

                if (req.status === 413) {
                    msg = 'размер файла превышает максимально допустимое значение. Пожалуйста, уменьшите размер картинки.';
                } else if (req.status === 415) {
                    msg = 'некорректный формат файла. Поддерживаются картинки в форматах: jpg, png.';
                }

                error('Ошибка загрузки: ' + msg);
            },
        });
    },

    /**
     * Удалить прикрепление
     */
    bindRemoveAttachment: function () {
        const self = this;

        $(document).on('click', '.uploaded-pic-container i', function () {
            const $parent = $(this).parent();
            const id = $parent.data('id');

            $.ajax({
                url: Routing.generate('irmag_' + IRMAG.Attachments.mode + '_remove_attachment'),
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function (data) {
                    if (data.success) {
                        const md = $('textarea.md-input').data('markdown');
                        let arr;
                        const text = md.getContent();
                        const re = /(!\[.*?\]\()(.+?)(\))/g;

                        // удаляем все упоминания аттача из текста поста
                        while (arr = re.exec(text)) {
                            if (arr[2] === id) {
                                $('textarea.md-input').trigger('focus');
                                md.setSelection(text.indexOf(arr[0]), text.indexOf(arr[0]) + arr[0].length + 1);
                                md.replaceSelection('');
                            }
                        }

                        self.removeAttachmentFromForm(id);
                        $parent.fadeOut(600, function () {
                            $parent.remove();
                            const inputs = $('.attachment-items input[type="hidden"]');
                            $.each(inputs, function () {
                                if ($(this).val() === id) {
                                    $(this).remove();
                                }
                            });
                        });
                    } else {
                        error('Что-то пошло не так. Пожалуйста, попробуйте удалить файл снова.');
                    }
                },
                fail: function () {
                    error('Не удаётся удалить файл. Обратитесь к администратору.');
                },
            });
        });
    },

    /**
     * Добавить код картинки в текст поста
     */
    bindLinkAttachToPost: function () {
        const self = this;

        $(document).on('click', '.uploaded-pic-container img', function () {
            const $textarea = $('textarea.md-input').trigger('focus');
            const $md = $textarea.data('markdown');
            const currentCursor = $textarea.prop('selectionStart');
            const attachId = $(this).parent().data('id');
            const chunk = '![Введите описание изображения](' + attachId + ')';

            $md.replaceSelection(chunk);
            $md.setSelection(currentCursor + 2, currentCursor + 30);
            $textarea.trigger('change');
            self.addAttachmentToForm(attachId);
        });
    },

    /**
     * Добавить код прототипа в форму.
     */
    addAttachmentToForm: function (attachId) {
        let countAttaches = $('.attachment-items input').length;
        const attachPrototype = $('.attachment-items').attr('data-prototype');
        let $attachInput = $('input[type="hidden"][value="' + attachId + '"]');

        if ($attachInput.length === 0) {
            countAttaches++;
            $attachInput = attachPrototype.replace(/__name__/g, countAttaches);
            $('.attachment-items').append($attachInput);
            $('.attachment-items input:last').val(attachId);
        }
    },

    /**
     * Удалить код прототипа из формы.
     */
    removeAttachmentFromForm: function (attachId) {
        $('input[type="hidden"][value="' + attachId + '"]:first').remove();
    },
};
