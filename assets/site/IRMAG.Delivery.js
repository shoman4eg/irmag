/**
 * Расчёт доставки.
 *
 * @namespace IRMAG.Delivery
 *
 * @requires jquery
 * @requires Routing
 * @requires bootbox
 * @requires Handlebars
 */

import Routing from '../core/modules/routing';
import deliveryMenuTmpl from './templates/delivery_menu.handlebars';

IRMAG.Delivery = {
    init: function () {
        this.bindChooseDeliveryCalculator();
        this.bindCityConfirmation();
        this.bindCityDismiss();

        IRMAG.Delivery.TransportCompany.init();
        IRMAG.Delivery.Post.init();
    },

    /**
     * Подтвердить город предположения.
     */
    bindCityConfirmation: function () {
        $(document).on('click', '#irmag-city-prediction-confirm', function () {
            const $block = $('#remote-delivery-msg');

            $.ajax({
                url: Routing.generate('irmag_delivery_confirm_city'),
                beforeSend: function () {
                    $block.addClass('block-loading');
                },
                success: function (response) {
                    if (response.success) {
                        window.location.reload();
                    }
                },
            });
        });
    },

    /**
     * Опровергнуть предполагаемый город.
     */
    bindCityDismiss: function () {
        $(document).on('click', '#irmag-city-prediction-dismiss', function () {
            const $block = $('#remote-delivery-msg');

            $.ajax({
                url: Routing.generate('irmag_delivery_dismiss_city'),
                beforeSend: function () {
                    $block.addClass('block-loading');
                },
                success: function (response) {
                    if (response.success) {
                        window.location.reload();
                    }
                },
            });
        });
    },

    /**
     * Выбор типа калькулятора по клику на кнопку.
     */
    bindChooseDeliveryCalculator: function () {
        $(document).off('click', '.delivery-service-calculate');
        $(document).on('click', '.delivery-service-calculate', function () {
            const context = {
                weight: $(this).data('weight'),
                volume: $(this).data('volume'),
            };

            $.fn.modal.Constructor.prototype.enforceFocus = function () {};
            bootbox.hideAll();
            bootbox.dialog({
                message: deliveryMenuTmpl(context),
                closeButton: true,
                className: 'delivery-modal',
            });
        });
    },

    /**
     * Загружает форму калькулятора.
     *
     * @param {string} route
     * @param {object} inputData
     * @param {function} onSuccessCallback
     */
    ajaxLoadCalculator: function (route, inputData, onSuccessCallback) {
        const self = this;

        $.ajax({
            url: Routing.generate(route),
            data: inputData,
            beforeSend: function () {
                self.toggleLoading(true);
            },
            success: function (content) {
                $.fn.modal.Constructor.prototype.enforceFocus = function () {};

                bootbox.hideAll();
                bootbox.dialog({
                    message: content,
                    closeButton: true,
                    className: 'delivery-modal',
                });

                if ('undefined' !== typeof onSuccessCallback) {
                    onSuccessCallback();
                }
            },
            error: function () {
                bootbox.alert('Не удалось загрузить калькулятор доставки. Пожалуйста, обновите страницу');
                self.toggleLoading(false);
            },
        });
    },

    /**
     * Перегружает блок с информацией о доставке через ajax.
     */
    reloadDeliveryBlock: function (route) {
        const $embed = $('#remote-delivery-embed');

        $.ajax({
            url: Routing.generate(route, {
                weight: $embed.data('weight'),
                volume: $embed.data('volume'),
                deliveryMethod: $embed.data('deliveryMethod'),
            }),
            success: function (content) {
                $embed.html(content);
            },
        });
    },

    /**
     * Включает/выключает спиннер загрузки.
     *
     * @param {boolean} needToShow
     */
    toggleLoading: function (needToShow) {
        const $selectWindow = $('.select-remote-delivery-method:first');
        const $deliverySpinner = $('.delivery-spinner');

        if ($selectWindow.length > 0) {
            if (true === needToShow) {
                $selectWindow.addClass('block-loading');
                $deliverySpinner.show();
            } else {
                $selectWindow.removeClass('block-loading');
                $deliverySpinner.hide();
            }
        }
    },
};
