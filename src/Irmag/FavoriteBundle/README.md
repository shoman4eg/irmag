# IrmagFavoriteBundle

See PHPDoc for more informations.

## Favorite Factory

```php
$favoriteFactory = $this->get('irmag.factory.favorite');
// get current user favorite
$favorite = $favoriteFactory->get($favoriteName);
$favorite = $favoriteFactory->getAnonymous();
$favorite = $favoriteFactory->getAuthenticated($favoriteName);
// get other user favorite
$favoriteFactory->setUser($otherUser);
$otherUserFavorite = $favoriteFactory->get($favoriteName);
// nullable other user
$favoriteFactory->setUser(null);
// again using current user
$favorite = $favoriteFactory->get($favoriteName);
```

`FavoriteFactoryException`

## Favorite Manager

```php
$favoriteManager = $this->get('irmag.manager.favorite');
// favorite manipulation for current user
$favoriteManager->addElement($element, $favoriteName);
$favoriteManager->removeElement($element, $favoriteName);
$favoriteManager->create($favoriteName);
$favoriteManager->rename($favoriteName, $newFavoriteName);
$favoriteManager->clear($favoriteName);
$favoriteManager->remove($favoriteName);
$favorite = $favoriteManager->getFavorite(); // eq $favoriteFactory->get(), but using simple cache
// favorite manipulation for other user
$favoriteManager->setFavorite($otherUserFavorite);
$favoriteManager->addElement($element, $favoriteName);
$favoriteManager->setFavorite(null);
// again using current user
$favoriteManager->addElement($element, $favoriteName);
```

`FavoriteManagerException`