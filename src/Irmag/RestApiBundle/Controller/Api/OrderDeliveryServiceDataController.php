<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData;
use Irmag\RestApiBundle\Form\Api\Type\OrderDeliveryServiceDataType;

/**
 * @Rest\RouteResource("OrderDeliveryServiceData")
 */
class OrderDeliveryServiceDataController extends AbstractRestController
{
    protected $entityClassName = OrderDeliveryServiceData::class;

    /**
     * Список всех данных по удалённой доставке транспортными компаниями.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке транспортными компаниями")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderDeliveryServiceData::class, groups={"api_delivery_service_data", "api_order_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_delivery_service_data", "api_order_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderDeliveryServiceData[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy([], null, $limit, $offset);
    }

    /**
     * Получить способ доставки по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке транспортными компаниями")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderDeliveryServiceData::class, groups={"api_delivery_service_data", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_delivery_service_data", "api_order_id"})
     *
     * @param OrderDeliveryServiceData $orderDeliveryServiceData
     *
     * @return OrderDeliveryServiceData
     */
    public function getAction(OrderDeliveryServiceData $orderDeliveryServiceData)
    {
        return $orderDeliveryServiceData;
    }

    /**
     * Обновить данные по доставке транспортными компаниями по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке транспортными компаниями")
     *
     * @SWG\Parameter(
     *     name="order_delivery_service_data",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderDeliveryServiceDataType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request                  $request
     * @param OrderDeliveryServiceData $orderDeliveryServiceData
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderDeliveryServiceData $orderDeliveryServiceData)
    {
        return $this->processForm($request, $orderDeliveryServiceData, OrderDeliveryServiceDataType::class);
    }
}
