/**
 * Корзина.
 *
 * @namespace IRMAG.Basket
 *
 * @requires jquery
 * @requires bootbox
 */

import log from '../core/modules/logger';
import { ajax, isElementInView } from '../core/modules/irmag-utils';
import { bind } from '../core/modules/jumper';

IRMAG.Basket = {
    init: function () {
        this.bindRemoveElementActionBtn();
        this.bindClearBasketActionBtn();
        this.bindMoveElementToFavorite();
        this.bindMoveAllElementsToFavorite();
        this.bindChangeElementAmountActionBtn();
        this.bindBonusPay();
        this.stickyFooterBtn();
        bind('bonus-pay');

        IRMAG.Delivery.init();
        IRMAG.Basket.PreOrderZone.init();
    },

    stickyFooterBtn: function () {
        const $btn = $('#checkout-wrapper');
        const $footer = $('#footer');
        const $totalPriceInfo = $('.total-price-info');

        if (0 === $totalPriceInfo.length) {
            return;
        }

        $(window).on('scroll load', function () {
            if (false === isElementInView($totalPriceInfo)
                && false === isElementInView($footer)
            ) {
                $btn.addClass('sticky');
            } else {
                $btn.removeClass('sticky');
            }
        });
    },

    bindClearBasketActionBtn: function () {
        log('[Basket]', 'bind clear basket btn');

        const self = this;

        $(document).on('click', '.clear-basket', function () {
            bootbox.dialog({
                message: 'Удалить все товары из корзины?',
                buttons: {
                    Отмена: {
                        className: 'btn-default',
                    },
                    'Всё в избранное': {
                        className: 'btn-warning',
                        callback: function () {
                            IRMAG.API.Basket.moveAllElementsToFavorite(null, function (data) {
                                if (data.success) {
                                    self.updateContent();
                                    IRMAG.Favorite.updateData();
                                }
                            });
                        },
                    },
                    Удалить: {
                        className: 'btn-danger',
                        callback: function () {
                            IRMAG.API.Basket.clear(function (data) {
                                if (data.success) {
                                    self.updateContent();
                                }
                            });
                        },
                    },
                },
            });
        });
    },

    bindAddElementActionBtn: function () {
        log('[Basket]', 'bind add element btn');

        $(document).on('click', '.add-element-to-basket[data-element-id]', function () {
            const $addBtn = $(this);
            const elementId = $addBtn.data('element-id');
            const amount = $addBtn.data('amount')
                ? $addBtn.data('amount')
                : $('.amount[data-mapped-to="' + elementId + '"]').val() || 1;
            // loading icon
            $addBtn.filter('.btn-primary').find('.btn-icon').addClass('loading fa-pulse');

            IRMAG.API.Basket.addElement(elementId, amount, function (data) {
                if (data.success) {
                    $addBtn.filter('.btn-primary')
                        .removeClass('btn-primary').addClass('btn-success')
                        .find('.btn-icon').removeClass('loading fa-pulse');

                    IRMAG.API.Basket.notifyAdd();
                    IRMAG.Basket.updateData();
                }
            });
        });
    },

    bindRemoveElementActionBtn: function () {
        log('[Basket]', 'bind remove element btn');

        const self = this;

        $(document).on('click', '.remove-element-from-basket[data-element-id]', function () {
            const elementId = $(this).data('element-id');

            IRMAG.API.Basket.removeElement(elementId, function (data) {
                if (data.success) {
                    IRMAG.API.Basket.notifyRemove();
                    self.updateContent();
                }
            });
        });
    },

    bindMoveElementToFavorite: function () {
        log('[Basket]', 'bind move element to favorite btn');

        const self = this;

        $(document).on('click', '.move-element-to-favorite[data-element-id], .move-element-to-favorite ~ ul.favorite-dropdown li:not(.disabled) a[data-element-id]', function () {
            const elementId = $(this).data('element-id');
            const favoriteName = $(this).data('favorite-name') || null;

            IRMAG.API.Basket.moveElementToFavorite(elementId, favoriteName, function (data) {
                if (data.success) {
                    IRMAG.API.Favorite.notifyAdd();
                    self.updateContent();
                    IRMAG.Favorite.updateData();
                }
            });
        });
    },

    bindMoveAllElementsToFavorite: function () {
        log('[Basket]', 'bind move all elements to favorite btn');

        const self = this;

        $(document).on('click', 'a.move-all-elements-to-favorite, .move-all-elements-to-favorite ul li a', function () {
            const favoriteName = $(this).data('favorite-name') || null;

            bootbox.confirm('Переместить все товары в избранное?', function (result) {
                if (true === result) {
                    IRMAG.API.Basket.moveAllElementsToFavorite(favoriteName, function (data) {
                        if (data.success) {
                            self.updateContent();
                            IRMAG.Favorite.updateData();
                        }
                    });
                }
            });
        });
    },

    bindChangeElementAmountActionBtn: function () {
        log('[Basket]', 'bind change amount btn');

        const self = this;
        const inputSelector = '.amount input[type="number"]';

        function oldValueSaveRunner() {
            $(inputSelector).each(function () {
                $(this).data('old-value', $(this).val());
            });
        }

        oldValueSaveRunner();

        $(document).on('change', inputSelector, function () {
            const oldVal = $(this).data('old-value') || 1;
            const val = $(this).val();

            if (val === oldVal) {
                return;
            }

            if (val < 1 || val > 999) {
                $(this).val(oldVal);
            } else {
                IRMAG.API.Basket.changeElementAmount($(this).data('element-id'), val, function (data) {
                    if (data.success) {
                        self.updateContent(function () {
                            oldValueSaveRunner();
                        });
                    }
                });
            }
        });
    },

    /**
     * Оплата фантиками.
     */
    bindBonusPay: function () {
        function callback(data) {
            if (data.success) {
                IRMAG.Basket.updateContent();
                IRMAG.Basket.updateData();
            }

            if (data.error) {
                bootbox.alert({
                    title: 'Ошибка',
                    message: data.error,
                });
            }
        }

        $(document)
            .on('click', '#irmag-bonus-pay-btn', function () {
                ajax('irmag_basket_bonus_pay', {
                    value: $('#irmag-bonus-value').val(),
                }, function (data) {
                    callback(data);
                });
            })
            .on('click', '#irmag-cancel-bonus-pay-btn', function () {
                ajax('irmag_basket_cancel_bonus_pay', null, function (data) {
                    callback(data);
                });
            })
        ;
    },

    /**
     * Обновляет контент корзины
     *
     * @param {function} callback
     */
    updateContent: function (callback) {
        const self = this;
        const $wrapper = $('#basket-ajax-wrapper').addClass('block-loading');

        IRMAG.API.Basket.getContent(function (data) {
            $wrapper
                .html(data)
                .removeClass('block-loading');

            // reinit pre order zone
            IRMAG.Basket.PreOrderZone.init();
            // reload delivery calculator
            IRMAG.Delivery.reloadDeliveryBlock('irmag_get_common_delivery');

            if ($.isFunction(callback)) {
                callback(data);
            }

            self.updateData();
        });
    },

    /**
     * Обновляет информацию о кол-ве эл-ов (header-blue-line) и мини корзину
     */
    updateData: function () {
        IRMAG.API.Basket.getData(function (data) {
            $('#total-count-basket-wrapper').html(data['total-count']);
            $('#mini-basket-wrapper').html(data['mini-basket']);
            IRMAG.Site.bindMiniBasketFavorite();
        });
    },
};
