<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Irmag\ProfileBundle\Entity\Role;
use Irmag\ProfileBundle\Entity\Group;

class IrmagGroupAddCommand extends Command
{
    protected static $defaultName = 'irmag:group:add';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param EntityManagerInterface $em
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->validator = $validator;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Create new Group')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->section('Adding a Group');

        $group = new Group();

        // name
        $name = $io->ask('Name');
        $this->validatePropertyValue($group, 'name', $name);
        $group->setName($name);

        // roles
        $roles = $this->em->createQueryBuilder()
            ->select('r.name')
            ->from(Role::class, 'r')
            ->getQuery()
            ->getResult();

        $roles = array_map('current', $roles);
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Select roles (Example: 0,1,3)',
            $roles
        );
        $question->setMultiselect(true);
        $roles = $helper->ask($input, $output, $question);

        if (!empty($roles)) {
            foreach ($roles as $roleName) {
                $role = $this->em->getRepository(Role::class)->findOneBy(['name' => $roleName]);
                if ($role) {
                    $group->addRole($role);
                }
            }
        }

        $errors = $this->validator->validate($group);

        if (\count($errors) > 0) {
            $io->error((string) $errors);
        } else {
            $this->em->persist($group);
            $this->em->flush();

            $io->success('Group created!');
        }
    }

    /**
     * @param Group  $group
     * @param string $propertyName
     * @param        $propertyValue
     */
    protected function validatePropertyValue(Group $group, string $propertyName, $propertyValue)
    {
        $violations = $this->validator->validatePropertyValue($group, $propertyName, $propertyValue);

        if (\count($violations)) {
            throw new \RuntimeException((string) $violations);
        }
    }
}
