<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Entity\Order;
use Irmag\RestApiBundle\Form\Api\Type\OrderType;

/**
 * @Rest\RouteResource("Order")
 */
class OrderController extends AbstractRestController
{
    protected $entityClassName = Order::class;

    /**
     * Список всех заказов.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description="Order status id.",
     *     required=false,
     *     type="integer"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Order::class, groups={"api_order", "api_user_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="status", requirements="\d+", nullable=true, default=null, description="Order status.")
     *
     * @Rest\View(serializerGroups={"api_order", "api_user_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Order[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), null, $limit, $offset);
    }

    /**
     * Получить заказ по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Order::class, groups={"api_order", "api_user_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order", "api_user_id"})
     *
     * @param Order $order
     *
     * @return \object
     */
    public function getAction(Order $order)
    {
        return $order;
    }

    /**
     * Создать заказ.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        $order = new Order();
        // поле isAgreementAccepted не передается через 1с, поэтому устанавливаем его вручную
        $order->setIsAgreementAccepted(true);
        $orderData = $request->request->all();

        // set manual generate value strategy
        if (!empty($orderData['id'])) {
            $metadata = $this->getEntityManager()->getClassMetadata(\get_class($order));
            $metadata->setIdGenerator(new \Doctrine\ORM\Id\AssignedGenerator());
        }

        return $this->processForm($request, $order, OrderType::class);
    }

    /**
     * Обновить существующий заказ по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, Order $order)
    {
        return $this->processForm($request, $order, OrderType::class);
    }
}
