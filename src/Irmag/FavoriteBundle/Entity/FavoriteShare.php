<?php

namespace Irmag\FavoriteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="favorites_shares")
 * @ORM\Entity(repositoryClass="Irmag\FavoriteBundle\Repository\FavoriteShareRepository")
 */
class FavoriteShare
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var Favorite
     *
     * @ORM\ManyToOne(targetEntity="Favorite")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $favorite;

    /**
     * Пользователь.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(name="token", type="string", unique=true)
     */
    private $token;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Collection|FavoriteShareElement[]
     *
     * @ORM\OneToMany(targetEntity="FavoriteShareElement", mappedBy="favoriteShare", cascade={"persist"}, orphanRemoval=true)
     */
    private $favoriteShareElements;

    /**
     * Дата последнего просмотра (для сборщика мусора).
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastVisitAt;

    public function __construct()
    {
        $this->favoriteShareElements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastVisitAt(): ?\DateTimeInterface
    {
        return $this->lastVisitAt;
    }

    public function setLastVisitAt(?\DateTimeInterface $lastVisitAt): self
    {
        $this->lastVisitAt = $lastVisitAt;

        return $this;
    }

    public function getFavorite(): ?Favorite
    {
        return $this->favorite;
    }

    public function setFavorite(?Favorite $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|FavoriteShareElement[]
     */
    public function getFavoriteShareElements(): Collection
    {
        return $this->favoriteShareElements;
    }

    public function addFavoriteShareElement(FavoriteShareElement $favoriteShareElement): self
    {
        if (!$this->favoriteShareElements->contains($favoriteShareElement)) {
            $this->favoriteShareElements[] = $favoriteShareElement;
            $favoriteShareElement->setFavoriteShare($this);
        }

        return $this;
    }

    public function removeFavoriteShareElement(FavoriteShareElement $favoriteShareElement): self
    {
        if ($this->favoriteShareElements->contains($favoriteShareElement)) {
            $this->favoriteShareElements->removeElement($favoriteShareElement);
            // set the owning side to null (unless already changed)
            if ($favoriteShareElement->getFavoriteShare() === $this) {
                $favoriteShareElement->setFavoriteShare(null);
            }
        }

        return $this;
    }
}
