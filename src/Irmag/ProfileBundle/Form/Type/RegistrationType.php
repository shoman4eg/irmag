<?php

namespace Irmag\ProfileBundle\Form\Type;

use Irmag\SiteBundle\Config;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\ProfileBundle\Entity\User;

class RegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'label.login',
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'password.do_not_match',
                'required' => true,
                'first_options' => ['label' => 'registration.label.password'],
                'second_options' => ['label' => 'registration.label.password_repeated'],
            ])
            ->add('hasEmail', CheckboxType::class, [
                'label' => 'registration.label.has_email',
                'required' => false,
                'mapped' => false,
            ])
            ->add('email', null, [
                'required' => false,
            ])
            ->add('isEmailSubscribed', null, [
                'label' => 'registration.label.is_email_subscribed',
                'required' => false,
            ])
            ->add('hasTelephone', CheckboxType::class, [
                'label' => 'registration.label.has_telephone',
                'required' => false,
                'mapped' => false,
            ])
            ->add('telephone', null, [
                'label' => 'registration.label.telephone',
                'required' => false,
            ])
            ->add('isMobileTelephone', null, [
                'label' => false,
                'data' => true,
            ])
            ->add('isSmsSubscribed', null, [
                'label' => 'profile.label.is_sms_subscribed',
            ])
            ->add('isEulaAccepted', CheckboxType::class, [
                'data' => true,
                'mapped' => false,
            ])
            ->add('timestamp', HiddenType::class, [
                'data' => base64_encode((new \DateTime())->format('U')),
                'mapped' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'registration.button.sign_up',
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();

                if (Config::TESTER_USERNAME === $data['username']) {
                    return;
                }

                $formTimestamp = base64_decode($data['timestamp'], true);
                $nowTimestamp = (new \DateTime())->format('U');

                // This is bot, "Respect My Authoritah"
                if (!is_numeric($formTimestamp)
                    ||
                    $nowTimestamp - $formTimestamp < 10
                    ||
                    false !== mb_strpos($data['username'], 'http:/')
                    ||
                    false !== mb_strpos($data['username'], 'www.')
                ) {
                    $event->getForm()->addError(new FormError('Respect My Authoritah!'));
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => User::class,
                'validation_groups' => ['Default', 'registration'],
                'translation_domain' => 'irmag_profile',
            ]);
    }
}
