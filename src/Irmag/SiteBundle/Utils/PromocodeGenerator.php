<?php

namespace Irmag\SiteBundle\Utils;

class PromocodeGenerator
{
    /**
     * @var array
     */
    private const REPLACE_CHARS = [
        ['O', 'o', 'l', 'i', 'I'], // from
        ['0', '0', '1', '1', '1'], // to
    ];

    /**
     * @param int $length
     *
     * @return string
     */
    public static function generate(int $length): string
    {
        $code = rtrim(str_replace(['+', '/'], '', base64_encode(random_bytes($length))), '=');

        if (mb_strlen($code) < $length) {
            $code = $code.self::generate($length - mb_strlen($code));
        }

        if (mb_strlen($code) > $length) {
            $code = mb_substr($code, 0, $length);
        }

        return str_replace(self::REPLACE_CHARS[0], self::REPLACE_CHARS[1], $code);
    }
}
