<?php

namespace Irmag\SiteBundle\Service\BasketFavoriteManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\BasketBundle\Entity\Basket;
use Irmag\BasketBundle\Entity\BasketElement;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Service\CatalogElementViewSwitcher;

class BasketManager
{
    use BasketFavoriteTrait;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param RequestStack           $requestStack
     * @param RouterInterface        $router
     * @param ContainerInterface     $container
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        RequestStack $requestStack,
        RouterInterface $router,
        ContainerInterface $container
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * Возвращает корзину, если её нет, то создаёт.
     * Если пользователь аутентифицирован, то по user_id,
     * иначе создаётся временная корзина по токену в сессии self::SESSION_ID.
     *
     * @param bool $createIfNotExist Создать корзину, если её нет
     *
     * @return Basket|null|[]
     */
    public function getBasket(bool $createIfNotExist = false)
    {
        $basket = null;

        // force not grouped
        $this->container->get(CatalogElementViewSwitcher::class)->setGrouped(CatalogElementViewSwitcher::NORMAL_TABLE_NAME);

        if ($this->isAuthenticated()) {
            // пользователь аутентифицирован
            // если есть корзина в кэше, то используем её
            if (isset($this->cache['basket_auth'])) {
                return $this->cache['basket_auth'];
            }

            // получаем корзину по user_id
            $user = $this->getUser();
            $basket = $this->em->getRepository(Basket::class)->findOneBy(['user' => $user]);

            // если корзины нет, создаём
            if (!$basket && true === $createIfNotExist) {
                $basket = new Basket();
                $basket->setUser($user);
                $this->em->persist($basket);
                $this->em->flush();
            }

            // кэшируем
            $this->cache['basket_auth'] = $basket ?? [];
        } else {
            // пользователь не аутентифицирован
            // если есть корзина в кэше, то используем её
            if (isset($this->cache['basket_tmp'])) {
                return $this->cache['basket_tmp'];
            }

            // получаем корзину по временной сессии
            if (empty($tmpSession = $this->getTmpSession())) {
                return null;
            }

            $basket = $this->em->getRepository(Basket::class)->findOneBy(['tmpSession' => $tmpSession]);

            // если корзины нет, создаём
            if (!$basket && true === $createIfNotExist) {
                $basket = new Basket();
                $basket->setTmpSession($tmpSession);
                $this->em->persist($basket);
                $this->em->flush();
            }

            // кэшируем
            $this->cache['basket_tmp'] = $basket ?? [];
        }

        return $basket;
    }

    /**
     * Возвращает элементы текущей корзины.
     *
     * @param bool $fetchOnlyIsActive
     *
     * @return BasketElement[]|null
     */
    public function getBasketElements(bool $fetchOnlyIsActive = false)
    {
        if (empty($basket = $this->getBasket())) {
            return null;
        }

        $cacheKey = 'basket_elements_'.(true === $fetchOnlyIsActive ? '1' : '0');

        // проверяем кэш
        if (isset($this->cache[$cacheKey])) {
            return $this->cache[$cacheKey];
        }

        $this->cache[$cacheKey] = $this->em->getRepository(BasketElement::class)->getElementsByBasketId($basket->getId(), $fetchOnlyIsActive);

        return $this->cache[$cacheKey];
    }

    /**
     * @return Element[]
     */
    public function getElements(): array
    {
        if (empty($basketElements = $this->getBasketElements())) {
            return [];
        }

        $elements = [];

        foreach ($basketElements as $basketElement) {
            $elements[] = $basketElement->getElement();
        }

        return $elements;
    }

    /**
     * @param bool $fetchOnlyIsActive
     *
     * @return bool
     */
    public function isEmpty(bool $fetchOnlyIsActive = false): bool
    {
        return empty($this->getBasketElements($fetchOnlyIsActive));
    }

    /**
     * null - нет трансферов
     * int - кол-во дней (возможно 0).
     *
     * @return int|null
     */
    public function getMaxTransferValue(): ?int
    {
        $max = null;

        foreach ($this->getBasketElements(true) as $basketElement) {
            if (!$element = $basketElement->getElement()) {
                continue;
            }

            if (null === $propTransfer = $element->getPropTransfer()) {
                continue;
            }

            if (null === $max) {
                $max = $propTransfer;
            } elseif ($propTransfer > $max) {
                $max = $propTransfer;
            }
        }

        return $max;
    }

    /**
     * Оплатить фантиками.
     *
     * @param int $spendBonus Кол-во фантиков
     */
    public function setSpendBonus(int $spendBonus): void
    {
        if (false === $this->isAuthenticated()) {
            return;
        }

        $basket = $this->getBasket();
        $basket->setSpendBonus($spendBonus);
        $this->em->persist($basket);
        $this->em->flush();
    }

    /**
     * Слияние временной корзины с аутентифицированной.
     * После слияние, временная корзина удаляется со всеми её товарами.
     *
     * @return bool
     */
    public function merge(): bool
    {
        if (false === $this->isAuthenticated()) {
            return false;
        }

        if (empty($tmpSession = $this->getTmpSession())) {
            return false;
        }

        // временная корзина
        $basketTmpSession = $this->em->getRepository(Basket::class)->findOneBy(['tmpSession' => $tmpSession]);

        if (!$basketTmpSession) {
            return false;
        }

        // элементы временной корзины
        $basketTmpElements = $this->em->getRepository(BasketElement::class)
            ->getElementsByBasketId($basketTmpSession->getId());
        // аутентифицированная корзина
        $basketAuth = $this->getBasket(true);

        // перебираем элементы временной корзины
        /* @var BasketElement $basketTmpElement */
        foreach ($basketTmpElements as $basketTmpElement) {
            // сам товар
            $element = $basketTmpElement->getElement();
            // элемент аутентифицированной корзины
            $basketAuthElement = $this->em->getRepository(BasketElement::class)
                ->findOneBy(['basket' => $basketAuth, 'element' => $element]);

            // существующий товар не трогаем,
            // переносим только новый
            if (!$basketAuthElement) {
                // создаём новый товар в аутентифицированной корзине
                $basketAuthElement = new BasketElement();
                $basketAuthElement->setBasket($basketAuth);
                $basketAuthElement->setElement($element);
                $basketAuthElement->setAmount($basketTmpElement->getAmount());
            }

            // удаляем товар у временной корзины
            $this->em->remove($basketTmpElement);
            $this->em->persist($basketAuthElement);
        }

        // удаляем временную корзины
        $this->em->remove($basketTmpSession);
        // обновляем время у корзины
        $basketAuth->setUpdatedAtNow();
        $this->em->persist($basketAuth);
        $this->em->flush();

        return true;
    }

    /**
     * Очищает корзину.
     *
     * @param bool $fetchOnlyIsActive
     *
     * @return bool
     */
    public function clear(bool $fetchOnlyIsActive = false): bool
    {
        $basket = $this->getBasket();
        $basketElements = $this->getBasketElements($fetchOnlyIsActive);

        if (!$basketElements) {
            return false;
        }

        foreach ($basketElements as $basketElement) {
            $this->em->remove($basketElement);
        }

        // обновляем время у корзины
        $basket->setUpdatedAtNow();
        $this->em->persist($basket);
        $this->em->flush();

        return true;
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function exists(Element $element): bool
    {
        return \in_array($element, $this->getElements(), true);
    }

    /**
     * Добавляет товар в корзину.
     *
     * @param Element $element Товар
     * @param int     $amount  Кол-во [optional]
     *
     * @throws BasketFavoriteManagerException
     *
     * @return bool
     */
    public function addElement(Element $element, int $amount = 1): bool
    {
        if ($amount < 1 || $amount > 999) {
            throw new BasketFavoriteManagerException('Amount is too big');
        }

        if (false === $element->getIsActive()) {
            throw new BasketFavoriteManagerException('Element is not active');
        }

        $basket = $this->getBasket(true);

        return $this->addElementToBasket($basket, $element, $amount);
    }

    /**
     * Добавить товар в корзину пользователю.
     *
     * XXX: BP, repeated code.
     *
     * @param User    $user    Пользователь
     * @param Element $element Товар
     * @param int     $amount  Количество
     *
     * @throws BasketFavoriteManagerException
     *
     * @return bool
     */
    public function addElementToUsersBasket(User $user, Element $element, int $amount = 1)
    {
        if ($amount < 1 || $amount > 999) {
            throw new BasketFavoriteManagerException('Amount is too big');
        }

        if (false === $element->getIsActive()) {
            throw new BasketFavoriteManagerException('Element is not active');
        }

        $basket = $this->em->getRepository(Basket::class)->findOneBy(['user' => $user]);

        return $this->addElementToBasket($basket, $element, $amount);
    }

    /**
     * Удаляет товар из корзины.
     *
     * @param Element $element Товар
     *
     * @return bool
     */
    public function removeElement(Element $element): bool
    {
        if (empty($basket = $this->getBasket())) {
            return false;
        }

        $basketElement = $this->em->getRepository(BasketElement::class)
            ->findOneBy(['basket' => $basket, 'element' => $element]);

        if ($basketElement) {
            $this->em->remove($basketElement);
            // обновляем время у корзины
            $basket->setUpdatedAtNow();
            $this->em->persist($basket);
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * Перемещает товар из корзины в избранное.
     *
     * @param Element $element      Товар
     * @param string  $favoriteName Название избранного [optional]
     *
     * @return bool
     */
    public function moveElementToFavorite(Element $element, $favoriteName = null): bool
    {
        if (true === $this->container->get(FavoriteManager::class)->addElement($element, $favoriteName)) {
            return $this->removeElement($element);
        }

        return false;
    }

    /**
     * Перемещает все товары корзины в избранное.
     *
     * @param string $favoriteName Название избранного [optional]
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function moveAllElementsToFavorite(string $favoriteName = null): bool
    {
        if (empty($basket = $this->getBasket())) {
            return false;
        }

        $basketElements = $this->em->getRepository(BasketElement::class)
            ->findBy(['basket' => $basket]);

        // begin transaction
        $this->em->getConnection()->beginTransaction();

        try {
            /* @var BasketElement $basketElement */
            foreach ($basketElements as $basketElement) {
                $this->container->get(FavoriteManager::class)->addElement($basketElement->getElement(), $favoriteName);
                $this->em->remove($basketElement);
            }

            // обновляем время у корзины
            $basket->setUpdatedAtNow();
            $this->em->persist($basket);
            $this->em->flush();
            // commit
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            // rollback
            $this->em->getConnection()->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * Изменяет кол-во (шт.) у товара в корзине.
     *
     * @param Element $element Товар
     * @param int     $amount  Кол-во
     *
     * @return bool
     */
    public function changeElementAmount(Element $element, int $amount): bool
    {
        if ($amount < 1 || $amount > 999) {
            return false;
        }

        if (empty($basket = $this->getBasket())) {
            return false;
        }

        $basketElement = $this->em->getRepository(BasketElement::class)
            ->findOneBy(['basket' => $basket, 'element' => $element]);

        if ($basketElement) {
            $basketElement->setAmount($amount);
            // обновляем время у корзины
            $basket->setUpdatedAtNow();
            $this->em->persist($basketElement);
            $this->em->persist($basket);
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * Добавляет элемент в указанную корзину.
     *
     * @param Basket  $basket  Корзина
     * @param Element $element Товар
     * @param int     $amount  Количество единиц товара
     *
     * @return bool
     */
    private function addElementToBasket(Basket $basket, Element $element, int $amount = 1): bool
    {
        $basketElement = $this->em->getRepository(BasketElement::class)
            ->findOneBy(['basket' => $basket, 'element' => $element]);

        if ($basketElement) {
            // существующий товар, обновляем $amount
            $amount = $basketElement->getAmount() + $amount;
        } else {
            // новый товар в корзине
            $basketElement = new BasketElement();
            $basketElement->setBasket($basket);
            $basketElement->setElement($element);
        }

        $basketElement->setAmount($amount);
        // обновляем время у корзины
        $basket->setUpdatedAtNow();

        $this->em->persist($basket);
        $this->em->persist($basketElement);
        $this->em->flush();

        return true;
    }
}
