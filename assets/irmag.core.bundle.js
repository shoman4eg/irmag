import './core/stylesheets/main.scss';

import './core/IRMAG';
import './core/IRMAG.API';
import './core/IRMAG.Attachments';
import './core/IRMAG.Editor';
import './core/IRMAG.Emoji';
import './core/IRMAG.Poll';
import './core/IRMAG.ThreadComment';
import './core/IRMAG.View';
import './core/jquery.multiTelephoneField';

/*
 * Header
 */
console.log('%cI %cR %cM %cA %cG', 'font-size: 5em; color: #ffc405;', 'font-size: 5em; color: #81d852;', 'font-size: 5em; color: #61c1f5;', 'font-size: 5em; color: #e07dae;', 'font-size: 5em; color: #f8a94f;');
console.log('          %c@}}%c>--,--`--', 'font-size: 2em; color: #ff0000;', 'font-size: 2em; color: #00ff00;');
console.log('                 %cYOU', 'font-size: 3em; color: #eeeeee;');
