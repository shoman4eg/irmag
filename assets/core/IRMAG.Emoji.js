/**
 * @namespace IRMAG.Emoji
 *
 * @requires jquery
 * @requires atwho
 * @requires emojify
 */

import Config from './modules/irmag-config';
import log from './modules/logger';

IRMAG.Emoji = {
    /**
     * @param {Object} $elm Textarea/input element
     */
    bindInput: function ($elm) {
        log('[Emoji] bindInput:', $elm);

        const emojis = $.map(emojify.emojiNames, function (value) {
            return { key: value, name: value };
        });

        const emojiConfig = {
            at: ':',
            data: emojis,
            displayTpl: '<li>${name} <img src="' + Config.emojisDir + '/${key}.png"  height="20" width="20"></li>',
            insertTpl: ':${key}:',
            delay: 400,
        };

        $elm.atwho(emojiConfig);
    },

    /**
     * @param {Object[]} $elms Elements to render emoji from text
     */
    emojify: function ($elms) {
        log('[Emoji] emojify:', $elms);

        emojify.setConfig({
            img_dir: Config.emojisDir,
        });

        $elms.each(function () {
            emojify.run(this);
        });
    },

    /**
     * @param str String to render emoji from
     */
    emojifyString: function (str) {
        log('[Emoji] emojifyString');

        emojify.setConfig({
            img_dir: Config.emojisDir,
        });

        return emojify.replace(str);
    },
};
