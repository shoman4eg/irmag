<?php

namespace Irmag\Exchange1CBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Irmag\Exchange1CBundle\Event\Exchange1CQueueEvent;
use Irmag\Exchange1CBundle\IrmagExchange1CEvents;
use Irmag\Exchange1CBundle\Service\Exchange1CQueueManager;

class Exchange1CQueueSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Exchange1CQueueManager
     */
    private $manager;

    /**
     * @param EntityManagerInterface $em
     * @param Exchange1CQueueManager $manager
     */
    public function __construct(
        EntityManagerInterface $em,
        Exchange1CQueueManager $manager
    ) {
        $this->em = $em;
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_STARTED => 'startQueueItemProcess',
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_FAILED => 'failQueueItem',
            IrmagExchange1CEvents::QUEUE_ITEM_PROCESS_COMPLETED => 'removeQueueItem',
        ];
    }

    /**
     * @param Exchange1CQueueEvent $event
     */
    public function startQueueItemProcess(Exchange1CQueueEvent $event): void
    {
        $queueItem = $event->getExchange1CQueue();
        $data = $event->getData();

        $this->manager->markRunning($queueItem, $data['pid']);
    }

    /**
     * @param Exchange1CQueueEvent $event
     */
    public function failQueueItem(Exchange1CQueueEvent $event): void
    {
        $queueItem = $event->getExchange1CQueue();
        $data = $event->getData();

        $this->manager->fail($queueItem, $data['message']);
    }

    /**
     * @param Exchange1CQueueEvent $event
     */
    public function removeQueueItem(Exchange1CQueueEvent $event): void
    {
        $queueItem = $event->getExchange1CQueue();
        $this->manager->release($queueItem);
    }
}
