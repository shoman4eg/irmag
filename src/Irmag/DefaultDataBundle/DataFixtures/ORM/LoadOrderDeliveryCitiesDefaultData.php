<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;

class LoadOrderDeliveryCitiesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $cities = [
            ['Иркутск', 'irkutsk'],
            ['Байкальский тракт', 'baikalskiy_trakt'],
            ['Голоустненский тракт', 'goloustenskiy_trakt'],
            ['Мельничный тракт', 'melnichniy_trakt'],
            ['Александровский тракт', 'alexandrovskiy_trakt'],
            ['Качугский тракт', 'kachugskiy_trakt'],
            ['Плишкинский тракт', 'plishkinskiy_trakt'],
            ['Ангарск (Московский тракт)', 'angarsk'],
            ['Шелехов (Култукский тракт)', 'shelekhov'],
            ['Аршан', 'arshan'],
            ['Байкальск', 'baikalsk'],
            ['Братск', 'bratsk'],
            ['Бохан', 'bohan'],
            ['Залари', 'zalari'],
            ['Зима', 'zima'],
            ['Куйтун', 'kuytun'],
            ['Култук', 'kultuk'],
            ['Кырен', 'kiren'],
            ['Нижнеудинск', 'nijneudinsk'],
            ['Саянск', 'sayansk'],
            ['Слюдянка', 'sludyanka'],
            ['Тайшет', 'taishet'],
            ['Тулун', 'tulun'],
            ['Улан-Удэ', 'ulan_ude'],
            ['Усолье', 'usolie'],
            ['Усть-Кут', 'ust-kut'],
            ['Усть-Ордынский', 'ust-orda'],
            ['Черемхово', 'cheremhovo'],
            ['Чита', 'chita'],
            ['Другой (выберу в расчёте доставки)', 'other'],
        ];

        $i = $manager->getRepository(OrderDeliveryCity::class)->getLastDeliveryCityPosition() ?: 0;

        foreach ($cities as $cityData) {
            $existingCity = $manager->getRepository(OrderDeliveryCity::class)->findOneBy(['shortname' => $cityData[1]]);

            if (!$existingCity) {
                ++$i;
                $city = new OrderDeliveryCity();
                $city->setName($cityData[0]);
                $city->setShortname($cityData[1]);
                $city->setPosition($i);

                $manager->persist($city);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 6;
    }
}
