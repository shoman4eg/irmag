<?php

namespace Irmag\SiteBundle\Repository;

class NewsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Возвращает QueryBuilder новостей.
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getNewsQb()
    {
        return $this->createQueryBuilder('e')
            ->addSelect('t')
            ->leftJoin('e.thread', 't')
            ->where('e.isActive = true')
            ->orderBy('e.position', 'ASC');
    }

    /**
     * Возвращает последнюю созданную новость.
     */
    public function getLatest()
    {
        return $this->createQueryBuilder('e')
            ->where('e.isActive = true')
            ->orderBy('e.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Возвращает последнюю новость помеченную как "важная" для header-warning-line.
     */
    public function getWarningNews()
    {
        return $this->createQueryBuilder('e')
            ->select('partial e.{id, title, slug}')
            ->where('e.isActive = true')
            ->andWhere('e.isImportant = true')
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает баннеры для главной страницы.
     *
     * @param int $limit
     *
     * @return array
     */
    public function getIndexBanners($limit = 10)
    {
        return $this->createQueryBuilder('e')
            ->select('partial e.{id, slug, title, extraContentOnBanner}, partial b.{id, path}, partial bg.{id, path}')
            ->leftJoin('e.banner', 'b')
            ->leftJoin('e.bannerBg', 'bg')
            ->where('e.isActive = true')
            ->andWhere('e.isShowInIndexBanners = true')
            ->andWhere('b.path IS NOT NULL')
            ->andWhere('bg.path IS NOT NULL')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
