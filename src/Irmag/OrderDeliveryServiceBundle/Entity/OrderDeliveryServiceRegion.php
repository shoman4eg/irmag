<?php

namespace Irmag\OrderDeliveryServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_delivery_services_regions")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryServiceRegion
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $name;

    /**
     * Города региона.
     *
     * @var Collection|OrderDeliveryServiceCity[]
     *
     * @ORM\OneToMany(targetEntity="OrderDeliveryServiceCity", mappedBy="orderDeliveryServiceRegion", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $cities;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryServiceCity[]
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(OrderDeliveryServiceCity $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
            $city->setOrderDeliveryServiceRegion($this);
        }

        return $this;
    }

    public function removeCity(OrderDeliveryServiceCity $city): self
    {
        if ($this->cities->contains($city)) {
            $this->cities->removeElement($city);
            // set the owning side to null (unless already changed)
            if ($city->getOrderDeliveryServiceRegion() === $this) {
                $city->setOrderDeliveryServiceRegion(null);
            }
        }

        return $this;
    }
}
