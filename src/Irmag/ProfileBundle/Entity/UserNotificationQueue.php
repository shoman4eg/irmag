<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\Traits\NotificationDataTrait;

/**
 * @ORM\Table(name="users_notification_queues")
 * @ORM\Entity
 */
class UserNotificationQueue
{
    use TimestampableEntity;
    use NotificationDataTrait;

    public const DEFAULT_TTL = '+2 days';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isDone;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPushDone;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isEmailDone;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSiteDone;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSmsDone;

    public function __construct()
    {
        $this->isDone = false;
        $this->isPushDone = false;
        $this->isEmailDone = false;
        $this->isSiteDone = false;
        $this->isSmsDone = false;
        $this->calculateExpiredAt();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getIsPushDone(): ?bool
    {
        return $this->isPushDone;
    }

    public function setIsPushDone(bool $isPushDone): self
    {
        $this->isPushDone = $isPushDone;

        return $this;
    }

    public function getIsEmailDone(): ?bool
    {
        return $this->isEmailDone;
    }

    public function setIsEmailDone(bool $isEmailDone): self
    {
        $this->isEmailDone = $isEmailDone;

        return $this;
    }

    public function getIsSiteDone(): ?bool
    {
        return $this->isSiteDone;
    }

    public function setIsSiteDone(bool $isSiteDone): self
    {
        $this->isSiteDone = $isSiteDone;

        return $this;
    }

    public function getIsSmsDone(): ?bool
    {
        return $this->isSmsDone;
    }

    public function setIsSmsDone(bool $isSmsDone): self
    {
        $this->isSmsDone = $isSmsDone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
