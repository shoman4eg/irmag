<?php

namespace Irmag\OnlinePaymentBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\SiteBundle\Entity\OrderStatus;
use Irmag\SiteBundle\Service\OrderManager\OrderManager;

class IrmagFixUnpaidOrdersCommand extends Command
{
    /**
     * Через сколько дней автоматически отменять зависший заказ с онлайн-оплатой.
     */
    private const DAY_CUTOFF = 3;

    /**
     * Отсечка по времени, когда незавершенный заказ можно считать "зависшим", в минутах.
     */
    private const TIME_CUTOFF = 20;

    protected static $defaultName = 'irmag:online_payments:fix';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserStateManager
     */
    private $userStateManager;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $providerConfigs;

    /**
     * @param EntityManagerInterface $em
     * @param UserStateManager       $userStateManager
     * @param OrderManager           $orderManager
     * @param ContainerInterface     $container
     * @param array                  $providerConfigs
     */
    public function __construct(
        EntityManagerInterface $em,
        UserStateManager $userStateManager,
        OrderManager $orderManager,
        ContainerInterface $container,
        array $providerConfigs
    ) {
        $this->em = $em;
        $this->userStateManager = $userStateManager;
        $this->orderManager = $orderManager;
        $this->container = $container;
        $this->providerConfigs = $providerConfigs;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Fixes online payments caused users\'side redirect blocking.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting...');

        $providerServices = [];

        foreach ($this->providerConfigs as $method => $config) {
            $providerServices[$method] = $this->container->get($config['service']);
        }

        if ($output->isVerbose()) {
            $output->writeln('');
            $output->writeln('<info>Searching stuck orders...</info>');
        }

        // завершаем те, которые "зависли"
        $this->fixStuckOrders($providerServices, $output);

        // отменяем те, которые уже просрочены
        $this->fixUnpayedAndDeclined($output);

        $output->writeln('');
        $output->writeln(sprintf('<info>OK.</info>'));
    }

    /**
     * Пофиксить заказы, застрявшие с оплатой.
     *
     * @param array           $providerServices
     * @param OutputInterface $output
     */
    private function fixStuckOrders(array $providerServices, OutputInterface $output): void
    {
        $payments = $this->em->getRepository(OrderOnlinePayment::class)->getAllStuckedPayments();
        $now = new \DateTime();

        if (!empty($payments)) {
            $i = 0;

            foreach ($payments as $payment) {
                $dateDiff = $now->diff($payment->getCreatedAt(), true);

                if (0 === $dateDiff->days && 0 === $dateDiff->h && $dateDiff->i >= self::TIME_CUTOFF) {
                    ++$i;
                    $order = $payment->getOrder();

                    $paymentMethodShortname = $order->getPaymentMethod()->getShortname();
                    $currentService = $providerServices[$paymentMethodShortname];
                    $paymentData = $currentService->getState($payment);

                    if (OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME === $paymentData->getStatus()) {
                        $this->userStateManager->setUser($order->getUser());

                        if ($this->userStateManager->get(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY)) {
                            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);
                        }

                        if ($this->userStateManager->get(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY)) {
                            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY);
                        }

                        if ($this->userStateManager->get(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH)) {
                            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH);
                        }

                        if ($output->isVerbose()) {
                            $output->write(sprintf('Fixing order №%d ...', $order->getId()));
                        }

                        $newOrderStatus = $this->em->getRepository(OrderStatus::class)->findOneBy(['shortname' => 'N']);
                        $order->setStatus($newOrderStatus);

                        $this->em->persist($order);
                        $this->em->flush();

                        if ($output->isVerbose()) {
                            $output->writeln(' OK');
                        }
                    }
                }
            }

            if ($output->isVerbose() && $i > 0) {
                $output->writeln(sprintf('Total stuck orders: %d', $i));
            } else {
                $output->writeln('No stuck orders found.');
            }
        } else {
            $output->writeln('No stuck orders found.');
        }
    }

    /**
     * Пофиксить заказы, платежи которых уже отменились по таймауту.
     *
     * @param OutputInterface $output
     */
    private function fixUnpayedAndDeclined(OutputInterface $output): void
    {
        $payments = $this->em->getRepository(OrderOnlinePayment::class)->getAllUnpayedAndDeclined();

        if ($output->isVerbose()) {
            $output->writeln('');
            $output->writeln('<info>Searching broken payments ...</info>');
        }

        if (!empty($payments)) {
            $output->writeln(sprintf('Total broken payments: <info>%d</info>', \count($payments)));

            foreach ($payments as $payment) {
                // если платеж висит больше 3-х дней, то отменить заказ
                $order = $payment->getOrder();
                $now = new \DateTime();
                $dateDiff = $now->diff($order->getCreatedAt(), true);

                if ($dateDiff->days > self::DAY_CUTOFF) {
                    $output->writeln(sprintf('<comment>Cancelling order №%d ...</comment>', $order->getId()));
                    $this->orderManager->setOrderById($order->getId(), false);
                    $this->orderManager->cancel();
                }
            }
        } else {
            $output->writeln('No broken payments found.');
        }
    }
}
