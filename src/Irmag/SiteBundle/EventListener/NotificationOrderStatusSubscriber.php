<?php

namespace Irmag\SiteBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\SiteBundle\Entity\Order;

class NotificationOrderStatusSubscriber implements EventSubscriber
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'onFlush',
        ];
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Order) {
                continue;
            }

            $changes = $uow->getEntityChangeSet($entity);

            if (isset($changes['status'])) {
                // $oldStatus = $changes['status'][0];
                /** @var \Irmag\SiteBundle\Entity\OrderStatus $newStatus */
                $newStatus = $changes['status'][1];

                $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::ORDER_STATUS);
                $url = $this->router->generate('irmag_profile_order', ['id' => $entity->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
                $this->userNotificationQueueManager->create($event, $entity->getUser(), sprintf('#%d - %s.', $entity->getId(), $newStatus->getName()), $url, true);
            }
        }
    }
}
