<?php

namespace Irmag\ForumBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\ForumBundle\Entity\ForumTopic;

class ForumTopicEvent extends Event
{
    /**
     * @var ForumTopic
     */
    private $topic;

    /**
     * @param ForumTopic $topic
     */
    public function __construct(ForumTopic $topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return ForumTopic
     */
    public function getForumTopic(): ForumTopic
    {
        return $this->topic;
    }
}
