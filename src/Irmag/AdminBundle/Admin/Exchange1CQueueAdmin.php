<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Irmag\SiteBundle\Config;

class Exchange1CQueueAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
            ->add('postpone_queue_processing', $this->getRouterIdParameter().'/postpone_queue_processing')
            ->add('up_queue_priority', $this->getRouterIdParameter().'/up_queue_priority')
            ->add('down_queue_priority', $this->getRouterIdParameter().'/down_queue_priority')
            ->add('download_xml', $this->getRouterIdParameter().'/download_xml')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true])
            ->add('command', null, ['disabled' => true])
            ->add('file', null, ['disabled' => true])
            ->add('status', ChoiceType::class, [
                'choices' => array_flip($this->getSubject()->statuses),
            ])
            ->add('createdAt', null, [
                'format' => Config::DATETIME_FORMAT_RFC3339,
                'widget' => 'single_text',
            ])
            ->add('runAt', null, [
                'format' => Config::DATETIME_FORMAT_RFC3339,
                'widget' => 'single_text',
                'disabled' => true,
            ])
            ->add('pid', null, ['disabled' => true])
            ->add('attempts')
            ->add('lastOutputMessage')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('command')
            ->add('file')
            ->add('createdAt')
            ->add('runAt')
            ->add('pid')
            ->add('attempts')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('command', null, ['template' => '@IrmagAdmin/Exchange1CQueue/list__command.html.twig'])
            ->add('file')
            ->add('status', null, [
                'template' => '@IrmagAdmin/Exchange1CQueue/list__status.html.twig',
            ])
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('priority', null, ['template' => '@IrmagAdmin/Exchange1CQueue/list__priority.html.twig'])
            ->add('runAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('pid')
            ->add('attempts')
            ->add('_action', null, [
                'actions' => [
                    'stop' => [
                        'template' => '@IrmagAdmin/Exchange1CQueue/list__action_stop.html.twig',
                    ],
                    'retry' => [
                        'template' => '@IrmagAdmin/Exchange1CQueue/list__action_retry.html.twig',
                    ],
                    'download' => [
                        'template' => '@IrmagAdmin/Exchange1CQueue/list__action_download_xml.html.twig',
                    ],
                ],
            ])
        ;
    }
}
