<?php

namespace Irmag\ForumBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ForumBundle\Entity\Forum;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ForumBundle\Entity\ForumTopicTag;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumTopicSubscription;
use Irmag\ForumBundle\Exception\IrmagForumException;

class ForumService
{
    const ROLE_DISALLOW_CREATE_POST = 'ROLE_IRMAG_DISALLOW_FORUM_POST_CREATE';
    const ROLE_DISALLOW_CREATE_TOPIC = 'ROLE_IRMAG_DISALLOW_FORUM_TOPIC_CREATE';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var array
     */
    private $disallowRoles;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Создаёт тему.
     *
     * @param array $options
     *
     * @return ForumTopic
     */
    public function newTopic(array $options): ForumTopic
    {
        $user = $this->getEntityByIdOrThrowException(User::class, (int) $options['user']);
        $forum = $this->getEntityByIdOrThrowException(Forum::class, (int) $options['forum']);

        $topic = new ForumTopic();
        $topic
            ->setTitle($options['title'])
            ->setForum($forum)
            ->setUser($user)
            ->setIsImportant(true);

        if (!empty($options['description'])) {
            $topic->setDescription($options['description']);
        }

        if (!empty($options['tags'])) {
            $tags = explode(',', $options['tags']);
            $this->setTopicTags($tags, $topic);
        }

        $this->em->persist($topic);

        $postData = $options['post'];
        $postData['topic'] = $topic->getId();

        $this->createOrUpdatePost($postData);
        $this->em->flush();

        return $topic;
    }

    /**
     * @param array $options
     *
     * @return ForumPost
     */
    public function createOrUpdatePost(array $options): ForumPost
    {
        if (!empty($options['id'])) {
            $post = $this->getEntityByIdOrThrowException(ForumPost::class, (int) $options['id']);
        }

        $post = $post ?? new ForumPost();
        $topic = $this->getEntityByIdOrThrowException(ForumTopic::class, $options['topic']);

        if (!empty($options['in_reply_to'])) {
            $postInReplyTo = $this->getEntityByIdOrThrowException(ForumPost::class, (int) $options['in_reply_to'], false);
        }

        $user = $this->getEntityByIdOrThrowException(User::class, (int) $options['user']);
        $userModifiedBy = $this->getEntityByIdOrThrowException(User::class, (int) $options['user'], false);

        $post
            ->setText($options['text'])
            ->setTopic($topic)
            ->setInReplyTo($postInReplyTo ?? null)
            ->setUser($user)
            ->setUserModifiedBy($userModifiedBy);

        $this->em->persist($post);
        $this->em->flush();

        return $post;
    }

    /**
     * Возвращает данные о том, подписан ли пользователь на обновление темы форума.
     *
     * @param ForumTopic $topic
     *
     * @return bool
     */
    public function isUserSubscribedToTopic(ForumTopic $topic): bool
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if ($user instanceof User) {
            $subscriptions = $this->em->getRepository(ForumTopicSubscription::class)->findBy([
                'topic' => $topic,
                'user' => $user,
            ]);

            $isSubscribed = \count($subscriptions) > 0;
        }

        return $isSubscribed ?? false;
    }

    /**
     * Забанен ли пользователь на написание постов?
     *
     * @return bool
     */
    public function isUserBannedToWritePost(): bool
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $user instanceof User
            ? \in_array(self::ROLE_DISALLOW_CREATE_POST, $user->getRoles(), true)
            : false;
    }

    /**
     * Забанен ли пользователь на создание тем?
     *
     * @return bool
     */
    public function isUserBannedToCreateTopic(): bool
    {
        $user = $this->tokenStorage->getToken()->getUser();

        return $user instanceof User
            ? \in_array(self::ROLE_DISALLOW_CREATE_TOPIC, $user->getRoles(), true)
            : false;
    }

    /**
     * Является ли текущий пост самым последним?
     *
     * @param ForumPost $post
     *
     * @return bool
     */
    public function isTheLatestPost(ForumPost $post): bool
    {
        $topic = $post->getTopic();
        $latestPost = $this->em->getRepository(ForumPost::class)->getLatestCreatedPost($topic);

        if ($latestPost) {
            $isTheLatestPost = ($post === $latestPost);
        }

        return $isTheLatestPost ?? false;
    }

    /**
     * Получить список всех использовавшихся тегов в алфавитном порядке.
     *
     * @return array
     */
    public function getAllUserTopicsTags(): array
    {
        $tagNames = [];
        $tags = $this->em->getRepository(ForumTopicTag::class)->findAll();

        foreach ($tags as $tag) {
            $tagNames[] = $tag->getName();
        }

        sort($tagNames, SORT_LOCALE_STRING);

        return $tagNames;
    }

    /**
     * Установить теги для темы.
     *
     * @param array      $tags  Массив тегов
     * @param ForumTopic $topic Тема форума
     */
    private function setTopicTags(array $tags, ForumTopic $topic): void
    {
        if (!empty($tags)) {
            $currentTopicTags = $topic->getForumTopicTags();

            foreach ($currentTopicTags as $tag) {
                $topic->removeForumTopicTag($tag);
                $this->em->persist($topic);
            }

            foreach ($tags as $tag) {
                $topicTag = $this->em->getRepository(ForumTopicTag::class)->findOneBy(['name' => $tag]);

                if (!$topicTag) {
                    $topicTag = new ForumTopicTag();
                    $topicTag->setName($tag);
                    $topicTag->addForumTopic($topic);
                    $this->em->persist($topicTag);
                }

                $topic->addForumTopicTag($topicTag);
                $this->em->persist($topic);
            }
        }
    }

    /**
     * Вернуть экземпляр сущности по id или бросить исключение (если необходимо).
     *
     * @param string $class
     * @param int    $id
     * @param bool   $needToThrowException
     *
     * @throws IrmagForumException
     *
     * @return \object|null
     */
    private function getEntityByIdOrThrowException(string $class, int $id, bool $needToThrowException = true): ?object
    {
        $entity = $this->em->getRepository($class)->find($id);

        if (!$entity && false === $needToThrowException) {
            throw new IrmagForumException(sprintf('Could not find entity of class %s with id %d', $class, $id));
        }

        return $entity ?? null;
    }
}
