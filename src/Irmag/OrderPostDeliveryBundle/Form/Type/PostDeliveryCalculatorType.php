<?php

namespace Irmag\OrderPostDeliveryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\OrderPostDeliveryBundle\Form\Helper\PostDeliveryCalculatorHelper;

class PostDeliveryCalculatorType extends AbstractType
{
    /**
     * @var PostDeliveryCalculatorHelper
     */
    private $helper;

    /**
     * @param PostDeliveryCalculatorHelper $deliveryCalculatorHelper
     */
    public function __construct(PostDeliveryCalculatorHelper $deliveryCalculatorHelper)
    {
        $this->helper = $deliveryCalculatorHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('indexTo', IntegerType::class, [
                'label' => 'Индекс назначения',
                'data' => $this->helper->getCurrentIndex(),
                'attr' => [
                    'placeholder' => $this->helper->getIndexPlaceholder(),
                    'max' => 999999,
                ],
            ])
            ->add('weight', HiddenType::class, [
                'data' => (float) $options['weight'],
            ])
            ->add('submit', ButtonType::class, [
                'label' => 'Рассчитать',
                'attr' => [
                    'class' => 'btn btn-info post-delivery-calculator-submit',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => false,
        ]);

        $resolver->setRequired('weight');
        $resolver->setAllowedTypes('weight', 'float');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'post_calculator';
    }
}
