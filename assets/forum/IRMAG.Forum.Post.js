/**
 * Посты форуме.
 *
 * @namespace IRMAG.Forum.Post
 *
 * @requires jquery
 * @requires Routing
 * @requires Handlebars
 * @requires bootbox
 */

import Routing from '../core/modules/routing';
import { bind, go, suffix } from '../core/modules/jumper';
import postRatingTmpl from './templates/post_rating.handlebars';

IRMAG.Forum.Post = {
    init: function () {
        this.initEditor();
        this.bindCite();
        this.bindReplyTo();
        this.bindUndoReply();
        this.bindVote();
        this.bindBlockOnSubmit();

        bind('question');
    },

    /* Активировать редактор */
    initEditor: function () {
        const isAdvancedCommentator = $('#forum-form').data('advanced');
        const $forumPostText = $('#forum_post_text');
        const initialText = $forumPostText.data('initial');
        let tmpText = $forumPostText.val().trim();

        if (initialText !== '' && tmpText.indexOf(initialText.trim()) === -1) {
            tmpText = initialText + '\n\n' + tmpText;
        }

        if ($forumPostText.length > 0) {
            IRMAG.Editor.defaults.hiddenButtons.push('Heading');
            IRMAG.Editor.defaults.needToEmojifyPreview = true;

            // прикреплять видео и картинки по ссылками могут только избранные
            if (!isAdvancedCommentator) {
                IRMAG.Editor.defaults.hiddenButtons.push('cmdVideo', 'Image');
            }

            IRMAG.Editor.defaults.ajaxConfig = function (e) {
                return {
                    url: Routing.generate('irmag_markdown_to_html'),
                    data: {
                        md: e.getContent(),
                        mode: 'forum_post',
                        object: $('#forum_post_id').val(),
                    },
                };
            };

            IRMAG.Editor.init($forumPostText).setContent(tmpText);

            if (window.location.hash === suffix + 'question') {
                $forumPostText.focus();

                const cursorPosition = $forumPostText.prop('selectionStart') + tmpText.length + 3;

                $forumPostText.prop('selectionStart', cursorPosition);
                $forumPostText.prop('selectionEnd', cursorPosition);
                $forumPostText.trigger('change');
            }
        }

        IRMAG.Emoji.bindInput($forumPostText);
    },

    /* Инициализация emoji */
    initEmojify: function () {
        IRMAG.Emoji.emojify($('.forum-post-body'));
    },

    /* Ответить на пост */
    bindReplyTo: function () {
        $(document).on('click', 'a.forum-reply', function (e) {
            e.preventDefault();
            const editorId = 'forum_post_text';
            const postId = $(this).data('post');

            $('#post-reply-id').text(postId);
            $('#forum_post_in_reply_to').val(postId);
            $('.forum-post-reply-sign').show();
            go(editorId);
            $('#' + editorId).focus();
        });
    },

    /* Отменить ответ */
    bindUndoReply: function () {
        $(document).on('click', '.forum-post-reply-sign .fa-close', function (e) {
            e.preventDefault();
            $('#post-reply-id').text('');
            $('#forum_post_in_reply_to').val('');
            $('.forum-post-reply-sign').hide();
        });
    },

    /* Цитировать пост */
    bindCite: function () {
        $(document).on('click', 'a.forum-cite', function (e) {
            e.preventDefault();

            const editorId = 'forum_post_text';
            const postId = $(this).data('post');

            $.ajax({
                url: Routing.generate('irmag_forum_get_quote'),
                dataType: 'json',
                data: {
                    id: postId,
                },
                success: function (data) {
                    if (data.success) {
                        $('#forum_post_text').data('markdown').setContent(data.md);
                        go(editorId);
                        $('#' + editorId).focus();
                    }
                },
                error: function () {
                    bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз');
                },
            });
        });
    },

    /* Голосовать за пост */
    bindVote: function () {
        $(document).on('click', 'i.forum-post-vote', function (e) {
            e.preventDefault();

            const score = parseInt($(this).data('score'), 10);
            const rate = parseInt($(this).parent().find('.post-rating').text(), 10);
            const id = parseInt($(this).data('id'), 10);
            const $parent = $(this).parent();

            $.ajax({
                url: Routing.generate('irmag_forum_vote'),
                dataType: 'json',
                data: {
                    id: id,
                    score: score,
                },
                success: function (data) {
                    if (data.success) {
                        $parent.find('.forum-post-vote').remove();
                        $parent.find('.post-rating').remove();

                        $parent.append(postRatingTmpl({ rating: parseInt((rate + score), 10) }));
                    }
                },
                error: function () {
                    bootbox.alert('Что-то пошло не так. Пожалуйста, повторите ещё раз.');
                },
            });
        });
    },

    /* Показать спиннер, чтобы пользователи не тыкали */
    bindBlockOnSubmit: function () {
        $(document).on('click', 'button#forum_post_button', function () {
            $('.forum-new-post-form').addClass('block-loading');
            $('.forum-spinner').show();
        });
    },
};
