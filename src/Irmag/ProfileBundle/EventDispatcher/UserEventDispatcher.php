<?php

namespace Irmag\ProfileBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Event\UserEvent;
use Irmag\ProfileBundle\Events;

class UserEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param User $user
     */
    public function dispatchUserRegistrationEmailConfirmed(User $user): void
    {
        $this->eventDispatcher->dispatch(Events::USER_REGISTRATION_EMAIL_CONFIRMED, $this->getUser($user));
    }

    /**
     * @param User $user
     *
     * @return UserEvent
     */
    private function getUser(User $user): UserEvent
    {
        return new UserEvent($user);
    }
}
