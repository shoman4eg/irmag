/**
 * @namespace IRMAG.Registration
 *
 * @requires jquery
 * @requires jquery.multiTelephoneField
 * @requires Routing
 */

import Routing from '../core/modules/routing';
import log from '../core/modules/logger';

IRMAG.Registration = {
    form: null,
    username: null,
    passwordFirst: null,
    passwordSecond: null,
    email: null,
    hasEmail: null,
    hasTelephone: null,

    init: function () {
        log('[Registration]', 'init');

        this.form = $('form[name="registration"]');
        this.username = $('#registration_username');
        this.passwordFirst = $('#registration_password_first');
        this.passwordSecond = $('#registration_password_second');
        this.email = $('#registration_email');
        this.hasEmail = $('#registration_hasEmail');
        this.hasTelephone = $('#registration_hasTelephone');

        this.initMultiTelephoneField();
        this.initCheckLogin();
        this.initHasEmail();
        this.initHasTelephone();
        this.initValidate();

        // do not dd it, scroll does not work if element is hidden in DOM.
        IRMAG.Site.bindScrollToError();
    },

    initCheckLogin: function () {
        this.username.on('blur change paste', function () {
            const login = $(this).val();
            const $parent = $(this).parent('.form-group');
            const $submit = $('#registration_save');

            $.ajax({
                url: Routing.generate('security_check_login'),
                data: {
                    login: login,
                },
                success: function (data) {
                    const $alert = $parent.children('.alert-danger');

                    if (false === data.isFree) {
                        $parent.addClass('has-error');
                        $submit.attr('disabled', true);

                        if ($alert.length === 0) {
                            $parent.append([
                                '<div class="alert alert-danger irmag-error mt-5 mb-0">',
                                    '<span class="fa fa-warning fa-fw"></span>',
                                    'Этот логин уже занят, попробуйте другой',
                                '</div>',
                            ].join('\n'));
                        }
                    } else {
                        $parent.removeClass('has-error');
                        $submit.removeAttr('disabled');

                        if ($alert.length > 0) {
                            $alert.remove();
                        }
                    }
                },
            });
        });
    },

    initHasEmail: function () {
        const self = this;
        const $checkboxSubscribed = $('#email-subscribed');

        this.email.on('keyup.me', function () {
            if ($(this).val()) {
                $checkboxSubscribed.removeClass('hidden')
                    .find('input').prop('checked', true);
            } else {
                $checkboxSubscribed.addClass('hidden')
                    .find('input').prop('checked', false);
            }
        }).trigger('keyup.me');

        const $emailRow = $('#email-row');
        const $checkboxHasEmail = this.hasEmail;

        $checkboxHasEmail.on('change.me', function () {
            if ($(this).is(':checked')) {
                $emailRow.removeClass('hidden');
            } else {
                $emailRow.addClass('hidden');
                self.email.val('').trigger('keyup.me');
            }
        }).trigger('change.me');
    },

    initHasTelephone: function () {
        const $telephoneRow = $('#telephone-row');

        this.hasTelephone.on('change', function () {
            if ($(this).is(':checked')) {
                $telephoneRow.removeClass('hidden');
            } else {
                $telephoneRow.addClass('hidden');
            }
        }).trigger('change');
    },

    initMultiTelephoneField: function () {
        $('.multi-telephone-field').multiTelephoneField({
            onCheckboxChanged: function (checkbox) {
                if ($(checkbox).is(':checked')) {
                    IRMAG.Profile.enableSmsSubscribeRow();
                } else {
                    IRMAG.Profile.disableSmsSubscribeRow();
                }
            },
        });
    },

    initValidate: function () {
        const self = this;
        // validate password
        const passwordFirstElm = this.passwordFirst.get(0);
        const passwordSecondElm = this.passwordSecond.get(0);

        this.passwordFirst.add(this.passwordSecond).on('change', function () {
            if (self.passwordFirst.val() !== self.passwordSecond.val()) {
                passwordSecondElm.setCustomValidity('Пароли не совпадают');
            } else {
                passwordSecondElm.setCustomValidity('');
            }

            if (self.passwordSecond.val().length < 6) {
                passwordFirstElm.setCustomValidity('Поле \'Пароль\' должно быть не менее 6 символов');
            } else {
                passwordFirstElm.setCustomValidity('');
            }
        });
    },
};
