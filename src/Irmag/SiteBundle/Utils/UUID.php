<?php

namespace Irmag\SiteBundle\Utils;

class UUID
{
    const VALID_PATTERN = '^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$';

    /**
     * @param string $uuid
     *
     * @return bool
     */
    public static function isValid(string $uuid): bool
    {
        if (!preg_match('/'.self::VALID_PATTERN.'/', $uuid)) {
            return false;
        }

        return true;
    }
}
