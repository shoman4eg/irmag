<?php

namespace Irmag\SiteBundle\Service\PriceManager\Adapter;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\ProfileBundle\Entity\User;

class UserAdapter
{
    /**
     * @var int|null
     */
    protected $discount;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Возвращает скидку пользователя.
     *
     * @return int|null
     */
    public function getDiscount(): ?int
    {
        if (null === $this->discount) {
            $token = $this->tokenStorage->getToken();

            if (null !== $token && $token->getUser() instanceof User) {
                $this->discount = $token->getUser()->getDiscount();
            } else {
                $this->discount = 0;
            }
        }

        return $this->discount;
    }
}
