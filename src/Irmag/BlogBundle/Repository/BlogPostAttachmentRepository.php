<?php

namespace Irmag\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Irmag\AttachmentBundle\Traits\AttachmentsRepositoryTrait;

class BlogPostAttachmentRepository extends EntityRepository
{
    use AttachmentsRepositoryTrait;
}
