<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Mailer\ApiMailer;

/**
 * @Rest\RouteResource("Send_Email", pluralize=false)
 */
class SendEmailController extends AbstractRestController
{
    /**
     * Отправить Email.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Sender")
     *
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\SendEmailType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        $result = $this->get(ApiMailer::class)->sendMessage($content['to'], ['subject' => $content['subject'], 'text' => $content['body']]);

        return new View(['result' => $result], Response::HTTP_CREATED);
    }
}
