<?php

namespace Irmag\ThreadCommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(
 *     name="threads",
 *     indexes={
 *         @ORM\Index(columns={"avg_rating"}),
 *     }
 * )
 * @ORM\Entity
 */
class Thread
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isCommentable;

    /**
     * Вкл./Выкл. очки комментария Like/Dislike.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isScorable;

    /**
     * Вкл./Выкл рейтинг от 1 до 5.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isRatable;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $commentsTotalCount;

    /**
     * Ср.знач. оценки.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=1, options={"default": 0}, nullable=true)
     */
    private $avgRating;

    /**
     * @var Collection|ThreadComment[]
     *
     * @ORM\OneToMany(targetEntity="ThreadComment", mappedBy="thread")
     */
    private $comments;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $ownerFQCN;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $ownerId;

    /**
     * Массив с информацией для генерации url.
     *
     * [
     *     'name' => '...'
     *     'parameters' => [
     *         'id' => '...',
     *         'slug => '...,
     *     ]
     * ]
     *
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $ownerRoute;

    public function __construct()
    {
        $this->isCommentable = true;
        $this->isScorable = true;
        $this->isRatable = true;
        $this->commentsTotalCount = 0;
        $this->avgRating = 0.00;
        $this->comments = new ArrayCollection();
    }

    /**
     * @return array
     */
    public static function getChoiceList(): array
    {
        return [
            'Товар' => 'Element',
            'Новость' => 'News',
            'Акция' => 'Action',
            'Блог' => 'BlogPost',
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsCommentable(): ?bool
    {
        return $this->isCommentable;
    }

    public function setIsCommentable(bool $isCommentable): self
    {
        $this->isCommentable = $isCommentable;

        return $this;
    }

    public function getIsScorable(): ?bool
    {
        return $this->isScorable;
    }

    public function setIsScorable(bool $isScorable): self
    {
        $this->isScorable = $isScorable;

        return $this;
    }

    public function getIsRatable(): ?bool
    {
        return $this->isRatable;
    }

    public function setIsRatable(bool $isRatable): self
    {
        $this->isRatable = $isRatable;

        return $this;
    }

    public function getCommentsTotalCount(): ?int
    {
        return $this->commentsTotalCount;
    }

    public function setCommentsTotalCount(int $commentsTotalCount): self
    {
        $this->commentsTotalCount = $commentsTotalCount;

        return $this;
    }

    public function getAvgRating()
    {
        return $this->avgRating;
    }

    public function setAvgRating($avgRating): self
    {
        $this->avgRating = $avgRating;

        return $this;
    }

    public function getOwnerFQCN(): ?string
    {
        return $this->ownerFQCN;
    }

    public function setOwnerFQCN(string $ownerFQCN): self
    {
        $this->ownerFQCN = $ownerFQCN;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function setOwnerId(int $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getOwnerRoute(): ?array
    {
        return $this->ownerRoute;
    }

    public function setOwnerRoute(array $ownerRoute): self
    {
        $this->ownerRoute = $ownerRoute;

        return $this;
    }

    /**
     * @return Collection|ThreadComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(ThreadComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setThread($this);
        }

        return $this;
    }

    public function removeComment(ThreadComment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getThread() === $this) {
                $comment->setThread(null);
            }
        }

        return $this;
    }
}
