<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementKit;

class ElementKitParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root kit')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->truncate(ElementKit::class);

        $this->getCrawler()->filter('root kit')->each(function (Crawler $node) {
            $elementKit = new ElementKit();

            $node->filter('element')->each(function (Crawler $node, $i) use (&$elementKit) {
                $elementId = (int) $node->filter('element')->attr('id');

                // только четыре товара в комплекте
                if ($i > 3) {
                    $this->output->writeln('');
                    $this->output->writeln(sprintf('<error>Found more than four products, element "%d" skipped.</error>', $elementId));

                    return;
                }

                $element = $this->em->getRepository(Element::class)->find($elementId);
                $forAllgroupBy = (int) $node->filter('element')->attr('for-all-group-by');

                if (!$element) {
                    $this->output->writeln('');
                    $this->output->writeln(sprintf('<error>Element with id "%d" not found, skipped.</error>', $elementId));

                    return;
                }

                $fieldName = 'element'.($i + 1);

                $this->accessor->setValue($elementKit, $fieldName, $element);

                if ($forAllgroupBy) {
                    $this->accessor->setValue($elementKit, $fieldName.'ForAllGroupBy', $forAllgroupBy);
                }

                $this->em->persist($elementKit);
            });

            ++$this->operations;
            $this->flushIfNeed();

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }
        });
    }
}
