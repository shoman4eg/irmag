<?php

namespace Irmag\SiteBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\CoreBundle\Utils\Formatter;
use Irmag\SiteBundle\Service\FileManager\FileManager;

class IrmagSearchOrphanFileCommand extends Command
{
    protected static $defaultName = 'irmag:files:find_orphan';

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @param FileManager $fileManager
     */
    public function __construct(
        FileManager $fileManager
    ) {
        $this->fileManager = $fileManager;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Finds and deletes orphaned garbage files, that have no links with entity objects.')
            ->addOption('list', null, InputOption::VALUE_NONE, 'List all orphaned files')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Counting files on disk ...</comment>');
        $files = $this->fileManager->getFiles();
        $diskFilePaths = $this->fileManager->getFilePaths($files);
        $totalFiles = \count($files);
        $totalSize = Formatter::formatBytes($this->fileManager->getTotalFileSize($files));

        $output->writeln('<comment>Counting files in database ...</comment>');
        $registeredFiles = $this->fileManager->getRegisteredFiles();
        $registeredFilePaths = [];

        foreach ($registeredFiles as $file) {
            $registeredFilePaths[] = $file->getPath();
        }

        $totalRegisteredFiles = \count($registeredFilePaths);
        $output->writeln('<comment>Counting orphaned files ...</comment>');
        $orphanedSize = Formatter::formatBytes(0);
        $orphanedFilePaths = array_diff($diskFilePaths, $registeredFilePaths);

        if (!empty($orphanedFilePaths)) {
            $orphanedFiles = $this->fileManager->filterFilesByPaths($files, $orphanedFilePaths);
            $orphanedSize = Formatter::formatBytes($this->fileManager->getTotalFileSize($orphanedFiles));
        }

        $brokenLinks = array_diff($registeredFilePaths, $diskFilePaths);

        $output->writeln('');
        $output->writeln(sprintf('Total files on disk: <info>%d</info> (%s)', $totalFiles, $totalSize));
        $output->writeln(sprintf('Total files in database: <info>%d</info>', $totalRegisteredFiles));
        $output->writeln(sprintf('Orphaned files on disk: <info>%d</info> (%s)', \count($orphanedFilePaths), $orphanedSize));
        $output->writeln(sprintf('Entities with broken file links: <info>%d</info>', \count($brokenLinks)));

        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('list')) {
            $output->writeln('');

            if (\count($orphanedFilePaths) > 0) {
                $output->writeln('<comment>Orphaned files:</comment>');
                $output->writeln('');
                foreach ($orphanedFilePaths as $path) {
                    $output->writeln(sprintf('<info>%s</info>', $path));
                }
            } else {
                $output->writeln('<comment>Nothing to list: no orphaned files found.</comment>');
            }
        }

        if (\count($orphanedFilePaths) > 0 && !empty($orphanedFiles) && $io->confirm('Clear orphaned files?', false)) {
            $output->writeln('<comment>Clearing orphaned files ...</comment>');
            $this->fileManager->remove($orphanedFiles);
            $io->success('Done');
        }

        if (\count($brokenLinks) > 0 && $io->confirm('Remove broken links entities from DB?', false)) {
            $output->writeln('<comment>Removing entites ...</comment>');
            $this->fileManager->clearEntitiesByPaths($brokenLinks);
            $io->success('Done');
        }
    }
}
