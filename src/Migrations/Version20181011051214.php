<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181011051214 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_notification_subscriptions_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_notification_events_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_notification_subscriptions (id INT NOT NULL, user_id INT NOT NULL, event_id INT NOT NULL, is_push_enabled BOOLEAN DEFAULT \'false\' NOT NULL, is_email_enabled BOOLEAN DEFAULT \'false\' NOT NULL, is_site_enabled BOOLEAN DEFAULT \'false\' NOT NULL, is_sms_enabled BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B9927267A76ED395 ON users_notification_subscriptions (user_id)');
        $this->addSql('CREATE INDEX IDX_B992726771F7E88B ON users_notification_subscriptions (event_id)');
        $this->addSql('CREATE TABLE users_notification_events (id INT NOT NULL, name VARCHAR(32) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE users_notification_subscriptions ADD CONSTRAINT FK_B9927267A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_notification_subscriptions ADD CONSTRAINT FK_B992726771F7E88B FOREIGN KEY (event_id) REFERENCES users_notification_events (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql("
            INSERT INTO users_notification_events (id, name, description) VALUES
            (nextval('users_notification_events_id_seq'), 'favorite_element_is_active', 'Товар из отслеживаемого Избранного на остатках'),
            (nextval('users_notification_events_id_seq'), 'favorite_element_price', 'Изменилась цена у товара из отслеживаемого Избранного'),
            (nextval('users_notification_events_id_seq'), 'action_new', 'Новая акция на сайте'),
            (nextval('users_notification_events_id_seq'), 'order_status', 'Изменился статус заказа'),
            (nextval('users_notification_events_id_seq'), 'bonus_send', 'Начислен бонус'),
            (nextval('users_notification_events_id_seq'), 'news_new', 'Новость на сайте'),
            (nextval('users_notification_events_id_seq'), 'forum_post_reply', 'Ответ на сообщение на форуме'),
            (nextval('users_notification_events_id_seq'), 'blog_comment_reply', 'Ответ на сообщение в блоге'),
            (nextval('users_notification_events_id_seq'), 'site_comment_reply', 'Ответ на сообщение в акции, новости или в товаре'),
            (nextval('users_notification_events_id_seq'), 'blog_post_new', 'Новый обзор в блоге'),
            (nextval('users_notification_events_id_seq'), 'newsletter', 'Массовая рассылка с уникальными предложениями')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_subscriptions DROP CONSTRAINT FK_B992726771F7E88B');
        $this->addSql('DROP SEQUENCE users_notification_subscriptions_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_notification_events_id_seq CASCADE');
        $this->addSql('DROP TABLE users_notification_subscriptions');
        $this->addSql('DROP TABLE users_notification_events');
    }
}
