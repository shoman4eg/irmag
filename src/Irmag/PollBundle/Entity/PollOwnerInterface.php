<?php

namespace Irmag\PollBundle\Entity;

interface PollOwnerInterface
{
    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Set poll.
     *
     * @param Poll|null $poll
     *
     * @return self
     */
    public function setPoll(?Poll $poll);

    /**
     * Get poll.
     *
     * @return Poll|null
     */
    public function getPoll(): ?Poll;

    /**
     * Set ownerRoute.
     */
    public function setPollOwnerRoute(): void;
}
