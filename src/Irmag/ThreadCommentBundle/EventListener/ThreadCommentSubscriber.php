<?php

namespace Irmag\ThreadCommentBundle\EventListener;

use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;
use Irmag\ThreadCommentBundle\Manager\ThreadManager;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;

class ThreadCommentSubscriber implements EventSubscriber
{
    /**
     * @var ThreadManager
     */
    private $threadManager;

    /**
     * @param ThreadManager $threadManager
     */
    public function __construct(
        ThreadManager $threadManager
    ) {
        $this->threadManager = $threadManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $this->update($args);
    }

    /**
     * Пересчитывает кол-во комментариев и среднее значение рейтинга.
     *
     * @param LifecycleEventArgs $args
     */
    private function update(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof ThreadComment) {
            if ($thread = $entity->getThread()) {
                $this->threadManager
                    ->setThread($thread)
                    ->updateCommentsTotalCount()
                    ->updateAvgRating()
                    ->save();
            }
        }
    }
}
