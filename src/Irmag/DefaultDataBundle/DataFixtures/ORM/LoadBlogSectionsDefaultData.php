<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\BlogBundle\Entity\BlogSection;

class LoadBlogSectionsDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $blogSections = [
            ['БИО продукты', 'bio-products'],
            ['Товары для дома', 'homeware-goods'],
            ['Профессиональные средства', 'professional-tools'],
            ['Косметика и парфюмерия', 'cosmetics-and-perfumery'],
        ];

        foreach ($blogSections as $key => $data) {
            $existingSection = $manager->getRepository(BlogSection::class)->findOneBy(['shortlink' => $data[1]]);

            if (!$existingSection) {
                $section = new BlogSection();
                $section->setId($key + 1);
                $section->setName($data[0]);
                $section->setShortlink($data[1]);

                $manager->persist($section);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 14;
    }
}
