<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\ActionElement;

class ActionElementRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Action $action
     * @param bool   $showAll
     *
     * @return array
     */
    public function getElementsByAction(Action $action, bool $showAll)
    {
        $actionElements = $this->createQueryBuilder('ae')
            ->where('ae.action = :action')
            ->setParameter('action', $action)
            ->getQuery()
            ->getResult();

        $ids = array_map(function (ActionElement $actionElement) {
            $element = $actionElement->getElement();

            if ($element) {
                return $element->getId();
            }
        }, $actionElements);

        if (empty($ids)) {
            return [];
        }

        return $this->getElementsByIds($ids, $showAll);
    }

    /**
     * @param array $ids
     * @param bool  $showAll
     *
     * @return array
     */
    private function getElementsByIds(array $ids, bool $showAll)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('e, ep, epf, etp, t')
            ->from(Element::class, 'e')
            ->leftJoin('e.pictures', 'ep')
            ->leftJoin('ep.file', 'epf')
            ->leftJoin('e.tonePicture', 'etp')
            ->leftJoin('e.thread', 't')
            ->where('e.id IN (:ids)');

        if (false === $showAll) {
            $qb->andWhere('e.isActive = true');
        }

        return $qb
            ->setParameter('ids', $ids)
            ->orderBy('e.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
