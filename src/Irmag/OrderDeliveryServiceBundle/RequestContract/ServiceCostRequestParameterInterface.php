<?php

namespace Irmag\OrderDeliveryServiceBundle\RequestContract;

use Irmag\OrderDeliveryServiceBundle\Model\OrderDeliveryServiceAddress;

interface ServiceCostRequestParameterInterface
{
    /**
     * @return OrderDeliveryServiceAddress
     */
    public function getPickupAddress(): OrderDeliveryServiceAddress;

    /**
     * @param OrderDeliveryServiceAddress $pickupAddress
     */
    public function setPickupAddress(OrderDeliveryServiceAddress $pickupAddress);

    /**
     * @return OrderDeliveryServiceAddress
     */
    public function getDeliveryAddress(): OrderDeliveryServiceAddress;

    /**
     * @param OrderDeliveryServiceAddress $deliveryAddress
     */
    public function setDeliveryAddress(OrderDeliveryServiceAddress $deliveryAddress);

    /**
     * @return bool
     */
    public function getIsSelfPickup(): bool;

    /**
     * @param bool $isSelfPickup
     */
    public function setIsSelfPickup(bool $isSelfPickup);

    /**
     * @return bool
     */
    public function getIsSelfDelivery(): bool;

    /**
     * @param bool $isSelfDelivery
     */
    public function setIsSelfDelivery(bool $isSelfDelivery);

    /**
     * @return float
     */
    public function getWeight(): float;

    /**
     * @param float $weight
     */
    public function setWeight(float $weight);

    /**
     * @return float
     */
    public function getVolume(): ?float;

    /**
     * @param float $volume
     */
    public function setVolume(float $volume);

    /**
     * @param string $serviceCode
     */
    public function setServiceCode(string $serviceCode);
}
