<?php

namespace Irmag\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\AdminBundle\Controller\Traits\ActivateDeactivateTrait;
use Irmag\ThreadCommentBundle\Entity\Thread;

class ThreadCommentAdminController extends Controller
{
    use ActivateDeactivateTrait;

    public function getOwnerNameAction(Thread $thread)
    {
        if (!$repo = $this->get('doctrine.orm.default_entity_manager')->getRepository($thread->getOwnerFQCN())) {
            return null;
        }

        $owner = $repo->find($thread->getOwnerId());

        return $this->renderWithExtraParams('@IrmagAdmin/ThreadComment/thread_owner_name.html.twig', [
            'name' => (string) $owner,
            'ownerRoute' => $thread->getOwnerRoute(),
        ]);
    }

    public function payBonusAction()
    {
        /** @var \Irmag\ThreadCommentBundle\Entity\ThreadComment $object */
        if (!$object = $this->admin->getSubject()) {
            throw new NotFoundHttpException('Object not found');
        }

        $bonusManager = $this->container->get(BonusManager::class)->setUser($object->getUser());

        if ($object->getIsBonusPayed()) {
            $bonusManager->update(-1, 'Отмена за комментарий к товару');
            $object->setIsBonusPayed(false);

            $this->addFlash('sonata_flash_success', 'Бонус был отменён');
        } else {
            $bonusManager->update(1, 'За комментарий к товару');
            $object->setIsBonusPayed(true);

            $this->addFlash('sonata_flash_success', 'Бонус начислен');
        }

        $this->admin->update($object);

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }
}
