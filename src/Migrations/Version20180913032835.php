<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180913032835 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE user_push_queue_user_push_id (user_push_queue_id INT NOT NULL, user_push_id_id INT NOT NULL, PRIMARY KEY(user_push_queue_id, user_push_id_id))');
        $this->addSql('CREATE INDEX IDX_A7EEA862CDA51BA4 ON user_push_queue_user_push_id (user_push_queue_id)');
        $this->addSql('CREATE INDEX IDX_A7EEA862D6A8EA01 ON user_push_queue_user_push_id (user_push_id_id)');
        $this->addSql('ALTER TABLE user_push_queue_user_push_id ADD CONSTRAINT FK_A7EEA862CDA51BA4 FOREIGN KEY (user_push_queue_id) REFERENCES users_push_queue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_push_queue_user_push_id ADD CONSTRAINT FK_A7EEA862D6A8EA01 FOREIGN KEY (user_push_id_id) REFERENCES users_push_ids (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE user_push_queue_user_push_id');
    }
}
