<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewableInterface;
use Irmag\PollBundle\Entity\Traits\PollTrait;
use Irmag\PollBundle\Entity\PollOwnerInterface;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="forum_topics")
 * @ORM\Entity(repositoryClass="\Irmag\ForumBundle\Repository\ForumTopicRepository")
 */
class ForumTopic implements IrmagViewableInterface, PollOwnerInterface
{
    use TimestampableEntity;
    use PollTrait;

    const VIEW_CLASS_NAME = ForumTopicView::class;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Название темы.
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * Форум, к которому относится тема.
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ForumBundle\Entity\Forum", inversedBy="forumTopics")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $forum;

    /**
     * Тема важная.
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isImportant;

    /**
     * Признак активности (true - активна, false - закрыта).
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * Скрытая тема или нет
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isHidden;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSpecial;

    /**
     * Создатель темы.
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * Описание темы.
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * Теги темы.
     *
     * @ORM\ManyToMany(targetEntity="ForumTopicTag", inversedBy="forumTopics")
     * @ORM\JoinTable(name="forum_topics_tags")
     */
    private $forumTopicTags;

    /**
     * @ORM\OneToMany(targetEntity="ForumPost", mappedBy="topic")
     */
    private $topicPosts;

    /**
     * @ORM\OneToMany(targetEntity="\Irmag\ForumBundle\Entity\ForumVisit", mappedBy="topic")
     */
    private $visits;

    /**
     * @ORM\OneToMany(targetEntity="ForumTopicSubscription", mappedBy="topic")
     */
    private $subscriptions;

    /**
     * @ORM\OneToOne(targetEntity="ForumPost")
     * @ORM\JoinColumn(name="last_post_id", referencedColumnName="id")
     */
    private $lastPost;

    /**
     * @ORM\Column(type="integer", name="total_posts", options={"default": 0})
     */
    private $totalPosts;

    /**
     * @ORM\Column(type="integer", name="views", options={"default": 0})
     */
    private $views;

    public function __construct()
    {
        $this->isImportant = false;
        $this->isActive = true;
        $this->isHidden = false;
        $this->isSpecial = false;
        $this->totalPosts = 0;
        $this->views = 0;
        $this->topicPosts = new ArrayCollection();
        $this->visits = new ArrayCollection();
        $this->forumTopicTags = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->title;
    }

    /**
     * XXX: Custom for PollBundle.
     */
    public function setPollOwnerRoute(): void
    {
        $this->getPoll()->setOwnerRoute([
            'name' => 'irmag_forum_goto_topic',
            'parameters' => [
                'id' => $this->getId(),
            ],
        ]);
    }

    /**
     * XXX: custom.
     * Get view object class.
     *
     * @return string
     */
    public function getViewClass(): string
    {
        return self::VIEW_CLASS_NAME;
    }

    /**
     * Get all topic tags as comma-separated string.
     * XXX: custom, plz delete fucking to array transform and refactor fucking tags.
     *
     * @return string
     */
    public function getPostTagsAsString(): string
    {
        $tags = $this->getForumTopicTags()->toArray();
        $tagStr = '';

        foreach ($tags as $key => $tag) {
            $tagStr .= (0 === $key || $key === \count($tags) - 1) ? $tag->getName() : ', '.$tag->getName();
        }

        return $tagStr;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setIsImportant(bool $isImportant): self
    {
        $this->isImportant = $isImportant;

        return $this;
    }

    public function getIsImportant(): ?bool
    {
        return $this->isImportant;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsHidden(bool $isHidden): self
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    public function getIsHidden(): ?bool
    {
        return $this->isHidden;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setForum(?Forum $forum): self
    {
        $this->forum = $forum;

        return $this;
    }

    public function getForum(): ?Forum
    {
        return $this->forum;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function addTopicPost(ForumPost $topicPost): self
    {
        if (!$this->topicPosts->contains($topicPost)) {
            $this->topicPosts[] = $topicPost;
            $topicPost->setTopic($this);
        }

        return $this;
    }

    public function removeTopicPost(ForumPost $topicPost): self
    {
        if ($this->topicPosts->contains($topicPost)) {
            $this->topicPosts->removeElement($topicPost);
            // set the owning side to null (unless already changed)
            if ($topicPost->getTopic() === $this) {
                $topicPost->setTopic(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumPost[]
     */
    public function getTopicPosts(): Collection
    {
        return $this->topicPosts;
    }

    public function addVisit(ForumVisit $visit): self
    {
        if (!$this->visits->contains($visit)) {
            $this->visits[] = $visit;
            $visit->setTopic($this);
        }

        return $this;
    }

    public function removeVisit(ForumVisit $visit): self
    {
        if ($this->visits->contains($visit)) {
            $this->visits->removeElement($visit);
            // set the owning side to null (unless already changed)
            if ($visit->getTopic() === $this) {
                $visit->setTopic(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumVisit[]
     */
    public function getVisits(): Collection
    {
        return $this->visits;
    }

    public function addForumTopicTag(ForumTopicTag $forumTopicTag): self
    {
        if (!$this->forumTopicTags->contains($forumTopicTag)) {
            $this->forumTopicTags[] = $forumTopicTag;
        }

        return $this;
    }

    public function removeForumTopicTag(ForumTopicTag $forumTopicTag): self
    {
        if ($this->forumTopicTags->contains($forumTopicTag)) {
            $this->forumTopicTags->removeElement($forumTopicTag);
        }

        return $this;
    }

    /**
     * @return Collection|ForumTopicTag[]
     */
    public function getForumTopicTags(): Collection
    {
        return $this->forumTopicTags;
    }

    public function addSubscription(ForumTopicSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setTopic($this);
        }

        return $this;
    }

    public function removeSubscription(ForumTopicSubscription $subscription): self
    {
        if ($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
            // set the owning side to null (unless already changed)
            if ($subscription->getTopic() === $this) {
                $subscription->setTopic(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumTopicSubscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function setLastPost(?ForumPost $lastPost): self
    {
        $this->lastPost = $lastPost;

        return $this;
    }

    public function getLastPost(): ?ForumPost
    {
        return $this->lastPost;
    }

    public function setTotalPosts(int $totalPosts): self
    {
        $this->totalPosts = $totalPosts;

        return $this;
    }

    public function getTotalPosts(): ?int
    {
        return $this->totalPosts;
    }

    public function setViews(int $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getViews(): int
    {
        return $this->views;
    }

    public function setIsSpecial(bool $isSpecial): self
    {
        $this->isSpecial = $isSpecial;

        return $this;
    }

    public function getIsSpecial(): ?bool
    {
        return $this->isSpecial;
    }
}
