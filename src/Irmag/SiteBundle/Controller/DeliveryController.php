<?php

namespace Irmag\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\SiteBundle\Service\GeoLocator\GeoLocator;

class DeliveryController extends Controller
{
    /**
     * @Route("/commondelivery", name="irmag_get_common_delivery", host="%irmag_site_domain%", options={"expose": true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $weight = $request->query->getInt('weight');
        $volume = $request->query->getInt('volume');
        $geolocator = $this->get(GeoLocator::class);

        if ('RU' === $geolocator->getCurrentCountry() && $geolocator->isAnotherCity()) {
            $data = $geolocator->getData();
            $cityPrediction = $data['city']['name_ru'];
        }

        return $this->render('@IrmagSite/Delivery/delivery.html.twig', [
            'weight' => $weight,
            'volume' => $volume,
            'cityPrediction' => $cityPrediction ?? '',
        ]);
    }

    /**
     * @Route("/orderdelivery", name="irmag_get_order_delivery", host="%irmag_site_domain%", options={"expose": true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderAction(Request $request)
    {
        $weight = $request->query->getInt('weight');
        $volume = $request->query->getInt('volume');
        $deliveryMethodShortname = $request->get('deliveryMethod');

        return $this->render('@IrmagSite/Delivery/delivery-at-order.html.twig', [
            'weight' => $weight,
            'volume' => $volume,
            'deliveryMethodShortname' => $deliveryMethodShortname,
        ]);
    }

    /**
     * @Route("/confirm_city", name="irmag_delivery_confirm_city", options={"expose": true}, condition="request.isXmlHttpRequest()", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function confirmCityPredictionAction(Request $request)
    {
        $session = $this->get('session');
        $geolocator = $this->get(GeoLocator::class);
        $session->set(GeoLocator::SESSION_CITY_PREDICTION_CONFIRMED_LITERAL, true);

        // по умолчанию - DPD
        $deliveryService = $this->get('doctrine.orm.entity_manager')->getRepository(OrderDeliveryService::class)->findOneBy(['shortname' => 'dpd']);
        $deliveryServiceCity = $geolocator->getCurrentOrderDeliveryServiceCity($deliveryService);

        if (!empty($deliveryServiceCity)) {
            $this->get(OrderDeliveryServiceManager::class)->saveDeliveryServiceData([
                'deliveryService' => $deliveryService->getId(),
                'deliveryCity' => $deliveryServiceCity->getId(),
            ]);
        }

        $currentZip = $geolocator->getCurrentMainIndex();
        $session->set(GeoLocator::SESSION_PREDICTION_POST_INDEX, $currentZip);

        return new JsonResponse([
            'success' => true,
        ]);
    }

    /**
     * @Route("/dismiss_city", name="irmag_delivery_dismiss_city", options={"expose": true}, condition="request.isXmlHttpRequest()", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function dismissCityPrediction(Request $request)
    {
        $this->get('session')->set(GeoLocator::SESSION_CITY_PREDICTION_CONFIRMED_LITERAL, false);

        return new JsonResponse([
            'success' => true,
        ]);
    }
}
