<?php

namespace Irmag\SiteBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Irmag\SiteBundle\Utils\BonusCalculator;

class BonusCalculatorExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('get_bonus_percent', [$this, 'getBonusPercentByPrice']),
            new TwigFunction('get_bonus', [$this, 'getBonusByPrice']),
        ];
    }

    /**
     * @param float $price Цена
     *
     * @return int
     */
    public function getBonusPercentByPrice(float $price): int
    {
        return BonusCalculator::getBonusPercentByPrice($price);
    }

    /**
     * @param float $price Цена
     *
     * @return int
     */
    public function getBonusByPrice(float $price): int
    {
        return BonusCalculator::getBonusByPrice($price);
    }
}
