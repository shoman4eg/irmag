# IrmagBasketBundle

See PHPDoc for more informations.

## Basket Factory

```php
$basketFactory = $this->get('irmag.factory.basket');
// get current user basket
$basket = $basketFactory->get();
$basket = $basketFactory->getAnonymous();
$basket = $basketFactory->getAuthenticated();
// get other user basket
$basketFactory->setUser($otherUser);
$otherUserBasket = $basketFactory->get();
// nullable other user
$basketFactory->setUser(null);
// again using current user
$basket = $basketFactory->get();
```

`BasketFactoryException`

## Basket Manager

```php
$basketManager = $this->get('irmag.manager.basket');
// basket manipulation for current user
$basketManager->addElement($element, 2);
$basketManager->removeElement($element);
$basketManager->setQuantity($element, 13);
$basketManager->getElements();
$basketManager->clear();
$basketManager->remove();
$basket = $basketManager->getBasket(); // eq $basketFactory->get(), but using simple cache
// basket manipulation for other user
$basketManager->setBasket($otherUserBasket);
$basketManager->addElement($element, 3);
$basketManager->setBasket(null);
// again using current user
$basketManager->addElement($element, 2);
```

`BasketManagerException`