<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\ProfileBundle\Entity\City;

class LoadUserCityDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var array|bool
     */
    private $citylist;

    public function __construct()
    {
        $this->citylist = file(__DIR__.'/../../Resources/data/citylist.txt');
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach ($this->citylist as $cityName) {
            $existingCity = $manager->getRepository(City::class)->findOneBy(['name' => $cityName]);

            if (!$existingCity) {
                $city = new City();
                $city->setName(trim($cityName));
                $manager->persist($city);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 4;
    }
}
