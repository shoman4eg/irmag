<?php

namespace Irmag\OnlinePaymentBundle\ProviderContract;

use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;

interface PaymentProviderInterface
{
    /**
     * Инициализировать платёж.
     *
     * @param array $options
     *
     * @return array
     */
    public function register(array $options): array;

    /**
     * Отменить транзакцию и разблокировать средства.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param int                $amount  Сумма операции (в копейках)
     *
     * @return array
     */
    public function revert(OrderOnlinePayment $payment, int $amount): array;

    /**
     * Списать средства.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param int                $amount  Сумма операции (в копейках)
     *
     * @return array
     */
    public function deposit(OrderOnlinePayment $payment, int $amount): array;

    /**
     * Вернуть средства.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param int                $amount  Сумма операции (в копейках)
     *
     * @return array
     */
    public function refund(OrderOnlinePayment $payment, int $amount): array;

    /**
     * Получить состояние платежа.
     *
     * @param OrderOnlinePayment $payment Платёж
     *
     * @return PaymentStateInterface
     */
    public function getState(OrderOnlinePayment $payment): PaymentStateInterface;

    /**
     * Получить URL формы для оплаты.
     *
     * @param OrderOnlinePayment $payment           Платёж
     * @param bool               $isDeferredPayment Является ли платеж отложенным?
     *
     * @return string|null
     */
    public function getPaymentFormUrl(OrderOnlinePayment $payment, bool $isDeferredPayment = false): ?string;
}
