<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\User;

/**
 * Голоса пользователей за пост.
 *
 * @ORM\Table(name="forum_post_votes")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ForumPostVote
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Голос (+1 или -1).
     *
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $score;

    /**
     * Пост, за который голосуют.
     *
     * @var ForumPost
     *
     * @ORM\ManyToOne(targetEntity="ForumPost", inversedBy="postVotes")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $post;

    /**
     * Проголосовавший пользователь.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\PostPersist
     */
    public function updatePostRating(): void
    {
        $currentPostRating = $this->post->getTotalScore();
        $score = $this->getScore();

        $currentPostRating += $score;
        $this->post->setTotalScore($currentPostRating);
    }

    /**
     * @ORM\PreRemove
     */
    public function excludeFromPostRating(): void
    {
        $currentPostRating = $this->post->getTotalScore();
        $score = $this->getScore();

        $currentPostRating -= $score;
        $this->post->setTotalScore($currentPostRating);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getPost(): ?ForumPost
    {
        return $this->post;
    }

    public function setPost(?ForumPost $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
