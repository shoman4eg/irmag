<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Entity\Page;

class DefaultController extends Controller
{
    /**
     * @Route("/rules/", name="irmag_forum_rules")
     */
    public function rulesAction(): Response
    {
        $page = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Page::class)
            ->findOneBy(['slug' => 'forum-rules', 'isActive' => true]);

        if (!$page) {
            throw $this->createNotFoundException('Page for forum rules did not found!');
        }

        return $this->render('@IrmagForum/Default/rules.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/denied/", name="irmag_forum_denied")
     */
    public function deniedAccessAction(): Response
    {
        return $this->render('@IrmagForum/Default/denied.html.twig');
    }
}
