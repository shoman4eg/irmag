<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\OrderPaymentMethod;

class LoadOrderPaymentMethodsDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $paymentMethods = [
            ['Наличный расчёт', 'cash', 'Вы сможете оплатить стоимость заказа наличными при получении заказа от курьера.'],
            ['Курьеру банковской картой (через терминал)', 'terminal', 'Вы можете оплатить заказ курьеру с помощью банковской карты через мобильный терминал.'],
            ['Перевод на карту Сбербанка', 'sberbank_transfer', 'Номер карты 4276180010603461'],
            ['Онлайн', 'online', 'Оплатите покупку прямо сейчас с помощью вашей карты VISA или MasterCard.'],
            ['Webmoney', 'webmoney', 'Олатите покупку через Webmoney.'],
            ['Расчётный счёт', 'legal_rs', 'Вы можете оплатить заказ переводом на банковский счет.'],
            ['Смешанный', 'mixed', 'Вы можете воспользоваться разными способами оплаты для одного заказа.'],
            ['Оплата на счёт через Сбербанк-онлайн', 'sberbank_online', '', false],
            ['Apple Pay', 'apple_pay', 'Оплатите покупку прямо сейчас с помощью  Apple Pay. Только с Safari.'],
            ['Android Pay', 'android_pay', 'Оплатите покупку прямо сейчас с помощью  AndroidPay. Только с Google Chrome.'],
        ];

        foreach ($paymentMethods as $method) {
            $existingPaymentMethod = $manager->getRepository(OrderPaymentMethod::class)->findOneBy(['shortname' => $method[1]]);

            if (!$existingPaymentMethod) {
                $paymentMethod = new OrderPaymentMethod();
                $paymentMethod->setName($method[0]);
                $paymentMethod->setShortname($method[1]);
                $paymentMethod->setDescription($method[2]);

                if (isset($method[3])) {
                    $paymentMethod->setIsActive($method[3]);
                }

                $manager->persist($paymentMethod);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 8;
    }
}
