<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseElementCountryCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.element_country';
    const XML_NAME = 'elements_countries';
}
