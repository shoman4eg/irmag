<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\SeoTrait;

/**
 * @Gedmo\Tree(type="nested")
 *
 * @ORM\Table(
 *     name="sections",
 *     indexes={
 *         @ORM\Index(columns={"lft", "rgt", "root"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\SectionRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Section
{
    use TimestampableEntity;
    use SeoTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $name;

    /**
     * @var int
     *
     * @Gedmo\TreeLeft
     *
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @var int
     *
     * @Gedmo\TreeLevel
     *
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @var int
     *
     * @Gedmo\TreeRight
     *
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @var int
     *
     * @Gedmo\TreeRoot
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @var Section
     *
     * @Gedmo\TreeParent
     *
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var Collection|Section[]
     *
     * @ORM\OneToMany(targetEntity="Section", mappedBy="parent")
     * @ORM\OrderBy({"lft": "ASC"})
     */
    private $children;

    /**
     * @ORM\Column(name="tsv", type="tsvector", nullable=true)
     */
    private $tsv;

    /**
     * Кол-во товаров.
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $elementsCount;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->elementsCount = 0;
        $this->isActive = true;
        $this->children = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    public function getLvl(): ?int
    {
        return $this->lvl;
    }

    public function setLvl(int $lvl): self
    {
        $this->lvl = $lvl;

        return $this;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    public function getRoot(): ?int
    {
        return $this->root;
    }

    public function setRoot(?int $root): self
    {
        $this->root = $root;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Section[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getElementsCount(): ?int
    {
        return $this->elementsCount;
    }

    public function setElementsCount(int $elementsCount): self
    {
        $this->elementsCount = $elementsCount;

        return $this;
    }
}
