<?php

namespace Irmag\RestApiBundle\EventListener;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Asset\Packages;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementPicture;

class ElementSerializationSubscriber implements EventSubscriberInterface
{
    const IMAGINE_ELEMENT_FILTER_NAME = 'catalog_elements';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var CacheManager
     */
    private $imagineCacheManager;

    /**
     * @var Packages
     */
    private $assetPackages;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var string
     */
    private $driveApiDomain;

    /**
     * @param UrlGeneratorInterface $router
     * @param CacheManager          $imagineCacheManager
     * @param Packages              $assetPackages
     * @param RequestStack          $requestStack
     * @param string                $driveApiDomain
     */
    public function __construct(
        UrlGeneratorInterface $router,
        CacheManager $imagineCacheManager,
        Packages $assetPackages,
        RequestStack $requestStack,
        string $driveApiDomain
    ) {
        $this->router = $router;
        $this->imagineCacheManager = $imagineCacheManager;
        $this->assetPackages = $assetPackages;
        $this->requestStack = $requestStack;
        $this->driveApiDomain = $driveApiDomain;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ['event' => 'serializer.post_serialize', 'class' => Element::class, 'method' => 'onPostSerialize'],
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onPostSerialize(ObjectEvent $event): void
    {
        // only for Drive API domain
        if ($this->requestStack->getCurrentRequest()->getHost() !== $this->driveApiDomain) {
            return;
        }

        /* @var Element $object */
        $object = $event->getObject();

        if (!$object instanceof Element) {
            return;
        }

        /* @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();
        $visitor->setData('image', $this->getElementPictureWebPath($object));
        $visitor->setData('toneImage', $this->getElementTonePictureWebPath($object));
    }

    /**
     * @param Element $element
     *
     * @return string
     */
    private function getElementPictureWebPath(Element $element): string
    {
        /** @var ElementPicture $elementPicture */
        if ($elementPicture = $element->getPictures()->first()) {
            if ($picture = $elementPicture->getFile()) {
                $path = $picture->getWebPath();
            }
        }

        if (!isset($path)) {
            $path = $this->assetPackages->getUrl(Element::NOT_FOUND_IMAGE);
        }

        return $this->imagineCacheManager->getBrowserPath($path, self::IMAGINE_ELEMENT_FILTER_NAME);
    }

    /**
     * @param Element $element
     *
     * @return string|null
     */
    private function getElementTonePictureWebPath(Element $element): ?string
    {
        if ($tonePicture = $element->getTonePicture()) {
            return $this->imagineCacheManager->getBrowserPath($tonePicture->getWebPath(), self::IMAGINE_ELEMENT_FILTER_NAME);
        }

        return null;
    }
}
