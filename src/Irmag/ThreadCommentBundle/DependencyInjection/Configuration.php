<?php

namespace Irmag\ThreadCommentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('irmag_thread_comment');

        $rootNode
            ->children()
                ->arrayNode('configs')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('template')->defaultValue('@IrmagThreadComment/thread.html.twig')->cannotBeEmpty()->end()
                            ->scalarNode('fqcn')->cannotBeEmpty()->end()
                            ->booleanNode('is_commentable')->defaultTrue()->end()
                            ->booleanNode('is_scorable')->defaultTrue()->end()
                            ->booleanNode('is_ratable')->defaultTrue()->end()
                            ->arrayNode('disallow_roles')
                                ->children()
                                    ->scalarNode('create')->defaultValue('ROLE_IRMAG_DISALLOW_THREAD_COMMENT_CREATE')->cannotBeEmpty()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
