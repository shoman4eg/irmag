<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Irmag\AttachmentBundle\Service\AttachmentService;
use Irmag\ForumBundle\Entity\ForumPostAttachment;

/**
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 * @Route("/upload",
 *     options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_forum_domain%",
 * )
 */
class AttachmentController extends Controller
{
    /**
     * @Route("/post/", name="irmag_forum_post_upload_pics")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadPostPicAction(Request $request): JsonResponse
    {
        $files = $request->files->all();

        if (empty($files['forum_post']['uploader']['uploader'][0])) {
            return new JsonResponse(['success' => false]);
        }
        $file = $files['forum_post']['uploader']['uploader'][0];
        $attach = new ForumPostAttachment();
        $response = $this->get(AttachmentService::class)->upload($file, $attach);

        return new JsonResponse(['success' => true, 'response' => $response]);
    }

    /**
     * @Route("/topic/", name="irmag_forum_topic_upload_pics")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadTopicPicAction(Request $request): JsonResponse
    {
        $files = $request->files->all();

        if (empty($files['forum_topic']['post']['uploader']['uploader'][0])) {
            return new JsonResponse(['success' => false]);
        }

        $file = $files['forum_topic']['post']['uploader']['uploader'][0];
        $attach = new ForumPostAttachment();
        $response = $this->get(AttachmentService::class)->upload($file, $attach);

        return new JsonResponse(['success' => true, 'response' => $response]);
    }

    /**
     * @Route("/remove/postpic/", name="irmag_forum_topic_remove_attachment")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeTopicPicAction(Request $request): JsonResponse
    {
        return $this->removePostPicAction($request);
    }

    /**
     * @Route("/remove/postpic/", name="irmag_forum_post_remove_attachment")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removePostPicAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $attach = $em->getRepository(ForumPostAttachment::class)->find($id);

        if (!$attach) {
            throw $this->createNotFoundException('Attachment did not found');
        }

        $this->get(AttachmentService::class)->remove($attach);

        return new JsonResponse(['success' => true]);
    }
}
