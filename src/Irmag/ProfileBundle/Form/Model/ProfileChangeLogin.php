<?php

namespace Irmag\ProfileBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProfileChangeLogin extends ProfileCurrentPassword
{
    /**
     * @Assert\NotBlank(
     *     message="Поле 'Логин' не должно быть пустым",
     *     groups={"change_profile"},
     * )
     * @Assert\Length(
     *     min=4,
     *     max=25,
     *     minMessage="Поле 'Логин' должно быть не менее {{ limit }} символов",
     *     maxMessage="Поле 'Логин' должно быть не более {{ limit }} символов",
     * )
     */
    protected $newUsername;

    public function getNewUsername()
    {
        return $this->newUsername;
    }

    public function setNewUsername($newUsername)
    {
        $this->newUsername = $newUsername;
    }

    /**
     * @Assert\Callback
     */
    public function usernameValidate(ExecutionContextInterface $context, $payload)
    {
        if (false !== mb_strpos($this->getNewUsername(), '@')) {
            $context->buildViolation('error.username_is_email')
                ->setTranslationDomain('irmag_profile')
                ->atPath('newUsername')
                ->addViolation();
        }
    }
}
