<?php

namespace Irmag\SiteBundle\Service\ReferralManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\UserReferralOrderPayout;
use Irmag\SiteBundle\Entity\UserReferralTree;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\UserReferralLink;
use Irmag\ProfileBundle\Entity\User;

class ReferralManager
{
    private static $bonusPaymentValueByLvl = [
        1 => 100,
        2 => 50,
        3 => 25,
        4 => 10,
        5 => 5,
        6 => 1, // and more
    ];

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BonusManager
     */
    private $bonusManager;

    /**
     * @param SessionInterface       $session
     * @param EntityManagerInterface $em
     * @param BonusManager           $bonusManager
     */
    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        BonusManager $bonusManager
    ) {
        $this->session = $session;
        $this->em = $em;
        $this->bonusManager = $bonusManager;
    }

    /**
     * Возвращает бонусное вознаграждение в зависимости от уровня вложения реферала.
     *
     * @param int $lvl Уровень вложения
     *
     * @throws ReferralManagerException When $lvl is wrong
     *
     * @return int
     */
    public static function getBonusPaymentValueByLvl(int $lvl): int
    {
        if ($lvl < 1) {
            throw new ReferralManagerException('Level less than 1.');
        }

        if ($lvl > 6) {
            $lvl = 6;
        }

        return self::$bonusPaymentValueByLvl[$lvl];
    }

    /**
     * Запоминает реферала, если таковой перешёл по ссылке.
     *
     * @param User $userReferral
     *
     * @return bool
     */
    public function rememberFromLink(User $userReferral): bool
    {
        if ($this->session->has(Config::REFERRAL_LINK_SESSION_NAME)) {
            if ($userOwner = $this->em->getRepository(User::class)->find($this->session->get(Config::REFERRAL_LINK_SESSION_NAME))) {
                $userReferralLink = new UserReferralLink();
                $userReferralLink->setUserReferral($userReferral);
                $userReferralLink->setUserOwner($userOwner);
                $this->em->persist($userReferralLink);
                $this->em->flush();
                $this->session->remove(Config::REFERRAL_LINK_SESSION_NAME);

                return true;
            }
        }

        return false;
    }

    /**
     * @param Order $order
     *
     * @throws ReferralManagerException
     */
    public function doOrderPayment(Order $order): void
    {
        $userReferral = $order->getUser();
        $referralTreeRepository = $this->em->getRepository(UserReferralTree::class);

        /** @var UserReferralTree $userReferralNode */
        if (!$userReferralNode = $referralTreeRepository->findOneBy(['user' => $userReferral])) {
            throw new ReferralManagerException(sprintf('User with id "%d" from UserReferralTree not found!', $userReferral->getId()));
        }

        if (!$parentNodes = $referralTreeRepository->getParentNodes($userReferralNode)) {
            throw new ReferralManagerException(sprintf('User with id "%d" from UserReferralTree found, but do not have parents.', $userReferral->getId()));
        }

        $lvl = 1;

        foreach ($parentNodes as $parentNode) {
            $userOwner = $parentNode->getUser();

            $msg = sprintf('Начислено за заказ №%d по реферальной ссылке. Пользователь: %s. Уровень вложения: %d.',
                $order->getId(),
                $userReferral->getUsername(),
                $lvl
            );

            $this->em->beginTransaction();

            try {
                $bonusHistory = $this->bonusManager
                    ->setUser($userOwner)
                    ->update(self::getBonusPaymentValueByLvl($lvl), $msg);

                $userReferralOrderPayout = new UserReferralOrderPayout();
                $userReferralOrderPayout->setUserOwner($userOwner);
                $userReferralOrderPayout->setUserReferral($userReferral);
                $userReferralOrderPayout->setOrder($order);
                $userReferralOrderPayout->setUserBonusHistory($bonusHistory);
                $this->em->persist($userReferralOrderPayout);
                $this->em->flush();

                $this->em->commit();
            } catch (\Exception $exception) {
                $this->em->rollback();
                throw $exception;
            }

            ++$lvl;
        }
    }

    /**
     * @param User $userReferral
     *
     * @return bool
     */
    public function registerToTreeIfNeed(User $userReferral): bool
    {
        if ($userReferralLink = $this->getUserReferralLink($userReferral)) {
            return $this->registerToTree($userReferralLink->getUserOwner(), $userReferral);
        }

        return false;
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isSupportedForPutToQueue(Order $order): bool
    {
        if ($order->getParent() instanceof Order) {
            return false;
        }

        $userReferral = $order->getUser();

        if (true === $this->hasOwnerUserReferralLink($userReferral)) {
            return true;
        }

        if (true === $this->hasUserReferralTreeParents($userReferral)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $userReferral
     *
     * @return bool
     */
    private function hasOwnerUserReferralLink(User $userReferral): bool
    {
        if ($userReferralLink = $this->getUserReferralLink($userReferral)) {
            return $userReferralLink->getUserOwner() instanceof User;
        }

        return false;
    }

    /**
     * @param User $userReferral
     *
     * @return UserReferralLink|null
     */
    private function getUserReferralLink(User $userReferral): ?UserReferralLink
    {
        return $this->em->getRepository(UserReferralLink::class)->findOneBy(['userReferral' => $userReferral]);
    }

    /**
     * @param User $userReferral
     *
     * @return bool
     */
    private function hasUserReferralTreeParents(User $userReferral): bool
    {
        $referralTreeRepository = $this->em->getRepository(UserReferralTree::class);
        $referralNode = $referralTreeRepository->findOneBy(['user' => $userReferral]);

        if (!$referralNode) {
            return false;
        }

        if (!$referralTreeRepository->getParentNodes($referralNode)) {
            return false;
        }

        return true;
    }

    /**
     * Регистрирует пользователя в дереве рефералов, если такового нет.
     *
     * @param User $userOwner
     * @param User $userReferral
     *
     * @return bool
     */
    private function registerToTree(User $userOwner, User $userReferral): bool
    {
        $referralTreeRepository = $this->em->getRepository(UserReferralTree::class);

        if (!$userOwnerNode = $referralTreeRepository->findOneBy(['user' => $userOwner])) {
            $userOwnerNode = new UserReferralTree();
            $userOwnerNode->setUser($userOwner);
            $this->em->persist($userOwnerNode);
        }

        if ($userReferralNode = $referralTreeRepository->findOneBy(['user' => $userReferral])) {
            return false;
        }

        $userReferralNode = new UserReferralTree();
        $userReferralNode->setUser($userReferral);
        $userReferralNode->setParent($userOwnerNode);
        $this->em->persist($userReferralNode);

        $this->em->flush();

        return true;
    }
}
