<?php

namespace Irmag\ProfileBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\ProfileBundle\Entity\UserOrderProfile;

class UserOrderProfileRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Убирает все основные профили заказа.
     *
     * @param UserInterface $user
     *
     * @return mixed
     */
    public function unsetAllPrimary(UserInterface $user)
    {
        return $this->createQueryBuilder('uop')
            ->update(UserOrderProfile::class, 'uop')
            ->set('uop.isPrimary', ':isPrimary')
            ->where('uop.user = :user')
            ->setParameter('isPrimary', false)
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

    /**
     * Возвращает основной профиль заказа.
     *
     * @param UserInterface $user
     *
     * @return mixed
     */
    public function getPrimary(UserInterface $user)
    {
        return $this->createQueryBuilder('uop')
            ->where('uop.user = :user')
            ->andWhere('uop.isPrimary = true')
            ->setParameter('user', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
