<?php

namespace Irmag\OnlinePaymentBundle\Repository;

use Irmag\SiteBundle\Entity\Order;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;

class OrderOnlinePaymentRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Получить все онлайн-платежи, статусы которых могут измениться.
     *
     * @return mixed
     */
    public function getAllPaymentsToSync()
    {
        return $this->createQueryBuilder('op')
            ->select('op', 'ops')
            ->join('op.status', 'ops')
            ->where('op.needToSync = true')
            ->orderBy('op.updatedAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Установить флаг синхронизации в false для старых платежей.
     *
     * @return mixed
     */
    public function disableSyncingForOldPayments()
    {
        $cutoffTime = new \DateTime();
        $cutoffTime->modify('-1 month');

        return $this->createQueryBuilder('op')
            ->update(OrderOnlinePayment::class, 'op')
            ->set('op.needToSync', ':needToSync')
            ->where('op.createdAt < :cutoffTime')
            ->setParameter('needToSync', false)
            ->setParameter('cutoffTime', $cutoffTime)
            ->getQuery()
            ->execute();
    }

    /**
     * Получить все оплаты с отказами, по которым "повисли" заказы.
     *
     * @return mixed
     */
    public function getAllUnpayedAndDeclined()
    {
        return $this->createQueryBuilder('op')
            ->join('op.status', 'ops')
            ->join('op.order', 'or')
            ->join('or.status', 'ors')
            ->where('ors.shortname = :orderStatus')
            ->andWhere('ops.name = :paymentStatus')
            ->setParameter('orderStatus', 'S')
            ->setParameter('paymentStatus', OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить все оплаты, которые были подтверждены, но по тем или иным причинам заказ по ним не оформился.
     *
     * @return OrderOnlinePayment[]
     */
    public function getAllStuckedPayments(): array
    {
        return $this->createQueryBuilder('op')
            ->join('op.order', 'or')
            ->join('or.status', 'ors')
            ->where('ors.shortname = :orderStatus')
            ->setParameter('orderStatus', 'S')
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить актуальную оплату для заказа.
     *
     * @param Order $order
     *
     * @return mixed
     */
    public function getActualOrderPayment(Order $order): ?OrderOnlinePayment
    {
        return $this->createQueryBuilder('op')
            ->join('op.status', 'ops')
            ->where('op.order = :order')
            ->andWhere('ops.name IN (:statusRegistered, :statusACS, :statusApproved)')
            ->orderBy('op.createdAt', 'ASC')
            ->setParameter('order', $order)
            ->setParameter('statusRegistered', OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME)
            ->setParameter('statusACS', OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME)
            ->setParameter('statusApproved', OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Получить последний платеж по заказу.
     *
     * @param Order $order
     *
     * @return mixed
     */
    public function getLastPayment(Order $order): ?OrderOnlinePayment
    {
        return $this->createQueryBuilder('op')
            ->where('op.order = :order')
            ->orderBy('op.createdAt', 'ASC')
            ->setParameter('order', $order)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Получить совершенную оплату для заказа.
     *
     * @param Order $order
     *
     * @return mixed
     */
    public function getCompletePaymentByOrder(Order $order): ?OrderOnlinePayment
    {
        return $this->createQueryBuilder('op')
            ->join('op.status', 'ops')
            ->where('op.order = :order')
            ->andWhere('ops.name != :statusDeclined AND ops.name != :statusRegistered AND ops.name != :statusACS')
            ->orderBy('op.createdAt', 'ASC')
            ->setParameter('order', $order)
            ->setParameter('statusDeclined', OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME)
            ->setParameter('statusRegistered', OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME)
            ->setParameter('statusACS', OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
