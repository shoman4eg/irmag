<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Irmag\ProfileBundle\NotificationEvents;

final class Version20181211040937 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE users SET is_site_subscribed = true');
        $this->addSql('TRUNCATE users_notification_subscriptions');

        $this->eventSubscribe(NotificationEvents::FAVORITE_ELEMENT_IS_ACTIVE);
        $this->eventSubscribe(NotificationEvents::ACTION_NEW);
        $this->eventSubscribe(NotificationEvents::ORDER_STATUS);
        $this->eventSubscribe(NotificationEvents::BONUS_SEND);
        $this->eventSubscribe(NotificationEvents::NEWS_NEW);
        $this->eventSubscribe(NotificationEvents::FORUM_POST_REPLY);
        $this->eventSubscribe(NotificationEvents::BLOG_COMMENT_REPLY);
        $this->eventSubscribe(NotificationEvents::SITE_COMMENT_REPLY);
        $this->eventSubscribe(NotificationEvents::BLOG_POST_NEW);
        $this->eventSubscribe(NotificationEvents::NEWSLETTER);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE users_notification_subscriptions');
    }

    private function eventSubscribe(string $eventName): void
    {
        $this->addSql(sprintf("
            INSERT INTO users_notification_subscriptions (id, user_id, event_id, is_push_enabled, is_email_enabled, is_site_enabled, is_sms_enabled)
                SELECT nextval('users_notification_subscriptions_id_seq'), id, (SELECT id FROM users_notification_events WHERE name = '%s'), FALSE, FALSE, TRUE, FALSE
                FROM users
                WHERE is_active = true
                AND last_login_at >= MAKE_DATE(2017, 01, 01)
        ", $eventName));
    }
}
