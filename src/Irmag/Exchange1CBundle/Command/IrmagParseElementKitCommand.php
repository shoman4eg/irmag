<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseElementKitCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.element_kit';
    const XML_NAME = 'elements_kits';
}
