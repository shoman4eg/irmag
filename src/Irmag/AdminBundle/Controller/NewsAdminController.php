<?php

namespace Irmag\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Irmag\AdminBundle\Controller\Traits\ActivateDeactivateTrait;

class NewsAdminController extends Controller
{
    use ActivateDeactivateTrait;
}
