<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180315045027 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX idx_dfc4904e13c13f1');
        $this->addSql('ALTER TABLE users_bonus_history DROP id1c');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DFC4904EC009BE4B ON users_bonus_history (uuid1c)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX UNIQ_DFC4904EC009BE4B');
        $this->addSql('ALTER TABLE users_bonus_history ADD id1c INT DEFAULT NULL');
        $this->addSql('CREATE INDEX idx_dfc4904e13c13f1 ON users_bonus_history (id1c)');
    }
}
