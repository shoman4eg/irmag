<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180831034617 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE elements_brands ADD seo_keywords VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_brands ADD seo_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_brands ADD seo_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_brands ADD seo_h1 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_manufacturers ADD seo_keywords VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_manufacturers ADD seo_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_manufacturers ADD seo_title VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE elements_manufacturers ADD seo_h1 VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE elements_brands DROP seo_keywords');
        $this->addSql('ALTER TABLE elements_brands DROP seo_description');
        $this->addSql('ALTER TABLE elements_brands DROP seo_title');
        $this->addSql('ALTER TABLE elements_brands DROP seo_h1');
        $this->addSql('ALTER TABLE elements_manufacturers DROP seo_keywords');
        $this->addSql('ALTER TABLE elements_manufacturers DROP seo_description');
        $this->addSql('ALTER TABLE elements_manufacturers DROP seo_title');
        $this->addSql('ALTER TABLE elements_manufacturers DROP seo_h1');
    }
}
