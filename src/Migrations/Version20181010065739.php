<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181010065739 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("INSERT INTO groups VALUES (nextval('groups_id_seq'), 'Курьеры', 'Для drive.irmag.ru', 'drivers')");
        $this->addSql("INSERT INTO roles VALUES (nextval('roles_id_seq'), 'ROLE_IRMAG_DRIVER', 'Курьер')");
        $this->addSql("INSERT INTO groups_roles VALUES (
            (SELECT id FROM groups WHERE name = 'Курьеры'),
            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_DRIVER')
        )");

        $this->addSql("INSERT INTO roles_childrens VALUES (
            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_DRIVER'),
            (SELECT id FROM roles WHERE name = 'ROLE_USER')
        )");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM groups WHERE shortname = 'drivers'");
        $this->addSql("DELETE FROM roles WHERE name = 'ROLE_IRMAG_DRIVER'");
    }
}
