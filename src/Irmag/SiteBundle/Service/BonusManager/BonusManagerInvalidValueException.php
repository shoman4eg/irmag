<?php

namespace Irmag\SiteBundle\Service\BonusManager;

use Irmag\CoreBundle\Exception\IrmagException;

class BonusManagerInvalidValueException extends IrmagException
{
}
