<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Irmag\SiteBundle\Config;
use Irmag\ProfileBundle\Entity\User;

class IrmagMarkInvalidEmailCommand extends Command
{
    /**
     * @const int
     */
    const OPERATIONS_BEFORE_FLUSH = 50;

    protected static $defaultName = 'irmag:user:mark_invalid_email';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var int
     */
    private $operations;

    /**
     * @param EntityManagerInterface $em
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->operations = 0;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Mark invalid emails')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = new \DateTime();
        $output->writeln(sprintf('<info>Started at %s</info>', $now->format(Config::DATETIME_FORMAT)));

        $users = $this->em->getRepository(User::class)->findByNotNullAndValidEmail();

        if ($output->isVerbose()) {
            $progress = new ProgressBar($output, \count($users));
            $progress->start();
        }

        $emailConstraint = new EmailConstraint();
        $emailConstraint->checkHost = true;

        foreach ($users as $user) {
            ++$this->operations;
            $email = $user->getEmail();
            $errors = $this->validator->validate($email, $emailConstraint);

            if ($errors->count() > 0) {
                $user->setIsEmailInvalid(true);

                if ($output->isVerbose()) {
                    $output->writeln('');
                    $output->writeln(sprintf('<comment>Email "%s" was marked as invalid</comment>', $email));
                }
            } else {
                $user->setIsEmailInvalid(false);
            }

            $this->em->persist($user);

            if ($output->isVerbose()) {
                $progress->advance();
            }

            if (0 === $this->operations % self::OPERATIONS_BEFORE_FLUSH) {
                $this->em->flush();
            }
        }

        if ($output->isVerbose()) {
            $progress->finish();
        }

        $this->em->flush();
        $this->em->clear();

        $endTime = new \DateTime();
        $output->writeln('');
        $output->writeln(sprintf('<info>Finished at %s</info>', $endTime->format(Config::DATETIME_FORMAT)));
    }
}
