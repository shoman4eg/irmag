<?php

namespace Irmag\OrderDeliveryServiceBundle\Model;

use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCountry;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceRegion;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;

class OrderDeliveryServiceAddress
{
    /**
     * @var OrderDeliveryServiceCountry
     */
    private $country;

    /**
     * @var OrderDeliveryServiceRegion
     */
    private $region;

    /**
     * @var OrderDeliveryServiceCity
     */
    private $city;

    /**
     * Get country.
     *
     * @return OrderDeliveryServiceCountry
     */
    public function getCountry(): OrderDeliveryServiceCountry
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param OrderDeliveryServiceCountry $country
     *
     * @return $this
     */
    public function setCountry(OrderDeliveryServiceCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get region.
     *
     * @return OrderDeliveryServiceRegion
     */
    public function getRegion(): OrderDeliveryServiceRegion
    {
        return $this->region;
    }

    /**
     * Set region.
     *
     * @param OrderDeliveryServiceRegion $region
     *
     * @return $this
     */
    public function setRegion(OrderDeliveryServiceRegion $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get city.
     *
     * @return OrderDeliveryServiceCity
     */
    public function getCity(): OrderDeliveryServiceCity
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param OrderDeliveryServiceCity $city
     *
     * @return $this
     */
    public function setCity(OrderDeliveryServiceCity $city): self
    {
        $this->city = $city;

        return $this;
    }
}
