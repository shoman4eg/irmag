<?php

namespace Irmag\BlogBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Event\BlogPostEvent;
use Irmag\BlogBundle\Mailer\BlogMailer;
use Irmag\BlogBundle\IrmagBlogEvents;

class BlogPostEmailSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BlogMailer
     */
    private $mailer;

    /**
     * @param EntityManagerInterface $em
     * @param BlogMailer             $mailer
     */
    public function __construct(
        EntityManagerInterface $em,
        BlogMailer $mailer
    ) {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagBlogEvents::BLOG_NEW_POST_CREATED => 'sendEmailOnBlogPostCreate',
            IrmagBlogEvents::BLOG_POST_STATUS_CHANGED => 'sendEmailOnBlogPostStatusChange',
            IrmagBlogEvents::BLOG_POST_GIVE_REWARD_TO_USER => 'sendEmailOnBlogPostReward',
        ];
    }

    /**
     * @param BlogPostEvent $event
     */
    public function sendEmailOnBlogPostCreate(BlogPostEvent $event): void
    {
        $post = $event->getBlogPost();

        // уведомление модераторам
        $moderatorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => 'blog_moderators']);

        if ($moderatorGroup) {
            $moderators = $moderatorGroup->getUsers();

            foreach ($moderators as $moderator) {
                if (!empty($moderator->getEmail()) && $post->getUser() !== $moderator) {
                    $this->mailer->sendNewPostOnModerationMessage($moderator->getEmail(), ['post' => $post]);
                }
            }
        }
    }

    /**
     * @param BlogPostEvent $event
     */
    public function sendEmailOnBlogPostStatusChange(BlogPostEvent $event): void
    {
        $post = $event->getBlogPost();
        $user = $post->getUser();
        $email = $user->getEmail();

        if (!empty($email) && !$user->getIsEmailInvalid()) {
            if (BlogPost::BLOG_POST_STATUS_DECLINED === $post->getStatus()) {
                $this->mailer->sendPostWasDeclinedMessage($email, ['post' => $post]);
            } elseif (BlogPost::BLOG_POST_STATUS_PUBLISHED === $post->getStatus()) {
                $this->mailer->sendPostWasPublishedMessage($email, ['post' => $post]);
            }
        }
    }

    /**
     * @param BlogPostEvent $event
     */
    public function sendEmailOnBlogPostReward(BlogPostEvent $event): void
    {
        $post = $event->getBlogPost();

        if (!empty($post->getUser()->getEmail()) && !$post->getUser()->getIsEmailInvalid()) {
            $this->mailer->sendPostAchievedRewardMessage($post->getUser()->getEmail(), [
                'post' => $post,
            ]);
        }
    }
}
