CREATE OR REPLACE VIEW elements_grouped
AS
SELECT e.*
FROM (
    SELECT MAX(id) AS id
    FROM elements
    GROUP BY group_by, CASE WHEN group_by IS NULL THEN id END, is_active
) t
LEFT JOIN elements e ON e.id = t.id;
