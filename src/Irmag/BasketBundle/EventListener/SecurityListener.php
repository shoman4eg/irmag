<?php

namespace Irmag\BasketBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\BasketBundle\Transformer\BasketAnonymousToAuthenticatedTransformer;

class SecurityListener
{
    /**
     * @var BasketAnonymousToAuthenticatedTransformer
     */
    private $transformer;

    /**
     * @param BasketAnonymousToAuthenticatedTransformer $transformer
     */
    public function __construct(BasketAnonymousToAuthenticatedTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $this->transformer->transform();
    }
}
