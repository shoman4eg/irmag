<?php

namespace Irmag\Exchange1CBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\Exchange1CBundle\Parser\ParserInterface;

class ParserEvent extends Event
{
    /**
     * @var ParserInterface
     */
    private $parser;

    /**
     * @var string
     */
    private $parserServiceName;

    /**
     * @param ParserInterface $parser
     * @param string          $parserServiceName
     */
    public function __construct(
        ParserInterface $parser,
        string $parserServiceName
    ) {
        $this->parser = $parser;
        $this->parserServiceName = $parserServiceName;
    }

    /**
     * @return ParserInterface
     */
    public function getParser(): ParserInterface
    {
        return $this->parser;
    }

    /**
     * @return string
     */
    public function getParserServiceName(): string
    {
        return $this->parserServiceName;
    }
}
