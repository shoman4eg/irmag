<?php

namespace Irmag\SiteBundle\Repository;

class IndexElementsCarouselRepository extends \Doctrine\ORM\EntityRepository
{
    public function getCarousels()
    {
        return $this->createQueryBuilder('c')
            ->select('c, e, ep, etp, f, t')
            ->leftJoin('c.elements', 'e')
            ->leftJoin('e.pictures', 'ep')
            ->leftJoin('e.tonePicture', 'etp')
            ->leftJoin('ep.file', 'f')
            ->leftJoin('e.thread', 't')
            ->where('c.isActive = true')
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
