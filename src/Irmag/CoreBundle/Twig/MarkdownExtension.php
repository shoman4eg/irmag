<?php

namespace Irmag\CoreBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Irmag\CoreBundle\Utils\Markdown;

class MarkdownExtension extends AbstractExtension
{
    /**
     * @var Markdown
     */
    private $parser;

    /**
     * @param Markdown $parser
     */
    public function __construct(Markdown $parser)
    {
        $this->parser = $parser;
    }

    public function getFilters()
    {
        return [
            new TwigFilter(
                'md2html',
                [$this, 'markdownToHtml'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * @param string $md
     *
     * @return string
     */
    public function markdownToHtml(string $md): string
    {
        return $this->parser->toHtml($md);
    }
}
