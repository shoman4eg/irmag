import Routing from './routing';
import log from './logger';

/**
 * Плюрализация.
 *
 * @param {integer}  number
 * @param {string[]} variants
 *
 * @example
 * let days = 5;
 * plural(days, ['день', 'дня', 'дней']); // 5 дней
 *
 * @return {string}
 */
export function plural(number, variants) {
    let i = 2;

    if (number % 10 === 1 && number % 100 !== 11) {
        i = 0;
    } else if (number % 10 >= 2 && number % 10 <= 4 && (number % 100 < 10 || number % 100 >= 20)) {
        i = 1;
    }

    return variants[i];
}

/**
 * Обёртка над jQuery.ajax.
 *
 * @param {string}       route    Symfony route name
 * @param {object|array} data     Ajax request data
 * @param {function}     callback Callback with response data [optional]
 */
export function ajax(route, data, callback) {
    return $.ajax({
        url: Routing.generate(route),
        data: data,
        success: function (result) {
            if ($.isFunction(callback)) {
                callback(result);
            }
        },
    });
}

/**
 * Триммер для input/textarea.
 *
 * @param {string} selector
 */
export function bindTrimmer(selector = '.bindTrimmer') {
    log('[bindTrimmer]', selector);

    $(document).on('focusout', selector, function () {
        $(this).val($(this).val().trim());
    });
}

/**
 * @param {object} obj
 * @param {string} path
 */
// export function getDeepPropValueByPath(obj, path) {
//     let current = obj;
//     path.split('.').forEach(function (p) { current = current[p]; });
//
//     return current;
// }

/**
 * @return {string}
 */
export function getFingerprint() {
    // https://github.com/darkskyapp/string-hash
    function checksum(str) {
        let hash = 5381;
        let i = str.length;

        while (i--) {
            hash = (hash * 33) ^ str.charCodeAt(i);
        }

        return hash >>> 0;
    }

    // https://stackoverflow.com/a/4167870/1250044
    function map(arr, fn) {
        let i = 0;
        const len = arr.length;
        const ret = [];

        while (i < len) {
            ret[i] = fn(arr[i++]);
        }

        return ret;
    }

    return checksum([
        navigator.userAgent,
        [window.screen.height, window.screen.width, window.screen.colorDepth].join('x'),
        new Date().getTimezoneOffset(),
        map(navigator.plugins, function (plugin) {
            return [
                plugin.name,
                plugin.description,
                map(plugin, function (mime) {
                    return [mime.type, mime.suffixes].join('~');
                }).join(','),
            ].join('::');
        }).join(';'),
    ].join('###'));
}

/**
 * @param {object}  $element    jQuery element
 * @param {boolean} isFully [optional] element is fully in view
 *
 * @return {boolean}
 */
export function isElementInView($element, isFully = false) {
    const pageTop = $(window).scrollTop();
    const pageBottom = pageTop + $(window).height();
    const elementTop = $element.offset().top;
    const elementBottom = elementTop + $element.height();

    return true === isFully
        ? pageTop < elementTop && pageBottom > elementBottom
        : elementTop <= pageBottom && elementBottom >= pageTop;
}
