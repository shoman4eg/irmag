<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181206101419 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT fk_d155ca4654f462e5');
        $this->addSql('DROP INDEX idx_d155ca4654f462e5');
        $this->addSql('ALTER TABLE orders_changes DROP delivery_time_id');
        $this->addSql('ALTER TABLE orders_changes DROP delivery_date');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes ADD delivery_time_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_changes ADD delivery_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT fk_d155ca4654f462e5 FOREIGN KEY (delivery_time_id) REFERENCES orders_delivery_times (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d155ca4654f462e5 ON orders_changes (delivery_time_id)');
    }
}
