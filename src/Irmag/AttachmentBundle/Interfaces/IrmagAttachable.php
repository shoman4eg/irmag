<?php

namespace Irmag\AttachmentBundle\Interfaces;

use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\File;

interface IrmagAttachable
{
    /**
     * Set file.
     *
     * @param File $file
     *
     * @return self
     */
    public function setFile(File $file);

    /**
     * Get file.
     *
     * @return File
     */
    public function getFile(): ?File;

    /**
     * Set attachedBy.
     *
     * @param User|null $userAttachedBy
     *
     * @return self
     */
    public function setUserAttachedBy(?User $userAttachedBy);

    /**
     * Get attachedBy.
     *
     * @return User
     */
    public function getUserAttachedBy(): ?User;

    /**
     * Set object to be attached to.
     *
     * @param IrmagAttachmentPoint|null $object
     *
     * @return self
     */
    public function setObject(?IrmagAttachmentPoint $object);

    /**
     * Get object to be attached to.
     *
     * @return IrmagAttachmentPoint
     */
    public function getObject(): ?IrmagAttachmentPoint;
}
