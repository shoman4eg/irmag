<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\ForumBundle\Form\Type\ForumSearchType;
use Irmag\ForumBundle\Entity\ForumTopicTag;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumTopic;

class SearchController extends Controller
{
    /**
     * Поиск по тегам
     *
     * @Route("/tag/", name="irmag_forum_search_by_tag", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchByTagAction(Request $request): Response
    {
        $tagName = trim($request->query->get('tag'));
        $em = $this->get('doctrine.orm.default_entity_manager');
        $tag = $em->getRepository(ForumTopicTag::class)->findOneBy(['name' => $tagName]);
        $topics = $tag ? $tag->getForumTopics() : [];

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $topics,
            $request->query->getInt('page', 1),
            20,
            []
        );

        return $this->render('@IrmagForum/Search/tag_result.html.twig', [
            'query' => $tagName,
            'total' => \count($topics),
            'pagination' => $pagination,
        ]);
    }

    /**
     * Получить форму поиска.
     *
     * @Route("/search/", name="irmag_forum_search")
     *
     * @return Response
     */
    public function getForumSearchFormAction(): Response
    {
        $form = $this->createForm(ForumSearchType::class, null);

        return $this->render('@IrmagForum/Search/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Поиск по содержанию.
     *
     * @Route("/search/results/", name="irmag_forum_search_result", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function forumUserSearchResults(Request $request): Response
    {
        $params = $request->request->get('forum_search');
        $mode = $params['mode'];
        $query = $params['query'];
        $em = $this->get('doctrine.orm.default_entity_manager');

        if ('tags' === $mode) {
            return $this->redirectToRoute('irmag_forum_search_by_tag', ['tag' => $query]);
        } elseif ('name' === $mode) {
            $result = $em
                ->getRepository(ForumTopic::class)
                ->createQueryBuilder('ft')
                ->where('ft.isActive = true')
                ->andWhere('LOWER(ft.title) LIKE :term')
                ->setParameter('term', '%'.mb_strtolower($query).'%')
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();
        } else {
            $result = $em
                ->getRepository(ForumPost::class)
                ->createQueryBuilder('fp')
                ->where('fp.isActive = true')
                ->andWhere('LOWER(fp.text) LIKE :term')
                ->setParameter('term', '%'.mb_strtolower($query).'%')
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();
        }

        return $this->render('@IrmagForum/Search/result.html.twig', [
            'query' => $query,
            'mode' => $mode,
            'result' => $result,
        ]);
    }
}
