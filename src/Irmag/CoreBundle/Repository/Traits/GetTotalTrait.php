<?php

namespace Irmag\CoreBundle\Repository\Traits;

trait GetTotalTrait
{
    /**
     * @return int
     */
    public function getTotalPerAllTime(): int
    {
        /* @var \Doctrine\ORM\EntityRepository $this */
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->getQuery()
            ->useResultCache(true, 7200)
            ->getSingleScalarResult();
    }

    /**
     * @return int
     */
    public function getTotalPerMonths(): int
    {
        $startDate = new \DateTime();
        $startDate->setTime(0, 0);
        $startDate->setDate($startDate->format('Y'), $startDate->format('m'), 1);

        /* @var \Doctrine\ORM\EntityRepository $this */
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->where('u.createdAt >= :startDate')
            ->setParameter('startDate', $startDate)
            ->getQuery()
            ->useResultCache(true, 7200)
            ->getSingleScalarResult();
    }

    /**
     * @return int
     */
    public function getTotalPerToday(): int
    {
        $startDate = new \DateTime();
        $startDate->setTime(0, 0);

        /* @var \Doctrine\ORM\EntityRepository $this */
        return $this->createQueryBuilder('u')
            ->select('COUNT(u.id)')
            ->where('u.createdAt >= :startDate')
            ->setParameter('startDate', $startDate)
            ->getQuery()
            ->useResultCache(true, 7200)
            ->getSingleScalarResult();
    }
}
