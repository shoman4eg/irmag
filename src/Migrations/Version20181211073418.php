<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Irmag\ProfileBundle\NotificationEvents;

final class Version20181211073418 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("
            DELETE FROM users_notification_subscriptions
            WHERE event_id = (SELECT id from users_notification_events WHERE name = '%s')
        ", NotificationEvents::BLOG_POST_NEW));

        $this->addSql(sprintf("
            DELETE FROM users_notification_subscriptions
            WHERE event_id = (SELECT id from users_notification_events WHERE name = '%s')
        ", NotificationEvents::ACTION_NEW));

        $this->addSql(sprintf("
            DELETE FROM users_notification_subscriptions
            WHERE event_id = (SELECT id from users_notification_events WHERE name = '%s')
        ", NotificationEvents::NEWS_NEW));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
