<?php

namespace Irmag\ThreadCommentBundle\Event;

use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Symfony\Component\EventDispatcher\Event;

class ThreadCommentEvent extends Event
{
    /**
     * @var ThreadComment
     */
    private $comment;

    /**
     * @param ThreadComment $comment
     */
    public function __construct(ThreadComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return ThreadComment
     */
    public function getThreadComment()
    {
        return $this->comment;
    }
}
