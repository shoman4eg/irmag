<?php

namespace Irmag\SiteBundle\Service\PriceManager\Adapter;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\ActionElement;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Service\CatalogElementViewSwitcher;

class ActionAdapter
{
    /**
     * @var array|null
     */
    private $elements;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var CatalogElementViewSwitcher
     */
    private $viewSwitcher;

    /**
     * @param EntityManagerInterface     $em
     * @param CatalogElementViewSwitcher $viewSwitcher
     */
    public function __construct(
        EntityManagerInterface $em,
        CatalogElementViewSwitcher $viewSwitcher
    ) {
        $this->em = $em;
        $this->viewSwitcher = $viewSwitcher;
    }

    /**
     * Возвращает список элементов, которые участвуют в акции.
     *
     * @example
     * [
     *     elementId => [
     *         'price'   => (float),
     *         'actions' => Action[],
     *     ],
     *     ...
     * ]
     *
     * @return array
     */
    public function getElements(): array
    {
        if (null === $this->elements) {
            $this->viewSwitcher->setGrouped(CatalogElementViewSwitcher::NORMAL_TABLE_NAME);

            /* @var Action[] $actions */
            $actions = $this->em->createQueryBuilder()
                ->select('a, ae, e, apel')
                ->from(Action::class, 'a')
                ->leftJoin('a.elements', 'ae')
                ->leftJoin('ae.element', 'e')
                ->leftJoin('a.promocodeElementsList', 'apel')
                ->where('a.isActive = true')
                ->andWhere('a.endDateTime > :now')
                ->andWhere('a.startDateTime < :now')
                ->orderBy('a.createdAt', 'DESC')
                ->setParameter('now', new \DateTime())
                ->getQuery()
                ->getResult();

            // присваиваем $elements не null, т.к одноразовая операция $actions
            $this->elements = [];

            foreach ($actions as $action) {
                /* @var ActionElement $actionElement */
                foreach ($action->getElements() as $actionElement) {
                    $element = $actionElement->getElement();

                    if (!$element) {
                        continue;
                    }

                    // prepare
                    if (!isset($this->elements[$element->getId()])) {
                        $this->elements[$element->getId()] = [];
                        $this->elements[$element->getId()]['actions'] = [];
                    }

                    // если у акции есть цена на товар
                    if (null !== $actionElement->getPrice()) {
                        $this->elements[$element->getId()]['price'] = $actionElement->getPrice();
                    }

                    $this->elements[$element->getId()]['actions'][] = $actionElement->getAction();
                }
            }

            $this->viewSwitcher->setGrouped(CatalogElementViewSwitcher::GROUPED_VIEW_NAME);
        }

        return $this->elements;
    }

    /**
     * @return array
     */
    public function getElementIds(): array
    {
        $elements = $this->getElements();

        return array_keys($elements);
    }

    /**
     * Проверяет, присутствует ли товар в акциях.
     *
     * @param Element $element Товар
     *
     * @return bool
     */
    public function exists(Element $element): bool
    {
        $elements = $this->getElements();

        return isset($elements[$element->getId()]);
    }

    /**
     * Возвращает акции, в которых участвует товар.
     *
     * @param Element $element Товар
     *
     * @return Action[]|null
     */
    public function getActions(Element $element): ?array
    {
        $elements = $this->getElements();

        return !empty($elements[$element->getId()]['actions']) ? $elements[$element->getId()]['actions'] : null;
    }

    /**
     * Поверяет, присутствует ли цена у товара в акциях.
     *
     * @param Element $element Товар
     *
     * @return bool
     */
    public function hasPrice(Element $element): bool
    {
        $elements = $this->getElements();

        return isset($elements[$element->getId()]['price']);
    }

    /**
     * Возвращает цену товара по акции.
     *
     * @param Element $element Товар
     *
     * @return float|null
     */
    public function getPrice(Element $element): ?float
    {
        $elements = $this->getElements();

        return $elements[$element->getId()]['price'] ?? null;
    }
}
