<?php

namespace Irmag\CoreBundle\Mailer;

class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $mailerFrom;

    /**
     * @var string
     */
    protected $mailerFromName;

    /**
     * @param \Swift_Mailer     $mailer
     * @param \Twig_Environment $twig
     * @param string            $mailerFrom
     * @param string            $mailerFromName
     */
    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $twig,
        string $mailerFrom,
        string $mailerFromName
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->mailerFrom = $mailerFrom;
        $this->mailerFromName = $mailerFromName;
    }

    /**
     * @see Irmag/CoreBundle/Resources/views/Mailer/layout.html.twig
     *
     * @param string|array $to
     * @param string       $template
     * @param array        $context
     *
     * @return int
     */
    public function sendMessage($to, string $template, array $context = []): int
    {
        $context = $this->twig->mergeGlobals($context);
        $template = $this->twig->loadTemplate($template);
        $subject = $template->renderBlock('subject', $context);
        $htmlBody = $template->render($context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom([$this->mailerFrom => $this->mailerFromName])
            ->setTo($to)
            ->setBody($htmlBody, 'text/html');

        return $this->mailer->send($message);
    }
}
