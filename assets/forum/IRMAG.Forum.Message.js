/**
 * Персональное сообщение на форуме.
 *
 * @namespace IRMAG.Forum.Message
 *
 * @requires jquery
 * @requires select2
 * @requires Routing
 * @requires bootbox
 * @requires Handlebars
 */

import Routing from '../core/modules/routing';

IRMAG.Forum.Message = {
    init: function () {
        this.initUserSearchfield();
        this.initForm();
        this.initEditor();
        this.initEmojify();
        this.bindMarkAsRead();
        this.bindDeleteMessage();
        this.bindShowReply();
        this.bindBlockFormOnSubmit();
    },

    initUserSearchfield: function () {
        $('#forum-users-searchfield').select2({
            placeholder: 'Введите никнейм или имя пользователя',
            minimumInputLength: 2,
            allowClear: true,
            width: '100%',
            ajax: {
                url: Routing.generate('irmag_forum_get_users'),
                cache: true,
                dataType: 'json',
                type: 'POST',
                delay: 250,
                data: function (term) {
                    return { term: term };
                },
                results: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.name,
                                id: item.id,
                            };
                        }),
                    };
                },
            },
        });
    },

    initEditor: function () {
        IRMAG.Editor.defaults.hiddenButtons.push('Heading');
        IRMAG.Editor.defaults.ajaxConfig = function (e) {
            return {
                url: Routing.generate('irmag_markdown_to_html'),
                data: {
                    md: e.getContent(),
                    mode: 'forum_message',
                },
            };
        };

        IRMAG.Editor.defaults.needToEmojifyPreview = true;
        IRMAG.Editor.init($('#forum_message_text'));
    },

    initEmojify: function () {
        IRMAG.Emoji.emojify($('.forum-message-text'));
        IRMAG.Emoji.bindInput($('#forum_message_text'));
    },

    initForm: function () {
        const $recipient = $('.forum-message-whom');
        const recipientName = $recipient.data('to');

        if (recipientName !== '') {
            $('input.forum-nickname').val($recipient.data('recipient'));
            $('input#forum_message_recipient').val(recipientName);
        }
    },

    /* Пометить сообщение как прочитанное по прошествии 3 секунд просмотра */
    bindMarkAsRead: function () {
        setTimeout(function () {
            const isRead = $('.forum-message-container').data('read') !== '';
            const isInbox = $('.forum-message-container').data('type') === 'inbox';

            if (isRead === false && isInbox === true) {
                $.ajax({
                    url: Routing.generate('irmag_forum_mark_as_read'),
                    dataType: 'json',
                    data: {
                        id: $('.forum-message-container').data('id'),
                    },
                    success: function (data) {
                        if (data.success) {
                            $('.forum-message-container').data('read', 1);
                        }
                    },
                });
            }
        }, 3000);
    },

    /* Удалить сообщение (не отображать в папке пользователя) */
    bindDeleteMessage: function () {
        $(document).on('click', '.forum-message-remove', function (e) {
            e.preventDefault();
            const id = parseInt($('.forum-message-container').data('id'), 10);
            const type = $('.forum-message-container').data('type');

            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: 'Вы действительно хотите удалить это сообщение?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            url: Routing.generate('irmag_forum_message_remove'),
                            data: {
                                id: id,
                                type: type,
                            },
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    window.location = Routing.generate('irmag_forum_messages_list', { slug: type });
                                }
                            },
                            fail: function () {
                                bootbox.alert('Не удаётся удалить сообщение. Попробуйте ещё раз или обратитесь к администратору.');
                            },
                        });
                    }
                },
            });
        });
    },

    bindShowReply: function () {
        $(document).on('click', '#forum-message-reply', function (e) {
            e.preventDefault();
            const title = $('.forum-message-title strong').text();
            const id = $('.forum-message-container').data('id');

            $('#forum-new-msg').removeClass('hidden').fadeIn('slow');
            $('input#forum_message_title').val('Re: ' + title);
            $('input#forum_message_in_reply_to').val(id);
            $('#forum_message_text').focus();
            IRMAG.Emoji.bindInput($('#forum_message_text'));
        });
    },

    bindBlockFormOnSubmit: function () {
        function check(e) {
            const $searchfield = $('#forum-users-searchfield');

            if (0 !== $searchfield.length && !$searchfield.val()) {
                bootbox.alert('Введите получателя!');

                e.preventDefault();
                return false;
            }

            $('.forum-new-message-container').addClass('block-loading');
            $('.forum-spinner').show();
        }

        $('#forum_message_button').on('click', function (e) {
            return check(e);
        });
        $('form[name="forum_message"]').on('submit', function (e) {
            return check(e);
        });
    },
};
