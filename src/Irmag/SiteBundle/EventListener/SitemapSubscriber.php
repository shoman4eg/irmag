<?php

namespace Irmag\SiteBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Section;

class SitemapSubscriber implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param UrlGeneratorInterface  $router
     * @param EntityManagerInterface $em
     */
    public function __construct(
        UrlGeneratorInterface $router,
        EntityManagerInterface $em
    ) {
        $this->router = $router;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'registerPages',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function registerPages(SitemapPopulateEvent $event): void
    {
        if (empty($event->getSection()) || 'default' === $event->getSection()) {
            // все активные акции
            $actions = $this->em->getRepository(Action::class)->getActionsQb()->getQuery()->getResult();

            /** @var Action $action */
            foreach ($actions as $action) {
                $event->getUrlContainer()->addUrl(
                    new UrlConcrete(
                        $this->router->generate(
                            'irmag_action_detail',
                            ['slug' => $action->getSlug()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        new \DateTime(),
                        UrlConcrete::CHANGEFREQ_WEEKLY,
                        1
                    ),
                    'default'
                );
            }

            // последние 50 товаров
            $elements = $this->em->getRepository(Element::class)->findBy(['isActive' => true], ['updatedAt' => 'desc'], 50);

            /** @var Element $element */
            foreach ($elements as $element) {
                $event->getUrlContainer()->addUrl(
                    new UrlConcrete(
                        $this->router->generate(
                            'irmag_catalog_element',
                            ['id' => $element->getId()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        new \DateTime(),
                        UrlConcrete::CHANGEFREQ_DAILY,
                        1
                    ),
                    'default'
                );
            }

            // 1 уровень каталога
            $sections = $this->em->getRepository(Section::class)->findBy(['lvl' => 0, 'isActive' => true], ['updatedAt' => 'desc']);

            /** @var Section $section */
            foreach ($sections as $section) {
                $event->getUrlContainer()->addUrl(
                    new UrlConcrete(
                        $this->router->generate(
                            'irmag_catalog_section',
                            ['sectionId' => $section->getId()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        new \DateTime(),
                        UrlConcrete::CHANGEFREQ_WEEKLY,
                        0.9
                    ),
                    'default'
                );
            }
        }
    }
}
