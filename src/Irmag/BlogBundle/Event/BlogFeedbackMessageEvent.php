<?php

namespace Irmag\BlogBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\BlogBundle\Entity\BlogFeedbackMessage;

class BlogFeedbackMessageEvent extends Event
{
    /**
     * @var BlogFeedbackMessage
     */
    private $message;

    /**
     * @param BlogFeedbackMessage $message
     */
    public function __construct(BlogFeedbackMessage $message)
    {
        $this->message = $message;
    }

    /**
     * @return BlogFeedbackMessage
     */
    public function getBlogFeedbackMessage(): BlogFeedbackMessage
    {
        return $this->message;
    }
}
