<?php

namespace Irmag\OrderDeliveryServiceBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\BasketBundle\Traits\BasketContentTrait;
use Irmag\SiteBundle\EventDispatcher\DeliveryEventDispatcher;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Entity\Order;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData;

class OrderDeliveryServiceManager
{
    use BasketContentTrait;

    const DELIVERY_RESULTS_SESSION_LITERAL = 'delivery_results_data';
    const DELIVERY_DATA_SESSION_LITERAL = 'delivery_service_data';
    const DELIVERY_FLASH_MESSAGE = 'delivery_notice_basket_changed';
    const DELIVERY_DATA_TERMINALS = 'delivery_terminals';

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var DeliveryEventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param SessionInterface        $session
     * @param EntityManagerInterface  $em
     * @param BasketManager           $basketManager
     * @param DeliveryEventDispatcher $eventDispatcher
     */
    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        BasketManager $basketManager,
        DeliveryEventDispatcher $eventDispatcher
    ) {
        $this->session = $session;
        $this->em = $em;
        $this->basketManager = $basketManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Сохранить рассчитанные результаты.
     *
     * @param array $results
     */
    public function setCalculatedResults(array $results): void
    {
        $this->session->set(self::DELIVERY_RESULTS_SESSION_LITERAL, $results);
    }

    /**
     * Получить сохраненные рассчитанные результаты.
     *
     * @return array
     */
    public function getCalculatedResults(): array
    {
        return $this->session->get(self::DELIVERY_RESULTS_SESSION_LITERAL, []);
    }

    /**
     * Получить выбранный вариант.
     *
     * @return array
     */
    public function getSelectedData(): array
    {
        return $this->session->get(self::DELIVERY_DATA_SESSION_LITERAL, []);
    }

    /**
     * Сохранить данные о выбранном варианте доставки.
     *
     * @param array $options
     */
    public function saveDeliveryServiceData(array $options): void
    {
        $data = $this->session->get(self::DELIVERY_DATA_SESSION_LITERAL, []);

        if (!empty($options)) {
            $data = array_merge($data, $options);

            if (isset($options['deliveryService'])) {
                $deliveryService = $this->em
                    ->getRepository(OrderDeliveryService::class)
                    ->find($options['deliveryService']);

                if (!$deliveryService) {
                    throw new NotFoundHttpException(sprintf('Order delivery service (id = %d) was not found', $options['deliveryService']));
                }

                $data['deliveryServiceShortname'] = $deliveryService->getShortname();
                $data['deliveryServiceName'] = $deliveryService->getName();
            }

            if (isset($options['deliveryCity'])) {
                $deliveryCity = $this->em
                    ->getRepository(OrderDeliveryServiceCity::class)
                    ->find($options['deliveryCity']);

                if (!$deliveryCity) {
                    throw new NotFoundHttpException(sprintf('Order delivery service city (id = %d) was not found', $options['deliveryCity']));
                }

                $data['deliveryCityName'] = $deliveryCity->getName();
            }

            if (!empty($options['deliveryPointAddress'])) {
                $pointAddress = $data['deliveryPointAddress'];
                $chunks = explode("\n", $pointAddress);
                $address = '';

                foreach ($chunks as $chunk) {
                    $address .= trim($chunk);
                }

                $data['deliveryPointAddressFormatted'] = $address;
            }
        }

        $this->session->set(self::DELIVERY_DATA_SESSION_LITERAL, $data);
    }

    /**
     * Выбрать вариант доставки.
     *
     * @param array $variants
     */
    public function selectDeliveryVariant(array $variants): void
    {
        if (!$this->session->has(self::DELIVERY_RESULTS_SESSION_LITERAL)) {
            return;
        }

        $data = $this->session->get(self::DELIVERY_RESULTS_SESSION_LITERAL);
        $data = $this->unselectAllDeliveryVariants($data);

        if (!empty($variants['type']) && !empty($variants['variant'])) {
            $data[$variants['type']][$variants['variant']]['isSelected'] = true;
        }

        $this->session->set(self::DELIVERY_RESULTS_SESSION_LITERAL, $data);

        $options = $data[$variants['type']][$variants['variant']];
        $options['type'] = $variants['type'];
        $this->saveDeliveryServiceData($options);

        $this->eventDispatcher->dispatchSelectTransportCompany();
    }

    /**
     * Сохранить данные о выбранном варианте доставки.
     *
     * @param Order $order
     */
    public function setOrderDeliveryServiceData(Order $order): void
    {
        if ($this->session->has(self::DELIVERY_DATA_SESSION_LITERAL)) {
            $data = $this->session->get(self::DELIVERY_DATA_SESSION_LITERAL);

            if ($data['deliveryCity']) {
                $city = $this->em
                    ->createQueryBuilder()
                    ->select('c', 's', 'r')
                    ->from(OrderDeliveryServiceCity::class, 'c')
                    ->join('c.orderDeliveryService', 's')
                    ->join('c.orderDeliveryServiceRegion', 'r')
                    ->where('c.id = :cityId')
                    ->setParameter('cityId', $data['deliveryCity'])
                    ->getQuery()
                    ->getOneOrNullResult();

                if (!$city) {
                    throw new NotFoundHttpException(sprintf('Can not find OrderDeliveryServiceCity with id = "%d"', $data['deliveryCity']));
                }

                if (!empty($data['serviceName'])) {
                    $deliveryData = new OrderDeliveryServiceData();
                    $deliveryData
                        ->setOrder($order)
                        ->setOrderDeliveryServiceCity($city)
                        ->setOrderDeliveryService($city->getOrderDeliveryService())
                        ->setDays($data['days'])
                        ->setCost($data['cost'])
                        ->setTariff($data['serviceName'])
                        ->setType($data['type']);

                    if (!empty($data['deliveryPoint'])) {
                        $deliveryData
                            ->setDeliveryPoint($data['deliveryPoint'])
                            ->setDeliveryPointAddress($data['deliveryPointAddressFormatted']);
                    }

                    $this->em->persist($deliveryData);
                    $this->em->flush();
                    $this->em->refresh($order);

                    $this->clearDeliveryServiceData();
                }
            }
        }
    }

    /**
     * Очистить все данные расчёта доставки.
     */
    public function clearDeliveryServiceData(): void
    {
        if ($this->session->has(self::DELIVERY_DATA_SESSION_LITERAL)) {
            $this->session->remove(self::DELIVERY_DATA_SESSION_LITERAL);
        }

        if ($this->session->has(self::DELIVERY_RESULTS_SESSION_LITERAL)) {
            $this->session->remove(self::DELIVERY_RESULTS_SESSION_LITERAL);
        }

        if ($this->session->has(self::DELIVERY_DATA_TERMINALS)) {
            $this->session->remove(self::DELIVERY_DATA_TERMINALS);
        }

        $this->clearBasketContentSessionData();
    }

    /**
     * Стереть результаты доставки и выбранный вариант (неактуальны).
     */
    public function resetCalculatedResults(): void
    {
        if ($this->session->has(self::DELIVERY_RESULTS_SESSION_LITERAL)) {
            $this->session->remove(self::DELIVERY_RESULTS_SESSION_LITERAL);
        }

        if ($this->session->has(self::DELIVERY_DATA_SESSION_LITERAL)) {
            $data = $this->session->get(self::DELIVERY_DATA_SESSION_LITERAL);

            if (!empty($data['serviceName'])) {
                $this->addFlashMessageAboutBasketContentChanging(self::DELIVERY_FLASH_MESSAGE, 'Содержимое вашей корзины было изменено. Необходимо пересчитать стоимость доставки.');
            }

            $this->session->set(self::DELIVERY_DATA_SESSION_LITERAL, []);
        }
    }

    /**
     * Сохранить данные о пунктах доставки.
     *
     * @param array $data
     */
    public function setDeliveryTerminalsData(array $data): void
    {
        if (!empty($data['code'])) {
            $data[] = $data;
        }

        $this->session->set(self::DELIVERY_DATA_TERMINALS, $data);
    }

    /**
     * Получить данные по пунктам доставки в указанном городе.
     *
     * @return array
     */
    public function getDeliveryTerminalsData(): array
    {
        return $this->session->get(self::DELIVERY_DATA_TERMINALS, []);
    }

    /**
     * Отметить все варианты доставки как невыбранные.
     *
     * @param array $data Данные о доставке
     *
     * @return array
     */
    private function unselectAllDeliveryVariants(array $data): array
    {
        foreach ($data as &$value) {
            if (true === \is_array($value)) {
                foreach ($value as &$item) {
                    if (isset($item['isSelected'])) {
                        $item['isSelected'] = false;
                    }
                }
            }
        }

        return $data;
    }
}
