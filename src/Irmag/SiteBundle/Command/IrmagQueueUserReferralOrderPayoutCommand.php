<?php

namespace Irmag\SiteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Irmag\SiteBundle\Entity\UserReferralOrderPayoutQueue;
use Irmag\SiteBundle\Service\ReferralManager\ReferralManager;

class IrmagQueueUserReferralOrderPayoutCommand extends Command
{
    protected static $defaultName = 'irmag:queue:user_referral_order_payout';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ReferralManager
     */
    private $referralManager;

    /**
     * @param EntityManagerInterface $em
     * @param ReferralManager        $referralManager
     */
    public function __construct(
        EntityManagerInterface $em,
        ReferralManager $referralManager
    ) {
        $this->em = $em;
        $this->referralManager = $referralManager;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Do payment for user referral order')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userReferralOrderPayoutQueues = $this->em->getRepository(UserReferralOrderPayoutQueue::class)->findBy(['isPaid' => false]);

        foreach ($userReferralOrderPayoutQueues as $userReferralOrderPayoutQueue) {
            $this->referralManager->registerToTreeIfNeed($userReferralOrderPayoutQueue->getOrder()->getUser());
            $this->referralManager->doOrderPayment($userReferralOrderPayoutQueue->getOrder());
            $userReferralOrderPayoutQueue->setPaidAt(new \DateTime());
            $userReferralOrderPayoutQueue->setIsPaid(true);
            $this->em->persist($userReferralOrderPayoutQueue);
            $this->em->flush();
        }

        $output->writeln(sprintf('<info>Complete. Queues processed: %d.</info>', \count($userReferralOrderPayoutQueues)));
    }
}
