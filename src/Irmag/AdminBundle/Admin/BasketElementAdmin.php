<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class BasketElementAdmin extends AbstractBaseAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('basket')
            ->add('element', ModelAutocompleteType::class, [
                'property' => ['id', 'name', 'toneName'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('amount')
        ;
    }
}
