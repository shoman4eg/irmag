/**
 * Карусель товаров на главной.
 *
 * @namespace IRMAG.IndexElementsCarousel
 *
 * @requires jquery
 * @requires slick
 */

import log from '../core/modules/logger';

IRMAG.IndexElementsCarousel = {
    init: function () {
        const $container = $('#index-elements-carousel');
        const $slides = $container.find('.carousel-slide');

        if (0 !== $slides.length) {
            log('[IndexElementsCarousel]', 'init');

            $container.slick({
                dots: true,
                adaptiveHeight: true,
            });
        }
    },
};
