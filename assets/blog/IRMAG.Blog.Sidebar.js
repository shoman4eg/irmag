/**
 * Визуальные эффекты для боковой панели и менюшек мобильных версий.
 *
 * @namespace IRMAG.Blog.Sidebar
 *
 * @requires jquery
 */

import gif1 from './images/left-menu-gfx-1.gif';
import gif2 from './images/left-menu-gfx-2.gif';
import gif3 from './images/left-menu-gfx-3.gif';
import gif4 from './images/left-menu-gfx-4.gif';

IRMAG.Blog.Sidebar = {
    init: function () {
        this.bindSidebarHover();
        this.bindHighlightSidebarItem();
        this.bindTapSidebarItem();
    },

    /**
     * Тап = клик
     */
    bindTapSidebarItem: function () {
        $(document).on('tap', function () {
            $(this).click();
        });
    },

    /**
     * Подсветка пунктов меню при ховере.
     */
    bindSidebarHover: function () {
        const self = this;
        const offsets = [57, 49, 71, 57];
        const $leftMenuTopItems = $('ul.left-menu-top-items li');
        const basicColour = 'rgb(97, 100, 100)';
        let currentColor;

        $leftMenuTopItems.each(function () {
            const currentClass = $(this).attr('class');
            const currentClassId = parseInt(currentClass.substr(-1), 10);

            $('.' + currentClass).hover(function () {
                $(this).children('a:first').css('color', '#fff');
                self.moveBackgroundPosition('.' + currentClass, offsets[currentClassId - 1]);
            }, function () {
                $(this).children('a:first').css('color', '#616464');
                self.moveBackgroundPosition('.' + currentClass, offsets[currentClassId - 1]);
            });
        });

        $('ul.left-menu-bottom-items li').hover(function () {
            currentColor = $(this).find('span.left-menu-bottom-item-title:first').css('color');
            $(this).find('span.left-menu-bottom-item-title:first').css('color', '#4897cf').css('border-bottom-color', '#4897cf');
            $(this).find('span.left-menu-bottom-item-subtitle:first').css('color', '#fff');
        }, function () {
            if (currentColor === basicColour) {
                $(this).find('span.left-menu-bottom-item-title:first').css('color', '#616464').css('border-bottom-color', '#616464');
                $(this).find('span.left-menu-bottom-item-subtitle:first').css('color', '#616464');
            }
        });
    },

    /**
     * Визуальные эффекты для меню.
     */
    bindHighlightSidebarItem: function () {
        $(document).ready(function () {
            const category = $('.layout-pad').data('category');
            const slug = $('.layout-pad').data('slug');
            const sidebarBottomItems = ['items', 'users'];
            const sidebarTopItems = [
                {
                    slug: 'bio-products',
                    offset: '0 40px',
                    xsOffset: '0 25px',
                },
                {
                    slug: 'homeware-goods',
                    offset: '0 34px',
                    xsOffset: '0 25px',
                },
                {
                    slug: 'professional-tools',
                    offset: '0 50px',
                    xsOffset: '0 32px',
                },
                {
                    slug: 'cosmetics-and-perfumery',
                    offset: '0 40px',
                    xsOffset: '0 26px',
                },
            ];

            $('.top-menu li').find('span').css('backgroundPosition', '0 0');
            $('ul.left-menu-bottom-items li a span').css('color', '#616464').css('border-bottom-color', '#616464');
            $('.top-menu li a i').css('color', '#616464');

            if (category !== undefined) {
                for (let i = 0; i < sidebarTopItems.length; i++) {
                    if (sidebarTopItems[i].slug === category) {
                        // костыль для предотвращения кеширования гифок для сайдбара
                        const rand = IRMAG.Blog.Sidebar.getRandomInt(100000, 999999);
                        const imageUrls = [
                            gif1 + '?rnd=' + rand,
                            gif2 + '?rnd=' + rand,
                            gif3 + '?rnd=' + rand,
                            gif4 + '?rnd=' + rand,
                        ];

                        $('ul.left-menu-top-items li')
                            .find('i').removeAttr('class')
                            .parent().find('span').css('visibility', 'visible');
                        $('.left-menu-top-item-' + (i + 1))
                            .find('a:first').css('cursor', 'default')
                            .parent().find('span').css('visibility', 'hidden')
                            .parent().find('i:first').attr('class', 'left-menu-gfx-' + (i + 1)).css('background-image', 'url(' + imageUrls[i] + ')')
                            .parent().find('p:first').css('color', '#fff');
                        $('.top-menu-item-' + (i + 1)).css('backgroundPosition', sidebarTopItems[i].offset);
                        $('.top-menu-xs-item-pic-' + (i + 1)).css('backgroundPosition', sidebarTopItems[i].xsOffset);
                    }
                }
            }

            if (slug !== undefined) {
                for (let j = 0; j < sidebarBottomItems.length; j++) {
                    if (sidebarBottomItems[j] === slug) {
                        $('li.left-menu-bottom-item-' + (j + 1))
                            .find('.left-menu-bottom-item-title').css('color', '#4897cf').css('border-bottom-color', '#4897cf')
                            .parent().find('.left-menu-bottom-item-subtitle').css('color', '#fff');
                        $('i.top-menu-' + sidebarBottomItems[j]).css('color', '#fff');
                    }
                }
            }
        });
    },

    /**
     * Смещать подложку элементов меню.
     */
    moveBackgroundPosition: function (selector, step) {
        const bgPos = $(selector + '-pic:first').css('backgroundPosition').split(' ');
        $(selector + '-pic:first').css('backgroundPosition', parseInt(bgPos[0], 10) + ' ' + (parseInt(bgPos[1], 10) + step) + 'px');
    },

    /**
     * Получить рандомное целое число.
     */
    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
};
