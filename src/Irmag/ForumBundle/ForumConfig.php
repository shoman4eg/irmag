<?php

namespace Irmag\ForumBundle;

class ForumConfig
{
    /** Топиков на странице */
    const TOPICS_PER_PAGE = 15;
    /** Постов на странице */
    const POSTS_PER_PAGE = 20;

    /** Множитель за кол-во лайков */
    const FORUM_RATING_LIKES_MULTIPLIER = 3;
    /** Множитель за кол-во постов */
    const FORUM_RATING_POSTS_AMOUNT_MULTIPLIER = 1;
}
