<?php

use Irmag\Kernel;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

function fuckOffPlz()
{
    header('Location: https://irmag.ru/');
    http_response_code(666);
    die();
}

function checkIntAndFuckOffIfBadGuys($impliedPositive32bitInteger)
{
    if (!is_numeric($impliedPositive32bitInteger)) {
        fuckOffPlz();
    }

    if (!ctype_digit((string) $impliedPositive32bitInteger)) {
        fuckOffPlz();
    }

    $impliedPositive32bitInteger = (int) $impliedPositive32bitInteger;

    if ($impliedPositive32bitInteger < 0 || $impliedPositive32bitInteger > 2147483647) {
        fuckOffPlz();
    }
}

$checkMePlz = function (array $arr) use (&$checkMePlz) {
    foreach ($arr as $key => $value) {
        if (is_array($value)) {
            $checkMePlz($value);
        } else {
            if (1 === preg_match('/(select|union|--)/', $value)) {
                fuckOffPlz();
            }
        }
    }
};

$checkMePlz($_GET);
//$checkMePlz($_POST);
//checkMe($_REQUEST);
unset($checkMePlz);

require __DIR__.'/../vendor/autoload.php';

// dont use .env file in prod, pass it in `php/fpm/pool.d/www.conf` file.
if (!isset($_SERVER['APP_ENV'])) {
    (new Dotenv())->load(__DIR__.'/../.env');
}

$env = $_SERVER['APP_ENV'] ?? 'dev';
$debug = (bool) ($_SERVER['APP_DEBUG'] ?? ('prod' !== $env));

if ($debug) {
    if (class_exists(Debug::class)) {
        Debug::enable();
    }
}

$kernel = new Kernel($env, $debug);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
