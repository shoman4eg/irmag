<?php

namespace Irmag\BasketBundle\Transformer;

use Doctrine\ORM\EntityManager;
use Irmag\BasketBundle\Factory\BasketFactory;

class BasketAnonymousToAuthenticatedTransformer
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var BasketFactory
     */
    private $basketFactory;

    /**
     * @param BasketFactory $basketFactory
     * @param EntityManager $em
     */
    public function __construct(
        EntityManager $em,
        BasketFactory $basketFactory
    ) {
        $this->em = $em;
        $this->basketFactory = $basketFactory;
    }

    /**
     * Перемещает товары из анонимной корзины в аутентифицированную.
     */
    public function transform()
    {
        $anonymousBasket = $this->basketFactory->getAnonymous();
        $authenticatedBasket = $this->basketFactory->getAuthenticated();

        /** @var \Irmag\BasketBundle\Entity\BasketElement $anonymousBasketElement */
        foreach ($anonymousBasket->getBasketElements() as $basketElement) {
            $basketElement->setBasket($authenticatedBasket);
            $this->em->persist($basketElement);
        }

        $this->em->remove($anonymousBasket);
        $this->em->flush();
    }
}
