<?php

namespace Irmag\AdminBundle\Admin;

use Irmag\SiteBundle\Entity\Element;
use Qbbr\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class SectionElementAdmin extends AbstractBaseAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('section', ModelAutocompleteType::class, [
                'property' => ['id', 'name'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('element', EntityHiddenType::class, [
                'class' => Element::class,
            ])
        ;
    }
}
