<?php

namespace Irmag\SiteBundle\Service\BasketFavoriteManager;

use Irmag\CoreBundle\Exception\IrmagException;

class BasketFavoriteManagerException extends IrmagException
{
}
