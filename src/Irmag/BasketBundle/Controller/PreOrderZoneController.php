<?php

namespace Irmag\BasketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Irmag\SiteBundle\Entity\Element;

class PreOrderZoneController extends Controller
{
    public function indexAction()
    {
        $preOrderZoneCarousels = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Element::class)
            ->getPreOrderZoneCarousels();

        return $this->render('@IrmagBasket/PreOrderZone/index.html.twig', [
            'carousels' => $preOrderZoneCarousels,
        ]);
    }
}
