<?php

namespace Irmag\BasketBundle\Repository;

use Irmag\SiteBundle\Entity\Element;

class BasketElementRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Возвращает массив с товарами корзины.
     *
     * @param int  $basketId          Ид корзины
     * @param bool $fetchOnlyIsActive Выбирать только активные
     *
     * @return Element[]
     */
    public function getElementsByBasketId(int $basketId, bool $fetchOnlyIsActive = false): array
    {
        $qb = $this->createQueryBuilder('be')
            ->addSelect('e, p, file, tp')
            ->where('be.basket = :basketId')
            ->leftJoin('be.element', 'e')
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'file')
            ->leftJoin('e.tonePicture', 'tp')
            ->addOrderBy('e.isActive', 'ASC')
            ->addOrderBy('e.name', 'ASC')
            ->addOrderBy('e.toneName', 'ASC')
            ->setParameter('basketId', $basketId);

        if (true === $fetchOnlyIsActive) {
            $qb->andWhere('e.isActive = true');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $basketId
     *
     * @return Element[]
     */
    public function getElements(int $basketId): array
    {
        return $this->createQueryBuilder('be')
            ->select('e')
            ->where('be.basket = :basketId')
            ->leftJoin('be.element', 'e')
            ->setParameter('basketId', $basketId)
            ->getQuery()
            ->getResult();
    }
}
