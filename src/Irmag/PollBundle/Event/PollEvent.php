<?php

namespace Irmag\PollBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\PollBundle\Entity\Poll;

class PollEvent extends Event
{
    /**
     * @var Poll
     */
    private $poll;

    /**
     * @param Poll $poll
     */
    public function __construct(Poll $poll)
    {
        $this->poll = $poll;
    }

    /**
     * @return Poll
     */
    public function getPoll()
    {
        return $this->poll;
    }
}
