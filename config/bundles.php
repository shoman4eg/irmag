<?php

/**
 * Available environments: (all) or (dev, test, prod).
 */
$bundles = [
    // Symfony core Bundles
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    // 3rd party Bundles
    JMS\SerializerBundle\JMSSerializerBundle::class => ['all' => true],
    Knp\Bundle\PaginatorBundle\KnpPaginatorBundle::class => ['all' => true],
    Liip\ImagineBundle\LiipImagineBundle::class => ['all' => true],
    Presta\SitemapBundle\PrestaSitemapBundle::class => ['all' => true],
    Qbbr\EntityHiddenTypeBundle\EntityHiddenTypeBundle::class => ['all' => true],
    Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    // IRMAG Bundles
    Irmag\AttachmentBundle\IrmagAttachmentBundle::class => ['all' => true],
    Irmag\BasketBundle\IrmagBasketBundle::class => ['all' => true],
    Irmag\BlogBundle\IrmagBlogBundle::class => ['all' => true],
    Irmag\CoreBundle\IrmagCoreBundle::class => ['all' => true],
    Irmag\Exchange1CBundle\IrmagExchange1CBundle::class => ['all' => true],
    Irmag\FavoriteBundle\IrmagFavoriteBundle::class => ['all' => true],
    Irmag\ForumBundle\IrmagForumBundle::class => ['all' => true],
    Irmag\OnlinePaymentBundle\IrmagOnlinePaymentBundle::class => ['all' => true],
    Irmag\OrderDeliveryServiceBundle\IrmagOrderDeliveryServiceBundle::class => ['all' => true],
    Irmag\OrderPostDeliveryBundle\IrmagOrderPostDeliveryBundle::class => ['all' => true],
    Irmag\PollBundle\IrmagPollBundle::class => ['all' => true],
    Irmag\ProfileBundle\IrmagProfileBundle::class => ['all' => true],
    Irmag\SberbankAcquiringBundle\IrmagSberbankAcquiringBundle::class => ['all' => true],
    Irmag\SiteBundle\IrmagSiteBundle::class => ['all' => true],
    Irmag\ThreadCommentBundle\IrmagThreadCommentBundle::class => ['all' => true],
    Irmag\DriveBundle\IrmagDriveBundle::class => ['all' => true],
    // IRMAG API
    FOS\RestBundle\FOSRestBundle::class => ['all' => true],
    Nelmio\ApiDocBundle\NelmioApiDocBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
    Irmag\RestApiBundle\IrmagRestApiBundle::class => ['all' => true],
    // IRMAG Admin
    Sonata\CoreBundle\SonataCoreBundle::class => ['all' => true],
    Sonata\BlockBundle\SonataBlockBundle::class => ['all' => true],
    Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle::class => ['all' => true],
    Sonata\AdminBundle\SonataAdminBundle::class => ['all' => true],
    Sonata\FormatterBundle\SonataFormatterBundle::class => ['all' => true],
    FOS\CKEditorBundle\FOSCKEditorBundle::class => ['all' => true],
    Knp\Bundle\MenuBundle\KnpMenuBundle::class => ['all' => true],
    Knp\Bundle\MarkdownBundle\KnpMarkdownBundle::class => ['all' => true],
    SimpleThings\EntityAudit\SimpleThingsEntityAuditBundle::class => ['all' => true],
    Irmag\AdminBundle\IrmagAdminBundle::class => ['all' => true],
    // only: dev, test
    Symfony\Bundle\DebugBundle\DebugBundle::class => ['dev' => true, 'test' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Sensio\Bundle\DistributionBundle\SensioDistributionBundle::class => ['dev' => true, 'test' => true],
    Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle::class => ['dev' => true, 'test' => true],
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['dev' => true, 'test' => true],
    FOS\JsRoutingBundle\FOSJsRoutingBundle::class => ['dev' => true, 'test' => true],
    // only: dev
    Symfony\Bundle\WebServerBundle\WebServerBundle::class => ['dev' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
];

if (class_exists(IrmagExtra\GeneratorBundle\IrmagExtraGeneratorBundle::class)) {
    $bundles[IrmagExtra\GeneratorBundle\IrmagExtraGeneratorBundle::class] = ['dev' => true, 'test' => true];
}

return $bundles;
