<?php

namespace Irmag\SiteBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Irmag\SiteBundle\Variable\IrmagVariable;

class IrmagExtension extends AbstractExtension implements GlobalsInterface
{
    /**
     * @var IrmagVariable
     */
    private $irmagVariable;

    /**
     * @param IrmagVariable $irmagVariable
     */
    public function __construct(IrmagVariable $irmagVariable)
    {
        $this->irmagVariable = $irmagVariable;
    }

    /**
     * {@inheritdoc}
     */
    public function getGlobals()
    {
        return [
            'irmag' => $this->irmagVariable,
        ];
    }
}
