<?php

namespace Irmag\ThreadCommentBundle\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Qbbr\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Irmag\AttachmentBundle\Form\Type\AttachmentUploaderType;
use Irmag\ThreadCommentBundle\Entity\ThreadCommentAttachment;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;

class ThreadCommentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', null, [
                'label' => false,
            ])
            ->add('parent', EntityHiddenType::class, [
                'class' => ThreadComment::class,
            ])
            ->add('rating', HiddenType::class)
            ->add('send', SubmitType::class, [
                'label' => 'Отправить',
            ])
            ->add('uploader', AttachmentUploaderType::class)
            ->add('attachments', CollectionType::class, [
                'entry_type' => EntityHiddenType::class,
                'entry_options' => [
                    'label' => false,
                    'required' => false,
                    'class' => ThreadCommentAttachment::class,
                ],
                'label' => false,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'attr' => [
                    'class' => 'attachment-items',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'irmag_thread_comment';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ThreadComment::class,
            'translation_domain' => false,
        ]);
    }
}
