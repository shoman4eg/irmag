<?php

namespace Irmag\OrderDeliveryServiceBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;

class OrderDeliverySelectPostSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrderDeliveryServiceManager
     */
    private $manager;

    /**
     * @param OrderDeliveryServiceManager $manager
     */
    public function __construct(OrderDeliveryServiceManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::DELIVERY_SELECT_POST => 'onSelectPost',
        ];
    }

    /**
     * Стирает данные расчета доставки почтой.
     */
    public function onSelectPost(): void
    {
        $this->manager->resetCalculatedResults();
    }
}
