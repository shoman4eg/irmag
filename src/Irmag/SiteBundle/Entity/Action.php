<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Irmag\ThreadCommentBundle\Entity\Traits\ThreadTrait;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;

/**
 * @ORM\Table(
 *     name="actions",
 *     indexes={
 *         @ORM\Index(columns={"start_date_time"}),
 *         @ORM\Index(columns={"end_date_time"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\ActionRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Action implements ThreadOwnerInterface
{
    use TimestampableEntity;
    use ThreadTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode_elements_list", "search"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max=128)
     *
     * @Gedmo\Slug(fields={"title"})
     *
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * Фон баннера.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $bannerBg;

    /**
     * Баннер.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $banner;

    /**
     * Стикер для товара.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $sticker;

    /**
     * Товары участвующие в акции.
     *
     * @var Collection|ActionElement[]
     *
     * @ORM\OneToMany(targetEntity="ActionElement", mappedBy="action", cascade={"persist"}, orphanRemoval=true)
     */
    private $elements;

    /**
     * @ORM\Column(name="tsv", type="tsvector", nullable=true)
     */
    private $tsv;

    /**
     * Дата старта акции.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private $startDateTime;

    /**
     * Дата окончания акции.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private $endDateTime;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $position;

    /**
     * Для ранжирования на главной (вначале "Важные").
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isImportant;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * @var PromocodeElementsList
     *
     * @ORM\OneToOne(targetEntity="PromocodeElementsList", mappedBy="action")
     */
    private $promocodeElementsList;

    /**
     * Условие - минимальная сумма товаров.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true, options={"default": null})
     */
    private $conditionMinSum;

    /**
     * Условие - минимальное кол-во товаров.
     *
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true, options={"default": null})
     */
    private $conditionMinCount;

    /**
     * Условие - способ оплаты.
     *
     * @ORM\ManyToOne(targetEntity="OrderPaymentMethod")
     *
     * @var OrderPaymentMethod
     */
    private $conditionPaymentMethod;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $conditionIsForAllElements;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $conditionMaxGifts;

    public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->startDateTime = new \DateTime();
        $this->position = 0;
        $this->isImportant = false;
        $this->isActive = true;
        $this->conditionIsForAllElements = false;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->title;
    }

    /**
     * XXX: Custom for ThreadCommentBundle.
     */
    public function setThreadOwnerRoute(): void
    {
        $this->getThread()->setOwnerRoute([
            'name' => 'irmag_action_detail',
            'parameters' => [
                'slug' => $this->getSlug(),
            ],
        ]);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getStartDateTime(): ?\DateTimeInterface
    {
        return $this->startDateTime;
    }

    public function setStartDateTime(\DateTimeInterface $startDateTime): self
    {
        $this->startDateTime = $startDateTime;

        return $this;
    }

    public function getEndDateTime(): ?\DateTimeInterface
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(\DateTimeInterface $endDateTime): self
    {
        $this->endDateTime = $endDateTime;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getIsImportant(): ?bool
    {
        return $this->isImportant;
    }

    public function setIsImportant(bool $isImportant): self
    {
        $this->isImportant = $isImportant;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getBannerBg(): ?File
    {
        return $this->bannerBg;
    }

    public function setBannerBg(?File $bannerBg): self
    {
        $this->bannerBg = $bannerBg;

        return $this;
    }

    public function getBanner(): ?File
    {
        return $this->banner;
    }

    public function setBanner(?File $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    public function getSticker(): ?File
    {
        return $this->sticker;
    }

    public function setSticker(?File $sticker): self
    {
        $this->sticker = $sticker;

        return $this;
    }

    /**
     * @return Collection|ActionElement[]
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addElement(ActionElement $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
            $element->setAction($this);
        }

        return $this;
    }

    public function removeElement(ActionElement $element): self
    {
        if ($this->elements->contains($element)) {
            $this->elements->removeElement($element);
            // set the owning side to null (unless already changed)
            if ($element->getAction() === $this) {
                $element->setAction(null);
            }
        }

        return $this;
    }

    public function getPromocodeElementsList(): ?PromocodeElementsList
    {
        return $this->promocodeElementsList;
    }

    public function setPromocodeElementsList(?PromocodeElementsList $promocodeElementsList): self
    {
        $this->promocodeElementsList = $promocodeElementsList;

        // set (or unset) the owning side of the relation if necessary
        $newAction = null === $promocodeElementsList ? null : $this;
        if ($newAction !== $promocodeElementsList->getAction()) {
            $promocodeElementsList->setAction($newAction);
        }

        return $this;
    }

    public function getTsv()
    {
        return $this->tsv;
    }

    public function setTsv($tsv): self
    {
        $this->tsv = $tsv;

        return $this;
    }

    public function getConditionMinSum()
    {
        return $this->conditionMinSum;
    }

    public function setConditionMinSum($conditionMinSum): self
    {
        $this->conditionMinSum = $conditionMinSum;

        return $this;
    }

    public function getConditionMinCount(): ?int
    {
        return $this->conditionMinCount;
    }

    public function setConditionMinCount(?int $conditionMinCount): self
    {
        $this->conditionMinCount = $conditionMinCount;

        return $this;
    }

    public function getConditionPaymentMethod(): ?OrderPaymentMethod
    {
        return $this->conditionPaymentMethod;
    }

    public function setConditionPaymentMethod(?OrderPaymentMethod $conditionPaymentMethod): self
    {
        $this->conditionPaymentMethod = $conditionPaymentMethod;

        return $this;
    }

    public function getConditionIsForAllElements(): ?bool
    {
        return $this->conditionIsForAllElements;
    }

    public function setConditionIsForAllElements(bool $conditionIsForAllElements): self
    {
        $this->conditionIsForAllElements = $conditionIsForAllElements;

        return $this;
    }

    public function getConditionMaxGifts(): ?int
    {
        return $this->conditionMaxGifts;
    }

    public function setConditionMaxGifts(?int $conditionMaxGifts): self
    {
        $this->conditionMaxGifts = $conditionMaxGifts;

        return $this;
    }
}
