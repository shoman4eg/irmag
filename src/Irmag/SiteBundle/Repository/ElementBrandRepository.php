<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\Traits\TSearchBrandTrait;

class ElementBrandRepository extends \Doctrine\ORM\EntityRepository
{
    use TSearchBrandTrait;
}
