<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180628075953 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_delivery_selfservice_methods_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_delivery_selfservice_methods (id INT NOT NULL, name VARCHAR(255) NOT NULL, delivery_street VARCHAR(255) NOT NULL, delivery_house VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A8B6595B5E237E06 ON orders_delivery_selfservice_methods (name)');

        $this->addSql("
            INSERT INTO orders_delivery_selfservice_methods
                (id, name, delivery_street, delivery_house)
            VALUES
                (nextval('orders_delivery_selfservice_methods_id_seq') ,'ИРМАГ', 'Восточный пер.', '8/2'),
                (nextval('orders_delivery_selfservice_methods_id_seq'), 'Класс-Маркет', 'Юрия Тена', '21')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE orders_delivery_selfservice_methods CASCADE');
    }
}
