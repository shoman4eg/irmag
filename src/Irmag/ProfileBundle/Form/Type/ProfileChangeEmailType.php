<?php

namespace Irmag\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Irmag\ProfileBundle\Form\Model\ProfileChangeEmail;

class ProfileChangeEmailType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('newEmail', null, [
                'label' => 'Новый Email',
                'required' => true,
            ])
            ->add('currentPassword', PasswordType::class, [
                'label' => 'Текущий пароль',
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Изменить email'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProfileChangeEmail::class,
            'translation_domain' => false,
        ]);
    }
}
