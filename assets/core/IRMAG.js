/**
 * @namespace IRMAG
 *
 * @requires jquery
 * @requires bootbox
 */

import Config from './modules/irmag-config';
import error from './modules/errorbox';

global.IRMAG = {
    init: function () {
        // set default POST method for ajax requests
        // and error handler
        $.ajaxSetup({
            method: 'POST',
            timeout: 60000, // 60 sec
            error: function (xhr) {
                if ('timeout' === xhr.statusText) {
                    return error('Превышено время ожидания запроса. Попробуйте обновить страницу и повторить действие.');
                } if ('abort' === xhr.statusText) {
                    return;
                }

                let text = 'Ошибка AJAX запроса'
                    + '\n'
                    + '\n' + 'Request Status: ' + xhr.status
                    + '\n' + 'Status Text: ' + xhr.statusText;

                if (Config.debug) {
                    // nl2br
                    text = text.replace(/([^>])\n/g, '$1<br>');

                    if ('undefined' !== typeof xhr.responseJSON && 'undefined' !== xhr.responseJSON.message) {
                        text += '<br>' + 'Message: <code class="text-danger">' + xhr.responseJSON.message + '</code>';
                    }

                    text += '<div class="mt-15"><code>' + xhr.responseText + '</code></div>';
                    error(text, { className: 'irmag-modal-error lg' });
                } else {
                    console.log(text);
                }
            },
        });

        // bootbox defaults
        bootbox.setDefaults({
            locale: Config.language,
            onEscape: true,
        });
    },
};
