<?php

namespace Irmag\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ProfileBundle\Entity\UserConfirmation;
use Irmag\ProfileBundle\Entity\User;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\SiteBundle\Entity\Order;

class MailerController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * Рендер Email шаблонов (dev env only).
     *
     * @example /_mailer/@App/path/template.html.twig
     *
     * @param string $templatePath
     *
     * @return Response
     */
    public function renderAction(string $templatePath): Response
    {
        // first User
        $user = $this->em->getRepository(User::class)->findOneBy([], ['id' => 'ASC']);
        // last UserConfirmation
        $userConfirmation = $this->em->getRepository(UserConfirmation::class)->findOneBy([], ['createdAt' => 'DESC']);
        // last Order
        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy([], ['id' => 'DESC']);
        /* @var \Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment $payment */
        //$payment = $order->getOnlinePayments()->first();
        // last OrderOnlinePayment
        $payment = $this->em->getRepository(OrderOnlinePayment::class)->findOneBy([], ['createdAt' => 'DESC']);
        // last ThreadComment
        $comment = $this->em->getRepository(ThreadComment::class)->findOneBy([], ['createdAt' => 'DESC']);
        // last BlogPost
        $post = $this->em->getRepository(BlogPost::class)->findOneBy([], ['createdAt' => 'DESC']);
        // forum

        return $this->render('@IrmagCore/Mailer/render.html.twig', [
            'templatePath' => $templatePath,
            'user' => $user,
            'userConfirmation' => $userConfirmation,
            'order' => $order,
            'payment' => $payment,
            'comment' => $comment,
            'post' => $post,
        ]);
    }
}
