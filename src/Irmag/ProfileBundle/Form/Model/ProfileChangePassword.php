<?php

namespace Irmag\ProfileBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ProfileChangePassword extends ProfileCurrentPassword
{
    /**
     * @Assert\NotBlank(message="Поле 'Новый пароль' не должно быть пустым")
     * @Assert\Length(
     *     min=6,
     *     max=64,
     *     minMessage="Поле 'Новый пароль' должно быть не менее {{ limit }} символов",
     *     maxMessage="Поле 'Новый пароль' должно быть не более {{ limit }} символов",
     * )
     */
    protected $newPassword;

    public function getNewPassword()
    {
        return $this->newPassword;
    }

    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }
}
