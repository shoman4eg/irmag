<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="users_bonus_history")
 * @ORM\Entity(repositoryClass="Irmag\ProfileBundle\Repository\UserBonusHistoryRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class UserBonusHistory
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Uuid
     *
     * @ORM\Column(type="guid", nullable=true, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $uuid1C;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $user;

    /**
     * @var int
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $value;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $message;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_user_bonus_history"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid1C(): ?string
    {
        return $this->uuid1C;
    }

    public function setUuid1C(?string $uuid1C): self
    {
        $this->uuid1C = $uuid1C;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
