<?php

namespace Irmag\ProfileBundle\Service\UserNotificationEventManager;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;

class UserNotificationEventManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @param string $name Event name
     *
     * @throws IrmagException When UserNotificationEvent not found
     *
     * @return UserNotificationEvent
     */
    public function getEventByName(string $name): UserNotificationEvent
    {
        $event = $this->em->getRepository(UserNotificationEvent::class)->findOneBy(['name' => $name]);

        if (!$event instanceof UserNotificationEvent) {
            throw new IrmagException(sprintf('UserNotificationEvent with name "%s" not found.', $name));
        }

        return $event;
    }
}
