<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\BlogBundle\BlogConfig;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostVote;
use Irmag\BlogBundle\EventDispatcher\BlogPostEventDispatcher;

class VoteController extends Controller
{
    /**
     * @Route("/ajax/vote/",
     *     name="irmag_blog_vote_for_post",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"},
     *     host="%irmag_blog_domain%",
     * )
     *
     * @param Request $request
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function voteForPostAction(Request $request): JsonResponse
    {
        $flag = false;
        $score = $request->request->getInt('score');
        $postId = $request->request->getInt('post');
        $user = $this->getUser();
        $em = $this->get('doctrine.orm.default_entity_manager');
        /** @var BlogPost $post */
        $post = $em->getRepository(BlogPost::class)->find($postId);

        if (!$post) {
            throw $this->createNotFoundException('Post was not found!');
        }

        $oldVote = ($user) ?
            $em->getRepository(BlogPostVote::class)->findBy(['post' => $post, 'user' => $user]) :
            $em->getRepository(BlogPostVote::class)->findBy(['post' => $post, 'token' => $this->getToken($request)]);

        if (!$oldVote && 0 !== $score) {
            $vote = new BlogPostVote();
            $vote->setPost($post);
            $vote->setScore($score);

            if ($user) {
                $vote->setUser($user);
            } else {
                $token = $this->getToken($request);
                $vote->setToken($token);
            }

            $em->persist($vote);

            $currentRating = $post->getRating();
            $post->setRating($currentRating + $score);
            $em->persist($post);

            $em->flush();

            $flag = true;
        }

        $this->rewardForPost($post);

        return new JsonResponse([
            'status' => $flag,
        ]);
    }

    /**
     * Начисляет бонусы за пост
     *
     * @param BlogPost $post Пост в блоге
     */
    private function rewardForPost(BlogPost $post): void
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $currentRating = $post->getRating();
        $likesForReward = BlogConfig::BLOG_BONUS_REWARD_LIKES_AMOUNT;

        if (!$post->getIsBonusPayed() && $currentRating >= $likesForReward) {
            $bonusManager = $this->get(BonusManager::class);
            $bonusManager->setUser($post->getUser());

            $rewardAmount = BlogConfig::BLOG_BONUS_REWARD_SUM;

            $message = sprintf('За полезный пост в Блоге Ирмага «%s»', $post->getTitle());
            $bonusManager->update($rewardAmount, $message);
            $post->setIsBonusPayed(true);

            $em->persist($post);
            $em->flush();

            $this->get(BlogPostEventDispatcher::class)->dispatchBlogPostGetReward($post);
        }
    }

    /**
     * Генерирует токен для голосования.
     *
     * @param Request $request
     *
     * @return string
     */
    private function getToken(Request $request): string
    {
        return md5($request->getClientIp().$request->headers->get('User-Agent'));
    }
}
