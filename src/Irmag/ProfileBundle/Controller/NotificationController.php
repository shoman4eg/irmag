<?php

namespace Irmag\ProfileBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\ProfileBundle\Entity\UserNotification;

/**
 * @Route("/notifications")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class NotificationController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @Route("/", name="irmag_profile_notifications")
     */
    public function indexAction(Request $request): Response
    {
        $notifications = $this->get('knp_paginator')->paginate(
            $this->em->getRepository(UserNotification::class)->findBy(['user' => $this->getUser()], ['isAsRead' => 'ASC', 'createdAt' => 'DESC']),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('@IrmagProfile/Notification/index.html.twig', [
            'notifications' => $notifications,
        ]);
    }

    /**
     * @Route("/mark-as-read/",
     *     name="irmag_profile_notification_mark_as_read",
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true},
     *     methods={"POST"}
     * )
     */
    public function markAsReadAction(Request $request): Response
    {
        $id = $request->request->getInt('id');
        $notify = $this->em->getRepository(UserNotification::class)->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if ($notify) {
            $notify->setIsAsRead(true);
            $this->em->persist($notify);
            $this->em->flush();
        }

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/mark-all-as-read/",
     *     name="irmag_profile_notification_mark_all_as_read",
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true},
     *     methods={"POST"}
     * )
     */
    public function markAllAsReadAction(): Response
    {
        $notifications = $this->em->getRepository(UserNotification::class)->findBy(['user' => $this->getUser(), 'isAsRead' => false]);

        /** @var UserNotification $notify */
        foreach ($notifications as $notify) {
            $notify->setIsAsRead(true);
            $this->em->persist($notify);
        }

        $this->em->flush();

        return new JsonResponse(['success' => true]);
    }
}
