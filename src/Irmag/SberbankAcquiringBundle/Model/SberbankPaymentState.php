<?php

namespace Irmag\SberbankAcquiringBundle\Model;

use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentStateInterface;
use Irmag\SberbankAcquiringBundle\Exception\SberbankAcquiringException;

class SberbankPaymentState implements PaymentStateInterface
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var array
     */
    private $extraData;

    /**
     * @param array|null $data
     */
    public function __construct(array $data = null)
    {
        if (!empty($data)) {
            if (!empty($data['orderStatus'])) {
                $this->status = $data['orderStatus'];
            }

            if (!empty($data['amount'])) {
                $this->amount = (int) $data['amount'];
            }

            if (!empty($data['orderNumber'])) {
                $this->orderId = $data['orderNumber'];
            }

            $this->extraData = $data;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Вернуть статус платежа.
     *
     * @throws SberbankAcquiringException
     *
     * @return string
     */
    public function getStatus(): string
    {
        switch ($this->status) {
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REVERTED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_REVERTED_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME;
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED:
                $paymentStatus = OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME;
                break;
            default:
                throw new SberbankAcquiringException(sprintf('OrderOnlinePayment status id %d did not match any existing status', $this->status));
        }

        return $paymentStatus;
    }

    /**
     * @param int $amount
     *
     * @return SberbankPaymentState
     */
    public function setAmount(int $amount): self
    {
        $this->setAmount($amount);

        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param string $orderId
     *
     * @return SberbankPaymentState
     */
    public function setOrderId(string $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return array
     */
    public function getExtraData(): array
    {
        return $this->extraData ?? [];
    }

    /**
     * @param array $extraData
     *
     * @return SberbankPaymentState
     */
    public function setExtraData(array $extraData): self
    {
        $this->extraData = $extraData;

        return $this;
    }
}
