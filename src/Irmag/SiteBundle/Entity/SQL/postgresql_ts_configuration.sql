--
-- public.irmag ts vector configuration
--
-- set to `/etc/postgresql/9.6/main/postgresql.conf`:
--     default_text_search_config = 'public.irmag'
--
-- put dicts to `/usr/share/postgresql/9.6/tsearch_data/`:
--     irmag_synonym.syn
--     irmag_thesaurus.ths
--
-- use (see tsrank for order by rank):
--     SELECT * FROM elements WHERE tsv @@ phraseto_tsquery('irmag', 'лак для волос');
--

-- fork default russian config
DROP TEXT SEARCH CONFIGURATION IF EXISTS public.irmag;
CREATE TEXT SEARCH CONFIGURATION public.irmag ( COPY = pg_catalog.russian );

-- synonym dict (irmag_synonym.syn)
DROP TEXT SEARCH DICTIONARY IF EXISTS irmag_synonym;
CREATE TEXT SEARCH DICTIONARY irmag_synonym (
    TEMPLATE = synonym,
    SYNONYMS = irmag_synonym
);

-- thesaurus dict (irmag_thesaurus.ths)
DROP TEXT SEARCH DICTIONARY IF EXISTS irmag_thesaurus;
CREATE TEXT SEARCH DICTIONARY irmag_thesaurus (
    TEMPLATE = thesaurus,
    DictFile = irmag_thesaurus,
    Dictionary = pg_catalog.russian_stem
);

-- map types
ALTER TEXT SEARCH CONFIGURATION irmag
    ALTER MAPPING FOR asciiword, asciihword, hword_asciipart,
                      word, hword, hword_part
    WITH irmag_thesaurus, irmag_synonym, russian_stem;

-- drop unised type mapping
ALTER TEXT SEARCH CONFIGURATION irmag
    DROP MAPPING FOR email, url, url_path, sfloat, float;

-- elements auto update trigger
DROP TRIGGER IF EXISTS tsvectorupdate_elements ON elements;
CREATE TRIGGER tsvectorupdate_elements BEFORE INSERT OR UPDATE ON elements
    FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'public.irmag', name, tone_name, description, barcode);

-- manufacturers auto update trigger
DROP TRIGGER IF EXISTS tsvectorupdate_manufacturers ON elements_manufacturers;
CREATE TRIGGER tsvectorupdate_manufacturers BEFORE INSERT OR UPDATE ON elements_manufacturers
    FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'public.irmag', name);

-- brands auto update trigger
DROP TRIGGER IF EXISTS tsvectorupdate_brands ON elements_brands;
CREATE TRIGGER tsvectorupdate_brands BEFORE INSERT OR UPDATE ON elements_brands
    FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'public.irmag', name);

-- sections auto update
DROP TRIGGER IF EXISTS tsvectorupdate_sections ON sections;
CREATE TRIGGER tsvectorupdate_sections BEFORE INSERT OR UPDATE ON sections
    FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'public.irmag', name);

-- action auto update
DROP TRIGGER IF EXISTS tsvectorupdate_actions ON actions;
CREATE TRIGGER tsvectorupdate_actions BEFORE INSERT OR UPDATE ON actions
    FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(tsv, 'public.irmag', title, body);

-- synonym manual update
ALTER TEXT SEARCH DICTIONARY irmag_synonym ( dummy );
-- thesaurus manual update
ALTER TEXT SEARCH DICTIONARY irmag_thesaurus ( dummy );

-- elements GIN index
DROP INDEX IF EXISTS idx_tsv_on_elements;
CREATE INDEX idx_tsv_on_elements ON elements USING GIN (tsv);
-- manufacturers GIN index
DROP INDEX IF EXISTS idx_tsv_on_elements_manufacturers;
CREATE INDEX idx_tsv_on_elements_manufacturers ON elements_manufacturers USING GIN (tsv);
-- brands GIN index
DROP INDEX IF EXISTS idx_tsv_on_elements_brands;
CREATE INDEX idx_tsv_on_elements_brands ON elements_brands USING GIN (tsv);
-- sections GIN index
DROP INDEX IF EXISTS idx_tsv_on_sections;
CREATE INDEX idx_tsv_on_sections ON sections USING GIN (tsv);
-- actions GIN index
DROP INDEX IF EXISTS idx_tsv_on_actions;
CREATE INDEX idx_tsv_on_actions ON actions USING GIN (tsv);

-- elements manual update
UPDATE elements SET name = name;
-- manufacturers manual update
UPDATE elements_manufacturers SET name = name;
-- brands manual update
UPDATE elements_brands SET name = name;
-- sections manual update
UPDATE sections SET name = name;
-- actions manual update
UPDATE actions SET title = title;
