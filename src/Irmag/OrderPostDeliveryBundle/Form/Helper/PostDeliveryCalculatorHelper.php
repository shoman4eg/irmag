<?php

namespace Irmag\OrderPostDeliveryBundle\Form\Helper;

use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;
use Irmag\SiteBundle\Service\GeoLocator\GeoLocator;

class PostDeliveryCalculatorHelper
{
    /**
     * @var OrderPostDeliveryManager
     */
    private $dm;

    /**
     * @var GeoLocator
     */
    private $geolocator;

    /**
     * @param OrderPostDeliveryManager $dm
     * @param GeoLocator               $geoLocator
     */
    public function __construct(
        OrderPostDeliveryManager $dm,
        GeoLocator $geoLocator
    ) {
        $this->dm = $dm;
        $this->geolocator = $geoLocator;
    }

    /**
     * Вернуть текущий индекс, если он был указан пользователем.
     *
     * @return int|null
     */
    public function getCurrentIndex(): ?int
    {
        $result = $this->dm->getResult();
        $currentIndex = null;

        if (!empty($result)) {
            $currentIndex = $result['indexTo'];
        }

        return $currentIndex;
    }

    /**
     * Вернуть индекс основного почтового отделения в нас. пункте, если такой удастся получить.
     *
     * @return int|null
     */
    public function getIndexPlaceholder(): ?int
    {
        if (empty($this->getCurrentIndex())) {
            $placeholder = $this->geolocator->getCurrentMainIndex();
        }

        return $placeholder ?? null;
    }
}
