<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementKit;
use Irmag\SiteBundle\Entity\Section;

/**
 * @Route("/cat")
 */
class ElementController extends Controller
{
    const ELEMENT_KITS_TYPE_ELEMENT = 1;
    const ELEMENT_KITS_TYPE_CHILDRENS = 2;

    /**
     * Карточка товара.
     *
     * @Route("/{sectionId}/i{id}/",
     *     name="irmag_catalog_element_with_section",
     *     requirements={"sectionId": "\d+", "id": "\d+"}
     * )
     * @Route("/i{id}/", name="irmag_catalog_element", requirements={"id": "\d+"}, options={"expose": true})
     *
     * @param EntityManagerInterface $em
     * @param int                    $id        Ид товара
     * @param int|null               $sectionId Ид раздела
     *
     * @return Response
     */
    public function index(EntityManagerInterface $em, int $id, int $sectionId = null): Response
    {
        $elementRepo = $em->getRepository(Element::class);

        /** @var Element $element */
        if (!$element = $elementRepo->getOneById($id)) {
            throw $this->createNotFoundException(sprintf('Element with id "%d" not found.', $id));
        }

        $sectionRepo = $em->getRepository(Section::class);

        if (null !== $sectionId) {
            $section = $sectionRepo->find($sectionId);
        } elseif (false === $element->getSectionElements()->isEmpty()) {
            $section = $element->getSectionElements()->first()->getSection();
        }

        if (!isset($section) || !$section) {
            throw $this->createNotFoundException(sprintf('Section with id "%d" not found.', $sectionId));
        }

        return $this->render('@IrmagSite/Catalog/element/index.html.twig', [
            'element' => $element,
            'section_id' => $sectionId,
            'section_path' => $sectionRepo->getPath($section),
            'blog_posts_count' => $em->getRepository(BlogPost::class)->getBlogPostsCountForElement($element),
            'recommend_elements' => $elementRepo->getRecommendElements($element, $section, 6),
            'series_elements' => $elementRepo->getSeriesElements($element, 6),
            'childrens' => $elementRepo->getGroupByElements($element),
        ]);
    }

    /**
     * Комплекты.
     *
     * @param EntityManagerInterface $em
     * @param Element                $element
     * @param int                    $type
     *
     * @return Response
     */
    public function kit(EntityManagerInterface $em, Element $element, int $type = self::ELEMENT_KITS_TYPE_ELEMENT): Response
    {
        $kitRepo = $em->getRepository(ElementKit::class);

        if (self::ELEMENT_KITS_TYPE_CHILDRENS === $type) {
            $kit = $kitRepo->getByGroupBy($element);

            if (empty($kit)) {
                $kit = $kitRepo->getByElement($element);
            }
        } else {
            $kit = $kitRepo->getByElement($element);
        }

        if (empty($kit)) {
            return new Response();
        }

        return $this->render('@IrmagSite/Catalog/element/kit.html.twig', [
            'kit' => $kit,
        ]);
    }

    /**
     * @Route("/get_element_id_by_barcode/",
     *     name="irmag_get_element_id_by_barcode",
     *     methods={"POST"},
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true}
     * )
     *
     * @param EntityManagerInterface $em
     * @param Request                $request
     *
     * @return JsonResponse
     */
    public function getElementIdByBarcodeAction(EntityManagerInterface $em, Request $request): JsonResponse
    {
        $barcode = $request->request->get('barcode');
        $element = $em->getRepository(Element::class)->findOneBy(['barcode' => $barcode]);

        if ($element) {
            $id = $element->getId();
        }

        return new JsonResponse(['id' => $id ?? null]);
    }
}
