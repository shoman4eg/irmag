<?php

namespace Irmag\CoreBundle\Service\CacheManager;

/**
 * XXX: Менеджер кэша не будет очищать кэш через CLI, т.к разные области памяти APCu.
 */
class CacheManager
{
    /**
     * Баннеры на главной.
     */
    //const ID_INDEX_BANNERS = 'index_banners';

    /**
     * Карусель товаров на главной.
     */
    //const ID_INDEX_ELEMENTS_CAROUSEL = 'index_elements_carousel';

    /**
     * Новые обзоры в блоге.
     */
    //const ID_INDEX_LAST_BLOG_POSTS = 'index_last_blog_posts';

    /**
     * Меню каталога.
     */
    const ID_CATALOG_MENU = 'catalog_menu';

    /*
     * Счётчик кол-во товаров в подразделах.
     */
    //const ID_CATALOG_SECTIONS_COUNTERS = 'catalog_sections_counters';

    /*
     * Комплект/образ.
     */
    //const ID_ELEMENT_KIT = 'catalog_element_kit';
}
