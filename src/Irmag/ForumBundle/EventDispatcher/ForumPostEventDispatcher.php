<?php

namespace Irmag\ForumBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\ForumBundle\IrmagForumEvents;
use Irmag\ForumBundle\Event\ForumPostEvent;
use Irmag\ForumBundle\Entity\ForumPost;

class ForumPostEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param ForumPost $post
     */
    public function dispatchNewForumPostCreated(ForumPost $post): void
    {
        $this->eventDispatcher->dispatch(IrmagForumEvents::FORUM_NEW_POST_CREATED, $this->getForumPostEvent($post));
    }

    /**
     * @param ForumPost $post
     */
    public function dispatchForumPostEdited(ForumPost $post): void
    {
        $this->eventDispatcher->dispatch(IrmagForumEvents::FORUM_POST_EDITED, $this->getForumPostEvent($post));
    }

    /**
     * @param ForumPost $post
     *
     * @return ForumPostEvent
     */
    private function getForumPostEvent(ForumPost $post): ForumPostEvent
    {
        return new ForumPostEvent($post);
    }
}
