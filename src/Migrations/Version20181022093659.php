<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022093659 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_queues ADD is_push_done BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ADD is_email_done BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ADD is_site_done BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ADD is_sms_done BOOLEAN DEFAULT \'false\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_queues DROP is_push_done');
        $this->addSql('ALTER TABLE users_notification_queues DROP is_email_done');
        $this->addSql('ALTER TABLE users_notification_queues DROP is_site_done');
        $this->addSql('ALTER TABLE users_notification_queues DROP is_sms_done');
    }
}
