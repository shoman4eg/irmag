<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180606061504 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            UPDATE orders 
            SET payment_method_id = (SELECT id FROM orders_payment_methods WHERE shortname = 'apple_pay') 
            WHERE payment_method_id = (SELECT id FROM orders_payment_methods WHERE shortname = 'apple_pay_test')");

        $this->addSql("
            DELETE FROM orders_delivery_cities_orders_payment_methods 
            WHERE order_payment_method_id = 
              (SELECT id FROM orders_payment_methods WHERE shortname = 'apple_pay_test')"
        );

        $this->addSql("DELETE FROM orders_payment_methods WHERE shortname = 'apple_pay_test'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql("INSERT INTO orders_payment_methods (id, name, shortname, description, is_active) VALUES (NEXTVAL('orders_payment_methods_id_seq'), 'ApplePay Test', 'apple_pay_test', 'Тест для ApplePay', true)");
    }
}
