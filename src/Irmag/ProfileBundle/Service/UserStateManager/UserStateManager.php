<?php

namespace Irmag\ProfileBundle\Service\UserStateManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Irmag\ProfileBundle\Entity\UserState;
use Irmag\ProfileBundle\Service\UserTrait;

class UserStateManager implements UserStateManagerInterface
{
    use UserTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $user = $this->getUser(true);
        $userState = $this->em->getRepository(UserState::class)
            ->findOneBy(['user' => $user, 'key' => $key]);

        if (!$userState) {
            $userState = new UserState();
            $userState->setUser($user);
            $userState->setKey($key);
        }

        $userState->setValue($value);

        $errors = $this->validator->validate($userState);

        if (\count($errors) > 0) {
            throw new UserStateManagerException((string) $errors);
        }

        $this->em->persist($userState);
        $this->em->flush();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $defaultValue = null)
    {
        $result = $this->em->createQueryBuilder()
            ->select('us.value')
            ->from(UserState::class, 'us')
            ->where('us.user = :user')
            ->andWhere('us.key = :key')
            ->setParameters([
                'user' => $this->getUser(true),
                'key' => $key,
            ])
            ->getQuery()
            ->getOneOrNullResult();

        return $result['value'] ?? $defaultValue;
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        return $this->em->createQueryBuilder()
            ->delete(UserState::class, 'us')
            ->where('us.user = :user')
            ->andWhere('us.key = :key')
            ->setParameters([
                'user' => $this->getUser(true),
                'key' => $key,
            ])
            ->getQuery()
            ->execute();
    }
}
