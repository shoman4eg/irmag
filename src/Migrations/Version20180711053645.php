<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180711053645 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE blog_posts_votes ALTER score TYPE SMALLINT');
        $this->addSql('ALTER TABLE blog_posts_votes ALTER score DROP DEFAULT');
        $this->addSql('ALTER TABLE forum_post_votes ALTER score TYPE SMALLINT');
        $this->addSql('ALTER TABLE forum_post_votes ALTER score DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE blog_posts_votes ALTER score TYPE INT');
        $this->addSql('ALTER TABLE blog_posts_votes ALTER score DROP DEFAULT');
        $this->addSql('ALTER TABLE forum_post_votes ALTER score TYPE INT');
        $this->addSql('ALTER TABLE forum_post_votes ALTER score DROP DEFAULT');
    }
}
