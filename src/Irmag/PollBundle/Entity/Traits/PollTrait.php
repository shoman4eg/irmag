<?php

namespace Irmag\PollBundle\Entity\Traits;

use Irmag\PollBundle\Entity\Poll;

trait PollTrait
{
    /**
     * @var Poll
     *
     * @ORM\OneToOne(targetEntity="Irmag\PollBundle\Entity\Poll")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $poll;

    /**
     * Set poll.
     *
     * @param Poll|null $poll
     *
     * @return self
     */
    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        if (!empty($poll)) {
            $poll->setOwnerFQCN(self::class);
            $this->setPollOwnerRoute();
        }

        return $this;
    }

    /**
     * Get poll.
     *
     * @return Poll|null
     */
    public function getPoll(): ?Poll
    {
        return $this->poll;
    }
}
