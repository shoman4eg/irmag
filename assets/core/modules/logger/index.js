import Config from '../irmag-config';

/**
 * Логгирует (только для dev env).
 */
export default function log(...args) {
    if (true === Config.debug
        && 'undefined' !== typeof console
        && 'function' === typeof console.log
    ) {
        args.unshift('[IRMAG]');
        console.log(...args);
    }
}
