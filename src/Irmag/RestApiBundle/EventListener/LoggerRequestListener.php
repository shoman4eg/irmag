<?php

namespace Irmag\RestApiBundle\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Логирует запросы, если существует HEADER параметр.
 *
 * @see LoggerRequestListener::LOGGER_PARAM.
 *
 * @example curl -H "x-api-token: %IRMAG_API_TOKEN%" -H "x-api-logger-enabled: 1" https://api.irmag.local/users.json
 */
class LoggerRequestListener
{
    private const LOGGER_PARAM = 'x-api-logger-enabled';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $apiDomain;

    /**
     * @var string
     */
    private $selfserviceApiDomain;

    /**
     * @var string
     */
    private $driveApiDomain;

    /**
     * @param LoggerInterface $logger
     * @param string          $apiDomain
     * @param string          $selfserviceApiDomain
     * @param string          $driveApiDomain
     */
    public function __construct(
        LoggerInterface $logger,
        string $apiDomain,
        string $selfserviceApiDomain,
        string $driveApiDomain
    ) {
        $this->logger = $logger;
        $this->apiDomain = $apiDomain;
        $this->selfserviceApiDomain = $selfserviceApiDomain;
        $this->driveApiDomain = $driveApiDomain;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $host = $request->getHost();

        // only for API domains
        if (!\in_array($host, [$this->apiDomain, $this->selfserviceApiDomain, $this->driveApiDomain], true)) {
            return;
        }

        // logger
        if ($request->headers->has(self::LOGGER_PARAM)) {
            $this->logger->alert('API DUMP '.uniqid(), [
                'query' => $request->query->all(),
                'request' => $request->request->all(),
            ]);
        }
    }
}
