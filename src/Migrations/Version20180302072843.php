<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180302072843 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_delivery_cities_ban_days_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_delivery_cities_ban_days (id INT NOT NULL, delivery_city_id INT NOT NULL, ban_date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FB5FD6CC81B2DD71 ON orders_delivery_cities_ban_days (delivery_city_id)');
        $this->addSql('ALTER TABLE orders_delivery_cities_ban_days ADD CONSTRAINT FK_FB5FD6CC81B2DD71 FOREIGN KEY (delivery_city_id) REFERENCES orders_delivery_cities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE orders_delivery_cities_ban_days_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_delivery_cities_ban_days');
    }
}
