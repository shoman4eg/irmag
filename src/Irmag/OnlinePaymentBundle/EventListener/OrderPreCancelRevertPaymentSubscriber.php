<?php

namespace Irmag\OnlinePaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class OrderPreCancelRevertPaymentSubscriber implements EventSubscriberInterface
{
    /**
     * @var OnlinePaymentManager
     */
    private $onlinePaymentManager;

    /**
     * @param OnlinePaymentManager $onlinePaymentManager
     */
    public function __construct(
        OnlinePaymentManager $onlinePaymentManager
    ) {
        $this->onlinePaymentManager = $onlinePaymentManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_PRE_CANCEL => 'onOrderPreCancel',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderPreCancel(OrderEvent $event)
    {
        $order = $event->getOrder();

        $this->onlinePaymentManager->cancelHoldTransaction($order);
        $this->onlinePaymentManager->clearOnlinePaymentSession();
    }
}
