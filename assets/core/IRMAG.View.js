/**
 * Логирование просмотра страницы.
 *
 * @namespace IRMAG.View
 *
 * @requires jquery
 * @requires Routing
 */

import Routing from './modules/routing';
import log from './modules/logger';
import { getFingerprint } from './modules/irmag-utils';

IRMAG.View = {
    logVisit: function (route, objectId) {
        $.ajax({
            url: Routing.generate(route),
            dataType: 'json',
            data: {
                fingerprint: getFingerprint(),
                objectId: objectId,
            },
            success: function (data) {
                 if (!data.success) {
                     log('[logVisit]: unable to write view');
                 }
            },
            fail: function () {
                log('[logVisit]: could not log visit');
            },
        });
    },
};
