<?php

namespace Irmag\Exchange1CBundle\Parser;

use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\PromocodeElementsList;
use Symfony\Component\DomCrawler\Crawler;

class PromocodeElementsListParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'uuid1C',
        'name',
    ];

    /**
     * {@inheritdoc}
     */
    protected $fieldsRelationshipToFill = [
        'action' => Action::class,
    ];

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root list')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->getCrawler()->filter('root list')->each(function (Crawler $node) {
            $elementList = new PromocodeElementsList();
            $this->fillFields($elementList, $node);
            $this->fillFieldsRelationship($elementList, $node);

            $elementRepo = $this->em->getRepository(Element::class);

            $node->filter('elements id')->each(function (Crawler $node) use (&$elementList, $elementRepo) {
                $elementId = (int) $node->text();
                $element = $elementRepo->find($elementId);

                if (!$element) {
                    $this->output->writeln(sprintf('<error>Element with id "%d" not found, skipped.</error>', $elementId));
                } else {
                    $elementList->addElement($element);
                }
            });

            $this->em->persist($elementList);
            ++$this->operations;
            $this->flushIfNeed(true);

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }
        });
    }
}
