<?php

namespace Irmag\AdminBundle\Controller;

use Irmag\SiteBundle\Entity\Section;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Twig\Extension\FormExtension;

class SectionAdminController extends Controller
{
    public function listAction(Request $request = null)
    {
        $datagrid = $this->admin->getDatagrid();

        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension(FormExtension::class)->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->renderWithExtraParams('@IrmagAdmin/Catalog/section_list.html.twig', [
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ]);
    }

    public function treeAction(Request $request)
    {
        $sectionRepo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Section::class);

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        return $this->renderWithExtraParams('@IrmagAdmin/Catalog/section_tree.html.twig', [
            'action' => 'tree',
            'form' => $formView,
            'sections' => $sectionRepo->childrenHierarchy(),
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ]);
    }
}
