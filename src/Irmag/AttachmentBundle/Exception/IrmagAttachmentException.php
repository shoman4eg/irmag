<?php

namespace Irmag\AttachmentBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagAttachmentException extends IrmagException
{
}
