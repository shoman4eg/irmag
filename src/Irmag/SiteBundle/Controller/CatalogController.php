<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\CoreBundle\Service\CacheManager\CacheManager;
use Irmag\SiteBundle\Service\CatalogElementViewSwitcher;
use Irmag\SiteBundle\Service\SearchQueryNormalizer;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;
use Irmag\SiteBundle\Form\Type\CatalogFilterType;
use Irmag\SiteBundle\Form\Type\CatalogPerPageType;
use Irmag\SiteBundle\Form\Helper\CatalogFilterPropIsNotAllowedException;
use Irmag\SiteBundle\Repository\SectionRepository;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\SiteBundle\Entity\ElementBrand;

/**
 * @Route("/cat")
 */
class CatalogController extends Controller
{
    const FILTER_PARAMETER_NAME = 'filter';
    const SEARCH_PARAMETER_NAME = 'q';
    const SORT_PARAM = 'sort';
    const SORT_FIELD_WHITE_LIST = ['createdAt', 'avgRating', 'name', 'price', 'weight', 'volume', 'rank'];

    private $seoKeywords = '';
    private $seoDescription = '';
    private $seoTitle = '';
    private $seoH1 = '';

    /**
     * @var bool
     */
    private $isFilterUsed = false;

    /**
     * @var bool
     */
    private $isSearchUsed = false;

    /**
     * @var array
     */
    private $filterData = [];

    /**
     * @var string
     */
    private $searchQuery = '';

    /**
     * @Route("/", name="irmag_catalog", options={"expose": true, "sitemap": true})
     * @Route("/{sectionId}/", name="irmag_catalog_section", requirements={"sectionId": "\d+"})
     *
     * @param Request  $request
     * @param int|null $sectionId Ид раздела
     *
     * @return Response
     */
    public function indexAction(Request $request, int $sectionId = null)
    {
        // sort check, because contentAction is subrequest and exception cannot propaganda parent request
        if (
            $request->query->has(self::SORT_PARAM)
            &&
            !\in_array($request->query->get(self::SORT_PARAM), self::SORT_FIELD_WHITE_LIST, true)) {
            throw $this->createNotFoundException();
        }

        $this->initFilterData($request);
        $this->initSearchQuery($request);

        $em = $this->get('doctrine.orm.default_entity_manager');
        /** @var SectionRepository $sectionRepo */
        $sectionRepo = $em->getRepository(Section::class);
        /** @var Section $section */
        $section = (null === $sectionId) ? null : $sectionRepo->findOneBy(['id' => $sectionId, 'isActive' => true]);

        if ($request->isXmlHttpRequest()) {
            return $this->contentAction($request, $section);
        }

        if (null === $sectionId) {
            $sections = $sectionRepo->findBy(['lvl' => 0], ['name' => 'asc']);
        } else {
            if (!$section) {
                throw $this->createNotFoundException(sprintf('Section with id "%d" not found.', $sectionId));
            }

            $sections = $section->getChildren();
            $sectionPath = $sectionRepo->getPath($section);
        }

        // устанавливаем кол-во эл-ов в разделе
        foreach ($sections as $key => $sectionChild) {
            $target = $em->getRepository(Element::class)
                ->getElementsQueryBuilderBySection($sectionChild);

            // data filter
            if ($this->isFilterUsed) {
                $this->handleFilterData($target);
            }

            // text search
            if ($this->isSearchUsed) {
                $this->handleSearchQuery($target);
            }

            $this->get(CatalogElementViewSwitcher::class)->setGrouped(CatalogElementViewSwitcher::GROUPED_VIEW_NAME);

            $elementsCountRecursive = $this->getTargetCount($target);

            if (0 === $elementsCountRecursive) {
                unset($sections[$key]);
            }

            $sectionChild->elementsCountRecursive = $elementsCountRecursive;
        }

        if ($this->isFilterUsed) {
            $seoObject = $this->getManufacturerOrBrand($this->filterData);
        } elseif ($section) {
            $seoObject = $section;
        }

        if (!empty($seoObject)) {
            $this->seoKeywords = $seoObject->getSeoKeywords();
            $this->seoDescription = $seoObject->getSeoDescription();
            $this->seoTitle = $seoObject->getSeoTitle();
            $this->seoH1 = $seoObject->getSeoH1();
        }

        return $this->render('@IrmagSite/Catalog/index.html.twig', [
            'current_section' => $section,
            'section_path' => $sectionPath ?? null,
            'sections' => $sections,
            // search/filter sets
            'is_filter_used' => $this->isFilterUsed,
            'is_search_used' => $this->isSearchUsed,
            'filter_data' => $this->filterData,
            'search_query' => $this->searchQuery,
            // seo
            'meta_keywords' => $this->seoKeywords,
            'meta_description' => $this->seoDescription,
            'seo_title' => $this->seoTitle, // manual inj
            'seo_h1' => $this->seoH1, // manual inj
        ]);
    }

    /**
     * Товары в каталоге.
     *
     * @param Request $request
     * @param Section $section
     *
     * @return JsonResponse|Response
     */
    public function contentAction(Request $request, Section $section = null)
    {
        $this->initFilterData($request);
        $this->initSearchQuery($request);

        $groupedName = CatalogElementViewSwitcher::GROUPED_VIEW_NAME;
        $defaultSort = 'createdAt';

        $sort = $request->query->get('sort');
        $direction = $request->query->get('direction', 'desc');

        /** @var QueryBuilder $target */
        $target = $this->get('doctrine.orm.default_entity_manager')->getRepository(Element::class)
            ->getElementsQueryBuilderBySection($section);

        // data filter
        if ($this->isFilterUsed) {
            $this->handleFilterData($target);
        }

        // text search
        if ($this->isSearchUsed) {
            $this->handleSearchQuery($target);
            $defaultSort = 'rank';
        }

        // for debug grouped (force mode)
        if ($request->query->has('grouped')) {
            $groupedName = $request->query->getBoolean('grouped') ? CatalogElementViewSwitcher::GROUPED_VIEW_NAME : CatalogElementViewSwitcher::NORMAL_TABLE_NAME;
        }

        // view
        $this->get(CatalogElementViewSwitcher::class)->setGrouped($groupedName);

        // ajax filter counter
        if ($request->request->has('filter-results-count')) {
            return new JsonResponse([
                'count' => $this->getTargetCount($target),
            ]);
        }

        $perPage = $request->query->getInt('perpage', CatalogPerPageType::PER_PAGE_DEFAULT);

        if (!\in_array($perPage, CatalogPerPageType::PER_PAGE_ALLOWED_LIST, true)) {
            $perPage = CatalogPerPageType::PER_PAGE_DEFAULT;
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $target,
            $request->query->getInt('page', 1),
            $perPage,
            [
                'sortFieldWhitelist' => self::SORT_FIELD_WHITE_LIST,
                'defaultSortFieldName' => $defaultSort,
                'defaultSortDirection' => 'desc',
            ]
        );

        // сохраняем фильтрацию при изменение CatalogPerPageType
        $action = $this->isFilterUsed
            ? '?'.http_build_query([self::FILTER_PARAMETER_NAME => $this->filterData])
            : '';

        $perPageForm = $this->createForm(CatalogPerPageType::class, null, [
            'method' => 'GET',
            'data' => ['perpage' => $perPage],
            'action' => $action,
        ]);

        // сохраняем поиск
        if ($this->isSearchUsed) {
            $perPageForm->add(self::SEARCH_PARAMETER_NAME, HiddenType::class, ['data' => $this->searchQuery]);
        }

        // сохраняем выбранное поле сортировки
        if ($request->query->has('sort')) {
            $perPageForm->add('sort', HiddenType::class, ['data' => $sort]);
        }

        // сохраняем выбранное направление сортировки
        if ($request->query->has('direction')) {
            $perPageForm->add('direction', HiddenType::class, ['data' => $direction]);
        }

        return $this->render('@IrmagSite/Catalog/index-content.html.twig', [
            'pagination' => $pagination,
            'perpage_form' => $perPageForm->createView(),
            'current_section' => $section,
            'is_grouped' => CatalogElementViewSwitcher::GROUPED_VIEW_NAME === $groupedName,
            'request' => $request,
            // search/filter sets
            'is_filter_used' => $this->isFilterUsed,
            'is_search_used' => $this->isSearchUsed,
            'filter_data' => $this->filterData,
            'search_query' => $this->searchQuery,
        ]);
    }

    /**
     * Форма фильтра.
     *
     * @Route("/filter", name="irmag_catalog_filter")
     *
     * @param Request      $request
     * @param Section|null $section
     *
     * @return Response
     */
    public function filterAction(Request $request, Section $section = null)
    {
        $this->initFilterData($request);
        $this->initSearchQuery($request);

        $filterForm = $this->createForm(CatalogFilterType::class, null, [
            'section' => $section,
            'is_filter_used' => $this->isFilterUsed,
            'is_search_used' => $this->isSearchUsed,
            'filter_data' => $this->filterData,
            'search_query' => $this->searchQuery,
        ])->handleRequest($request);

        return $this->render('@IrmagSite/Catalog/filter.html.twig', [
            'filter_form' => $filterForm->createView(),
            'request' => $request,
            // search/filter sets
            'is_filter_used' => $this->isFilterUsed,
            'is_search_used' => $this->isSearchUsed,
            'filter_data' => $this->filterData,
            'search_query' => $this->searchQuery,
        ]);
    }

    /**
     * XXX: DEV.
     *
     * @Route("/tree/", name="irmag_catalog_tree")
     */
    public function treeAction()
    {
        $repo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Section::class);
        $router = $this->get('router');

        $htmlTree = $repo->childrenHierarchy(
            null, /* starting from root nodes */
            false, /* true: load all children, false: only direct */
            [
                'decorate' => true,
                'representationField' => 'slug',
                'html' => true,
                'nodeDecorator' => function ($node) use ($router) {
                    return '<a href="'.$router->generate('irmag_catalog_section', ['sectionId' => $node['id']]).'">'.$node['name'].'</a>';
                },
            ]
        );

        return $this->render('@IrmagSite/Catalog/tree.html.twig', [
            'tree' => $htmlTree,
        ]);
    }

    /**
     * Меню каталога (3 уровня).
     */
    public function menuAction()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $cachedSections = $this->get('cache.app')->getItem(CacheManager::ID_CATALOG_MENU);

        if (!$cachedSections->isHit()) {
            $sections = $em->getRepository(Section::class)->childrenHierarchy();

            // сортируем 1й уровень по-алфавиту
            usort($sections, function ($a, $b) {
                return strcmp($a['name'], $b['name']);
            });

            // сортируем 2й уровень, в начале те, у которых больше детей.
            foreach ($sections as &$section) {
                usort($section['__children'], function ($a, $b) {
                    return \count($b['__children']) - \count($a['__children']);
                });
            }

            $cachedSections->set($sections);
            $this->get('cache.app')->save($cachedSections);
        } else {
            $sections = $cachedSections->get();
        }

        return $this->render('@IrmagSite/Catalog/menu.html.twig', [
            'sections' => $sections,
            'manufacturers_and_brands' => $em->getRepository(ElementManufacturer::class)->getRandomManufacturerAndBrandsWithPicture(),
        ]);
    }

    /**
     * @param array $filterData
     *
     * @return object|null
     */
    private function getManufacturerOrBrand(array $filterData): ?object
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        if (!empty($filterData['manufacturer'][0])) {
            /** @var ElementManufacturer $object */
            $object = $em->getRepository(ElementManufacturer::class)->find($filterData['manufacturer'][0]);
        } elseif (!empty($filterData['brand'][0])) {
            /** @var ElementBrand $object */
            $object = $em->getRepository(ElementBrand::class)->find($filterData['brand'][0]);
        }

        return $object ?? null;
    }

    /**
     * @param array $filterData
     *
     * @return Response
     */
    public function brandZoneAction(array $filterData)
    {
        $object = $this->getManufacturerOrBrand($filterData);

        if (null === $object) {
            return new Response();
        }

        $brandZoneDirName = mb_strtolower(str_replace(' ', '_', $object->getName()));
        $path = $this->getParameter('irmag_brand_zones_dir').'/'.$brandZoneDirName;

        if (false === file_exists($path.'/.enabled') || false === file_exists($path.'/template.html.twig')) {
            return new Response();
        }

        return $this->render(sprintf('@brand_zones/%s/template.html.twig', $brandZoneDirName), [
            'object' => $object,
            'web_path' => $this->getParameter('irmag_brand_zones_web_path').'/'.$brandZoneDirName,
        ]);
    }

    /**
     * @param QueryBuilder $target
     *
     * @return int
     */
    private function getTargetCount($target): int
    {
        return $target->select('COUNT(DISTINCT e.id)')->getQuery()->useResultCache(true)->getSingleScalarResult();
    }

    /**
     * @param Request $request
     */
    private function initFilterData(Request $request)/*: \void*/
    {
        if (true === $this->isFilterUsed) {
            return;
        }

        if ($request->query->has(self::FILTER_PARAMETER_NAME)) {
            $filterData = $request->query->get(self::FILTER_PARAMETER_NAME);

            if (\is_array($filterData) && !empty($filterData)) {
                $this->isFilterUsed = true;
                $this->filterData = $filterData;
            } else {
                throw $this->createNotFoundException('Filter is empty');
            }
        }
    }

    /**
     * @param Request $request
     */
    private function initSearchQuery(Request $request)/*: \void*/
    {
        if (true === $this->isSearchUsed) {
            return;
        }

        if (
            $request->query->has(self::SEARCH_PARAMETER_NAME)
            && !empty($searchQuery = trim($request->query->get(self::SEARCH_PARAMETER_NAME, '')))
        ) {
            $this->isSearchUsed = true;
            $this->searchQuery = $searchQuery;
        }
    }

    /**
     * @param QueryBuilder $target
     */
    private function handleSearchQuery(QueryBuilder $target)/*: \void*/
    {
        $searchQuery = SearchQueryNormalizer::normilize($this->searchQuery);

        $target
            ->addSelect('PLAINTSRANK(e.tsv, :searchQuery, \'irmag\') as HIDDEN rank')
            ->andWhere('PLAINTSQUERY(e.tsv, :searchQuery, \'irmag\') = true')
            ->setParameter('searchQuery', $searchQuery);
    }

    /**
     * @param QueryBuilder $target
     *
     * @throws CatalogFilterPropIsNotAllowedException
     */
    private function handleFilterData(QueryBuilder $target): void
    {
        // manufacturer
        $manufacturerIds = [];
        foreach ($this->filterData['manufacturer'] ?? [] as $manufacturerId) {
            checkIntAndFuckOffIfBadGuys($manufacturerId);
            $manufacturerIds[] = $manufacturerId;
        }
        if (!empty($manufacturerIds)) {
            $target->andWhere('e.manufacturer IN (:manufacturers)')->setParameter('manufacturers', $manufacturerIds);
        }

        // brand
        $brandIds = [];
        foreach ($this->filterData['brand'] ?? [] as $brandId) {
            checkIntAndFuckOffIfBadGuys($brandId);
            $brandIds[] = $brandId;
        }
        if (!empty($brandIds)) {
            $target->andWhere('e.brand IN (:brands)')->setParameter('brands', $brandIds);
        }

        // city
        $countryIds = [];
        foreach ($this->filterData['country'] ?? [] as $countryId) {
            checkIntAndFuckOffIfBadGuys($countryId);
            $countryIds[] = $countryId;
        }
        if (!empty($countryIds)) {
            $target->andWhere('e.country IN (:countries)')->setParameter('countries', $countryIds);
        }

        // prop
        foreach ($this->filterData['prop'] ?? [] as $propName) {
            if (false === \in_array($propName, Element::getAvailablePropNames(), true)) {
                throw new CatalogFilterPropIsNotAllowedException($propName);
            }

            $dqlPropName = 'prop'.$propName;

            switch ($propName) {
                case 'Action':
                    $target
                        ->andWhere('e.id IN (:actionElementIds)')
                        ->setParameter('actionElementIds', $this->get(ActionAdapter::class)->getElementIds());
                    break;

                case 'Transfer': // int
                    $target->andWhere(sprintf('e.%s >= 0', $dqlPropName));
                    break;

                default: // bool
                    $target->andWhere(sprintf('e.%s = true', $dqlPropName));
                    break;
            }
        }
        // price start
        if (!empty($this->filterData['price_start']) && is_numeric($this->filterData['price_start'])) {
            $target->andWhere('e.price >= :price_start')->setParameter('price_start', $this->filterData['price_start']);
        }
        // price end
        if (!empty($this->filterData['price_end']) && is_numeric($this->filterData['price_end'])) {
            $target->andWhere('e.price <= :price_end')->setParameter('price_end', $this->filterData['price_end']);
        }
    }
}
