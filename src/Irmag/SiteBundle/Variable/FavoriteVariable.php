<?php

namespace Irmag\SiteBundle\Variable;

use Irmag\SiteBundle\Service\BasketFavoriteManager\FavoriteManager;
use Irmag\SiteBundle\Entity\Element;

class FavoriteVariable
{
    /**
     * @var FavoriteManager
     */
    private $favoriteManager;

    /**
     * @param FavoriteManager $favoriteManager
     */
    public function __construct(
        FavoriteManager $favoriteManager
    ) {
        $this->favoriteManager = $favoriteManager;
    }

    /**
     * @return \Irmag\FavoriteBundle\Entity\Favorite[]
     */
    public function getFavorites(): array
    {
        return $this->favoriteManager->getFavorites();
    }

    /**
     * @return \Irmag\SiteBundle\Entity\Element[]
     */
    public function getElements(): array
    {
        return $this->favoriteManager->getElements();
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->favoriteManager->getTotalCount();
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isWatching(Element $element): bool
    {
        return $this->favoriteManager->isWatching($element);
    }
}
