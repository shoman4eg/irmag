<?php

namespace Irmag\ForumBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\ForumBundle\IrmagForumEvents;
use Irmag\ForumBundle\Event\ForumTopicEvent;
use Irmag\ForumBundle\Entity\ForumTopic;

class ForumTopicEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param ForumTopic $topic
     */
    public function dispatchNewForumTopicCreated(ForumTopic $topic): void
    {
        $this->eventDispatcher->dispatch(IrmagForumEvents::FORUM_TOPIC_CREATED, $this->getForumTopicEvent($topic));
    }

    /**
     * @param ForumTopic $topic
     *
     * @return ForumTopicEvent
     */
    private function getForumTopicEvent(ForumTopic $topic): ForumTopicEvent
    {
        return new ForumTopicEvent($topic);
    }
}
