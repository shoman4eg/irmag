<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Irmag\SiteBundle\Utils\UUID;

class PromocodeElementsListAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Идентификаторы')
                ->with('ID')
                    ->add('id')
                    ->add('uuid1C', null, ['label' => 'Идентификатор в 1C'])
                    ->add('name')
                    ->add('action', ModelAutocompleteType::class, [
                        'label' => 'Акция',
                        'property' => ['id'],
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                ->end()
            ->end()
            ->tab('Товары')
                ->with('')
                    ->add('elements', CollectionType::class, [
                        'label' => false,
                        'type_options' => [
                            'delete' => false,
                        ],
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
            ->tab('Промокоды')
                ->with('')
                    ->add('promocodes', CollectionType::class, [
                        'label' => false,
                        'btn_add' => false,
                        'type_options' => [
                            'delete' => false,
                        ],
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('uuid1C', CallbackFilter::class, [
                'callback' => function ($qb, $alias, $field, $value) {
                    $uuid = $value['value'];

                    if (UUID::isValid($uuid)) {
                        $qb->where($alias.'.'.$field.' = :uuid')
                            ->setParameter('uuid', $uuid);

                        return true;
                    }

                    $this->getConfigurationPool()->getContainer()->get('session')->getFlashBag()->add('warning', 'UUID is not valid.');

                    return false;
                },
            ])
            ->add('name')
            ->add('action', CallbackFilter::class, [
                'label' => 'Название акции',
                'callback' => function ($qb, $alias, $field, $value) {
                    if (!$value['value']) {
                        return false;
                    }

                    $qb
                        ->join(sprintf('%s.%s', $alias, $field), 'a')
                        ->where('LOWER(a.title) LIKE '.sprintf("'%%%s%%'", mb_strtolower($value['value'])));

                    return true;
                },
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('uuid1C')
            ->add('name')
            ->add('action', null, ['label' => 'Акция'])
        ;
    }
}
