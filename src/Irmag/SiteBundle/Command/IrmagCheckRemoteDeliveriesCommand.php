<?php

namespace Irmag\SiteBundle\Command;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryTrackingService;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceMapper;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\Order;

class IrmagCheckRemoteDeliveriesCommand extends Command
{
    protected static $defaultName = 'irmag:orders:check_deliveries';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Check orders remote delivery statuses');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($output->isVerbose()) {
            $now = new \DateTime();

            $output->writeln(sprintf('Started at %s', $now->format(Config::DATETIME_FORMAT)));
            $output->writeln('');
        }

        $em = $this->container->get('doctrine.orm.default_entity_manager');

        // сначала заказы по транспортным компаниям
        $companyOrdersOnDelivery = $em
            ->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->addSelect('od', 'ods')
            ->join('o.deliveryServiceData', 'od')
            ->join('od.orderDeliveryService', 'ods')
            ->where('o.isRemoteDeliveryCompleted = false')
            ->andWhere('od.trackCode IS NOT NULL')
            ->getQuery()
            ->getResult();

        // заказы с доставкой Почтой России
        $postOrdersOnDelivery = $em
            ->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->addSelect('op')
            ->join('o.postDeliveryData', 'op')
            ->where('o.isRemoteDeliveryCompleted = false')
            ->andWhere('op.trackCode IS NOT NULL')
            ->setMaxResults(OrderPostDeliveryTrackingService::OPERATIONS_PER_DAY_LIMIT)
            ->getQuery()
            ->getResult();

        if ($output->isVerbose()) {
            $output->writeln(sprintf('Total orders to check: <info>%d</info>', \count($companyOrdersOnDelivery) + \count($postOrdersOnDelivery)));

            if (0 === \count($companyOrdersOnDelivery) && 0 === \count($postOrdersOnDelivery)) {
                $output->writeln('Nothing to do!');
            } else {
                $output->writeln(sprintf('Orders with transport company delivery: <info>%d</info>', \count($companyOrdersOnDelivery)));
                $output->writeln(sprintf('Orders with post delivery: <info>%d</info>', \count($postOrdersOnDelivery)));
                $output->writeln('');
                $output->writeln('Starting to check ...');
            }
        }

        if ($companyOrdersOnDelivery) {
            if ($output->isVerbose()) {
                $output->writeln('<info>Checking orders with transport company delivery</info>');
                $output->writeln('');
                $progress = new ProgressBar($output, \count($companyOrdersOnDelivery));
                $progress->start();
            }

            $services = [];

            foreach (OrderDeliveryServiceMapper::DELIVERY_SERVICES_MAP as $key => $item) {
                $services[$key] = $this->container->get($item);
            }

            // тут ставим те компании, где требуется заранее запрашивать данные
            $eventsMap['dpd'] = $services['dpd']->getEvents();

            /** @var Order $order */
            foreach ($companyOrdersOnDelivery as $order) {
                $data = $order->getDeliveryServiceData();
                $companyShortname = $data->getOrderDeliveryService()->getShortname();

                if ($output->isVerbose()) {
                    $progress->advance();
                }

                try {
                    $service = $services[$companyShortname];
                    $events = !empty($eventsMap[$companyShortname]) ? $eventsMap[$companyShortname] : [];

                    if ($service->isParcelDelivered($data->getTrackCode(), $events)) {
                        $order->setIsRemoteDeliveryCompleted(true);
                        $em->persist($order);
                    }
                } catch (\Exception $ex) {
                    $output->writeln('');
                    $output->writeln(sprintf(
                        '<error>Caught exception for order № %d! Message: "%s"</error>',
                        $order->getId(),
                        $ex->getMessage()
                    ));
                    $output->writeln('');
                    continue;
                }
            }

            if ($output->isVerbose()) {
                $progress->finish();
                $output->writeln('');
            }

            $em->flush();
        }

        if ($postOrdersOnDelivery) {
            $service = $this->container->get(OrderPostDeliveryTrackingService::class);

            if ($output->isVerbose()) {
                $output->writeln('<info>Checking orders with post delivery</info>');
                $output->writeln('');
                $progress = new ProgressBar($output, \count($postOrdersOnDelivery));
                $progress->start();
            }

            /** @var Order $order */
            foreach ($postOrdersOnDelivery as $order) {
                $data = $order->getPostDeliveryData();

                if ($output->isVerbose()) {
                    $progress->advance();
                }

                try {
                    if ($service->isParcelDelivered($data->getTrackCode())) {
                        $order->setIsRemoteDeliveryCompleted(true);
                        $em->persist($order);
                    }
                } catch (\Exception $ex) {
                    $output->writeln(sprintf(
                        '<error>Caught exception! Message: "%s"</error>',
                        $ex->getMessage()
                    ));
                    continue;
                }
            }

            if ($output->isVerbose()) {
                $progress->finish();
                $output->writeln('');
            }

            $em->flush();
        }

        if ($output->isVerbose()) {
            $now = new \DateTime();
            $output->writeln('');
            $output->writeln(sprintf('Finished at %s', $now->format(Config::DATETIME_FORMAT)));
        }
    }
}
