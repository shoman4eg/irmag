<?php

namespace Irmag\OrderPostDeliveryBundle\Service;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use Irmag\OrderPostDeliveryBundle\Exception\IrmagOrderPostDeliveryBundleException;

class OrderPostDeliveryTrackingService
{
    /**
     * URL хоста для запросов к API (WSDL).
     */
    const API_HOST = 'https://tracking.russianpost.ru/rtm34?wsdl';

    /**
     * Лимит вызовов сервиса в сутки.
     */
    const OPERATIONS_PER_DAY_LIMIT = 100;

    /**
     * Тип сообщения по умолчанию.
     */
    const DEFAULT_MESSAGE_TYPE = 0;

    /**
     * Код операции вручения.
     *
     * @see https://tracking.pochta.ru/support/dictionaries/operation_codes
     */
    const PARCEL_DELIVERED_CODE = 2;

    /**
     * Коды, означающие получение адресатом отправления.
     *
     * @see https://tracking.pochta.ru/support/dictionaries/operation_codes
     */
    const SUCCESSFULLY_DELIVERED_CODES = [1, 3, 5, 6, 8, 10, 11, 12];

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $key;

    /**
     * @var \SoapClient
     */
    private $client;

    /**
     * @param string $login
     * @param string $key
     */
    public function __construct(string $login, string $key)
    {
        $this->login = $login;
        $this->key = $key;

        $this->client = new \SoapClient(self::API_HOST, [
            'stream_context' => stream_context_create([
                'http' => [
                    'user_agent' => 'IrmagPHPSoapClient',
                ],
            ]),
            'soap_version' => SOAP_1_2,
            'cache_wsdl' => WSDL_CACHE_NONE,
        ]);
    }

    /**
     * Доставлена ли посылка адресату.
     *
     * @param string $trackCode
     *
     * @return bool
     */
    public function isParcelDelivered(string $trackCode): bool
    {
        $history = $this->getOperationHistory($trackCode);
        $isDelivered = false;

        foreach ($history as $item) {
            if (self::PARCEL_DELIVERED_CODE !== $item['OperationParameters']['OperType']['Id']) {
                continue;
            }

            if (true === \in_array($item['OperationParameters']['OperAttr']['Id'], self::SUCCESSFULLY_DELIVERED_CODES, true)) {
                $isDelivered = true;
                break;
            }
        }

        return $isDelivered;
    }

    /**
     * Получить данные об отправлении.
     *
     * @param string $trackCode
     *
     * @throws IrmagOrderPostDeliveryBundleException
     *
     * @return array
     */
    protected function getOperationHistory(string $trackCode): array
    {
        $request = [
            'OperationHistoryRequest' => [
                'Barcode' => $trackCode,
                'MessageType' => self::DEFAULT_MESSAGE_TYPE,
            ],
            'AuthorizationHeader' => [
                'login' => $this->login,
                'password' => $this->key,
            ],
        ];

        try {
            $result = $this->client->getOperationHistory(new \SoapParam($request, 'OperationHistoryRequest'));
        } catch (\Exception $ex) {
            throw new IrmagOrderPostDeliveryBundleException(
                sprintf('Error while calling RussianPost tracking API. Message: %s', $ex->getMessage()),
                $ex->getCode()
            );
        }

        $result = SerializerBuilder::create()
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
            ->build()
            ->toArray($result);

        return (!empty($result['OperationHistoryData']['historyRecord'])) ? $result['OperationHistoryData']['historyRecord'] : [];
    }
}
