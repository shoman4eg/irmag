<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180528063354 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE IF EXISTS users_referral_trees');
        $this->addSql('DROP SEQUENCE IF EXISTS users_referral_trees_id_seq');
        $this->addSql('CREATE SEQUENCE users_referral_trees_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_referral_trees (id INT NOT NULL, root_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, user_id INT NOT NULL, lft INT NOT NULL, lvl INT NOT NULL, rgt INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_82AA179279066886 ON users_referral_trees (root_id)');
        $this->addSql('CREATE INDEX IDX_82AA1792727ACA70 ON users_referral_trees (parent_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_82AA1792A76ED395 ON users_referral_trees (user_id)');
        $this->addSql('CREATE INDEX IDX_82AA1792DA43925283ED1855D5E02D69 ON users_referral_trees (lft, lvl, rgt)');
        $this->addSql('ALTER TABLE users_referral_trees ADD CONSTRAINT FK_82AA179279066886 FOREIGN KEY (root_id) REFERENCES users_referral_trees (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_trees ADD CONSTRAINT FK_82AA1792727ACA70 FOREIGN KEY (parent_id) REFERENCES users_referral_trees (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_trees ADD CONSTRAINT FK_82AA1792A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_referral_trees DROP CONSTRAINT FK_82AA179279066886');
        $this->addSql('ALTER TABLE users_referral_trees DROP CONSTRAINT FK_82AA1792727ACA70');
        $this->addSql('DROP SEQUENCE users_referral_trees_id_seq CASCADE');
        $this->addSql('DROP TABLE users_referral_trees');
    }
}
