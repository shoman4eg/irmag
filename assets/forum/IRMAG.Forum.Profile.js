/**
 * Профиль на форуме.
 *
 * @namespace IRMAG.Forum.Profile
 *
 * @requires jquery
 * @requires Routing
 * @requires bootbox
 */

import Routing from '../core/modules/routing';

IRMAG.Forum.Profile = {

    init: function () {
        this.bindClickToUnsubscribe();
    },

    /* Отписаться от рассылки */
    bindClickToUnsubscribe: function () {
        $(document).on('click', '.forum-unsubscribe', function () {
            const _ = $(this).parent('li:first');
            const id = _.data('id');

            $.ajax({
                url: Routing.generate('irmag_forum_unsubscribe'),
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function () {
                    if ($('#forum-subscription-options li').length === 1) {
                        _.parent('ul').fadeOut(700, function () {
                            _.parents('.panel-body:first').append('<p>Вы не подписаны ни на одну тему </p>');
                        });
                    } else {
                        _.fadeOut(700, function () {
                            _.remove();
                        });
                    }
                },
                fail: function () {
                    bootbox.alert('Что-то пошло не так. Попробуйте снова или обратитесь к администратору.');
                },
            });
        });
    },
};
