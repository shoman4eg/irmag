<?php

namespace Irmag\RestApiBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class ApiMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendMessage($to, array $context): int
    {
        return $this->mailer->sendMessage($to, '@IrmagRestApi/email.html.twig', $context);
    }
}
