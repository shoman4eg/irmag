/**
 * @namespace IRMAG.SecretPage
 *
 * @requires jquery
 * @requires Routing
 * @requires clipboard
 */

import Routing from '../core/modules/routing';
import soundMp3 from './sounds/neuralyzer.mp3';
import './images/cats/svg/cat-in-black.svg';

IRMAG.SecretPage = {
    init: function () {
        this.bindShowBtn();
        IRMAG.Site.bindClipboard();

        this.audio = document.getElementById('neuralyzer-audio');
        $(this.audio).append($('<source src="' + soundMp3 + '" type="audio/mpeg">'));
    },

    bindShowBtn: function () {
        const self = this;

        $(document).on('click', '#cat-in-black-btn', function () {
            const _ = $(this).off('click').addClass('cursor-wait').prop('disabled', true);

            $.getJSON(Routing.generate('irmag_secret_page_get_promocode', { slug: _.data('slug') }), function (data) {
                if (!data.code) {
                    return;
                }

                $('#input-promocode').val(data.code);
                $('#promocode, #promocode-help').removeClass('hidden');
                self.animateMyPussyCatPlz();
                _.remove();
            });
        });
    },

    animateMyPussyCatPlz: function () {
        this.audio.pause();
        this.audio.currentTime = 0;
        this.audio.play();

        $('body').stop(true, true, true).animate({ opacity: 0 }, 250, function () {
            $(this).animate({ opacity: 1 }, 100);
        });

        const cd = $($('#cat-in-black > object')[0].contentDocument);
        cd.find('.st2').removeClass('st2').addClass('st2-old st2-new');
        cd.find('.st3').removeClass('st3').addClass('st3-old st3-new');
        cd.find('.st4').removeClass('st4').addClass('st4-old st4-new');

        $('#cat-in-black').attr('title', 'Now I\'m Pink Panther =)').tooltip('fixTitle');
    },
};
