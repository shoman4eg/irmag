<?php

namespace Irmag\CoreBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Irmag\CoreBundle\Utils\MaskEmail;

class MaskEmailExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('mask_email', [$this, 'maskEmail']),
        ];
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function maskEmail(string $email): string
    {
        return MaskEmail::mask($email);
    }
}
