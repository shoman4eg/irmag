<?php

namespace Irmag\OrderDeliveryServiceBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

/**
 * {@inheritdoc}
 */
class OrderDeliveryServiceRequestFailedException extends IrmagException
{
}
