<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="users_block_history")
 * @ORM\Entity
 */
class UserBlockHistory
{
    use TimestampableEntity;

    const USER_ACTION_BLOCK = 1;
    const USER_ACTION_UNBLOCK = 2;
    const USER_ACTION_FORUM_BLOCK = 4;
    const USER_ACTION_ELEMENT_COMMENT_BLOCK = 8;
    const USER_ACTION_NEWS_COMMENT_BLOCK = 16;
    const USER_ACTION_BLOG_COMMENT_BLOCK = 32;
    const USER_ACTION_ACTION_COMMENT_BLOCK = 64;
    const USER_ACTION_FORUM_UNBLOCK = 128;
    const USER_ACTION_ELEMENT_COMMENT_UNBLOCK = 256;
    const USER_ACTION_NEWS_COMMENT_UNBLOCK = 512;
    const USER_ACTION_BLOG_COMMENT_UNBLOCK = 1024;
    const USER_ACTION_ACTION_COMMENT_UNBLOCK = 2048;

    /**
     * @var array
     */
    public $availableStatuses = [
        self::USER_ACTION_BLOCK => 'Заблокирован',
        self::USER_ACTION_UNBLOCK => 'Разблокирован',
        self::USER_ACTION_FORUM_BLOCK => 'Бан на форуме',
        self::USER_ACTION_ELEMENT_COMMENT_BLOCK => 'Бан в комментариях (товары)',
        self::USER_ACTION_NEWS_COMMENT_BLOCK => 'Бан в комментариях (новости)',
        self::USER_ACTION_BLOG_COMMENT_BLOCK => 'Бан в комментариях (блог)',
        self::USER_ACTION_ACTION_COMMENT_BLOCK => 'Бан в комментариях (акции)',
        self::USER_ACTION_FORUM_UNBLOCK => 'Восстановлен на форуме',
        self::USER_ACTION_ELEMENT_COMMENT_UNBLOCK => 'Восстановлен в комментариях (товары)',
        self::USER_ACTION_NEWS_COMMENT_UNBLOCK => 'Восстановлен в комментариях (новости)',
        self::USER_ACTION_BLOG_COMMENT_UNBLOCK => 'Восстановлен в комментариях (блог)',
        self::USER_ACTION_ACTION_COMMENT_UNBLOCK => 'Восстановлен в комментариях (акции)',
    ];

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     *
     * @Assert\Choice({1, 2})
     *
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="blockHistory")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $admin;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
}
