/**
 * API для корзины.
 *
 * @namespace IRMAG.API.Basket
 */

import { ajax } from '../core/modules/irmag-utils';

IRMAG.API.Basket = {
    /**
     * Добавляет товар или группу товаров в корзину
     *
     * @param {number|array} elementId Ид элемента
     * @param {number}       amount    Кол-во [optional]
     * @param {function}     callback  [optional]
     */
    addElement: function (elementId, amount, callback) {
        ajax('irmag_basket_ajax_add_element', {
            elementId: elementId,
            amount: amount || 1,
        }, callback);
    },

    /**
     * Удаляет товар из корзины
     *
     * @param {number}   elementId Ид элемента
     * @param {function} callback  [optional]
     */
    removeElement: function (elementId, callback) {
        ajax('irmag_basket_ajax_remove_element', {
            elementId: elementId,
        }, callback);
    },

    /**
     * Очищает корзину
     *
     * @param {function} callback [optional]
     */
    clear: function (callback) {
        ajax('irmag_basket_ajax_clear', null, callback);
    },

    /**
     * Перемещает товар из корзины в избранное
     *
     * @param {number}   elementId    Ид элемента
     * @param {string}   favoriteName Название избранного [optional]
     * @param {function} callback     [optional]
     */
    moveElementToFavorite: function (elementId, favoriteName, callback) {
        ajax('irmag_basket_ajax_move_element_to_favorite', {
            elementId: elementId,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Перемещает всё в избранное
     *
     * @param {string}   favoriteName Название избранного [optional]
     * @param {function} callback     [optional]
     */
    moveAllElementsToFavorite: function (favoriteName, callback) {
        ajax('irmag_basket_ajax_move_all_elements_to_favorite', {
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Изменяет кол-во (шт.) у товара в корзине
     *
     * @param {number}   elementId Ид элемента
     * @param {number}   amount    Кол-во
     * @param {function} callback  [optional]
     */
    changeElementAmount: function (elementId, amount, callback) {
        ajax('irmag_basket_ajax_change_element_amount', {
            elementId: elementId,
            amount: amount || 1,
        }, callback);
    },

    /**
     * Получает контент корзины
     *
     * @param {function} callback [optional]
     */
    getContent: function (callback) {
        ajax('irmag_basket_ajax_content', null, callback);
    },

    /**
     * Получает данные о кол-ве товаров и мини корзину
     *
     * @param {function} callback [optional]
     */
    getData: function (callback) {
        ajax('irmag_basket_ajax_data', null, callback);
    },

    /**
     * Уведомляет, что товар добавлен в корзину
     */
    notifyAdd: function () {
        this._notify('basket add');
    },

    /**
     * Уведомляет, что товар удалён из корзины
     */
    notifyRemove: function () {
        this._notify('basket remove');
    },

    /**
     * Выводит уведомление при добавление/удаление
     * товара из корзины/избранного
     *
     * @see stylesheets/components/_notify.scss
     *
     * @param {string} cls CSS class
     */
    _notify: function (cls) {
        $('<div class="notify ' + cls + '">')
            .appendTo($('body'))
            .fadeIn(200)
            .delay(200)
            .fadeOut(1000, function () {
                $(this).remove();
            });
    },
};
